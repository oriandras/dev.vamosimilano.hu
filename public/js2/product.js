$(document).ready(function() {
	"use strict";
	$(".stop").sticky(
		{topSpacing: 90, zIndex: 2, stopper: "#stickystop"}
	);
});