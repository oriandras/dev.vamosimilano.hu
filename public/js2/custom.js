// Tooltips Initialization
$(function () {
$('[data-toggle="tooltip"]').tooltip()
})
// SideNav Button Initialization
$(".button-collapse").sideNav();
// SideNav Scrollbar Initialization
var sideNavScrollbar = document.querySelector('.custom-scrollbar');
var ps = new PerfectScrollbar(sideNavScrollbar);
/*
	Javascript alapú Cookie kezelés
	https://www.w3schools.com/js/js_cookies.asp
*/
function setCookie(cname, cvalue, exdays) {
	"use strict";
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires="+d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	//console.log(cname+" "+cvalue+" "+exdays);
}

function getCookie(cname) {
	"use strict";
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}
function ujCookie() {
	"use strict";
	var v = $("#cookietest").val();
	setCookie('test', v, '1');
}
/*
	Number.prototype.format(n, x, s, c)
	@param integer n: length of decimal
	@param integer x: length of whole part
	@param mixed s: sections delimiter
	@param mixed c: decimal delimiter
*/
Number.prototype.format = function(n, x, s, c) {
	"use strict";
	var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
	num = this.toFixed(Math.max(0, ~~n));
	return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};
/*
	Oldal funkciók
*/
$(function () {
	"use strict";
	$('[data-toggle="tooltip"]').tooltip()
});
$("#searchtoggle").click( function(){
	"use strict";
	var toggle;
	if($(".search-form").hasClass("iv")===true) {
		toggle="hidden";
	}
	if($(".search-form").hasClass("vi")===true) {
		toggle="show";
	}
	if(toggle==="hidden"){
		$(".search-form").removeClass("iv invisible fadeOut");
		$(".search-form").addClass("vi animation-r-to-left");
		
	}
	if(toggle==="show"){
		$(".search-form").removeClass("vi animation-r-to-left");
		$(".search-form").addClass("iv fadeOut");
	}
});
$(".dropdown-rolunk").hover(function(){
	"use strict";
	$('.rolunk-submenu').dropdown("toggle");
	$('.rolunk-submenu').dropdown('dispose');
});
$(".dropdown-fontos").hover(function(){
	"use strict";
	$('.fontos-submenu').dropdown("toggle");
	$('.fontos-submenu').dropdown('dispose');
});
$(".dropdown-megamenu").hover(function(){
	"use strict";
	$('.mega-menu').dropdown("toggle");
	$('.mega-menu').dropdown('dispose');
});
$(".dropdown").mouseleave(function(){
	"use strict";
	$(".dropdown").removeClass("show");
	$(".dropdown-menu").removeClass("show");
	
});
function sizeChange(id, size, type, productType, prodID) {
	/*
	id = product id
	size = S, M, L, XL
	type = a, b, c, full
	productType = e, l, u, fr.
	*/
	"use strict";
	//első lépésként az esetleg kipipált related productokat uncheckedre teszem, hogy a méretfüggő darabok ne maradjanak kiválastva
	
	$(".related").prop("checked", false);
	$(".sizelabel").addClass("animated fadeIn");
	setFeature();
	setTimeout(function(){
		$(".sizelabel").removeClass("animated fadeIn");
	},
		1000
	);
	
	$(".sizelabel").removeClass("active");
	$("#label-"+id).addClass("active");
	//console.log(id);
	//console.log($(".togleable input").data("size"));
	$(".togleable").addClass("hidden");
	$(".size-"+id).removeClass("hidden");
	if(type==="full"){
		$(".mixedsizes").removeClass("active");
		if( productType==='e' || productType==='fr' ){
			setCookie("size-a-"+prodID, size, 3);
			$("#product_size_a").val(size);
		}
		if(productType==='l'){
			setCookie("size-a-"+prodID, size, 3);
			setCookie("size-b-"+prodID, size, 3);
			$("#product_size_a").val(size);
			$("#product_size_b").val(size);
			
			$("#A"+size).addClass("active");
			$("#B"+size).addClass("active");
		}
		if(productType==='u'){
			setCookie("size-a-"+prodID, size, 3);
			setCookie("size-b-"+prodID, size, 3);
			setCookie("size-c-"+prodID, size, 3);
			$("#product_size_a").val(size);
			$("#product_size_b").val(size);
			$("#product_size_c").val(size);

			$("#A"+size).addClass("active");
			$("#B"+size).addClass("active");
			$("#C"+size).addClass("active");
		}
	}//if
	else {
		$(".sizelabel").removeClass("active");
		$(".sizelabel-input").prop("checked", false);
		if(type === "A") {
			setCookie("size-a-"+prodID, size, 3);
			$("#product_size_a").val(size);
			$(".mixed-a").removeClass("active");
			$("#A"+size).addClass("active");
		}
		if(type === "B") {
			setCookie("size-b-"+prodID, size, 3);
			$("#product_size_b").val(size);
			$(".mixed-b").removeClass("active");
			$("#B"+size).addClass("active");
		}
		if(type === "C") {
			setCookie("size-c-"+prodID, size, 3);
			$("#product_size_c").val(size);
			$(".mixed-c").removeClass("active");
			$("#C"+size).addClass("active");
		}
	}//else
	$(".sizes_input").change();
	var asize = getCookie("size-a-"+prodID);
	var bsize = getCookie("size-b-"+prodID);
	var csize = getCookie("size-c-"+prodID);
	if (asize==bsize && asize==csize && bsize==csize){
		$(".size-"+asize).addClass("active");
		$(".size-"+asize+" input").prop("checked", true);
		$("#other-sizes").removeClass("show");
		$("#other-sizes").addClass("fade");
	}
	else {
		if (asize==bsize && csize==""){
			$(".size-"+asize).addClass("active");
			$(".size-"+asize+" input").prop("checked", true);
			$("#other-sizes").removeClass("show");
			$("#other-sizes").addClass("fade");
		}	
	}
}
function mountingChange(side) {
	"use strict";
	var uri = $(".mounting-example").data("url");
	var tipus = $("#butortipus").val();
	if(side==="left") {
		$("#mounting-right").removeClass("active");
		$("#mounting-left").addClass("active");
		setCookie("lastusedmounting", side, 3);
		var src = uri+"balos.png";
		$(".mounting-example").attr("src",src);
		$(".bside").addClass("order-0");
		$(".bside").removeClass("order-2");
		$(".cside").addClass("order-2");
		$(".cside").removeClass("order-0");
		if(tipus==="l") {
			$(".aside").removeClass("border-right-0");
			$(".aside").addClass("border-left-0");
		}
	}
	if(side==="right") {
		$("#mounting-left").removeClass("active");
		$("#mounting-right").addClass("active");
		setCookie("lastusedmounting", side, 3);
		var src = uri+"jobbos.png";
		$(".mounting-example").attr("src",src);
		$(".bside").addClass("order-2");
		$(".bside").removeClass("order-0");
		$(".cside").addClass("order-0");
		$(".cside").removeClass("order-2");
		if(tipus==="l") {
			$(".aside").removeClass("border-left-0");
			$(".aside").addClass("border-right-0");
		}
	}
}
function blueprint(size, xy) {
	"use strict";
	var cm = xy.split("x");
	var x = cm[0];
	var y = cm[1];
	$("."+size+"side").css("width", x+"px");
	$("."+size+"side").css("height", y+"px");
	
	$("."+size+"w").text(x+"cm");
	$("."+size+"h").text(y+"cm");
	$("."+size+"w").data("w", x);

	$("."+size+"side table").css("width", x+"px");
	$("."+size+"side table").css("height", y+"px");
	
	var asize = $(".aw").data("w");
	var bsize = $(".bw").data("w");
	var csize = $(".cw").data("w");
	//alert(parseInt(asize)+parseInt(bsize)+parseInt(csize));
	var fullwidth = 0;
	if(csize>0) {
		fullwidth = parseInt(asize)+parseInt(bsize)+parseInt(csize);
		//alert("van c");
	}
	else {
		fullwidth = parseInt(asize)+parseInt(bsize);
		//alert("nincs c");
	}
	//alert(fullwidth);
	$(".fullwidthsize span").text(parseInt(fullwidth));
}
function seatChange(type) {
	"use strict";
	if(type==="sponge") {
		$("#sprung").removeClass("active");
		$("#sponge").addClass("active");
	}
	if(type==="sprung") {
		$("#sponge").removeClass("active");
		$("#sprung").addClass("active");
	}
}
$("#tab-full").click(function() {
	"use strict";
	$(".textiletypetabs").removeClass('shadow');
	$("#tab-full").addClass('shadow');
});
$("#tab-half").click(function() {
	"use strict";
	$(".textiletypetabs").removeClass('shadow');
	$("#tab-half").addClass('shadow');
});
$("#tab-random").click(function() {
	"use strict";
	$(".textiletypetabs").removeClass('shadow');
	$("#tab-random").addClass('shadow');
});
$('#textileselectmodal').on('show.bs.modal', function (event) {
	"use strict";
	var button = $(event.relatedTarget);
	var part = button.data('parttype');
	var uri = $("#catch_the_uri").val();
	//var modal = $(this);
	//console.log(uri + '/modaltextile/' + part);
	$("#textilecontent").load(uri + '/modaltextile/' + part);
});
$('#textileselectmodal').on('hide.bs.modal', function (event) {
	"use strict";
	var modal = $(this);
	modal.find('.modal-body').html('<p class="text-center" id="what"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></p>');
});
$('#textilepixmodal').on('show.bs.modal', function (event) {
	"use strict";
	var button = $(event.relatedTarget); // Button that triggered the modal
	var recipient = button.data('textilefullpix'); // Extract info from data-* attributes
	// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	var modal = $(this);
	//modal.find('.modal-title').text('New message to ' + recipient);
	modal.find('.modal-body img.img-fluid').attr("src",recipient);
});
function textileTabSwitch(felso, also, target) {
	"use strict";
	if(felso===also){
		$('#textile_changer_tabs a[href="#full"]').tab('show');
	}
	else {
		$('#textile_changer_tabs a[href="#half"]').tab('show');
	}
	$(".textilepicker").removeClass("active border-primary");
	$(".textilepicker").addClass("active border-dark");
	$(".pix-"+target).addClass("active border-primary");
}
function textilePreview(type, part, textile_id) {
	"use strict";
	//var new_src = "/images/textileditor/"+type+"-"+part+".png";
	//$("#item-textile-preview").attr("src",new_src);
	$(".textil-"+part+"-preview").attr("onclick","setTextile('"+part+"', '"+textile_id+"')");
	$(".textil-"+part+"-preview").attr("onmouseover","textilePreview('"+type+"', '"+part+"', '"+textile_id+"')");
}
function changeProductImage(prev, big) {
	"use strict";
	$(".the-product-image").attr("src",prev);
	$("#imagelink").attr("href",big);
}
function changeTextilePreview(top_small, top_big, bottom_small, bottom_big) {
	"use strict";
	$(".a-felso-bigimage").data("textilefullpix",top_big);
	$(".textil-felso-preview").css("background-image", "url("+top_small+")");
	
	$(".a-also-bigimage").data("textilefullpix",bottom_big);
	$(".textil-also-preview").css("background-image", "url("+bottom_small+")");
}
function refreshTextilePreview(small, big, type, name, price){
	"use strict";
	$(".a-"+type+"-bigimage").data("textilefullpix",big);
	$(".textil-"+type+"-preview").css("background-image", "url("+big+")");
	$("#textil-"+type+"-span").html(name +' '+ price);
}
function setBigPix(url, id) {
	"use strict";
	$("#textile-"+id+"-mini").css("background-image", "url("+url+")");
	$("#textile-"+id+"-mini").css("background-position", "center center");
}
function resetTextile() {
	"use strict";
	$(".resetfield").each(function() {
	var original = $(this).data('default');
		$(this).val(original);
	});
	$(".textile_preview").each(function() {
	var originalpix = $(this).data('originalpix');
	var originalid = $(this).data('originalid');
	var partname = $(this).data('part');
	var a = ".a-"+partname+"-bigimage";
		$(this).css("background-image", "url("+originalpix+")");
		$(a).data("textilefullpix", originalpix);
	});
}
function clearIfNotMixed(part) {
	"use strict";
	//alert(part);
	if(part==="butor" || part==="felso" || part==="also") {
		//minden ok, nem kell semmit törölni
		console.log("semmi változás");
	}
	else {
		console.log(part+" törlés");
		var value="";
		$("#textile_butor").val(value);
		$("#textile_felso").val(value);
		$("#textile_also").val(value);
	}
}
function removeCheck(id) {
	"use strict";
	$("#input-"+id).prop("checked", false);
}
function setFeature(id) {
	/*
	Ha bármelyik választható kiegészítőt kiválasztják, akkor végigfut az összes felsorolt kiegészítő terméken, ellenőrzi, hogy ki van-e választva és a kapott adatokat egy idióta ;-vel indított stringbe veszi fel. Ezt a stringet mentem el cookieban és írom vissza a megfelelő hidden mezőbe.
	*/
	"use strict";
	//alert(id);
	if(id==="828"){
		$(".input-related").prop("checked", false);
	}
		
	var newdata = [];
	var cookieData = [];
	var extraprice = 0;
	cookieData = getCookie("productfeatures");
	$(".related:checked").each(function() {
		var code = $(this).val();
		newdata.push(";"+code);
		var add_price = $(this).data("priceplus2");
		extraprice = Number(extraprice) + Number(add_price);
		//alert(extraprice);
		if( $(this).hasClass("garancia")){
			var garanciaar = $(this).data("garanciaar");
			console.log("garanciaar: "+garanciaar);
			$("#relatedgarprice").val(garanciaar);
		}
	});


	//setCookie("feature_array", newdata, 3);
	var strdata = newdata.join("");
	//alert(strdata);

	$("#relatedfdata").val(strdata);
	$("#related_product").val(strdata);
	$("#relatedfprice").val(extraprice);
	$("#priceplus_price_2_").val(extraprice);
	setCookie("productfeatures", strdata, 3);
	
	refreshPayment();
}
$(document).ready(function() {
	"use strict";
	$.ajaxSetup({
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
	});
	$('.mdb-select').materialSelect();
	$('.select-wrapper.md-form.md-outline input.select-dropdown').bind('focus blur', function () {
		$(this).closest('.select-outline').find('label').toggleClass('active');
		$(this).closest('.select-outline').find('.caret').toggleClass('active');
	});
	new WOW().init();
	//setCookie('filterlist', '', 3);
	//console.log("Az oldal kész.");
	if(getCookie('active_product')) {
		var productid = getCookie('active_product');
		//console.log(productid);
		if(productid){
			//console.log(productid + " terméket töltöttük be");
			var meretezett = getCookie("size-a-"+productid);
			console.log(meretezett);
			if(meretezett !== 'undefined'){
				console.log("Újratöltöm az árkalkulátort, hogy ne a default értékeket számolja.");
				refreshPayment();
				$(".default_selection").trigger('click');
			}
		}
		else {
			console.log("nem product oldal");
		}
	}
	var stop = 0;
	if (stop !== 1) {
		var allproducts = $(".product-listing").data("product");
		console.log(allproducts);
		var products = allproducts.split(",");
		console.log(products);
		jQuery.each(products, function(index, item) {
			//var p = $(this).data("product");
			console.log(this);
			//var card;
			if (this !== "") {
				var p = item;
				var uri = $("#catch_the_uri").val();
				//console.log(uri + '/blogproduct/' + p);
				//$(".product-listing").load(uri + '/blogproduct/' + p);
				$.get(uri + '/blogproduct/' + p, function(response){
					$(".product-listing").append(response);
				});
			}
			else {
				stop++;
			}
		console.log(stop);
		});
	}
});

function highlite(id){
	"use strict";
	if( $("#"+id).checked() ) {
		alert(id);
	}
}
function setTextile(part, textile_id) {
	"use strict";
	//alert(part+" "+textile_id);
	console.log(part+" "+textile_id);
	$("#textile_"+part).val(textile_id);

	$("#textile_"+part).change();
	refreshPayment();
}//setTextile
function refreshPayment() {
	"use strict";
	$('#priceall-target').html('<img src="/loader.gif" width="20px;">');
	$.ajax({
		dataType: "json",
		type: "post",
		data: $(".product-form").serialize(),
		url: '/cart/calculateprice',
	}).done(function(data) {
		$('#priceall-target').html(data.price);
		$('#priceall-target').data("priceall", data.price);
		$('#priceplus-target').html(data.textile_price);
		$('#default_price_').val(data.priceall);
		$('#priceplus_price_').val(data.priceplus);
		/*
		A priceplus2 értéket valahogy meg kell kapnom ez a garancia AKTUÁLIS ára
		*/
		var priceplus2 = Number($("#relatedfprice").val());
		$('#priceplus_price_2_').val(priceplus2);
		var garancia_ar = $("#relatedgarprice").val();
		var garancia_szorzo = $("input.garancia:checked").data("garancia");
		/*
			Nullával osztani nem kéne
		*/
		if(garancia_ar != 0) {
			garancia_ar = Math.round((parseInt(data.alapbutor)+parseInt(data.szovetfelar))*garancia_szorzo/100)*100;
		}
		$("input.related:checked").each(function () {
			var irdki = parseInt(priceplus2)+parseInt(garancia_ar);
			//alert(irdki);
			$('#relatedfprice').val(irdki);
			$('#priceother-target').html(irdki.format(0,'3',' ')+" "+$('#default_data').data('curr'));
		});
		var new_price = (parseInt($('#default_price_').val())*1) + (parseInt($('#relatedfprice').val())*1);
		$('#priceall-target').html(new_price.format(0,'3',' ')+" "+$('#default_data').data('curr'));
	});
}
function calcProductDetailPrice(){
"use strict";
refreshPayment();
}
function productItemImagePreview(id, url) {
	"use strict";
	$("#product-image-"+id).attr("src", url);
}
/*
	A GTM ecommerce funkcióhoz szükséges javascript kódok
*/
function gtmAddToCart(l,i,c,n,p,a,v,q){
	"use strict";
	if(v!='') {
		dataLayer.push({
		'fbprodid': l+i,
	    'fbprodvalue': p,
		'event': 'addToCart',
		'ecommerce': {
			'currencyCode': c,
				'add': {
					'products': [{
						'id': i,
						'name': n,
						'price': p,
						'category': a,
						'variant': v,
						'quantity': q
					}]
				}
			}
		});
	} else {
		dataLayer.push({
		'fbprodid': l+i,
		'fbprodvalue': p,
		'event': 'addToCart',
		'ecommerce': {
			'currencyCode': c,
				'add': {
					'products': [{
						'id': i,
						'name': n,
						'price': p,
						'category': a,
						'quantity': q
					}]
				}
			}
		});
	}
}

function gtmRemoveFromCart(i,n,p,a,v,q){
	"use strict";
	if(v!='') {
		dataLayer.push({
		'event': 'removeFromCart',
		'ecommerce': {
				'remove': {
					'products': [{
						'id': i,
						'name': n,
						'price': p,
						'category': a,
						'variant': v,
						'quantity': q
					}]
				}
			}
		});
	} else {
		dataLayer.push({
		'event': 'removeFromCart',
		'ecommerce': {
				'remove': {
					'products': [{
						'id': i,
						'name': n,
						'price': p,
						'category': a,
						'quantity': q
					}]
				}
			}
		});
	}
}

function gtmProductClick(l,n,i,p,c,o){
	"use strict";
	dataLayer.push({
		'event': 'productClick',
		'ecommerce': {
			'click': {
			'actionField': {'list': l},      // Optional list property.
			'products': [{
			'name': n,                      // Name or ID is required.
			'id': i,
			'price': p,
			'category': c,
			'position': o
			}]
			}
		}
	});
}
function sortProduct(mode) {
	"use strict";
	console.log("rendezés szempontja: "+mode);
	$(".product-card-container").each(function() {
		var x;
		if(mode=="asc") {
			//nő
			x = $(this).data('price');
		}
		if(mode=="desc") {
			//nő
			x = $(this).data('price');
			x = x*(-1);
		}
		if(mode=="basic") {
			//nő
			x = $(this).data('order');
		}
		$(this).css("order", x);
	});
	
}
function modifyFilter(y) {
	//x = filter type
	//y = filter value
	"use strict";
	$(".product-card-container").addClass("hidden");
	$(".product-card-container").removeClass("countme");
	var i=0;
	var count = $("#current-product-number").data("originalproductnumber");
	$(".filtercheckbox:checked").each(function() {
		var filter = $(this).val();
		//console.log(filter);
		$("."+filter).removeClass("hidden");
		$("."+filter).addClass("countme");
		count = $(".countme").length;
		i++;
	});
	if(i==0) {
		$(".product-card-container").removeClass("hidden");
	}
	$("#current-product-number").text(count);
}
function priceRange(){
	"use strict";
	$("#xsize").val( $("#xsize").data("max") ); //visszaállítjuk a méretszűrőket maxra
	$("#ysize").val( $("#ysize").data("max") ); //visszaállítjuk a méretszűrőket maxra
	var x = $("#pricerange").val();
	var count = $("#current-product-number").data("originalproductnumber");
	console.log("max price: "+x);
	$(".product-card-container").each(function() {
		var y = $(this).data("price");
		if(y <= x){
			$(this).removeClass("hidden");
			$(this).addClass("countme");
		}
		else {
			$(this).addClass("hidden");
			$(this).removeClass("countme");
		}
		count = $(".countme").length;
		$("#current-product-number").text(count);
		var newnum = Number(x);
		$("#selectedprice").text("(max: " + newnum.format(0,'3',' ') + " Ft)");
	});
}
function sizeRange(size){
	"use strict";
	$("#pricerange").val( $("#pricerange").data("max") ); //visszaállítjuk az árszűrőt maxra
	var s = $("#"+size+"size").val();
	var x = $("#xsize").val();
	var y = $("#ysize").val();
	var count = $("#current-product-number").data("originalproductnumber");
	$(".product-card-container").each(function() {
		var xmin = $(this).data("xmin");
		var ymin = $(this).data("ymin");
		if(xmin > x || ymin > y){
			$(this).addClass("hidden");
			$(this).removeClass("countme");
		}
		else {
			$(this).removeClass("hidden");
			$(this).addClass("countme");
		}
		//console.log(min+" "+s);
		count = $(".countme").length;
		$("#current-product-number").text(count);
		$("#selected"+size+"size").text("(max: " + s + " cm)");
	});
}
function wishList(pid, uid, btn){
	"use strict";
	var token = $("meta[name=csrf-token]").attr("content");
	//console.log(token);
	$.ajax({
	dataType: "json",
	type: "post",
	data: {_token: token},
	url: '/favorite/'+pid+'/'+uid,
	}).done(function(data) {
		var newbutton = "";
		$.each(data, function(index, element) {
			//console.log("fav id: "+element.favorite_id);
			//console.log("prod id: "+pid);
			if(element.favorite_id != "undefined"){
				newbutton = '<a id="wishlistbutton-'+pid+'" class="'+btn+'" href="javascript:void(0)" title="Eltávolítás a kedvencek közül" data-wishlist="1" onClick="removeWishList('+element.favorite_id+', '+pid+', '+uid+', `'+btn+'`)"><i class="fas fa-heart" aria-hidden="true"></i></a>';
			}
		});
		//console.log(newbutton);
		$("#favorite-"+pid).html(newbutton);
	});
	//ajax vége serialize()
}
function removeWishList(favid, pid, uid, type){
	"use strict";
	var token = $("meta[name=csrf-token]").attr("content");
	console.log(token);
	$.ajax({
	dataType: "json",
	type: "post",
	data: {_token: token},
	url: '/favorite/delete/'+favid, 
	}).done(function(data) {
		var newbutton = '<a id="wishlistbutton-'+pid+'" class="'+type+'" href="javascript:void(0)" title="Hozzáadás a kedvencekhez" data-wishlist="0" onClick="wishList('+pid+', '+uid+', `'+type+'`)"><i class="fal fa-heart" aria-hidden="true"></i></a>';
		$("#favorite-"+pid).html(newbutton);
	});
}
$('#showroom1').on('slide.bs.carousel', function () {
	"use strict";
 //alert();
	$("#showroom").removeClass("active");
});
$('#showroom1').on('slid.bs.carousel', function () {
	"use strict";
	$("#showroom").addClass("active");
});
function sideNavContent(target) {
	"use strict";
	if (target==="menu") {
		$(".cart-container").addClass("hidden");
		$(".cart-container").removeClass("show");
		$(".menu-container").removeClass("show");
		$(".menu-container").addClass("show");
		$(".social-container").addClass("show");
		$(".social-container").removeClass("hidden");
	}
	if (target==="cart") {
		$(".menu-container").addClass("hidden");
		$(".menu-container").removeClass("show");
		$(".cart-container").removeClass("show");
		$(".cart-container").addClass("show");
		$(".social-container").removeClass("show");
		$(".social-container").addClass("hidden");
	}
}
$(".sofont-ribbon").hover( 
	function() {
	"use strict";
	var tipus=$(this).data("tipus");
		$(".icon-vonal-"+tipus).addClass("icon-teli-"+tipus);
		$(".icon-teli-"+tipus).removeClass("icon-vonal-"+tipus);
	},
	function() {
	"use strict";
	var tipus=$(this).data("tipus");
		$(".icon-teli-"+tipus).addClass("icon-vonal-"+tipus);
		$(".icon-vonal-"+tipus).removeClass("icon-teli-"+tipus);
	}
);
/*
Cookie kezeléshez
*/
function cookiebar(x) {
	"use strict";
	if (x=="open"){
		$("#cookiecontainer").removeClass("hidden");
	}
	if (x=="close"){
		$("#cookiecontainer").addClass("hidden");
	}
}
/* IsInViewport plugin */
(function (global, factory) {
	"use strict";
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(require('jquery')) :
	typeof define === 'function' && define.amd ? define(['jquery'], factory) :
	(factory(global.jQuery));
}(this, (function ($) { 'use strict';

$ = $ && $.hasOwnProperty('default') ? $['default'] : $;

/**
 * @author  Mudit Ameta
 * @license https://github.com/zeusdeux/isInViewport/blob/master/license.md MIT
 */

// expose isInViewport as a custom pseudo-selector
$.extend($.expr.pseudos || $.expr[':'], {
  // if $.expr.createPseudo is available, use it
  'in-viewport': $.expr.createPseudo
    ? $.expr.createPseudo(function (argsString) { return function (currElement) { return isInViewport(currElement, getSelectorArgs(argsString)); }; })
  : function (currObj, index, meta) { return isInViewport(currObj, getSelectorArgs(meta[3])); }
});


// expose isInViewport as a function too
// this lets folks pass around actual objects as options (like custom viewport)
// and doesn't tie 'em down to strings. It also prevents isInViewport from
// having to look up and wrap the dom element corresponding to the viewport selector
$.fn.isInViewport = function(options) {
  return this.filter(function (i, el) { return isInViewport(el, options); });
};

$.fn.run = run;

// lets you chain any arbitrary function or an array of functions and returns a jquery object
function run(args) {
  var this$1 = this;

  if (arguments.length === 1 && typeof args === 'function') {
    args = [args];
  }

  if (!(args instanceof Array)) {
    throw new SyntaxError('isInViewport: Argument(s) passed to .do/.run should be a function or an array of functions');
  }

  args.forEach(function (arg) {
    if (typeof arg !== 'function') {
      console.warn('isInViewport: Argument(s) passed to .do/.run should be a function or an array of functions');
      console.warn('isInViewport: Ignoring non-function values in array and moving on');
    } else {
      [].slice.call(this$1).forEach(function (t) { return arg.call($(t)); });
    }
  });

  return this
}


// gets the width of the scrollbar
function getScrollbarWidth(viewport) {
  // append a div that has 100% width to get true width of viewport
  var el = $('<div></div>').css({
    width: '100%'
  });
  viewport.append(el);

  // subtract true width from the viewport width which is inclusive
  // of scrollbar by default
  var scrollBarWidth = viewport.width() - el.width();

  // remove our element from DOM
  el.remove();
  return scrollBarWidth;
}


// Returns true if DOM element `element` is in viewport
function isInViewport(element, options) {
  var ref = element.getBoundingClientRect();
  var top = ref.top;
  var bottom = ref.bottom;
  var left = ref.left;
  var right = ref.right;

  var settings = $.extend({
    tolerance: 0,
    viewport: window
  }, options);
  var isVisibleFlag = false;
  var $viewport = settings.viewport.jquery ? settings.viewport : $(settings.viewport);

  if (!$viewport.length) {
    console.warn('isInViewport: The viewport selector you have provided matches no element on page.');
    console.warn('isInViewport: Defaulting to viewport as window');
    $viewport = $(window);
  }

  var $viewportHeight = $viewport.height();
  var $viewportWidth = $viewport.width();
  var typeofViewport = $viewport[0].toString();

  // if the viewport is other than window recalculate the top,
  // bottom,left and right wrt the new viewport
  // the [object DOMWindow] check is for window object type in PhantomJS
  if ($viewport[0] !== window && typeofViewport !== '[object Window]' && typeofViewport !== '[object DOMWindow]') {
    // use getBoundingClientRect() instead of $.Offset()
    // since the original top/bottom positions are calculated relative to browser viewport and not document
    var viewportRect = $viewport[0].getBoundingClientRect();

    // recalculate these relative to viewport
    top = top - viewportRect.top;
    bottom = bottom - viewportRect.top;
    left = left - viewportRect.left;
    right = right - viewportRect.left;

    // get the scrollbar width from cache or calculate it
    isInViewport.scrollBarWidth = isInViewport.scrollBarWidth || getScrollbarWidth($viewport);

    // remove the width of the scrollbar from the viewport width
    $viewportWidth -= isInViewport.scrollBarWidth;
  }

  // handle falsy, non-number and non-integer tolerance value
  // same as checking using isNaN and then setting to 0
  // bitwise operators deserve some love too you know
  settings.tolerance = ~~Math.round(parseFloat(settings.tolerance));

  if (settings.tolerance < 0) {
    settings.tolerance = $viewportHeight + settings.tolerance; // viewport height - tol
  }

  // the element is NOT in viewport iff it is completely out of
  // viewport laterally or if it is completely out of the tolerance
  // region. Therefore, if it is partially in view then it is considered
  // to be in the viewport and hence true is returned. Because we have adjusted
  // the left/right positions relative to the viewport, we should check the
  // element's right against the viewport's 0 (left side), and the element's
  // left against the viewport's width to see if it is outside of the viewport.

  if (right <= 0 || left >= $viewportWidth) {
    return isVisibleFlag;
  }

  // if the element is bound to some tolerance
  isVisibleFlag = settings.tolerance ? top <= settings.tolerance && bottom >= settings.tolerance : bottom > 0 && top <= $viewportHeight;

  return isVisibleFlag;
}


// get the selector args from the args string proved by Sizzle
function getSelectorArgs(argsString) {
  if (argsString) {
    var args = argsString.split(',');

    // when user only gives viewport and no tolerance
    if (args.length === 1 && isNaN(args[0])) {
      args[1] = args[0];
      args[0] = void 0;
    }

    return {
      tolerance: args[0] ? args[0].trim() : void 0,
      viewport: args[1] ? $(args[1].trim()) : void 0
    };
  }
  return {};
}

})));

//# sourceMappingURL=isInViewport.js.map
/*
	Eladott termék számláló a főoldalon
*/

(function($){
	"use strict";
	$(document).ready(function() {
		$('#fp_product_counter').children().each(function() {
			if ($(this).isInViewport({"tolerance" :50})) {
				sofaCounter();
			}
			else {
				//$(this).css("background-color", "blue");
			}
		});
		$(window).scroll(function() {
			$('#container').children().each(function() {
				if ($(this).isInViewport({"tolerance" :50,"toleranceForLast": 432, "debug": true})) {
					sofaCounter();
				}
				else {
					//$(this).css("background-color", "blue");
				}
			});
		});
	});
}(jQuery));
function sofaCounter() {
	"use strict";
	//alert("hello");
	$(".sofa_counter").each(function() {
		var $this = $(this),
			countTo = $this.attr('data-count');
		$({ countNum: $this.text()}).animate({
		countNum: countTo
		},
		{
		duration: 8000,
		easing:'linear',
		step: function() {
			$this.text(Math.floor(this.countNum));
		},
		complete: function() {
			$this.text(this.countNum);
		}
		});
	});
}
window.twttr = (function(d, s, id) {
	"use strict";
	var js, fjs = d.getElementsByTagName(s)[0],
	t = window.twttr || {};
	if (d.getElementById(id)) {return t;}
	js = d.createElement(s);
	js.id = id;
	js.src = "https://platform.twitter.com/widgets.js";
	fjs.parentNode.insertBefore(js, fjs);

	t._e = [];
	t.ready = function(f) {
	t._e.push(f);
	};

	return t;
}(document, "script", "twitter-wjs"));