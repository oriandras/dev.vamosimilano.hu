function gtmAddToCart(l,i,c,n,p,a,v,q){
	if(v!='') {
		dataLayer.push({
		'fbprodid': l+i,
	    'fbprodvalue': p,
		'event': 'addToCart',
		'ecommerce': {
			'currencyCode': c,
				'add': {
					'products': [{
						'id': i,
						'name': n,
						'price': p,
						'category': a,
						'variant': v,
						'quantity': q
					}]
				}
			}
		});
	} else {
		dataLayer.push({
		'fbprodid': l+i,
	    'fbprodvalue': p,
		'event': 'addToCart',
		'ecommerce': {
			'currencyCode': c,
				'add': {
					'products': [{
						'id': i,
						'name': n,
						'price': p,
						'category': a,
						'quantity': q
					}]
				}
			}
		});
	}
}

function gtmRemoveFromCart(i,n,p,a,v,q){
	if(v!='') {
		dataLayer.push({
		'event': 'removeFromCart',
		'ecommerce': {
				'remove': {
					'products': [{
						'id': i,
						'name': n,
						'price': p,
						'category': a,
						'variant': v,
						'quantity': q
					}]
				}
			}
		});
	} else {
		dataLayer.push({
		'event': 'removeFromCart',
		'ecommerce': {
				'remove': {
					'products': [{
						'id': i,
						'name': n,
						'price': p,
						'category': a,
						'quantity': q
					}]
				}
			}
		});

	}
}

function gtmProductClick(l,n,i,p,c,o){
  dataLayer.push({
    'event': 'productClick',
    'ecommerce': {
      'click': {
        'actionField': {'list': l},      // Optional list property.
        'products': [{
          'name': n,                      // Name or ID is required.
          'id': i,
          'price': p,
          'category': c,
          'position': o
         }]
       }
     }
  });
}
