<?php


namespace App;


use Illuminate\Auth\UserTrait;

use Illuminate\Auth\Reminders\RemindableTrait;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword as CanResetPasswor;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


use DateTime;

use App\Database\R_DB;







class User extends R_DB implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPasswor, \Illuminate\Database\Eloquent\SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'customers';
	protected $primaryKey = 'customer_id';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public static $rules = array(
	    'firstname'=>'required|min:2',
	    'lastname'=>'required|min:2',
	    'email'=>'required|email|unique:customers',
	    'password'=>'required|min:3|confirmed',
	    'password_confirmation' => 'required|min:3'



    );


    public static function getFieldNames(){
        return array(
            'firstname' => t('Keresztnév'),
            'lastname' => t('Vezetéknév'),
            'email' => t('E-mail cím'),
            'phone' => t('Telefonszám'),
            'password' => t('Jelszó'),
            'password_confirmation' => t('Jelszó újra'),



        );
    }


    function __construct(){
         parent::__construct();
    }
    /**
     * getName function.
     * Felhasználó teljes neve
     *
     * @access public
     * @param bool $full (default: false)
     * @return void
     */
    public function getName($full = false){

        if($full == true){
            return $this->lastname." ".$this->firstname;
        }else{
            return $this->firstname;
        }
    }

    public static function getUserName($uid) {
        $user = User::withTrashed()->where('customer_id', $uid)->first();
        if (empty($user)) {
            return t('Ismeretlen felhasználó');
        }
        return $user->getName(true);
    }
    /**
     * getImageUrl function.
     * Felhasználó fotója
     *
     * @access public
     * @return void
     */
    public function getImageUrl(){
        if($this->image){
            return '/uploads/user/160/'.$this->image;
        }else{
            return '/img/user.png';
        }
    }

    /**
     * save_image function.
     * Kép mentése
     *
     * @access public
     * @param mixed $image
     * @param mixed $name
     * @return void
     */
    function save_image($image, $name = ''){
        $file = $image;
        if($name){
            $ext = strtolower(pathinfo($name, PATHINFO_EXTENSION));
        }else{
            $ext = strtolower(pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION));
        }
        $img = \Image::make($file);

        $sizes = ['300' => '300','160' => 160];
        $upload_dir = public_path().'/uploads/user';

        $file_name = $this->customer_id."_".rand(1000,9999).".".$ext;

        foreach($sizes as $width => $height){
            $img_new = clone $img;
            $img_new->fit($width,$height);
            if(!is_dir($upload_dir."/".$width)){
                mkdir($upload_dir."/".$width);
            }
            $img_new->save($upload_dir."/".$width."/".$file_name);
        }
        return $file_name;
    }

    function sendRegMail($password){
        $view = [
                    'email' =>  $this->email,
                    'password' => trim($password)
                ];

                \Mail::send(
                    'emails.auth.register',
                    $view,
                    function ($message) use ($view) {
                        $message->to($view['email'])
                            ->subject(t('Sikeres regisztráció')." - ".t('project_name','project'));
                    }
                );

    }

    public static function hashOldPassword($pass)
    {
        $replace = array(
            "0" => "h",
            "1" => "r",
            "2" => "g",
            "3" => "k",
            "4" => "l",
            "5" => "n",
            "6" => "z",
            "7" => "t",
            "8" => "i",
            "9" => "q",
            "a" => "m",
            "b" => "v",
            "c" => "w",
            "d" => "y",
            "e" => "u",
            "f" => "s"
        );
        return hash("SHA256", strtr(md5($pass), $replace));
    }

    public function saveToMaileon($custom_fields = []) {
        try {
            //Elmentjük maileonba az adatok
    	    $maileon = new \Maileon();
    	    $maileon->contact->uid = $this->customer_id;
    		$maileon->contact->email = $this->email;
    		$maileon->contact->standard_fields['FIRSTNAME'] = $this->firstname;
    		$maileon->contact->standard_fields['LASTNAME'] = $this->lastname;
    		$orders = \Order::where('uid', $this->customer_id)->count();
    		$maileon->contact->custom_fields['orders'] = $orders;
    		if ($orders) {
        		$order = \Order::where('uid', $this->customer_id)->orderBy('created_at', 'DESC')->first();
        		$maileon->contact->custom_fields['last_order_date'] = $order->created_at->format('Y-m-d');
    		}
    	    $maileon->saveContact();
        } catch (Exception $e) {

        }
    }






}
