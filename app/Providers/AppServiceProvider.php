<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Blade::setRawTags('{{', '}}');
        \Blade::setContentTags('{{{', '}}}');
        \Blade::setEscapedContentTags('{{{', '}}}');

        \View::share('page_content','');
		\View::share('page_title','');
		\View::share('breadcrumb','');
		\View::share('body_class', 'cms-about-porto cms-page-view page-layout-1column');

		$highlighted_products = \Product::lang()->active()->where('menu_top', 'yes')->get();
		\View::share('highlighted_products', $highlighted_products);
		
		if (isset($highlighted_products)) {
			$side_products = array();
			$m=4;
			if ($m>count($highlighted_products)-2) $m=count($highlighted_products)-2;
			$i=0;
			$ujjkell=false;
			while ($i<=$m) {
				$side_products[$i] = $highlighted_products[rand(0,(count($highlighted_products)-1))];
				$ell=true;
				while ($ell) {
					$ell=false;
					$j=0;
					foreach($side_products as $highlighted_product) {
						if ($j<count($side_products)-1) {
							if ($side_products[$i] === $highlighted_product) {
								$side_products[$i] = $highlighted_products[rand(0,(count($highlighted_products)-1))];
								$ell=true;
							}
						}
						$j++;
					}
				}
				$i++;
			}
		}
		\View::share('side_products', $side_products);

		$informations = \Carousels::where('type', 'informations')->orderBy('weight', 'ASC')->active()->get();
		\View::share('informations', $informations);

		\Session::put('not_found_lang', []);
		\Session::put('not_found_perm', []);

		\Seo::setDefault();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
