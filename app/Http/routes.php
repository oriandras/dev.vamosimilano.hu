<?php

Route::pattern('id', '[0-9]+');
Route::pattern('uid', '[0-9]+');
Route::pattern('pid', '[0-9]+');
Route::pattern('slug', '[a-zA-Z0-9-_]+');
Route::pattern('slug2', '[a-zA-Z0-9-_]+');
Route::pattern('product_url', '.*');
Route::pattern('session_id', '.*');
Route::pattern('order_number', '.*');
Route::pattern('any', '.*');

Route::group(array('prefix' => 'api/v1', 'before' => 'api'), function()
{
    Route::post('addtocart/{session_id}/{pid}', 'ApiController@addToCart');
    Route::get('products/{id?}', 'ApiController@getProducts');
	Route::get('getorders/{id}', 'ApiController@getOrders');
	Route::get('getcreditcardpayment/{id}', 'ApiController@getCreditCardPayment');
    Route::get('size/{id}', 'ApiController@getSize');
    Route::get('categories/{id?}', 'ApiController@getCategories');
    Route::get('product/{id?}', 'ApiController@getProduct');
    Route::get('textiles', 'ApiController@getTextiles');
    Route::get('textile/{id}', 'ApiController@getTextile');
    Route::get('user/{id}', 'ApiController@getUser');
    Route::get('getprice/{id}', 'ApiController@getPrice');
    Route::post('loginuser', 'ApiController@loginUser');

    Route::post('favorite/{pid}/{id}', 'ApiController@setFavorite');
    Route::post('favorite/update/{id}', 'ApiController@updateFavorite');
    Route::post('favorite/delete/{id}', 'ApiController@deleteFavorite');
    Route::get('favorites/{id}', 'ApiController@getFavorite');
	
	Route::get('getsizes/{id}', 'ApiController@getProductSizes');
	Route::get('calculateprice', 'ApiController@calculateprice');
	Route::get('getcarousel', 'ApiController@getCarousel');
	
	Route::get('imagerender1', 'ApiController@imagerender1');
	Route::get('imagerender2', 'ApiController@imagerender2');
	Route::post('imagerender3', 'ApiController@imagerender3');
});


Route::group(array('before' => 'auth','middleware' => 'auth'), function () {
    Route::get('logout', 'AuthController@logout');
    Route::get('profile', 'ProfileController@index');
    Route::get('orders', 'ProfileController@orders');
    Route::get('newsletter', 'ProfileController@newsletter');
    Route::post('profile-update', 'ProfileController@update');

});
//Route::group(['middleware' => ['web']], function () {
    Route::get('facebook-login', 'FacebookController@facebookLogin');
    Route::get('facebook-login-auth', 'FacebookController@facebookLoginAuth');


    Route::get(t('aruhitel', 'url'), 'CmsController@credit');
	
	Route::get(t('black-friday', 'url'), 'CmsController@blackfriday');
	Route::get(t('tavasziakcio', 'url'), 'CmsController@tavasziakcio');
	Route::get(t('keszletkisopres', 'url'), 'CmsController@keszletkisopres');
	Route::get(t('orszagosakcio', 'url'), 'CmsController@nyitasiakcio');
	Route::get(t('nyitasiakcio', 'url'), 'CmsController@nyitasiakcio');
	
	/*
	//régi hírlevél
    Route::post(t('subscribe', 'url'), 'SubscribeController@subscribe');
    Route::get(t('leiratkozas', 'url')."/{product_url}", 'SubscribeController@unSubscribe');
    Route::get(t('subscribe/success', 'url'), 'SubscribeController@subscribeSuccess');
    Route::post(t('leiratkozas', 'url')."/end/{product_url}", 'SubscribeController@unSubscribePost');
	*/
	//új hírlevél
	Route::get('subscribe', 'SubscribeController@SubscribeNewsletter');
	Route::get('subscribe/success', 'SubscribeController@SubscribeNew');
    Route::get('unsubscribe/success', 'SubscribeController@unsubscribeNew');
	Route::get('unsubscribe', 'SubscribeController@unsubscribeGDPR');
	Route::post('cookieaccept', 'WebshopController@cookieaccept');
	
    Route::get(t('blog', 'url')."/{product_url?}", 'BlogController@index');
	Route::get(t('karrier', 'url')."/{product_url?}", 'BlogController@karrier');
	Route::get(t('blogproduct', 'url')."/{product_url?}", 'BlogController@blogproduct');
	Route::get(t('butor-szepseg-verseny', 'url')."/{product_url?}", 'BlogController@butorszepsegverseny');
    Route::get(t('login', 'url'), 'AuthController@login');
    Route::get(t('register', 'url'), 'AuthController@register');
    Route::post('login-data', 'AuthController@loginSubmit');
    Route::post('reg-data', 'AuthController@regSubmit');
    Route::get('facebook-connect', 'AuthController@facebookConnect');
    Route::get(t('password/email', 'url'), ['as' => t('password/email', 'url'), 'uses' => 'PasswordController@getEmail']);
    Route::post('password/email', 'PasswordController@postEmail');
    Route::get('password/reset/{token}', 'PasswordController@getReset');
    Route::post('password/reset', 'PasswordController@postReset');

    Route::get(t('contact', 'url'),  'ContactController@index');
    Route::post('contact-send',  'ContactController@contactSend');

    Route::get(t('products','url')."/{product_url?}/".t('addtocart','url'),'WebshopController@productAddToCart')->where('slug', '.*');
    Route::get(t('products','url')."/{product_url?}/".t('selecttextile','url'),'WebshopController@productSelectTextile');

	

	Route::get(t('products','url')."/{product_url?}/".t('modaltextile','url')."/{partname}",'WebshopController@productModalTextile');
    Route::get(t('products','url')."/{product_url?}",'WebshopController@product')->where('slug', '.*');

	Route::get(t('products','url')."/{product_url?}/".t('addtocart','url'),'WebshopController@productAddToCart')->where('slug', '.*');
    //Route::get("butor"."/{product_url?}",'WebshopController@product')->where('slug', '.*');


    Route::get(t('kiemelt-akciok','url'),'WebshopController@sales');


    Route::get(t('butor','url')."/{product_url?}",'WebshopController@oldRoute')->where('slug', '.*');
    Route::get(t('lampahaz','url')."/{product_url?}",'WebshopController@oldRoute')->where('slug', '.*');

/*
	Új termékadatlaphoz az ajax funkciók
*/
	Route::post('ajaxtest', 'WebshopController@ajaxtest');
/*
	Új termékadatlap vége
*/

    Route::post('cart/addtocart', 'CartController@addToCart');
	Route::post('cart/puttocart', 'CartController@putToCart');
    Route::get('cart/removecart/{slug}', 'CartController@removeCart');
    Route::get(t('cart','url'), 'CartController@index');
    Route::get(t('cart/personal','url'), 'CartController@personal');
    Route::get(t('cart/store','url'), 'CartController@store');
    Route::get(t('cart/success','url')."/{slug}", 'CartController@success');
	Route::get(t('cart/cardsuccess','url')."/{slug}", 'CartController@successbarcley');
    Route::get(t('cart/error','url')."/{slug}", 'CartController@error');
	Route::get(t('cart/carderror','url')."/{slug}", 'CartController@error');
    Route::get("cart/restore/{session_id}", 'CartController@restore');
    Route::post("cart/calculateprice", 'CartController@calculatePrice');
    Route::post("cart/searchcity", 'CartController@searchCity');

	Route::post('favorite/{pid}/{id}', 'WebshopController@setFavorite');
    Route::post('favorite/update/{id}', 'WebshopController@updateFavorite');
    Route::post('favorite/delete/{id}', 'WebshopController@deleteFavorite');
    Route::get('favorites/{id}', 'WebshopController@getFavorite');

    Route::get(t('cart/overview','url'), 'CartController@overview');
    Route::post(t('cart/savepersonal','url'), 'CartController@savePersonal');

    Route::get('/cart/preview','CartController@preview');


    Route::post('/cart/validatecoupon','CartController@validateCoupon');
    Route::get('/cartremovecoupon','CartController@removeCoupon');

    Route::post('cart/updateitemqty/{slug}', 'CartController@updateItemQty');


    Route::post("cart/loadoverviewblock", 'CartController@loadOverviewBlock');

    Route::get('payment/{order_number}/full', 'UnicreditController@fullStart');
    Route::get('payment/{order_number}/adv', 'UnicreditController@advStart');
    Route::get('payment/{order_number}/diff', 'UnicreditController@diffStart');

    Route::post('cart/card/success', 'UnicreditController@successPayment');
    Route::post('cart/card/error', 'UnicreditController@errorPayment');

	Route::get('payment/cancel', 'UnicreditController@paymentcancel');
	Route::get('payment/accept', 'UnicreditController@paymentaccept');

    Route::get(t('search', 'url'), 'WebshopController@search');

     Route::get('sitemap.xml', 'BaseController@sitemap');
	 
	 Route::get('productsfeed.xml', 'BaseController@productsfeed');
	 Route::get('productsfeedfb.xml', 'BaseController@productsfeedfb');
	 
	 Route::get('homeinfo', 'BaseController@homeinfo');

    Route::get('/', 'CmsController@home');
	Route::post('/checkout/payment/{order}/paypal', [
		'name' => 'PayPal Express Checkout',
		'as' => 'checkout.payment.paypal',
		'uses' => 'PayPalController@checkout',
	]);
		
	Route::get('/paypal/checkout/{order}/completed', [
		'name' => 'PayPal Express Checkout',
		'as' => 'paypal.checkout.completed',
		'uses' => 'PayPalController@completed',
	]);

	Route::get('/paypal/checkout/{order}/cancelled', [
		'name' => 'PayPal Express Checkout',
		'as' => 'paypal.checkout.cancelled',
		'uses' => 'PayPalController@cancelled',
	]);
	
	Route::post('/webhook/paypal/{order?}/{env?}', [
		'name' => 'PayPal Express IPN',
		'as' => 'webhook.paypal.ipn',
		'uses' => 'PayPalController@webhook',
	]);	

    Route::get('{any?}', 'CmsController@index');

//});
