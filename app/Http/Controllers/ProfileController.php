<?php



class ProfileController extends BaseController
{


    public function index()
    {

        Seo::setTitle(t('Saját adatok kezelése'));


        $view = [
            'user' => Auth::user(),
            'body_class' => 'account customer-account-edit page-layout-2columns-left'
        ];
         return View::make('profile.profile', $view);
    }

    public function orders()
    {
        Seo::setTitle(t('Rendelések listája'));

        $view = [
            'user' => Auth::user(),
            'orders' => Order::where('uid', Auth::user()->customer_id)->orderBy('created_at', 'DESC')->get(),
            'body_class' => 'account customer-account-edit page-layout-2columns-left'
        ];
         return View::make('profile.orders', $view);
    }

    public function newsletter(){
        Seo::setTitle(t('Hírlevél'));

        $user = Auth::user();

        $exist = Newsletter::where('email', $user->email)
	        ->where('shop_id', getShopId())
	        ->withTrashed()
	        ->first();

        $view = [
            'user' => $user,
            'unsub' => $exist,
            'body_class' => 'account customer-account-edit page-layout-2columns-left'
        ];
         return View::make('profile.newsletter', $view);
    }






    /**
     * update function.
     * Profile update
     *
     * @access public
     * @return void
     */
    public function update()
    {

        $user_id = Auth::id();

        $user = User::find($user_id);

        $rules = array(
            'firstname'=>'required|min:2',
            'lastname'=>'required|min:2',
            'email'=>'required|email|unique:customers,email,'.$user_id.",customer_id,deleted_at,NULL",
        );
        $messages = array(
            'firstname'=> t('firstname'),
            'lastname'=> t('lastname'),
            'email'=> t('email'),

        );



        if (Input::get('password_new')) {
            $rules['password'] = 'required|min:3';
            $rules['password_new'] = 'required|min:3';

            $messages['password'] = t('password');
            $messages['password_new'] = t('password_new');



            if (Hash::check(Input::get('password'), $user->password)) {
                $user->password = Hash::make(Input::get('password_new'));
            } else {
                return Redirect::action('ProfileController@index')
                ->withInput()
                ->with('flash_error', t('passwordsErrors'));
            }
        }

        //$user->validation_rules = $rules;
        //$user->validation_msg = $messages;

        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($messages);

        if ($validator->passes()) {
            $user->firstname = Input::get('firstname');
            $user->lastname = Input::get('lastname');

            $user->email = Input::get('email');
            $user->phone = Input::get('phone');
            try {
                $user->save();
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
                die("");
            }


            return Redirect::to(action('ProfileController@index'))->with('flash_success', t('edit_success'));
        }

        return Redirect::to(action('ProfileController@index'))->withErrors($validator->errors())->withInput();
    }
    /**
     * LoginToUser function.
     * Belépés idegen felhasználóhoz
     *
     * @access public
     * @param mixed $uid
     * @return void
     */
    public function LoginToUser($uid)
    {
        if (Auth::id() != 1) {
            return Redirect::to('/');
        }

        $user = User::find($uid);

        Auth::login($user);

        return Redirect::to('/');
    }
}
