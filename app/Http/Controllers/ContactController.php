<?php



class ContactController extends BaseController
{



    public function index()
    {
        Seo::setTitle(t('Kapcsolat'));
        $view = [];
        return View::make('cms.contact', $view);
    }


    function contactSend(){

    	$return = array();

    	$string = '';
    	$error = $this->validateData();
	    if($error !== true){
    	    foreach($error->getMessages() as $item){
        	    $string.=$item[0]."<br>";
    	    }
		    return Redirect::to(action('ContactController@index'))->withInput()->with('flash_error', $string);
	    }

    	 Mail::send
            (
                'emails.contact',
                array(
                    'data' => Input::all(),
                ),
                function ($message) {
                    //Config::get('website.contact_mails.'.Input::get('toaddress'))
                    $message->to(Config::get('website.contact_mails.'.Input::get('toaddress')))
                        ->subject(t('subject_'.Input::get('subject')));
                }
            );

    	return Redirect::to(action('ContactController@index'))->with('flash_success', t('Üzenetét megkaptuk, munkatársunk hamarosan keresi Önt!'));

	}


	 /**
     * validateData function.
     * Adatok validálása
     *
     * @access public
     * @static
     * @return void
     */
    public static function validateData(){
	    $validator = Validator::make(
			array(
			    'name' => Input::get('name'),
			    'email' => Input::get('email'),
			    'key' => Input::get('key'),
			    'comment' => Input::get('comment'),
			    'subject' => Input::get('subject'),
			    'toaddress' => Input::get('toaddress')
			),
			array(
			   'name' => 'required',
			   'key' => 'required',
			   'subject' => 'required',
			   'toaddress' => 'required',
			   'email' => 'required|email',
			   'comment' => 'required|min:20',
			)
	    );
		$niceNames = array(
			    'name' => t('Name'),
			    'key' => t('Azonosító'),
			    'subject' => t('Tárgy'),
			    'toaddress' => t('Címzett'),
			    'email' => t('E-mail cím'),
			    'comment' => t('Üzenet'),
		);
		$validator->setAttributeNames($niceNames);

		if ($validator->fails()){
			return $validator->messages();
		}

        return true;

    }

}
