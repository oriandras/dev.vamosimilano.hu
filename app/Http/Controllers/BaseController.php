<?php
use App\Http\Controllers\Controller;

class BaseController extends Controller
{


    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        /**
        * Ez a rész átkerült a AppServiceProvider-be.
        * Ott lehet meghatározni globális view változókat!
        **/
    }

    public function sitemap(){
        $products = Product::lang()->active()->get();
        $categories = ProductCategory::lang()->active()->get();
        $cms = Cms::lang()->where('cms_type', 'page')->active()->get();
        return Response::view('sitemap', ['products' => $products, 'cms' => $cms, 'categories' => $categories])->header('Content-Type', 'application/xml');
    }
	
	public function productsfeed(){
        $products = Product::lang()->active()->get();
        return Response::view('productsfeed', ['products' => $products])->header('Content-Type', 'application/xml');
    }
	public function productsfeedfb(){
        $products = Product::lang()->active()->get();
        return Response::view('productsfeedfb', ['products' => $products])->header('Content-Type', 'application/xml');
    }
	public function homeinfo(){
		$products = Product::where('menu_top', 'yes')->active()->lang()->get()->take(4);
        //return Response::view('homeinfofeed', ['products' => $products])->header('Content-Type', 'application/xml');
		
		$data = array();
		foreach( $products as $product ) {
			$data[] = array(
			"product"=>$product->getName(),
			"link"=>$product->getUrl(),
			"product_image"=>$product->getDefaultImageUrl('full'),
			"discount_price"=>$product->getDefaultPrice(),
			"price"=>$product->getDefaultFullPrice(),
			"discount_percent"=>$product->getPercentPrice()
			);
		}
		$array = [
            'items' => $data,
        ];

        return Response::json($array);
    }
}
