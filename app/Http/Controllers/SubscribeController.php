<?php



class SubscribeController extends BaseController
{


    public function subscribe()
    {
        $return = array();

    	$string = '';
    	$error = $this->validateData();
	    if($error !== true){
    	    foreach($error->getMessages() as $item){
        	    $string.=$item[0]."<br>";
    	    }
		    return Redirect::to(action('SubscribeController@subscribeSuccess'))->withInput()->with('flash_error', $string);
	    }

	    $exist = Newsletter::where('email', mb_strtolower(Input::get('email')))
	        ->where('shop_id', getShopId())
	        ->first();




	    if (empty($exist)) {

    	    //Elmentjük maileonba az adatok
    	    $maileon = new Maileon();
    	    $maileon->contact->uid = 0;
    		$maileon->contact->email = Input::get('email');
    		$maileon->contact->standard_fields['FIRSTNAME'] = Input::get('firstname');
    		$maileon->contact->standard_fields['LASTNAME'] = Input::get('lastname');
    	    $maileon->saveContact();



    	    $newsletter = new Newsletter();
            $newsletter->email = Input::get('email');
            $newsletter->unsub_url = str_replace(Config::get('maileon.'.getShopCode().'.UnsubLink'), '', $maileon->contact->custom_fields['Vegleges_leiratkozas']);
            $newsletter->shop_id = getShopId();
            $newsletter->firstname = Input::get('firstname');
            $newsletter->lastname = Input::get('lastname');
            $newsletter->save();



        }



        return Redirect::to(action('SubscribeController@subscribeSuccess')."?ok=1")->with('flash_success', t('Sikeresen feliratkozott a hirlevelünkre!'));

    }

    public static function subscribeSuccess(){
        Seo::setTitle(t('Feliratkozás'));
        return View::make('cms.subscribe');
    }
     /**
     * validateData function.
     * Adatok validálása
     *
     * @access public
     * @static
     * @return void
     */
    public static function validateData(){
	    $validator = Validator::make(
			array(
			    'firstname' => Input::get('firstname'),
			    'lastname' => Input::get('lastname'),
			    'email' => Input::get('email'),
			    'key' => Input::get('key'),

			),
			array(
			   'firstname' => 'required',
			   'lastname' => 'required',
			   'email' => 'required|email',
			   'key' => 'required',

			)
	    );
		$niceNames = array(
			'firstname' => t('Keresztnév'),
			'lastname' => t('Vezetéknév'),
			'email' => t('E-mail cím'),
		);
		$validator->setAttributeNames($niceNames);

		if ($validator->fails()){
			return $validator->messages();
		}

        return true;

    }


    public function unSubscribe($slug) {
        Seo::setTitle(t('Leiratkozás'));

        $hash = $slug;
        $maileonCore = new MaileonCore();
        $encodedString = $maileonCore->unsDecode($hash);
        $tmpArray = explode('::',$encodedString);
        $data['emailAddress'] = $tmpArray[0];
        $data['mode'] = $tmpArray[1];

        //TODO user vizsgálat hogy van e ilyen
        $maileonCore->getContact($data['emailAddress']);


        $view = [
            'email' => $data['emailAddress'],
            'slug' => $hash,
            'contact' => $maileonCore->maileoncontact,

        ];

        return View::make('cms.unsubscribe', $view);
    }

    public function unSubscribePost($slug) {
        $hash = $slug;
        $maileonCore = new MaileonCore();
        $encodedString = $maileonCore->unsDecode($hash);
        $tmpArray = explode('::',$encodedString);
        $data['emailAddress'] = $tmpArray[0];
        $data['mode'] = $tmpArray[1];
        $maileonCore->request("contacts/".$data['emailAddress']."/unsubscribe", Config::get('maileon.'.getShopCode().'.API_KEY'), 'DELETE');

        $exist = Newsletter::where('email', $data['emailAddress'])
	        ->first();
	    if (!empty($exist)){
    	    $exist->unsubscribe_time = (new DateTime())->format('Y-m-d H:i:s');
    	    $exist->save();
    	    $exist->delete();
	    }
	    sleep(20);

	    return Redirect::to(action('SubscribeController@unSubscribe', $slug))->with('flash_success', t('Sikeresen leiratkozott a hírlevelünkről!'));

    }

	public function SubscribeNew() {
		$view = [];
		return View::make('cms.subscribeNew', $view);
	}
	
	public function SubscribeNewsletter() {
		$view = [];
		return View::make('cms.subscribenewsletter', $view);
	}
	
	public function unsubscribeNew() {
		$view = [];
		return View::make('cms.unsubscribeNew', $view);
	}
	
	public function subscribeGDPR() {
		$view = [];
		return View::make('cms.subscribeGDPR', $view);
	}
	
	public function unsubscribeGDPR() {
		$view = [];
		return View::make('cms.unsubscribeGDPR', $view);
	}
}
