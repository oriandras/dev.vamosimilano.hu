<?php

/**
 * UnicreditController class.
 *
 * @extends Controller
 */
class UnicreditController extends BaseController {



    /**
     * fullStart function.
     * @TODO: hibakezelés
     *
     * @access public
     * @param mixed $order_number
     * @return void
     */
    public function fullStart($order_number){
        Session::forget('last_card_trans');

        $order = Order::where('order_number', $order_number)
            ->where('status', 'NEW')
            ->first();
        if (empty($order)) {
            return Redirect::action('CartController@error', $order_number)
                ->withErrors([t('Hiba történt a fizetés során, rendelése már fizetve lett, vagy nem létező rendelés!')]);
        }
        $tranzaction = new UniCredit();
        $url = $tranzaction->startPayment($order,'full');
        if ($url == false) {
            return Redirect::action('CartController@error', $order_number)
                ->withErrors([t('Hiba történt a fizetés során, kérjük próbálja újra!')]);
        }
        return Redirect::to($url);
    }

    public function advStart($order_number){
         Session::forget('last_card_trans');
        $order = Order::where('order_number', $order_number)
            ->where('status', 'NEW')
            ->first();
        if (empty($order)) {
            return Redirect::action('CartController@error', $order_number)
                ->withErrors([t('Hiba történt a fizetés során, rendelése már fizetve lett, vagy nem létező rendelés!')]);
        }
        $tranzaction = new UniCredit();
        $url = $tranzaction->startPayment($order,'advance');
        if ($url == false) {
            return Redirect::action('CartController@error', $order_number)
                ->withErrors([t('Hiba történt a fizetés során, kérjük próbálja újra!')]);
        }
        return Redirect::to($url);
    }
    public function diffStart($order_number){
         Session::forget('last_card_trans');
        $order = Order::where('order_number', $order_number)->first();
        if (empty($order)) {
            return Redirect::action('CartController@error', $order_number)
                ->withErrors([t('Hiba történt a fizetés során, rendelése már fizetve lett, vagy nem létező rendelés!')]);
        }
        $tranzaction = new UniCredit();
        $url = $tranzaction->startPayment($order,'different');
        if ($url == false) {
            return Redirect::action('CartController@error', $order_number)
                ->withErrors([t('Hiba történt a fizetés során, kérjük próbálja újra!')]);
        }
        return Redirect::to($url);
    }

    /**
     * successPayment function.
     * Sikeres oldalra irányítás
     *
     * @access public
     * @return void
     */
    public function successPayment() {
        $trans_id = Input::get('trans_id');
        $log = Payment::where('order_ref', $trans_id)
            ->where('status', 'START')
            ->orderBy('created_at', 'DESC')
            ->first();
        if (empty($log)) {
            return Redirect::action('CartController@error', '0')
                ->withErrors([t('Hiba történt a fizetés során, nem találunk ilyen tranzakciót!')]);
        }

        $result = UniCredit::CheckPayment($trans_id);

        $order = Order::where('order_number', $log->order_number)
            ->first();

        Session::put('last_card_trans', $result);

        if ($result['result'] == 'OK') {
            $order->status = 'PENDING';
            $order->save();
            $order->sendMail();
            $order->sendSuccessTransaction($result);

            return Redirect::action('CartController@success', $log->order_number)
                ->with('flash_success', t('Sikeres tranzakció!'));
        }else{
            $order->sendErrorTransaction($result);
            return Redirect::action('CartController@error', $log->order_number)
                ->withErrors([t('Hiba történt a fizetés során, kérjük próbálja újra!')]);
        }

    }
    /**
     * errorPayment function.
     * Sikertelen oldalra irányítás
     *
     * @access public
     * @return void
     */
    public function errorPayment() {
        $trans_id = Input::get('trans_id');
        $log = Payment::where('order_ref', $trans_id)
            ->where('status', 'START')
            ->orderBy('created_at', 'DESC')
            ->first();
        if (empty($log)) {
            return Redirect::action('CartController@error', '0')
                ->withErrors([t('Hiba történt a fizetés során, nem találunk ilyen tranzakciót!')]);
        }

        $result = Unicredit::CheckPayment($trans_id);

        //TODO - ERROR - EMAIL KÜLDÉS
        $order = Order::where('order_number', $log->order_number)->first();
        Session::put('last_card_trans', $result);
        $order->sendErrorTransaction($result);



        return Redirect::action('CartController@error', $log->order_number)
                ->withErrors([t('Hiba történt a fizetés során, kérjük próbálja újra!')]);


    }
}
