<?php

class BlogController extends BaseController
{


    public function index($content_url = '')
    {
		$tmp = explode("/", $content_url);
        $blog_url = end($tmp);
		
        $view = [
            'random_posts' =>  Cms::where('cms_type', 'blog')->orderBy(DB::raw('rand()'))->active()->lang()->take(4)->get(),
            'search' => ''
        ];
        Seo::setTitle(t('Blog'));

        if ($blog_url){
            $blog = Cms::where('cms_type', 'blog')->where('url', $blog_url)->active()->lang()->take(4)->first();
			if (isset($blog)) {
				while (strpos($blog->description,"{{ROW}}")) {
					$blog->description = str_replace(array('{{ROW}}','{{/ROW}}','{{','}}'),array('<div data-product="','" class="row product-listing mx-2 mx-lg-0"></div>','',','),$blog->description);
				}
				if (empty($blog)) {
					App::abort('404');
				}
				Seo::setTitle($blog->title);
				Seo::setDescription($blog->seo_description);
				$view['blog'] = $blog;
				$view['body_class'] = 'blog-index-index page-layout-2columns-right layout-1170 wide';
				return View::make('cms.blog_content', $view);
			} else {
				
				$blog_cat = $tmp[0];
				
				$blogs = Cms::where('cms_type', 'blog')->where('category','=',$blog_cat)->active()->lang()->orderBy('created_at', 'DESC');
				if (Input::get('q')) {
					$blogs->where(function($query) {
						return $query->where('title','like','%'.Input::get('q').'%')
							->orWhere('short_description','like','%'.Input::get('q').'%')
							->orWhere('description','like','%'.Input::get('q').'%');
					});
				}
				$blogs = $blogs->paginate(6);
				if (count($blogs)==0) {
					App::abort('404');
					return;
				}
				$view['blogs'] = $blogs;
				$view['search'] = Input::get('q');
				$view['body_class'] = 'blog-index-index page-layout-2columns-right layout-1170 wide';
				return View::make('cms.blog', $view);
				
			}
        }


        if ($blog_url == ''){
            $blogs = Cms::where('cms_type', 'blog')->active()->lang()->orderBy('created_at', 'DESC');
            if (Input::get('q')) {
                $blogs->where(function($query) {
                    return $query->where('title','like','%'.Input::get('q').'%')
                        ->orWhere('short_description','like','%'.Input::get('q').'%')
                        ->orWhere('description','like','%'.Input::get('q').'%');
                });
            }
            $blogs = $blogs->paginate(6);
            $view['blogs'] = $blogs;
            $view['search'] = Input::get('q');
            $view['body_class'] = 'blog-index-index page-layout-2columns-right layout-1170 wide';
            return View::make('cms.blog', $view);

        }else{

        }

    }
	
	function blogproduct($slug = ''){
		if ($slug == '') {
        	App::abort(404);
			return;
    	} else {
    	    $tmp = explode("/", $slug);
    	    $product = Product::where('url', end($tmp))->active()->lang()->first();
            if(sizeof($product)) {
                $view = [
                    'product' => $product
                ];
                return View::make('cms.blog_product', $view);
            } else {
                App::abort(404);
				return;
			}
		}
    }
	
	public function karrier($content_url = '')
    {
		$tmp = explode("/", $content_url);
        $blog_url = end($tmp);
		
        $view = [
            'random_posts' =>  Cms::where('cms_type', 'karrier')->orderBy(DB::raw('rand()'))->active()->lang()->take(4)->get(),
            'search' => ''
        ];
        Seo::setTitle(t('Karrier'));

        if ($blog_url){
            $blog = Cms::where('cms_type', 'karrier')->where('url', $blog_url)->active()->lang()->take(4)->first();
			if (isset($blog)) {
				while (strpos($blog->description,"{{ROW}}")) {
					$blog->description = str_replace(array('{{ROW}}','{{/ROW}}','{{','}}'),array('<div data-product="','" class="row product-listing mx-2 mx-lg-0"></div>','',','),$blog->description);
				}
				if (empty($blog)) {
					App::abort('404');
				}
				Seo::setTitle($blog->title);
				Seo::setDescription($blog->seo_description);
				$view['blog'] = $blog;
				$view['body_class'] = 'blog-index-index page-layout-2columns-right layout-1170 wide';
				return View::make('cms.karrier_content', $view);
			} else {
				
				$blog_cat = $tmp[0];
				
				$blogs = Cms::where('cms_type', 'karrier')->where('category','=',$blog_cat)->active()->lang()->orderBy('created_at', 'DESC');
				if (Input::get('q')) {
					$blogs->where(function($query) {
						return $query->where('title','like','%'.Input::get('q').'%')
							->orWhere('short_description','like','%'.Input::get('q').'%')
							->orWhere('description','like','%'.Input::get('q').'%');
					});
				}
				$blogs = $blogs->paginate(6);
				if (count($blogs)==0) {
					App::abort('404');
					return;
				}
				$view['blogs'] = $blogs;
				$view['search'] = Input::get('q');
				$view['body_class'] = 'blog-index-index page-layout-2columns-right layout-1170 wide';
				return View::make('cms.karrier', $view);
				
			}
        }


        if ($blog_url == ''){
            $blogs = Cms::where('cms_type', 'karrier')->active()->lang()->orderBy('created_at', 'DESC');
            if (Input::get('q')) {
                $blogs->where(function($query) {
                    return $query->where('title','like','%'.Input::get('q').'%')
                        ->orWhere('short_description','like','%'.Input::get('q').'%')
                        ->orWhere('description','like','%'.Input::get('q').'%');
                });
            }
            $blogs = $blogs->paginate(6);
            $view['blogs'] = $blogs;
            $view['search'] = Input::get('q');
            $view['body_class'] = 'blog-index-index page-layout-2columns-right layout-1170 wide';
            return View::make('cms.karrier', $view);

        }else{

        }

    }
	
	public function butorszepsegverseny($content_url = '')
    {
		$tmp = explode("/", $content_url);
        $blog_url = end($tmp);
		
        $view = [
            'random_posts' =>  Cms::where('cms_type', 'review')->orderBy(DB::raw('rand()'))->active()->lang()->take(4)->get(),
            'search' => ''
        ];
        Seo::setTitle(t('review'));

        if ($blog_url){
            $blog = Cms::where('cms_type', 'review')->where('url', $blog_url)->active()->lang()->take(4)->first();
			if (isset($blog)) {
				while (strpos($blog->description,"{{ROW}}")) {
					$blog->description = str_replace(array('{{ROW}}','{{/ROW}}','{{','}}'),array('<div data-product="','" class="row product-listing mx-2 mx-lg-0"></div>','',','),$blog->description);
				}
				if (empty($blog)) {
					App::abort('404');
				}
				Seo::setTitle($blog->title);
				Seo::setDescription($blog->seo_description);
				$view['blog'] = $blog;
				$view['body_class'] = 'blog-index-index page-layout-2columns-right layout-1170 wide';
				return View::make('cms.review_content', $view);
			} else {
				
				$blog_cat = $tmp[0];
				
				$blogs = Cms::where('cms_type', 'review')->where('category','=',$blog_cat)->active()->lang()->orderBy('created_at', 'DESC');
				if (Input::get('q')) {
					$blogs->where(function($query) {
						return $query->where('title','like','%'.Input::get('q').'%')
							->orWhere('short_description','like','%'.Input::get('q').'%')
							->orWhere('description','like','%'.Input::get('q').'%');
					});
				}
				$blogs = $blogs->paginate(6);
				if (count($blogs)==0) {
					App::abort('404');
					return;
				}
				$view['blogs'] = $blogs;
				$view['search'] = Input::get('q');
				$view['body_class'] = 'blog-index-index page-layout-2columns-right layout-1170 wide';
				return View::make('cms.review', $view);
				
			}
        }


        if ($blog_url == ''){
            $blogs = Cms::where('cms_type', 'review')->active()->lang()->orderBy('created_at', 'DESC');
            if (Input::get('q')) {
                $blogs->where(function($query) {
                    return $query->where('title','like','%'.Input::get('q').'%')
                        ->orWhere('short_description','like','%'.Input::get('q').'%')
                        ->orWhere('description','like','%'.Input::get('q').'%');
                });
            }
            $blogs = $blogs->paginate(6);
            $view['blogs'] = $blogs;
            $view['search'] = Input::get('q');
            $view['body_class'] = 'blog-index-index page-layout-2columns-right layout-1170 wide';
            return View::make('cms.review', $view);

        }else{

        }

    }
	
}
