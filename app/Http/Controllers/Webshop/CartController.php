<?php

use Omnipay\Omnipay;

class CartController extends BaseController {



	/**
	 * index function.
	 * Kosár tartalma
	 *
	 * @access public
	 * @return void
	 */
	function index(){
        //Cart::clear();
		
		$items = Cart::getContent();
		foreach($items as $item) {
			if ($item->id== 'floor') {
                continue;
            }
            $product = Product::lang()->active()->find($item['attributes']->product_id);
			if (isset($product) and ($product->category_id == Config::get('website.garancia_category'))) {
				if (isset($item['attributes']['parent_cartitem_id']) and ($item['attributes']['parent_cartitem_id'] != '0')) {
					$pitem = Cart::get($item['attributes']['parent_cartitem_id']);
					if (isset($pitem)) {
						if ($item->price != $pitem->price * $product->getDefaultPrice()) {
							Cart::remove($item[ 'id' ]);
							if ($product->getDefaultPrice()>0) {
								$p = array(
									'id' => $item[ 'id' ],
									'name' => $product->getName(),
									'price' => round($pitem->price * $product->getDefaultPrice(),-2),
									'quantity' => 1,
									'attributes' => $item['attributes'],
								);
								Cart::add($p);
							}
							CartHelper::saveCart();
						}
					}
				} else {
					Cart::remove($item[ 'id' ]);
				}
			}
		}
		
        Session::forget('last_card_trans');
        Seo::setTitle(t('Kosár tartalma'));
        $default_data = $this->getCartData();
    	$view = [
        	'content' => Cart::getContent(),
        	'default_data' => $default_data,
        	//'top_products' =>  Product::getProducts('home'),
    	];

        return View::make('webshop.cart.cart', $view);
	}

	/**
	 * personal function.
	 * Személyes adatok
	 *
	 * @access public
	 * @return void
	 */
	function personal(){

        $content = Cart::getContent();

    	if (!$content->count()) {
        	return Redirect::action('CartController@index');
    	}

        Seo::setTitle(t('Személyes adatok - Kosár'));


        $default_data = $this->getCartData();

        $view = [
           	'user' => Auth::user(),
           	'default_data' => $default_data,
           	'content' => $content,
           	'extra_prices' => ShippingExtra::lang()->where('price', '>', 0)->get(),
        ];

        return View::make('webshop.cart.personal', $view);
	}

	/**
	 * savePersonal function.
	 * Kosár adatok mentése session-be
	 *
	 * @access public
	 * @return void
	 */
	function savePersonal() {

        $input = Input::all();
        $input = $this->saveCartData($input);


        $order = new Order();
		$validation = $order->validation($input);


		// ERROR - Ha validálási hiba volt
		if ($validation !== true) {
			return Redirect::action('CartController@personal')
				->withErrors($validation);
		}




        return Redirect::action('CartController@overview');

	}



	/**
	 * addToCart function.
	 * Termék hozzáadása a kosárhoz
	 *
	 * @access public
	 * @return void
	 */
	function addToCart(){

        $cart_item_id = CartHelper::addToCart(Input::all());

        $input = Input::all();
        if (isset($input['related']) and $input['related'] != '0' and $input['related'] != '') {
			
			$relateds=explode(';',$input['related']);
			foreach($relateds as $relatedp) {
				if ($relatedp >0) {
					//return array('fn' => "flash('success','".$relatedp."'); reloadCartPreview(); setTimeout(function(){showMiniCart();},1000)");
					
					$related_ = ProductRelated::find($relatedp);
					if ($related_->rel_id == -1) {

					} else {
						if ($related_->rel_size_id > 0) {
							$size_ = ProductSize::find($related_->rel_size_id);
							$new_add = [
								'feature_id' => $related_->rel_size_id,
								'product_id' => $related_->rel_id,
								'custom_price' => $related_->product_price,
								'parent_cartitem_id' => $cart_item_id,
								'qty' => 1,
								'sizes' => [
									'a' => $size_->size_name,
								]
							];
						} else {
							$new_add = [
								'feature_id' => $related_->rel_size_id,
								'product_id' => $related_->rel_id,
								'custom_price' => $related_->product_price,
								'parent_cartitem_id' => $cart_item_id,
								'qty' => 1
								
							];
						}
						CartHelper::addToCart($new_add);
					}
					
				}
			}
			
        }

        CartHelper::saveCart();

        return array('fn' => "flash('success','".t('Sikeresen a kosárba tett egy terméket!')."'); reloadCartPreview(); setTimeout(function(){showMiniCart();},1000)");
		
	}
	
	function putToCart(){

        $cart_item_id = CartHelper::addToCart(Input::all());

        $input = Input::all();
        if (isset($input['related']) and $input['related'] != '0' and $input['related'] != '') {
			
			$relateds=explode(';',$input['related']);
			foreach($relateds as $relatedp) {
				if ($relatedp >0) {
					//return array('fn' => "flash('success','".$relatedp."'); reloadCartPreview(); setTimeout(function(){showMiniCart();},1000)");
					
					$related_ = ProductRelated::find($relatedp);
					if ($related_->rel_id == -1) {

					} else {
						if ($related_->rel_size_id > 0) {
							$size_ = ProductSize::find($related_->rel_size_id);
							$new_add = [
								'feature_id' => $related_->rel_size_id,
								'product_id' => $related_->rel_id,
								'custom_price' => $related_->product_price,
								'parent_cartitem_id' => $cart_item_id,
								'qty' => 1,
								'sizes' => [
									'a' => $size_->size_name,
								]
							];
						} else {
							$new_add = [
								'feature_id' => $related_->rel_size_id,
								'product_id' => $related_->rel_id,
								'custom_price' => $related_->product_price,
								'parent_cartitem_id' => $cart_item_id,
								'qty' => 1
								
							];
						}
						CartHelper::addToCart($new_add);
					}
					
				}
			}
			
        }

        CartHelper::saveCart();

        return Redirect::action('CartController@index')->with('flash_success', t('Sikeresen a kosárba tett egy terméket!'));
		
	}

	/**
	 * removeCart function.
	 * Törlés a kosárból
	 *
	 * @access public
	 * @param mixed $rowId
	 * @return void
	 */
	function removeCart($rowId){

    	CartHelper::removeItem($rowId);
		
		if (Cart::countItems()==0) {
			Cart::remove('floor');
		}

        return Redirect::to(action('CartController@index'))->with('flash_success', t('Sikeresen törölte a terméket a kosárból!'));
	}



	/**
	 * preview function.
	 * Kis áttekintő nézet.
	 *
	 * @access public
	 * @return void
	 */
	function preview(){
    	$return = array();
    	$return['html'] = View::make('webshop.cart.blocks.minicart', ['content' => Cart::getContent()])->render();
    	return $return;
	}


	function validateCoupon(){

    	$code = Input::get('coupon_code');
    	$code = strtoupper($code);
    	$validate = Coupons::validateCode($code);

    	if ($validate == 'NOTFOUND') {
            //return array('fn' => "flash('error','".t('Nem találtunk ilyen kupont!')."');");
			return Redirect::to(action('CartController@index'))->with('flash_error', t('Nem találtunk ilyen kupont!'));
    	}
    	if ($validate == 'LOGIN') {
            //return array('fn' => "flash('error','".t('A kupon beváltásához jelentkezzen be!')."');");
			return Redirect::to(action('CartController@index'))->with('flash_error', t('A kupon beváltásához jelentkezzen be!'));
    	}
    	if ($validate == 'DATE') {
            //return array('fn' => "flash('error','".t('Már nem érvényes kupon!')."');");
			return Redirect::to(action('CartController@index'))->with('flash_error', t('Már nem érvényes kupon!'));
    	}
    	if ($validate == 'LIMIT') {
            //return array('fn' => "flash('error','".t('A kupont túl sokszor beváltotta!')."');");
			return Redirect::to(action('CartController@index'))->with('flash_error', t('A kupont túl sokszor beváltotta!'));
    	}
    	if ($validate == 'PRODUCT') {
            //return array('fn' => "flash('error','".t('Nincs a kosárban olyan termék, amelyre aktiválni tudjuk a kupont!')."');");
			return Redirect::to(action('CartController@index'))->with('flash_error', t('Nincs a kosárban olyan termék, amelyre aktiválni tudjuk a kupont!'));
    	}

        if ($validate == 'OK') {
            CartHelper::saveCoupon($code, $this->getCartData());
            //return array('fn' => "flash('success','".t('Sikeresen aktiválta a kupont!')."'); setTimeout(function(){window.location.reload();},1000)");
			return Redirect::to(action('CartController@index'))->with('flash_success', t('Sikeresen aktiválta a kupont!'));
        }else{
            //return array('fn' => "flash('error','".t('Sikertelen kupon aktiválás!')."');");
			return Redirect::to(action('CartController@index'))->with('flash_error', t('Sikertelen kupon aktiválás!'));
        }

	}

	function removeCoupon(){
        CartHelper::removeCoupon($this->getCartData());
        return Redirect::to(action('CartController@index'))->with('flash_success', t('Sikeresen törölte a kupont!'));
	}



	/**
	 * updateItemQty function.
	 * Kosár elem darabszám növelés
	 *
	 * @access public
	 * @param mixed $item_id
	 * @return void
	 */
	function updateItemQty($item_id) {
    	if (Input::get('qty.'.$item_id) <= 0) {
        	CartHelper::removeItem($item_id);
    	}else{
        	$diff = (Input::get('qty.'.$item_id)-Input::get('qty_def'));
            Cart::update($item_id, array(
              'quantity' =>$diff,
            ));
        }

        CartHelper::saveCart();

        return Redirect::to(action('CartController@index'))->with('flash_success', t('Sikeresen módosította a terméket!'));
	}

	/**
	 * store function.
	 * Adatok mentése
	 *
	 * @access public
	 * @return void
	 */
	function store(){
        if (!Cart::getContent()) {
        	return Redirect::action('CartController@index');
    	}
        $input = $this->getCartData();
		if (($input['delivery_mode']=='COURIER') or ($input['delivery_mode']=='MPL')) {
			$validator = Validator::make($input, Order::$validation_rules);
		} else {
			$validator = Validator::make($input, Order::$validation_rules_billing);
		}
        $validator->setAttributeNames(Order::getFieldNames());

        // ERROR - Ha validálási hiba volt
        if (!$validator->passes()) {
            return Redirect::action('CartController@personal')
				->withErrors($validator->errors());
        }
        $input['email'] = trim(strtolower($input['email']));
        if (!Auth::user()){
            $user = User::where('email', $input['email'])->first();
            if (empty($user)){
                $password = "A".rand(1000,9999);
                $user = new User;
                $user->firstname = $input['firstname'];
                $user->lastname = $input['lastname'];
                $user->email = $input['email'];
                $user->password = Hash::make($password);
                $user->shop_id = getShopId();
                $user->save();
                $user->sendRegMail($password);

    		    if (Input::get('is_subscribed')){


    		    }

		    Auth::login($user);
			$uid = $user->customer_id;
            }else{
                $uid = $user->customer_id;
                Auth::login($user);
            }
        }else{
            $uid = Auth::user()->customer_id;
            $user = Auth::user();
        }

        $order = new Order();
        $order->shop_id = getShopId();
        $order->order_number = Order::getOrderNumber();
        $order->user_lastname = $input['user_lastname'];
        $order->user_firstname = $input['user_firstname'];
        $order->email = $input['email'];
        $order->lastname = $input['lastname'];
        $order->firstname = $input['firstname'];
        $order->country = $input['country'];
        $order->city = $input['city'];
        $order->address = $input['address'];
        $order->zip = $input['zip'];
        $order->floor = $input['floor'];
        //$order->other_addr = $input['other_addr'];
        $order->phone = $input['phone'];
        $order->message = $input['description'];
        $order->floor = $input['floor'];
        //$order->message_courier = $input['message_courier'];
        $order->billing_name = $input['billing_name'];
        $order->billing_country = $input['billing_country'];
        $order->billing_city = $input['billing_city'];
        $order->billing_address = $input['billing_address'];
        $order->billing_zip = $input['billing_zip'];
        //$order->billing_message = $input['billing_message'];
        $order->billing_tax_number = $input['billing_tax_number'];
        //$order->billing_other_addr = $input['billing_other_addr'];
        $order->coupon_code = (isset($input['coupon']) ? $input['coupon'] : '');
        $order->payment = $input['payment'];
        $order->uid = $uid;
        $order->pickpack_id = ($input['delivery_mode'] == 'PPP' ? $input['ppp'] : 0);
        $order->delivery_mode = $input['delivery_mode'];
        $order->discount_price = Cart::getDiscount();
        $order->delivery_price = Cart::getShipping(false);
        $order->total_price = (Cart::getCartTotal(false, false)-CartHelper::getFloorCost($input));
        $order->cost_price = CartHelper::getFloorCost($input);
        $order->delivery_price = Cart::getShipping();
        $order->payable_price = Cart::getCartTotal();
        $order->status = ($input['payment'] == 'COD' ? 'PENDING' : 'NEW');
        $order->advance_price = (strstr($order->payment, 'ADV') ? CartHelper::getAdvPrice() : 0 );						if (($order->delivery_mode == 'COURIER') and isset($input['COURIER_FREE']) and ($input['COURIER_FREE'])) {			/*			ez volt az ingyenes szállítás gyártandó-outlet bontás előtt			$teljesosszeg = $order->payable_price;			$shippingcost = $order->delivery_price;			$termekek = $order->total_price;			$shippingcost_shipping = Cart::getShipping(false); //ez ingyenes ha elfogadja			$shippingcost_extra = $shippingcost-$shippingcost_shipping;			$termekek = $termekek + $shippingcost_extra; //ez beleszámít a 200e ft-ba			$shippingcost_timeout = 0;			$termekekmore = 0;			if ( ($termekek >= 200000) ) {				$teljesosszeg = $teljesosszeg-$shippingcost_shipping;				$order->payable_price = $teljesosszeg;				$shippingcost = $shippingcost_extra;				$order->delivery_price = $shippingcost;				$shippingcost_timeout = $shippingcost_shipping;				$order->delivery_free = $shippingcost_timeout;				$order->delivery_mode = 'COURIER_FREE';			}			*/								//ingyenes házhozszállítás számolása			/*1.0			$shippingcost_timeout = 0;			$teljesosszeg = $order->payable_price;						$shippingcost = Cart::getShipping();			$shippingManufacture = Cart::getShippingManufacture();			$shippingOutlet = Cart::getShippingOutlet();			$termekek = $order->total_price ;			$gyartassum = CartHelper::getCartManufactureItemsSum();			$outletsum = CartHelper::getCartOutletItemsSum();			$egyebsum = CartHelper::getCartOtherItemsSum();			$gyartasrelatedsum = CartHelper::getCartManufactureRelatedItemsSum();			$outletrelatedsum = CartHelper::getCartOutletRelatedItemsSum();			$egyebsum = $egyebsum - $gyartasrelatedsum - $outletrelatedsum;			*/
			
						/*
			die(
				"shippingcost_timeout ".$shippingcost_timeout ."<br>".
				"teljesosszeg ".$teljesosszeg ."<br>".
				"shippingcost ".$shippingcost ."<br>".
				"shippingManufacture ".$shippingManufacture ."<br>".
				"shippingOutlet ".$shippingOutlet ."<br>".
				"termekek ".$termekek ."<br>".
				"gyartassum ".$gyartassum ."<br>".
				"outletsum ".$outletsum ."<br>".
				"egyebsum ".$egyebsum ."<br>".
				"gyartasrelatedsum ".$gyartasrelatedsum ."<br>". 
				"outletrelatedsum ".$outletrelatedsum ."<br>"
			);
			if (isset($order->delivery_mode) and ($order->delivery_mode == 'COURIER') and isset($input[ 'COURIER_FREE' ]) and ($input[ 'COURIER_FREE' ] ) ) {
				if ($outletsum>0) {
					if ($outletsum+$egyebsum+$outletrelatedsum >= 200000) {
						$shippingcost = $shippingcost - $shippingOutlet;
						$shippingcost_timeout = $shippingcost_timeout + $shippingOutlet;
						$teljesosszeg = $teljesosszeg - $shippingOutlet;
						
						$order->payable_price = $teljesosszeg;
						$order->delivery_price = $shippingcost;
						$order->delivery_free = $shippingcost_timeout;
						$order->delivery_mode = 'COURIER_FREE';
					}
				}
				if ($gyartassum>0) {
					if ($gyartassum+$gyartasrelatedsum >= 200000) {
						$shippingcost = $shippingcost - $shippingManufacture;
						$shippingcost_timeout = $shippingcost_timeout + $shippingManufacture;
						$teljesosszeg = $teljesosszeg - $shippingManufacture;
						
						$order->payable_price = $teljesosszeg;
						$order->delivery_price = $shippingcost;
						$order->delivery_free = $shippingcost_timeout;
						$order->delivery_mode = 'COURIER_FREE';
					}
				}
			}
			*/

			//Ingyenes házhozszállítás 2.0
			$shippingcost_timeout = 0;
			$mpl_shipping = 0;
			$teljesosszeg = $order->payable_price;			
			$shippingcost = Cart::getShipping();
			$shippingManufacture = Cart::getShippingManufacture();
			$shippingOutlet = Cart::getShippingOutlet();
			$termekek = $order->total_price ;
			$gyartassum = CartHelper::getCartManufactureItemsSum();
			$outletsum = CartHelper::getCartOutletItemsSum();
			$egyebsum = CartHelper::getCartOtherItemsSum();
			$gyartasrelatedsum = CartHelper::getCartManufactureRelatedItemsSum();
			$outletrelatedsum = CartHelper::getCartOutletRelatedItemsSum();
			$egyebsum = $egyebsum - $gyartasrelatedsum - $outletrelatedsum;
			
			$items_main = CartHelper::getCartMainItemsCount();
			$items_additional = CartHelper::getCartAdditionalItemsCount();
			$items_outlet = CartHelper::getCartOutletItemsCount();
			$items_value = CartHelper::getCartItemsSum(); //???
			
			$freeshipping_main = false;
			$freeshipping_outlet = false;
			if ($items_main>1) $freeshipping_main = true;
			//if (($items_main==1) and ($items_additional+$items_outlet>0)) $freeshipping_main = true;
			if (($items_main==1) and ($items_additional>0)) $freeshipping_main = true;
			if ($items_outlet>1) $freeshipping_outlet = true;
			//if (($items_outlet==1) and ($items_additional>0)) $freeshipping = true;
			if ($items_value>=500000) {
				$freeshipping_main = true;
				$freeshipping_outlet = true;
			}
			
			if (isset($order->delivery_mode) and ($order->delivery_mode == 'COURIER') and isset($input[ 'COURIER_FREE' ]) and ($input[ 'COURIER_FREE' ] ) and (($freeshipping_main) or ($freeshipping_outlet)) ) {
				//$shippingcost_timeout = $shippingOutlet + $shippingManufacture;
				//$teljesosszeg = $teljesosszeg - $shippingOutlet - $shippingManufacture;
				if ($freeshipping_main) {
					$shippingcost_timeout = $shippingcost_timeout + $shippingManufacture;
					$teljesosszeg = $teljesosszeg - $shippingManufacture;
					$shippingcost = $shippingcost - $shippingManufacture;
				}
				if ($freeshipping_outlet) {
					$shippingcost_timeout = $shippingcost_timeout + $shippingOutlet;
					$teljesosszeg = $teljesosszeg - $shippingOutlet;
					$shippingcost = $shippingcost - $shippingOutlet;
				}
				if ($shippingcost>0) {
					$mpl_shipping = $shippingcost - $shippingOutlet;
					$shippingcost = $shippingcost - $mpl_shipping;
				}
				$order->payable_price = $teljesosszeg;
				$order->delivery_price = $shippingcost+$mpl_shipping;
				$order->delivery_free = $shippingcost_timeout;
				$order->delivery_mode = 'COURIER_FREE';
			}					}
        $order->save();

        foreach (Cart::getContent() as $item) {

            if ($item->id== 'floor') {
                continue;
            }
            $product = Product::lang()->active()->find($item['attributes']->product_id);
            $price_full = 0;

            $feature = ($item['attributes']->feature_id ? ProductSize::find($item['attributes']->feature_id): []);
            $options = [];
            foreach ($item['attributes'] as $k => $v){
                $options[$k] = $v;
            }

            $order_item = new OrderItem();
            $order_item->order_id = $order->order_id;
            $order_item->qty = $item->quantity;
            $order_item->original_price = $price_full;
            $order_item->item_payable_price = $item->price;
            $order_item->product_id = $item['attributes']->product_id;
            $order_item->feature_id = 0;
            $order_item->item_number = 0;
            $order_item->item_name = $product->getName();
            $order_item->options = serialize($options);

            $order_item->save();


        }
        if (isset($input['extra_prices'])) {
            foreach ($input['extra_prices'] as $find) {
                $extra_ = ShippingExtra::find($find);

                $order_item = new OrderItem();
                $order_item->order_id = $order->order_id;
                $order_item->qty = 1;
                $order_item->original_price = $extra_->price;
                $order_item->item_payable_price = $extra_->price;
                $order_item->product_id = 0;
                $order_item->feature_id = 0;
                $order_item->item_number = 'SHIPPING';
                $order_item->item_name = $extra_->name;
                $order_item->options = serialize([]);

                $order_item->save();

            }
        }


        //Töröljük a kosarat, és a mentett kosarat.
        Cart::clear();
        CartHelper::deleteItems();
        try {
            $exist_maileon = new MaileonCore();
            $exist_maileon->getContact($input['email']);
            if ($exist_maileon->maileoncontact != false){
                $user->saveToMaileon();
            }
        } catch (Exception $e) {

        }

        if ($order->payment == 'FULL_CARD') {
            return Redirect::action('UnicreditController@fullStart', $order->order_number);
        }
        if ($order->payment == 'ADV_CARD') {
            return Redirect::action('UnicreditController@advStart', $order->order_number);
        }
		if ($order->payment == 'FULL_BARCLEY_CARD') {
			
			$amount = $order->payable_price;
		
			$log = new Payment();
			$log->shop_id = getShopId();
			$log->order_number = $order->order_number;
			$log->order_ref = $order->order_number;
			$log->status = 'START';
			$log->price = $amount;
			$log->card_type = 'OTHER';
			$log->type = 'full';
			$log->data = serialize([]);
			$log->save();
			$log->order_ref = base64_encode("ORDER-".$log->payment_id);
			$log->save();
			
			
						
			$gateway = Omnipay::getFactory()->create('BarclaysEpdq\Essential');
			$gateway->setClientId('epdq3001071');
			$gateway->setShaIn('SHnARDyXtqbmp4HaRaM9mUqWva9VvVFvThERkswz');
			$gateway->setCurrency('GBP');

			$purchase = $gateway->purchase();
			$purchase->setTransactionId('ORDER-'.$log->payment_id); // Unique ID
			$purchase->setAmount($order->payable_price);
			//$purchase->setTestMode(true);

			$response = $purchase->send();
			 
			return $response->redirect();
        }
        if ($order->payment == 'ADV_BARCLEY_CARD') {
            $amount = $order->advance_price;
		
			$log = new Payment();
			$log->shop_id = getShopId();
			$log->order_number = $order->order_number;
			$log->order_ref = $order->order_number;
			$log->status = 'START';
			$log->price = $amount;
			$log->card_type = 'OTHER';
			$log->type = 'advance';
			$log->data = serialize([]);
			$log->save();
			$log->order_ref = base64_encode("ORDER-".$log->payment_id);
			$log->save();
						
			$gateway = Omnipay::getFactory()->create('BarclaysEpdq\Essential');
			$gateway->setClientId('epdq3001071');
			$gateway->setShaIn('SHnARDyXtqbmp4HaRaM9mUqWva9VvVFvThERkswz');
			$gateway->setCurrency('GBP');

			$purchase = $gateway->purchase();
			$purchase->setTransactionId('ORDER-'.$log->payment_id); // Unique ID
			$purchase->setAmount($order->advance_price);
			//$purchase->setTestMode(true);

			$response = $purchase->send();
			 
			return $response->redirect();
        }
		if ($order->payment == 'FULL_PAYPALL') {
			
			$amount = $order->payable_price;

			$log = new Payment();
			$log->shop_id = getShopId();
			$log->order_number = $order->order_number;
			$log->order_ref = $order->order_number;
			$log->status = 'START';
			$log->price = $amount;
			$log->card_type = 'PAYPAL';
			$log->type = 'full';
			$log->data = serialize($order);
			$log->save();
			$log->order_ref = base64_encode("ORDER-".$log->payment_id);
			$log->save();
			
			$view = [
				'log' => $log,
				'order' => $order,
				'amount' => $log->price,
				'description' => t('Teljes összeg'),
				'payment' =>(Session::has('last_card_trans') ? Session::get('last_card_trans') : []),

			];
			return View::make('webshop.cart.paypal', $view);
        }
		if ($order->payment == 'ADV_PAYPALL') {
			
			$amount = $order->advance_price;

			$log = new Payment();
			$log->shop_id = getShopId();
			$log->order_number = $order->order_number;
			$log->order_ref = $order->order_number;
			$log->status = 'START';
			$log->price = $amount;
			$log->card_type = 'PAYPAL';
			$log->type = 'full';
			$log->data = serialize($order);
			$log->save();
			$log->order_ref = base64_encode("ORDER-".$log->payment_id);
			$log->save();
			
			$view = [
				'log' => $log,
				'order' => $order,
				'amount' => $log->price,
				'description' => t('Előleg'),
				'payment' =>(Session::has('last_card_trans') ? Session::get('last_card_trans') : []),

			];
			return View::make('webshop.cart.paypal', $view);
        }
        $order->sendMail();

        return Redirect::action('CartController@success', $order->order_number);

	}

	/**
	 * success function.
	 * Sikeres rendelés feladás
	 *
	 * @access public
	 * @param mixed $order_number
	 * @return void
	 */
	function success($order_number) {

        Seo::setTitle(t('Sikeres megrendelés feladás'));
		
		//outlet eladás ellenőrzése
		//az order itmek között ha van outletes cucc, akkor azt le kell tiltani.
		
		$order = Order::where('order_number', $order_number)->first();
		
		$items = $order->items();
		foreach ($items as $item) {
			if ($item->product_id > 0) {
				$product = Product::lang()->active()->find($item->product_id);
				if ((isset($product)) and ($product->isOutlet())) {
					//outletes cucc, letiltani
					DB::table('products_lang')
						->where('product_id', $item->product_id)
						->update(['visible' => 'no']);
					//levél küldése
					$order->sendOutletMail();
					break;
				}
			}
		}

    	$view = [
        	'order' => $order,
        	'payment' =>(Session::has('last_card_trans') ? Session::get('last_card_trans') : []),

    	];
    	Session::forget('last_card_trans');
    	return View::make('webshop.cart.success', $view);
	}
	
	function successbarcley($order_number) {

        Seo::setTitle(t('Sikeres megrendelés feladás'));
		
		//outlet eladás ellenőrzése
		//az order itmek között ha van outletes cucc, akkor azt le kell tiltani.
		
		$order = Order::where('order_number', $order_number)->first();
		
		$items = $order->items();
		foreach ($items as $item) {
			if ($item->product_id > 0) {
				$product = Product::lang()->active()->find($item->product_id);
				if ($product->isOutlet()) {
					//outletes cucc, letiltani
					DB::table('products_lang')
						->where('product_id', $item->product_id)
						->update(['visible' => 'no']);
					//levél küldése
					$order->sendOutletMail();
					break;
				}
			}
		}
		
		$payment = Payment::where('order_number', $order_number)
                ->where('status', 'SUCCED')
                ->orderBy('created_at', 'DESC')
                ->first();

    	$view = [
        	'order' => $order,
        	//'payment' =>(Session::has('last_card_trans') ? Session::get('last_card_trans') : []),
			'payment' => $payment,

    	];
    	Session::forget('last_card_trans');
    	return View::make('webshop.cart.successbarclay', $view);
	}
	
	

	/**
	 * error function.
	 * Sikertelen rendelés feladás
	 *
	 * @access public
	 * @param mixed $order_number
	 * @return void
	 */
	function error($order_number) {

        Seo::setTitle(t('Sikertelen megrendelés feladás'));

    	$view = [
        	'order' => Order::where('order_number', $order_number)->first(),
            'payment' =>(Session::has('last_card_trans') ? Session::get('last_card_trans') : []),
    	];
    	Session::forget('last_card_trans');
    	return View::make('webshop.cart.error', $view);
	}


	/**
	 * getCartData function.
	 * Kosár adatok betöltése
	 *
	 * @access public
	 * @return void
	 */
	function getCartData() {
    	if (Session::has('cart_data')) {
            $cart_data = Session::get('cart_data');
        }
        if (!Session::has('cart_data')){			
        	$cart_data = [
                'delivery_mode' => 'COURIER',								'COURIER_FREE' => 1,
                'payment' => 'FULL_CARD',
                'billing_country' => 'Magyarország',
                'country' => 'Magyarország',


        	];
        	if (Auth::user()){
            	$user = Auth::user();
            	$cart_data['user_lastname'] = $user->lastname;
            	$cart_data['user_firstname'] = $user->firstname;
            	$cart_data['email'] = $user->email;
                $cart_data['phone'] = $user->phone;
                $last_order = Order::lang()->where('uid', $user->customer_id)
        	        ->orderBy('created_at', 'DESC')
                    ->first();
                if (sizeof($last_order)) {
                    $cart_data['lastname'] = $last_order->lastname;
                    $cart_data['firstname'] = $last_order->firstname;
                    $cart_data['city'] = $last_order->city;
                    $cart_data['address'] = $last_order->address;
                    $cart_data['zip'] = $last_order->zip;
                    $cart_data['floor'] = $last_order->floor;
                    //$cart_data['other_addr'] = $last_order->other_addr;
                    if ($cart_data['phone'] == '') {
                        $cart_data['phone'] = $last_order->phone;
                    }
                }
        	}
            Session::put('cart_data', $cart_data);
        	$cart_data = Session::get('cart_data');

    	}
    	if (isset($cart_data['zip']) and $cart_data['zip'] == '') {

            if (Auth::user()){
            	$user = Auth::user();

                $last_order = Order::lang()->where('uid', $user->customer_id)
        	        ->orderBy('created_at', 'DESC')
                    ->first();
                if (sizeof($last_order)) {
                    $cart_data['lastname'] = $last_order->lastname;
                    $cart_data['firstname'] = $last_order->firstname;
                    $cart_data['city'] = $last_order->city;
                    $cart_data['address'] = $last_order->address;
                    $cart_data['zip'] = $last_order->zip;
                    $cart_data['floor'] = $last_order->floor;
                    //$cart_data['other_addr'] = $last_order->other_addr;
                    if ($cart_data['phone'] == '') {
                        $cart_data['phone'] = $last_order->phone;
                    }
                }
        	}
    	}
    	if (!CartHelper::getAdvPrice()) {
        	//$cart_data['payment'] = 'FULL_CARD';
    	}

    	Session::put('cart_data', $cart_data);
        $cart_data = Session::get('cart_data');


        return $cart_data;
	}

	/**
	 * saveCartData function.
	 * Kosár adatok mentése
	 *
	 * @access public
	 * @param mixed $input
	 * @return void
	 */
	function saveCartData($input) {


        $input['phone'] = stringToPhone($input['phone']);
        $input['country'] = "Magyarország";


    	Session::put('cart_data', $input);
        Session::save();


    	return $input;
	}

	/**
	 * restore function.
	 * Kosár visszaállítása
	 *
	 * @access public
	 * @param mixed $session_id
	 * @return void
	 */
	function restore($session_id) {

    	$cart = CartHelper::where('session_id', $session_id)->first();
    	if (!sizeof($cart)) {
        	return Redirect::action('CartController@index');
    	}
    	//Beléptetjük a felhasználót
    	if (!Auth::user() and $cart->uid){
        	$user = User::find($cart->uid);
            Auth::login($user);
    	}

    	//Jelenlegi kosár ürítése
    	Cart::clear();

    	//Feltöltés termékekkel
    	$items = CartItem::where('cart_id', $cart->cart_id)->get();
        foreach ($items as $item) {
            $options = unserialize($item->opt);
            $add_to_cart = [
                'product_id' => $item->product_id,
                'feature_id' => $item->feature_id,
                'qty' => $item->qty,


            ];
            if (isset($options['mounting'])) {
                $add_to_cart['mounting'] = $options['mounting'];
            }
			if (isset($options['seat'])) {
                $add_to_cart['seat'] = $options['seat'];
            }
            if (isset($options['sizes'])) {
                $add_to_cart['sizes'] = $options['sizes'];
            }
            if (isset($options['textile'])) {
                $add_to_cart['textile'] = $options['textile'];
            }
            CartHelper::addToCart($add_to_cart);
        }
        return Redirect::action('CartController@index')->with('flash_success', t('Sikeresen visszaállította a kosara tartalmát!'));

	}

	/**
	 * loadOverviewBlock function.
	 * Html html átnézet
	 *
	 * @access public
	 * @return void
	 */
	function loadOverviewBlock(){
    	$input = Input::all();
        $input = $this->saveCartData($input);
        $default_data = $this->getCartData();


        if ((isset($default_data['floor'])) and ($default_data['floor']) and (isset($default_data['delivery_mode'])) and ($default_data['delivery_mode']=="COURIER")) {
			
            Cart::remove('floor');
            $product = array(
                'id' => 'floor',
                'name' => t('Emelet költség'),
                'price' => ((isset($default_data['elevator']) and $default_data['elevator'] == 1) ? 0 : getConfig('floor_cost')),
                'quantity' => $default_data['floor'],
                'attributes' => array(),
            );
            Cart::add($product);
			
        }else{
            Cart::remove('floor');
        }

        CartHelper::saveCart();
        $view = [
           	'user' => Auth::user(),
           	'default_data' => $default_data,
           	'content' => Cart::getContent(),
        ];
        if (isset($input['block'])) {
            if ($input['block'] == 'shipping_right') {
                $html = htmlToAjax(View::make('webshop.cart.blocks.shipping_right', $view)->render());
            }else{
                $html = htmlToAjax(View::make('webshop.cart.blocks.personal_right', $view)->render());
            }
        }else{
            $html = htmlToAjax(View::make('webshop.cart.blocks.personal_right', $view)->render());
        }



        return ['html' => $html, 'fn' => 'setTimeout(function(){checkCartRequired()},1000);'];
	}

	public function calculatePrice(){
    	$input = Input::all();

        $price_array = CartHelper::calculateItemPrice($input, true);

        $plus_price = 0;
        if (isset($input['related']) and $input['related'] != '0' and $input['related'] != '') {
            $related_ = ProductRelated::find($input['related']);
			if (isset($related)) {
				if ($related_->show_price > 0){
					//$plus_price = $related_->show_price;
				}
			}
        }

    	return [
    	    'furniture_price' => money($price_array['furniture_price']) ,
    	    'price' => money($price_array['all_price']+$plus_price),
    	    'textile_price' => money($price_array['textile_price']),
    	    'debug' => isset($price_array['debug']) ? $price_array['debug'] : [],
			'alapbutor' => $price_array['furniture_price'],
			'szovetfelar' => $price_array['textile_price'],
			'priceall' => ($price_array['all_price']+$plus_price),
    	    'priceplus' => ($price_array['textile_price']),
    	   ];
	}

	public function searchCity() {
		if (getShopCode()=='hu') {
			$city = Cities::where('shop_id',getShopId())
				->where('zipcode', Input::get('zip'))
				->first();

			if (empty($city)) {
				return array('fn' => "flash('error','".t('Adjon meg helyes irányítószámot!')."');");
			}else{
				return array('city' => $city->settlement);
			}
		} else {
			$city = Cities::where('shop_id',getShopId())
				->where(DB::raw("instr('".Input::get('zip')."',zipcode)"), '=', 1)
				->first();

			if (empty($city)) {
				//return array('fn' => "flash('error','".t('Ellenőrizze a címet, vagy a megadott címre nem tudunk házhozszállítást vállalni!')."');");
			}else{
				return array('city' => $city->settlement);
			}
		}
	}




}
