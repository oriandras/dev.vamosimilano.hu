<?php
function getShopId(){
	return 1;
}

function getConfig($type) {
	return Config::get('shop.'.getShopCode().'.'.$type);
}

    /**
 * money function.
 * Pénz formátum
 *
 * @access public
 * @param mixed $price
 * @return void
 */
function money($price, $from = false){
	if (App::getLocale() == 'en') {
		if ($price == ceil($price)) {
			return Config::get('shop.'.getShopCode().'.shop_currency').number_format($price, 0, '.', ' ')." ".($from ? '-tól' : '');
		} else {
			return Config::get('shop.'.getShopCode().'.shop_currency').number_format($price, 2, '.', ' ')." ".($from ? '-tól' : '');
		}
	} else {
		return number_format($price, 0, '.', ' ')." ".Config::get('shop.'.getShopCode().'.shop_currency').($from ? '-tól' : '');
	}
}

/**
 * stringToPhone function.
 * Stringet alakít telefonszámmá,.
 *
 * @access public
 * @param mixed $phone
 * @return void
 */
function stringToPhone($phone){

	$phone = trim($phone);

	if(substr($phone,0,1) == "+"){
		$phone = "+".preg_replace('/[^0-9\']/', '',$phone);
	}else{
		$phone = preg_replace('/[^0-9\']/', '',$phone);
	}
	if(substr($phone,0,2) == 30 or substr($phone,0,2) == 20 or substr($phone,0,2) == 31 or substr($phone,0,2) == 70){
		//Ha az elején 30-szerepel, akkor elé tesszük a +36-ot.
		$phone = "+36".$phone;
	}
	if(substr($phone,0,2) == "36"){
		//Ha az elején 36-szerepel, akkor elé tesszük a +-ot.
		$phone = "+".$phone;
	}
	if(substr($phone,0,2) == "06"){
		//Ha az elején 06-szerepel, akkor lecseréljük a +36-ra.
		$phone = "+36".substr($phone,2);
	}

	$phone = str_replace("+", "", $phone);
	return $phone;
}

function htmlToAjax($html){

    $html = trim(preg_replace('/\s\s+/', ' ',$html));
    $html = str_replace(array("\r\n", "\r", "\n"), "",$html);
    $html = str_replace("'", '"', $html);

    return $html;
}
/**
 * nicename function.
 *
 *
 * @access public
 * @param mixed $data
 * @return void
 */
function nicename($data){

    $data = mb_strtolower($data);
    $replace = array("á","Á","é","É","í","Í","ö","Ö","ő","Ő","ú","Ú","ü","Ü","ű","Ű","ó","Ó"," ");
    $validchar = array("a","A","e","E","i","I","o","O","o","O","u","U","u","U","u","U","o","O","-");
    $data = str_replace($replace,$validchar,$data);

    return mb_strtolower($data);
}
function base64image($filename, $filetype){

    return ImageCache::cache($filename, $filetype);
}

?>