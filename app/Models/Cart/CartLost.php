<?php

/**
 * CartLost class.
 * Elveszett kosár - email küldés
 *
 *
 */
class CartLost
{

    /**
     * send function.
     * Elhagyott kosár küldése, 1 napnál régebbi kosarakra
     *
     * @access public
     * @static
     * @return void
     */
    public static function send(){
        $now = new DateTime();
        $date = new DateTime();
        $date->modify('- 1 days');
        $lost = CartHelper::where('updated_at','<', $date->format('Y-m-d H:i:s'))
            ->whereNull('email_sent')
            ->where('email', '!=', '')
            ->where('session_id', '!=', '')
            ->groupBy('email')
            ->get();
        foreach ($lost as $item) {
            $items = CartItem::where('cart_id', $item->cart_id)->get();
            if (sizeof($items)) {
                $title = [];
                foreach ($items as $_pitem) {
                    $title[$_pitem->product_name] = $_pitem->product_name;
                }
                try {
                    //Elmentjük maileonba az adatokat
                    $exist_maileon = new MaileonCore();
                    $exist_maileon->getContact($item->email);
                    if ($exist_maileon->maileoncontact != false){

                	    $maileon = new Maileon();
                	    $maileon->contact->uid = 0;
                		$maileon->contact->email = $item->email;
                		$maileon->contact->standard_fields['FIRSTNAME'] = $item->firstname;
                		$maileon->contact->standard_fields['LASTNAME'] = $item->lastname;
                	    $maileon->saveContact();

                        $maileon = new Maileon();
                        $maileon->event->restore_link  = action('CartController@restore', $item->session_id);
                        $maileon->event->item = implode(',', $title);
                        $maileon->saveEvent($item->email, 'Cart_lost');
                    }
                } catch (Exception $e) {

                }

            }

            $item->email_sent = $now->format('Y-m-d H:i:s');
            $item->save();
        }
    }

}
