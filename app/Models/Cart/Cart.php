<?php
/**
 * Cart class.
 * A kosár műveleteket végzi.
 * @extends https://github.com/darryldecode/laravelshoppingcart
 */
class Cart extends Shoppingcart {
	
	public static function getShippingManufacture() {
		$total = Cart::getTotal();
		$shipping_price = 0;
		if ( Session::has( 'cart_data' ) ) {
			$cart_data   = Session::get( 'cart_data' );
			if ( ( ( isset( $cart_data[ 'city' ] ) ) and ( $cart_data[ 'city' ] != '' ) ) or ( ( isset( $cart_data[ 'delivery_mode' ] ) ) and ( strstr( $cart_data[ 'delivery_mode' ], 'PERSONAL' ) ) ) ) {
				$params = array(
					'city' => ( isset( $cart_data[ 'city' ] ) ? $cart_data[ 'city' ] : '' ),
					'address' => ( isset( $cart_data[ 'address' ] ) ? $cart_data[ 'address' ] : '' ),
					'zip' => ( isset( $cart_data[ 'zip' ] ) ? $cart_data[ 'zip' ] : '' ),
					'delivery_mode' => ( isset( $cart_data[ 'delivery_mode' ] ) ? $cart_data[ 'delivery_mode' ] : '' ) 
				);
				if ( ( isset( $cart_data[ 'delivery_mode' ] ) ) and ( $cart_data[ 'delivery_mode' ] != 'MPL' ) ) {
					if ( getShopCode() == 'hu' ) {
						$price_array = Shipping::getShippingCost( $params );
					}
					else {
						$price_array = Shipping::getShippingCostMiles( $params );
						if ( isset( $price_array[ "error" ] ) ) {
							return $price_array[ "error" ];
						}
					}
				} else {
					$price_array = Shipping::getMPLCost( $params );
				}
				$shipping_price = $price_array[ 'price' ];
				$cuccok         = Cart::countManufactureShippingItems();
				$shipping_price = $shipping_price * $cuccok;
			}
			return $shipping_price;
		}
		return 0;
	}
	
	public static function getShippingOutlet() {
		$total = Cart::getTotal();
		$shipping_price = 0;
		if ( Session::has( 'cart_data' ) ) {
			$cart_data   = Session::get( 'cart_data' );
			if ( ( ( isset( $cart_data[ 'city' ] ) ) and ( $cart_data[ 'city' ] != '' ) ) or ( ( isset( $cart_data[ 'delivery_mode' ] ) ) and ( strstr( $cart_data[ 'delivery_mode' ], 'PERSONAL' ) ) ) ) {
				$params = array(
					'city' => ( isset( $cart_data[ 'city' ] ) ? $cart_data[ 'city' ] : '' ),
					'address' => ( isset( $cart_data[ 'address' ] ) ? $cart_data[ 'address' ] : '' ),
					'zip' => ( isset( $cart_data[ 'zip' ] ) ? $cart_data[ 'zip' ] : '' ),
					'delivery_mode' => ( isset( $cart_data[ 'delivery_mode' ] ) ? $cart_data[ 'delivery_mode' ] : '' ) 
				);
				if ( ( isset( $cart_data[ 'delivery_mode' ] ) ) and ( $cart_data[ 'delivery_mode' ] != 'MPL' ) ) {
					if ( getShopCode() == 'hu' ) {
						$price_array = Shipping::getShippingCost( $params );
					}
					else {
						$price_array = Shipping::getShippingCostMiles( $params );
						if ( isset( $price_array[ "error" ] ) ) {
							return $price_array[ "error" ];
						}
					}
				} else {
					$price_array = Shipping::getMPLCost( $params );
				}
				$shipping_price = $price_array[ 'price' ];
				$cuccok         = Cart::countOutletShippingItems();
				$shipping_price = $shipping_price * $cuccok;
			}
			return $shipping_price;
		}
		return 0;
	}
	
	/**
	 * getShipping function.
	 * Szállítási költség kiszámolása
	 * @access public
	 * @static
	 * @return void
	 */
	public static function getShipping( $include_extra_price = true ) {
		$total = Cart::getTotal();
		$shipping_price = 0;
		if ( Session::has( 'cart_data' ) ) {
			$cart_data   = Session::get( 'cart_data' );
			$extra_price = $shipping_price = 0;
			if ( isset( $cart_data[ 'extra_prices' ] ) ) {
				foreach ( $cart_data[ 'extra_prices' ] as $find ) {
					$extra_ = ShippingExtra::find( $find );
					$extra_price += $extra_->price;
				} //$cart_data[ 'extra_prices' ] as $find
			} //isset( $cart_data[ 'extra_prices' ] )
			if ( ( ( isset( $cart_data[ 'city' ] ) ) and ( $cart_data[ 'city' ] != '' ) ) or ( ( isset( $cart_data[ 'delivery_mode' ] ) ) and ( strstr( $cart_data[ 'delivery_mode' ], 'PERSONAL' ) ) ) ) {
				$params = array(
					 'city' => ( isset( $cart_data[ 'city' ] ) ? $cart_data[ 'city' ] : '' ),
					'address' => ( isset( $cart_data[ 'address' ] ) ? $cart_data[ 'address' ] : '' ),
					'zip' => ( isset( $cart_data[ 'zip' ] ) ? $cart_data[ 'zip' ] : '' ),
					'delivery_mode' => ( isset( $cart_data[ 'delivery_mode' ] ) ? $cart_data[ 'delivery_mode' ] : '' ) 
				);
				if ( ( isset( $cart_data[ 'delivery_mode' ] ) ) and ( $cart_data[ 'delivery_mode' ] != 'MPL' ) ) {
					if ( getShopCode() == 'hu' ) {
						$price_array = Shipping::getShippingCost( $params );
					} //getShopCode() == 'hu'
					else {
						$price_array = Shipping::getShippingCostMiles( $params );
						if ( isset( $price_array[ "error" ] ) ) {
							return $price_array[ "error" ];
						} //isset( $price_array[ "error" ] )
					}
				} //( isset( $cart_data[ 'delivery_mode' ] ) ) and ( $cart_data[ 'delivery_mode' ] != 'MPL' )
				else {
					$price_array = Shipping::getMPLCost( $params );
				}
				$shipping_price = $price_array[ 'price' ];
				$cuccok         = Cart::countShippingItems();
				
				
				
				if ( ( isset( $cart_data[ 'delivery_mode' ] ) ) and ( $cart_data[ 'delivery_mode' ] != 'MPL' ) and ( $shipping_price > 0 ) ) {
					$shipping_price = $shipping_price * $cuccok;
				}
				
				if ( ( isset( $cart_data[ 'delivery_mode' ] ) ) and ( $cart_data[ 'delivery_mode' ] != 'MPL' )) {
					if (CartHelper::getCartOutletItemsSum()==0) {
						$t = Shipping::getMPLCost( $params );
						$shipping_price = $shipping_price + $t[ 'price' ];
					}
				}
				/*
				if ( getShopCode() == 'hu' ) {
					//magyar outlet szállítási költéség
					foreach ( Cart::getContent() as $item ) {
						if ( $item->id == 'floor' ) {
							continue;
						} //$item->id == 'floor'
						$product = Product::lang()->active()->find( $item[ 'attributes' ]->product_id );
						if ( ( isset( $product ) ) and ( $product->isOutlet() ) ) {
							if ( ( isset( $cart_data[ 'delivery_mode' ] ) ) and ( strstr( $params[ 'delivery_mode' ], 'PERSONAL' ) ) ) {
								//ha másik városban van az átvétele, akkor van + szállítási költsége
								if ( $params[ 'delivery_mode' ] != 'PERSONAL_BP' ) {
									if ($product->ship_type != "mpl") $shipping_price = $shipping_price + 19990;
								} //$params[ 'delivery_mode' ] != 'PERSONAL_BP'
							} //( isset( $cart_data[ 'delivery_mode' ] ) ) and ( strstr( $params[ 'delivery_mode' ], 'PERSONAL' ) )
							else {
								if ( $shipping_price > 0 ) {
									if ( $product->category_id != 131 )
										if ($product->ship_type != "mpl") $shipping_price = $shipping_price + 19990;
								} //$shipping_price > 0
							}
						} //( isset( $product ) ) and ( $product->isOutlet() )
					} //Cart::getContent() as $item
				} //getShopCode() == 'hu'
				*/
			} //( ( isset( $cart_data[ 'city' ] ) ) and ( $cart_data[ 'city' ] != '' ) ) or ( ( isset( $cart_data[ 'delivery_mode' ] ) ) and ( strstr( $cart_data[ 'delivery_mode' ], 'PERSONAL' ) ) )
			//return var_export($price_array, true);
			return ( $shipping_price + ( $include_extra_price ? $extra_price : 0 ) );
		} //Session::has( 'cart_data' )
		return 0;
	}
	public static function getDiscount( ) {
		if ( Session::has( 'cart_data' ) ) {
			$cart_data = Session::get( 'cart_data' );
			if ( isset( $cart_data[ 'coupon' ] ) and $cart_data[ 'coupon' ] != '' ) {
				if ( Coupons::runCheck( $cart_data[ 'coupon' ] ) ) {
					$coupons = Coupons::where( 'code', $cart_data[ 'coupon' ] )->first();
					$total   = ( Cart::getTotalForCoupon() );
					if ( $coupons->type == 'fix' ) {
							foreach ( Cart::getContent() as $item ) {
								if ( $item->id == 'floor' ) {
									continue;
								} //$item->id == 'floor'
								$product = Product::lang()->active()->find( $item[ 'attributes' ]->product_id );
								if ( !isset( $product ) ) {
									continue;
								} //!isset( $product )
								if ( $coupons->name == "MAK201808" ) {
									if ( ( $product->category_id != 4 ) and ( $product->category_id != 12 ) and ( $product->category_id != 24 ) and ( $product->category_id != 101 ) and ( $product->category_id != 102 ) and ( $product->category_id != 103 ) and ( $product->category_id != 104 ) and ( $product->category_id != 106 ) and ( $product->category_id != 107 ) and ( $product->category_id != 108 ) and ( $product->category_id != 109 ) and ( $product->category_id != 110 ) and ( $product->category_id != 112 ) and ( $product->category_id != 113 ) and ( $product->category_id != 115 ) and ( $product->category_id != 116 ) and ( $product->category_id != 117 ) and ( $product->category_id != 118 ) and ( $product->category_id != 119 ) and ( $product->category_id != 120 ) and ( $product->category_id != 121 ) and ( $product->category_id != 122 ) and ( $product->category_id != 123 ) and ( $product->category_id != 124 ) and ( $product->category_id != 125 ) and ( $product->category_id != 126 ) and ( $product->category_id != 127 ) and ( $product->category_id != 128 ) and ( $product->category_id != 129 ) and ( $product->category_id != 130 ) and ( $product->category_id != 131 ) and ( $product->category_id != 132 ) and ( $product->category_id != 133 ) and ( $product->category_id != 134 ) and ( $product->category_id != 135 ) and ( $product->category_id != 136 ) and ( $product->category_id != 137 ) and ( $product->category_id != 138 ) and ( $product->category_id != 139 ) and ( $product->category_id != 140 ) and ( $product->category_id != 141 ) and ( $product->category_id != 142 ) and ( $product->category_id != 143 ) and ( $product->category_id != 144 ) and ( $product->category_id != 145 ) and ( $product->category_id != 146 ) and ( $product->category_id != 147 ) and ( $product->category_id != 148 ) and ( $product->category_id != 149 ) and ( $product->category_id != 150 ) and ( $product->category_id != 151 ) and ( $product->category_id != 152 ) and ( $product->category_id != 153 ) and ( $product->category_id != 154 ) and ( $product->category_id != 155 ) ) {
										continue;
									} //( $product->category_id != 4 ) and ( $product->category_id != 12 ) and ( $product->category_id != 24 ) and ( $product->category_id != 101 ) and ( $product->category_id != 102 ) and ( $product->category_id != 103 ) and ( $product->category_id != 104 ) and ( $product->category_id != 106 ) and ( $product->category_id != 107 ) and ( $product->category_id != 108 ) and ( $product->category_id != 109 ) and ( $product->category_id != 110 ) and ( $product->category_id != 112 ) and ( $product->category_id != 113 ) and ( $product->category_id != 115 ) and ( $product->category_id != 116 ) and ( $product->category_id != 117 ) and ( $product->category_id != 118 ) and ( $product->category_id != 119 ) and ( $product->category_id != 120 ) and ( $product->category_id != 121 ) and ( $product->category_id != 122 ) and ( $product->category_id != 123 ) and ( $product->category_id != 124 ) and ( $product->category_id != 125 ) and ( $product->category_id != 126 ) and ( $product->category_id != 127 ) and ( $product->category_id != 128 ) and ( $product->category_id != 129 ) and ( $product->category_id != 130 ) and ( $product->category_id != 131 ) and ( $product->category_id != 132 ) and ( $product->category_id != 133 ) and ( $product->category_id != 134 ) and ( $product->category_id != 135 ) and ( $product->category_id != 136 ) and ( $product->category_id != 137 ) and ( $product->category_id != 138 ) and ( $product->category_id != 139 ) and ( $product->category_id != 140 ) and ( $product->category_id != 141 ) and ( $product->category_id != 142 ) and ( $product->category_id != 143 ) and ( $product->category_id != 144 ) and ( $product->category_id != 145 ) and ( $product->category_id != 146 ) and ( $product->category_id != 147 ) and ( $product->category_id != 148 ) and ( $product->category_id != 149 ) and ( $product->category_id != 150 ) and ( $product->category_id != 151 ) and ( $product->category_id != 152 ) and ( $product->category_id != 153 ) and ( $product->category_id != 154 ) and ( $product->category_id != 155 )
								} //$coupons->name == "MAK201808"
							} //Cart::getContent() as $item
						if ( $total < $coupons->value ) {
							return $total;
						} //$total < $coupons->value
						else {
							return $coupons->value;
						}
					} //$coupons->type == 'fix'
					else {
						if ( $coupons->type == 'basepercent' ) {
							//csak az alapbútorra érvényes engedmény
							$discount = 0;
							foreach ( Cart::getContent() as $item ) {
								if ( $item->id == 'floor' ) {
									continue;
								} //$item->id == 'floor'
								$product = Product::lang()->active()->find( $item[ 'attributes' ]->product_id );
								if ( !isset( $product ) ) {
									continue;
								} //!isset( $product )
								/*
								if (isset($item['attributes']['parent_cartitem_id']) and ($item['attributes']['parent_cartitem_id'] != '0'))
								{
								continue;;
								}
								*/
								if ( $coupons->name == "SBERBANK" ) {
									if ( ( $product->category_id != 4 ) and ( $product->category_id != 9 ) and ( $product->category_id != 12 ) and ( $product->category_id != 24 ) and ( $product->category_id != 144 ) and ( $product->category_id != 145 ) and ( $product->category_id != 146 ) and ( $product->category_id != 148 ) and ( $product->category_id != 149 ) and ( $product->category_id != 150 ) and ( $product->category_id != 151 ) and ( $product->category_id != 152 ) ) {
										continue;
									} //( $product->category_id != 4 ) and ( $product->category_id != 9 ) and ( $product->category_id != 12 ) and ( $product->category_id != 24 ) and ( $product->category_id != 144 ) and ( $product->category_id != 145 ) and ( $product->category_id != 146 ) and ( $product->category_id != 148 ) and ( $product->category_id != 149 ) and ( $product->category_id != 150 ) and ( $product->category_id != 151 ) and ( $product->category_id != 152 )
								} //$coupons->name == "SBERBANK"
								if ( $coupons->name == "VAMOSI18G" ) {
									if ( ( $product->category_id != 4 ) and ( $product->category_id != 9 ) and ( $product->category_id != 12 ) and ( $product->category_id != 24 ) and ( $product->category_id != 144 ) and ( $product->category_id != 145 ) and ( $product->category_id != 146 ) and ( $product->category_id != 148 ) and ( $product->category_id != 149 ) and ( $product->category_id != 150 ) and ( $product->category_id != 151 ) and ( $product->category_id != 152 ) ) {
										continue;
									} //( $product->category_id != 4 ) and ( $product->category_id != 9 ) and ( $product->category_id != 12 ) and ( $product->category_id != 24 ) and ( $product->category_id != 144 ) and ( $product->category_id != 145 ) and ( $product->category_id != 146 ) and ( $product->category_id != 148 ) and ( $product->category_id != 149 ) and ( $product->category_id != 150 ) and ( $product->category_id != 151 ) and ( $product->category_id != 152 )
								} //$coupons->name == "VAMOSI18T"
								if ( $coupons->name == "DRBALM17" ) {
									if ( $product->category_id != 128 ) {
										continue;
									} //$product->category_id != 128
								} //$coupons->name == "DRBALM17"
								if ( $coupons->name == "HIGH17" ) {
									if ( $product->category_id != 138 ) {
										continue;
									} //$product->category_id != 138
								} //$coupons->name == "HIGH17"
								$a = $product->getDefaultPrice(); //199
								$b = $product->getDefaultFullPrice(); //289
								if ( is_array( $item[ 'attributes' ][ 'sizes' ] ) ) {
									$max = 'S';
									if ( count( $item[ 'attributes' ][ 'sizes' ] ) == 1 ) {
										foreach ( $item[ 'attributes' ][ 'sizes' ] as $size ) {
											if ( isset( $size->size_name ) ) {
												$max = $size->size_name;
											} //isset( $size->size_name )
											else {
												$max = $size;
											}
										} //$item[ 'attributes' ][ 'sizes' ] as $size
									} //count( $item[ 'attributes' ][ 'sizes' ] ) == 1
									else {
										foreach ( $item[ 'attributes' ][ 'sizes' ] as $size ) {
											if ( $max == 'S' and ( $size == 'M' or $size == 'L' or $size == 'XL' ) ) {
												$max = $size;
											} //$max == 'S' and ( $size == 'M' or $size == 'L' or $size == 'XL' )
											elseif ( $max == 'M' and ( $size == 'L' or $size == 'XL' ) ) {
												$max = $size;
											} //$max == 'M' and ( $size == 'L' or $size == 'XL' )
												elseif ( $max == 'L' and ( $size == 'XL' ) ) {
												$max = $size;
											} //$max == 'L' and ( $size == 'XL' )
												elseif ( $size == 'XL' ) {
												$max = $size;
											} //$size == 'XL'
										} //$item[ 'attributes' ][ 'sizes' ] as $size
									}
									$size = ProductSize::lang()->where( 'product_id', $item[ 'attributes' ][ 'product_id' ] )->where( 'size_name', $max )->first();
									if ( !empty( $size ) ) {
										$a        = $size->getPrice();
										$b        = $size->list_price;
										$discount = $discount + ( ( $b * ( $coupons->value / 100 ) ) - ( $b - $a ) ) * $item->quantity;
									} //!empty( $size )
									else {
										$discount = $discount + ( ( $b * ( $coupons->value / 100 ) ) - ( $b - $a ) ) * $item->quantity;
									}
								} //is_array( $item[ 'attributes' ][ 'sizes' ] )
								else {
									$discount = $discount + ( ( $b * ( $coupons->value / 100 ) ) - ( $b - $a ) ) * $item->quantity;
								}
							} //Cart::getContent() as $item
							if ( $discount < 0 )
								$discount = 0;
							return $discount;
						} //$coupons->type == 'basepercent'
						else {
							$discount = ( $total * ( $coupons->value / 100 ) );
							if ( $total < $discount ) {
								return $total;
							} //$total < $discount
							else {
								return $discount;
							}
						}
					}
				} //Coupons::runCheck( $cart_data[ 'coupon' ] )
				else {
					CartHelper::removeCoupon( Session::get( 'cart_data' ) );
				}
			} //isset( $cart_data[ 'coupon' ] ) and $cart_data[ 'coupon' ] != ''
		} //Session::has( 'cart_data' )
		return 0;
	}
	public static function getItemDiscount( $cartitem ) {
		if ( Session::has( 'cart_data' ) ) {
			$cart_data = Session::get( 'cart_data' );
			if ( isset( $cart_data[ 'coupon' ] ) and $cart_data[ 'coupon' ] != '' ) {
				if ( Coupons::runCheck( $cart_data[ 'coupon' ] ) ) {
					$coupons = Coupons::where( 'code', $cart_data[ 'coupon' ] )->first();
					$total   = ( Cart::getTotalForCoupon() );
					if ( $coupons->type == 'fix' ) {
						if ( $total < $coupons->value ) {
							return 0;
						} //$total < $coupons->value
						else {
							return 0;
						}
					} //$coupons->type == 'fix'
					else {
						if ( $coupons->type == 'basepercent' ) {
							//csak az alapbútorra érvényes engedmény
							$discount = 0;
							foreach ( Cart::getContent() as $item ) {
								if ( $cartitem != $item ) {
									continue;
								} //$cartitem != $item
								if ( $item->id == 'floor' ) {
									continue;
								} //$item->id == 'floor'
								$product = Product::lang()->active()->find( $item[ 'attributes' ]->product_id );
								if ( !isset( $product ) ) {
									continue;
								} //!isset( $product )
								/*
								if (isset($item['attributes']['parent_cartitem_id']) and ($item['attributes']['parent_cartitem_id'] != '0'))
								{
								continue;;
								}
								*/
								if ( $coupons->name == "SBERBANK" ) {
									if ( ( $product->category_id != 4 ) and ( $product->category_id != 9 ) and ( $product->category_id != 12 ) and ( $product->category_id != 24 ) and ( $product->category_id != 144 ) and ( $product->category_id != 145 ) and ( $product->category_id != 146 ) and ( $product->category_id != 148 ) and ( $product->category_id != 149 ) and ( $product->category_id != 150 ) and ( $product->category_id != 151 ) and ( $product->category_id != 152 ) ) {
										continue;
									} //( $product->category_id != 4 ) and ( $product->category_id != 9 ) and ( $product->category_id != 12 ) and ( $product->category_id != 24 ) and ( $product->category_id != 144 ) and ( $product->category_id != 145 ) and ( $product->category_id != 146 ) and ( $product->category_id != 148 ) and ( $product->category_id != 149 ) and ( $product->category_id != 150 ) and ( $product->category_id != 151 ) and ( $product->category_id != 152 )
								} //$coupons->name == "SBERBANK"
								if ( $coupons->name == "VAMOSI18G" ) {
									if ( ( $product->category_id != 4 ) and ( $product->category_id != 9 ) and ( $product->category_id != 12 ) and ( $product->category_id != 24 ) and ( $product->category_id != 144 ) and ( $product->category_id != 145 ) and ( $product->category_id != 146 ) and ( $product->category_id != 148 ) and ( $product->category_id != 149 ) and ( $product->category_id != 150 ) and ( $product->category_id != 151 ) and ( $product->category_id != 152 ) ) {
										continue;
									} //( $product->category_id != 4 ) and ( $product->category_id != 9 ) and ( $product->category_id != 12 ) and ( $product->category_id != 24 ) and ( $product->category_id != 144 ) and ( $product->category_id != 145 ) and ( $product->category_id != 146 ) and ( $product->category_id != 148 ) and ( $product->category_id != 149 ) and ( $product->category_id != 150 ) and ( $product->category_id != 151 ) and ( $product->category_id != 152 )
								} //$coupons->name == "VAMOSI18T"
								if ( $coupons->name == "DRBALM17" ) {
									if ( $product->category_id != 128 ) {
										continue;
									} //$product->category_id != 128
								} //$coupons->name == "DRBALM17"
								if ( $coupons->name == "HIGH17" ) {
									if ( $product->category_id != 138 ) {
										continue;
									} //$product->category_id != 138
								} //$coupons->name == "HIGH17"
								$a = $product->getDefaultPrice(); //199
								$b = $product->getDefaultFullPrice(); //289
								if ( is_array( $item[ 'attributes' ][ 'sizes' ] ) ) {
									$max = 'S';
									if ( count( $item[ 'attributes' ][ 'sizes' ] ) == 1 ) {
										foreach ( $item[ 'attributes' ][ 'sizes' ] as $size ) {
											if ( isset( $size->size_name ) ) {
												$max = $size->size_name;
											} //isset( $size->size_name )
											else {
												$max = $size;
											}
										} //$item[ 'attributes' ][ 'sizes' ] as $size
									} //count( $item[ 'attributes' ][ 'sizes' ] ) == 1
									else {
										foreach ( $item[ 'attributes' ][ 'sizes' ] as $size ) {
											if ( $max == 'S' and ( $size == 'M' or $size == 'L' or $size == 'XL' ) ) {
												$max = $size;
											} //$max == 'S' and ( $size == 'M' or $size == 'L' or $size == 'XL' )
											elseif ( $max == 'M' and ( $size == 'L' or $size == 'XL' ) ) {
												$max = $size;
											} //$max == 'M' and ( $size == 'L' or $size == 'XL' )
												elseif ( $max == 'L' and ( $size == 'XL' ) ) {
												$max = $size;
											} //$max == 'L' and ( $size == 'XL' )
												elseif ( $size == 'XL' ) {
												$max = $size;
											} //$size == 'XL'
										} //$item[ 'attributes' ][ 'sizes' ] as $size
									}
									$size = ProductSize::lang()->where( 'product_id', $item[ 'attributes' ][ 'product_id' ] )->where( 'size_name', $max )->first();
									if ( !empty( $size ) ) {
										$a        = $size->getPrice();
										$b        = $size->list_price;
										$discount = $discount + ( ( $b * ( $coupons->value / 100 ) ) - ( $b - $a ) ) * $item->quantity;
									} //!empty( $size )
									else {
										$discount = $discount + ( ( $b * ( $coupons->value / 100 ) ) - ( $b - $a ) ) * $item->quantity;
									}
								} //is_array( $item[ 'attributes' ][ 'sizes' ] )
								else {
									$discount = $discount + ( ( $b * ( $coupons->value / 100 ) ) - ( $b - $a ) ) * $item->quantity;
								}
							} //Cart::getContent() as $item
							if ( $discount < 0 )
								$discount = 0;
							return $discount;
						} //$coupons->type == 'basepercent'
						else {
							$discount = ( $total * ( $coupons->value / 100 ) );
							if ( $total < $discount ) {
								return 0;
							} //$total < $discount
							else {
								return $discount;
							}
						}
					}
				} //Coupons::runCheck( $cart_data[ 'coupon' ] )
				else {
					CartHelper::removeCoupon( Session::get( 'cart_data' ) );
				}
			} //isset( $cart_data[ 'coupon' ] ) and $cart_data[ 'coupon' ] != ''
		} //Session::has( 'cart_data' )
		return 0;
	}
	/**
	 * getTotal function.
	 * A teljes fizetendő, a szállítási költséggel együtt.
	 * @access public
	 * @return void
	 */
	public static function getCartTotal( $disc = true, $shipping = true ) {
		$total = Cart::getTotal();
		if ( $shipping ) {
			$cost = Cart::getShipping();
		} //$shipping
		else {
			$cost = 0;
		}
		if ( $disc == true ) {
			$discount = Cart::getDiscount();
		} //$disc == true
		else {
			$discount = 0;
		}
		return ( $total + $cost - $discount );
	}
	public static function countItems( ) {
		if ( !Cart::getContent() ) {
			return 0;
		} //!Cart::getContent()
		$count = 0;
		foreach ( Cart::getContent() as $item ) {
			if ( $item->id == 'floor' ) {
				continue;
			} //$item->id == 'floor'
			$product = Product::lang()->active()->find( $item[ 'attributes' ]->product_id );
			if ( !isset( $product ) ) {
				Cart::remove( $item[ 'id' ] );
				continue;
			} //!isset( $product )
			if ( $product->category_id == 119 ) {
				continue;
			} //$product->category_id == 119
			if //(isset($item['attributes']['related']) and ($item['attributes']['related'] != '0'))
				( isset( $item[ 'attributes' ][ 'parent_cartitem_id' ] ) and ( $item[ 'attributes' ][ 'parent_cartitem_id' ] != '0' ) ) {
				continue;
			} //isset( $item[ 'attributes' ][ 'parent_cartitem_id' ] ) and ( $item[ 'attributes' ][ 'parent_cartitem_id' ] != '0' )
			$count += $item->quantity;
		} //Cart::getContent() as $item
		return $count;
	}
	public static function countShippingItems( ) {
		if ( !Cart::getContent() ) {
			return 0;
		} //!Cart::getContent()
		$count = 0;
		foreach ( Cart::getContent() as $item ) {
			if ( $item->id == 'floor' ) {
				continue;
			} //$item->id == 'floor'
			$product = Product::lang()->active()->find( $item[ 'attributes' ]->product_id );
			if ( !isset( $product ) ) {
				continue;
			} //!isset( $product )
			if ( $product->ship_type == "mpl" ) {
				continue;
			} //$product->ship_type == "mpl"
			if ( $product->category_id == 119 ) {
				continue;
			} //$product->category_id == 119
			if ( isset( $item[ 'attributes' ][ 'parent_cartitem_id' ] ) and ( $item[ 'attributes' ][ 'parent_cartitem_id' ] != '0' ) ) {
				continue;
			} //isset( $item[ 'attributes' ][ 'parent_cartitem_id' ] ) and ( $item[ 'attributes' ][ 'parent_cartitem_id' ] != '0' )
			$count += $item->quantity;
		} //Cart::getContent() as $item
		return $count;
	}
	
	public static function countManufactureShippingItems( ) {
		if ( !Cart::getContent() ) {
			return 0;
		}
		$count = 0;
		foreach ( Cart::getContent() as $item ) {
			if ( $item->id == 'floor' ) {
				continue;
			}
			$product = Product::lang()->active()->find( $item[ 'attributes' ]->product_id );
			if ( !isset( $product ) ) {
				continue;
			}
			if ( $product->ship_type == "mpl" ) {
				continue;
			}
			if ( $product->category_id == 119 ) {
				continue;
			}
			if ( isset( $item[ 'attributes' ][ 'parent_cartitem_id' ] ) and ( $item[ 'attributes' ][ 'parent_cartitem_id' ] != '0' ) ) {
				continue;
			}
			if ((isset($product)) and (in_array($product->category_id, getConfig('manufacturecategory')))) $count += $item->quantity;
		}
		return $count;
	}
	
	public static function countOutletShippingItems( ) {
		if ( !Cart::getContent() ) {
			return 0;
		}
		$count = 0;
		foreach ( Cart::getContent() as $item ) {
			if ( $item->id == 'floor' ) {
				continue;
			}
			$product = Product::lang()->active()->find( $item[ 'attributes' ]->product_id );
			if ( !isset( $product ) ) {
				continue;
			}
			if ( $product->ship_type == "mpl" ) {
				continue;
			}
			if ( $product->category_id == 119 ) {
				continue;
			}
			if ( isset( $item[ 'attributes' ][ 'parent_cartitem_id' ] ) and ( $item[ 'attributes' ][ 'parent_cartitem_id' ] != '0' ) ) {
				continue;
			}
			if ((isset($product)) and (in_array($product->category_id, getConfig('outletcategory')))) $count += $item->quantity;
		}
		return $count;
	}
	
	public static function getProductIds( ) {
		if ( !Cart::getContent() ) {
			return array( );
		} //!Cart::getContent()
		$products = array( );
		foreach ( Cart::getContent() as $item ) {
			if ( $item->id == 'floor' ) {
				continue;
			} //$item->id == 'floor'
			$products[ $item[ 'attributes' ]->product_id ] = $item[ 'attributes' ]->product_id;
		} //Cart::getContent() as $item
		return $products;
	}
	/**
	 * getTotalForCoupon function.
	 * Amennyiben egy termékhez kuponok vannak rendelve,
	 * akkor a kedvezmény alapja a termékek összege.
	 * @access public
	 * @static
	 * @return void
	 */
	public static function getTotalForCoupon( ) {
		if ( !Cart::getContent() ) {
			return 0;
		} //!Cart::getContent()
		$cart_data = Session::get( 'cart_data' );
		$code      = '';
		if ( isset( $cart_data[ 'coupon' ] ) and $cart_data[ 'coupon' ] != '' ) {
			$code = $cart_data[ 'coupon' ];
		} //isset( $cart_data[ 'coupon' ] ) and $cart_data[ 'coupon' ] != ''
		$coupon_products = array( );
		$coupon_data     = Coupons::where( 'code', $code )->where( 'shop_id', getShopId() )->first();
		if ( !empty( $coupon_data ) ) {
			$products = CouponProducts::where( 'coupon_id', $coupon_data->coupon_id )->get();
			if ( sizeof( $products ) ) {
				foreach ( $products as $prod ) {
					$coupon_products[ $prod->product_id ] = $prod->product_id;
				} //$products as $prod
			} //sizeof( $products )
		} //!empty( $coupon_data )
		if ( !sizeof( $coupon_products ) ) {
			return ( Cart::getTotal() - CartHelper::getFloorCost( Session::get( 'cart_data' ) ) );
		} //!sizeof( $coupon_products )
		$products = Cart::getProductIds();
		$sum      = 0;
		foreach ( Cart::getContent() as $item ) {
			if ( $item->id == 'floor' ) {
				continue;
			} //$item->id == 'floor'
			if ( in_array( $item[ 'attributes' ]->product_id, $coupon_products ) ) {
				$sum += $item->getPriceSum();
			} //in_array( $item[ 'attributes' ]->product_id, $coupon_products )
		} //Cart::getContent() as $item
		return $sum;
	}
}