<?php
use App\Database\R_DB;

class Payment extends R_DB
{

    protected $table = 'payment_log';
    protected $primaryKey = 'payment_id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;

}
