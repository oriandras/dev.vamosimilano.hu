<?php

/**
 * Maileon class.
 * Dokumentáció: http://dev.maileon.com/api/rest-api-1-0/?lang=en
 */
class MaileonCore extends Maileon  {

    
   	
	
	public static function request($url, $api_key, $mode = 'GET', $return_type = 'xml', $data = '')
    {
		$headers = array(
			'Authorization: Basic '.base64_encode($api_key),
			'Expect:' 
		);

	    $url = 'https://api.maileon.com/1.0/'.$url;
	    
	    $curl = curl_init();	    
	    
	    switch ($mode):
			case 'POST':
				curl_setopt ($curl, CURLOPT_POST, true);		       	        
		        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); 	
		        $headers[] = 'Content-Type: application/json';
				$headers[] = 'Accept: application/json';			
		    break;
		    case 'POST2':
		    	
		        if($data){ 
			            
			        $view = array(
				        'standard_fields' => $data->standard_fields,
				        'custom_fields' => $data->custom_fields,
				        'email' => $data->email,
				        'external_id' => $data->uid,
			        );
			        
			        $xml = View::Make('maileon/put', $view)->render();
			       	 
			            
			        curl_setopt($curl, CURLOPT_POSTFIELDS, $xml );     
		        } 
		        
				$headers[] = 'Content-Type: application/vnd.maileon.api+xml';
				$headers[] = 'Accept: application/vnd.maileon.api+xml';	 
			break;
			case 'PUT':
		        if($data){       
			        $view = array(
				        'standard_fields' => $data->standard_fields,
				        'email' => $data->email,
				        'external_id' => $data->uid,
				        'custom_fields' => $data->custom_fields,
			        );
			        $xml = View::Make('maileon/put', $view)->render();	 
			            
			        curl_setopt($curl, CURLOPT_POSTFIELDS, $xml );     
		        } 
		        
				$headers[] = 'Content-Type: application/vnd.maileon.api+xml';
				$headers[] = 'Accept: application/vnd.maileon.api+xml';	 
			break;
			default:
	      		$headers[] = 'Content-Type: application/vnd.maileon.api+'.$return_type;
		  		$headers[] = 'Accept: application/vnd.maileon.api+'.$return_type;	      	
		  	break;
	    endswitch;
	    
	    $mode = ($mode == 'POST2' ? 'POST' : $mode);	    
	    
	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_FAILONERROR, true);	    
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($curl, CURLOPT_VERBOSE, 1);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 	        
	    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $mode);      

	    
	    $result = curl_exec($curl);   
	    $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	    curl_close($curl);

	    if($return_type == "xml"){
	    	$xml = simplexml_load_string($result);
			$json = json_encode($xml);
			$array = json_decode($json,TRUE);
			return $array;
		}

	    return json_decode($result); 
	}
	
	/**
	 * arrayToUrl function.
	 * Egy tömböt átalakít url paraméterekké
	 * 
	 * @access public
	 * @param mixed $array
	 * @return void
	 */
	public static function arrayToUrl($array){
		$tmp = array();
		foreach($array as $k=>$v){
			if(is_array($v)){
				foreach($v as $value){
					$tmp[] = $k."=".$value;
				}				
			}else{
				$tmp[] = $k."=".$v;
			}
			
		}
		return implode("&",$tmp);
	}

	
	/**
	 * __construct function.
	 * Indításkor a mailon beállítása
	 * 
	 * @access public
	 * @return void
	 */
	function __construct()
    {
        parent::__construct();        
    } 

}
