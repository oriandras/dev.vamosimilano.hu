<?php

$seo_title_global = '';
$seo_description_global = '';
$seo_og_image_global = '';

class Seo  {

    public static function setDefault(){
		global $seo_title_global, $seo_description_global, $seo_og_image_global;
		$seo_title_global = t('default_title', 'seo');
		$seo_description_global = t('default_description', 'seo');
		$seo_og_image_global = 'https://static.vamosimilano.hu/img/logo.png';
        //Session::put('seo_title', t('default_title', 'seo'));
        //Session::put('seo_description', t('default_description', 'seo'));
        //Session::put('og_image', 'https://static.vamosimilano.hu/img/logo.png');
    }

    public static function setTitle($title){
		global $seo_title_global;
        //Session::put('seo_title', $title);
		$seo_title_global = $title;
    }
	public static function getDescription(){
		global $seo_description_global;
		return $seo_description_global;
    	//return Session::get('seo_description');
	}
	public static function setDescription($text){
		global $seo_description_global;
		$seo_description_global = $text;
    	//return Session::put('seo_description', $text);
	}
	public static function getCanonical(){
    	return '';
	}

	public static function getType(){
    	//return Config::get('webshop.seo_type');
		return "website";
	}

	public static function getTitle(){
		global $seo_title_global;
    	//return Session::get('seo_title');;
		return $seo_title_global;
	}
    public static function setImage($image_url){
		global $seo_og_image_global;
		$seo_og_image_global = $image_url;
    	//Session::put('og_image', $image_url);
	}
	public static function getImage(){
		global $seo_og_image_global;
		//$og_url = Session::get('og_image');
		$og_url = $seo_og_image_global;
    	return str_replace('https://','http://',$og_url);
	}
	public static function getSSLImage(){
		global $seo_og_image_global;
		//$og_url = Session::get('og_image');
		$og_url = $seo_og_image_global;
    	return str_replace('http://','https://',$og_url);
	}

}
