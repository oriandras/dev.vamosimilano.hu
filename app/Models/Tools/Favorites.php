<?php
use App\Database\R_DB;

class Favorites extends R_DB
{

    protected $table = 'product_favorites';
    protected $primaryKey = 'favorite_id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;

    /**
     * getRowOptions function.
     * A serializált adatokat adja át.
     *
     * @access public
     * @return void
     */
    public function getRowOptions(){
        $options = unserialize($this->options);
        $return = [];
        if (sizeof($options)) {
            $return = $options;
        }else{
            $return = [
                'product_id' => 0,
                'sizes' => [],
                'type' => '',
                'mounting' => '',
				'seat' => '',
                'size_ids' => [],
                'textile' => [],
				'usedtextiles' => '',
				'backgroundid' => 0
            ];
        }
		if (!isset($return["image_url"])) {
			$return["image_url"] = "";
		}
		if (!isset($return["usedtextiles"])) {
			$return["usedtextiles"] = "";
		}
		if (!isset($return["backgroundid"])) {
			$return["backgroundid"] = 0;
		}
        return $return;
    }

    /**
     * setRowOptions function.
     * A bejövő input adatokat alakítja át
     *
     * @access public
     * @param mixed $input
     * @return void
     */
    public static function setRowOptions($input){

        if (!isset($input['product_id'])) {
            $input['product_id'] = 0;
        }
        if (!isset($input['sizes'])) {
            $input['sizes'] = [];
        }
        if (!isset($input['size_ids'])) {
            $input['size_ids'] = [];
        }
        if (!isset($input['mounting'])) {
            $input['mounting'] = '';
        }
		if (!isset($input['seat'])) {
            $input['seat'] = '';
        }
        if (!isset($input['textile'])) {
            $input['textile'] = [];
        }
        if (!isset($input['type'])) {
            $input['type'] = [];
        }
		if (!isset($input['image_url'])) {
            $input['image_url'] = '';
        }
		if (!isset($input['usedtextiles'])) {
            $input['usedtextiles'] = '';
        }
		if (!isset($input['backgroundid'])) {
            $input['backgroundid'] = 0;
        }

        $return = [
            'product_id' => $input['product_id'],
            'sizes' => $input['sizes'],
            'size_ids' => $input['size_ids'],
            'textile' => $input['textile'],
            'type' => $input['type'],
            'mounting' => $input['mounting'],
			'seat' => $input['seat'],
			'image_url' => $input['image_url'],
			'usedtextiles' => $input['usedtextiles'],
			'backgroundid' => $input['backgroundid']
        ];

        return $return;
    }
}
