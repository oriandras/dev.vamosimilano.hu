<?php

use App\Database\R_DB;

class Newsletter extends R_DB
{

    protected $table = 'newsletter';
    protected $primaryKey = 'newsletter_id';
    public $timestamps = true;
     use Illuminate\Database\Eloquent\SoftDeletes;

}
