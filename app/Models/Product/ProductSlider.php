<?php

use App\Database\R_DB;

class ProductSlider extends R_DB
{

    protected $table = 'product_sliders';
    protected $primaryKey = 'slider_id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;

     /**
	 * getImageUrl function.
	 * Kép méret
	 *
	 * @access public
	 * @return void
	 */
	function getImageUrl($size = 'default'){

        $sizes = Config::get('website.product_slider_sizes');
    	$url = Config::get('website.product_slider_image_url');
    	$path = Config::get('website.product_slider_image_path');
    	foreach($sizes as $width => $height){
        	if($size == 'default' or $size == $width){
            	if(!is_file($path.'/'.$width.'/'.$this->image_url)){
                	return get404Image($width, $height);
            	}
        	    return $url."/".$width."/".$this->image_url;
            }
    	}

	}


}
