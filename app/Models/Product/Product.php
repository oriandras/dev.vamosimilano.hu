<?php

use App\Database\R_DB;

class Product extends R_DB
{

    protected $table = 'products';
    protected $primaryKey = 'product_id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;

    /**
     * getUrl function.
     * Termék hivatkozása
     *
     * @access public
     * @return void
     */
    function getUrl(){
        $category = $this->getCategory();

        if (sizeof($category)) {
            return $category->getUrl($this->url);
        }else{
            return action('WebshopController@product', $this->url);
        }

    }

    function getUrlString(){
        $category = $this->getCategory();
        if (sizeof($category)) {
            return $category->getUrlString($this->url);
        }else{
            return $this->url;
        }

    }


    function getAddToCartUrl(){
        return $this->getUrl();
        $category = $this->getCategory();
        if (sizeof($category)) {
            return $category->getUrl($this->url.'/'.t('addtocart','url'));
        }else{
            return action('WebshopController@productAddToCart', $this->url);
        }

    }

    function getDefaultImageUrl($size = 'default') {

        if ($this->type == 'general') {
            $image = ProductGenImage::where('product_id', $this->product_id)
            ->where('visible', 'yes')
			->orderBy('image_id', 'ASC')
            ->first();
        }else{
            $image = ProductImage::where('product_id', $this->product_id)
            ->where('visible', 'yes')
            ->orderBy('num', 'ASC')
			->orderBy('image_id', 'ASC')
            ->first();
        }



        if (!empty($image)) {
            return $image->getImageUrl($size);
        }else{
            $sizes = Config::get('website.product_sizes');
            if ($size == 'default') {
                foreach ($sizes as $size_ => $size_m) {
                    $size = $size_;
                    continue;
                }
            }
            if ($size == 'full') {
                foreach ($sizes as $size_ => $size_m) {
                    $size = $size_;
                }
            }
            return get404Image($size, $sizes[$size]);
        }

    }

    function getPhotos($type = 'bottom') {
        return ProductPhoto::where('product_id', $this->product_id)
            ->where('visible', 'yes')
            ->where('type', $type)
            ->orderBy('num', 'ASC')
            ->get();
    }

    function getBannerImageUrl($size) {
        $image = ProductImage::where('product_id', $this->product_id)
            ->where('visible', 'yes')
            ->orderBy('num', 'ASC')
            ->first();

        if (!empty($image)) {
            return $image->getImageUrl($size);
        }else{
            $sizes = Config::get('website.product_sizes');
            return get404Image($size, $sizes[$size]);
        }
    }

    function getTextilePreviews($limit = 9999) {
        $product_id = $this->product_id;
        //return Cache::remember('textile_previews3'.$product_id, 15, function() use($product_id, $limit){

            return  \ProductImage::where('product_id', $product_id)
                ->leftjoin('product_textiles', 'product_textiles.textile_id', '=', 'product_images.textile1_id')
                ->leftjoin('product_textiles_lang', 'product_textiles.textile_id', '=', 'product_textiles_lang.textile_id')
                ->leftjoin('textile_extraprices_lang', 'textile_extraprices_lang.extraprice_category_id', '=', 'product_textiles.extraprice_category_id')
                ->select(['product_images.*', 'product_textiles.image', 'product_textiles_lang.url', 'product_textiles.textile_id', 'textile_extraprices_lang.extra_price'])
                ->where('product_images.visible', 'yes')
                ->where('product_textiles_lang.visible', 'yes')
                ->whereNull('product_textiles_lang.deleted_at')
                ->where('product_textiles_lang.shop_id',getShopId())
                ->orderBy('num', 'ASC')
				->orderBy('image_id', 'ASC')
                ->groupBy('product_images.textile1_id')
                ->groupBy('product_images.textile2_id')
                ->take($limit)
                ->get();
        //});
    }

    function getDescription(){
        $this->description = str_replace('<p>&nbsp;</p>', '', $this->description);
        if (trim($this->description) == '') {
            return $this->description;
        }
        if (!strstr('{', $this->description)){
            return $this->description;
        }
        $text = $this->description;
        $text = nl2br($text);
        $text = str_replace('{NL}', '<br>', $text);
        return $text;
    }


	/**
     * getName function.
     * Termék neve
     * @access public
     * @return void
     */
    function getName(){
        return $this->name;
    }

    /**
     * getSubTitle function.
     * Alcím megadása
     *
     * @access public
     * @return void
     */
    function getSubTitle(){
        return '';
    }

    /**
     * getCategory function.
     * Kategória adatai
     *
     * @access public
     * @return void
     */
    function getCategory(){
        return ProductCategory::lang()->where('product_categories.category_id', $this->category_id)->first();
    }

	function getParentCategoryAdminName(){
        if ($this->getCategory()->parent_id > 0) {
			return ProductCategory::find($this->getCategory()->parent_id)->admin_name."/";
		} else {
			return "";
		}
    }

    function scopeActive($query){
        return $query->where('visible', 'yes');
    }
    function scopeLang($query){
        return $query->join('products_lang', 'products_lang.product_id', '=', 'products.product_id')
            ->whereNull('products_lang.deleted_at')
            ->where('products_lang.shop_id', getShopId());
    }
	
	function scopeFavorit($query){
		$c = '';
		if (isset($_COOKIE["wishlist"])) $c = strval($_COOKIE["wishlist"]);
        return $query->join('product_favorites', 'product_favorites.product_id', '=', 'products.product_id')
            ->whereNull('product_favorites.deleted_at')
            ->where('product_favorites.uid', $c );
    }

    function getSizes($first = false){
        $size = ProductSize::lang()
            ->where('product_id', $this->product_id)
            ->orderBy('is_standard', 'DESC')
			->orderBy('size_x', 'ASC')
            ->orderBy('price', 'ASC');
            if ($first) {
                return $size->first();
            }
        return $size->get();
    }

    function getPrices(){
        if ($this->type == 'general') {
            return [];
        }
        return ProductSize::lang()
            ->where('product_id', $this->product_id)
            ->orderBy('price', 'ASC')
            ->get();
    }

    function getDefaultPrice(){
        if ($this->type == 'general') {
			return ($this->price ? $this->price : 0);
        }
        $price = ProductSize::lang()
            ->where('product_id', $this->product_id)
            ->orderBy('list_price', 'ASC')
            ->whereNull('deleted_at')
            ->first();
        if (!empty($price)) {
            return $price->getPrice();
        }
        return ($this->price ? $this->price : 0);
    }
    function getDefaultFullPrice(){
        if ($this->type == 'general') {
			return ($this->list_price ? $this->list_price : $this->price);
        }
        $price = ProductSize::lang()
            ->where('product_id', $this->product_id)
            ->orderBy('list_price', 'ASC')
            ->whereNull('deleted_at')
            ->first();
        if (!empty($price)) {
            return $price->list_price;
        }
        return ($this->list_price ? $this->list_price : $this->price);
    }

	function getPercentPrice(){
		if ($this->getDefaultFullPrice()>0)
			return round(($this->getDefaultFullPrice() - $this->getDefaultPrice())/$this->getDefaultFullPrice()*100,0);
		else {
			return 0;
		}
	}

    function getPartsCustom() {
            $parts = ProductPartSize::partData()
                ->where('product_id', $this->product_id)
                ->orderBy('num','ASC')
                ->get();

            if (sizeof($parts)){
                $return = [];
                foreach ($parts as $part) {
                    $return[$part->type][] = $part;
                }
                return $return;
            }
            return ['0' => []];
    }

    function getParts($random = false){
        if ($random == true) {

        }


        $parts = ProductPartSize::partData()
            ->where('type', 'half')
            ->where('product_id', $this->product_id)
            ->orderBy('num','ASC')
            ->get();

        if (sizeof($parts)){
            return $parts;
        }

        $part =  ProductPartSize::partData()
            ->where('type', 'full')
            ->where('product_id', $this->product_id)
            ->first();
        if (empty($part)) {
            return [];
        }
        return ['0' => $part];

    }
    function getAllParts(){
        return ProductPartSize::partData()
            ->where('type', 'random')
             ->orderBy('num','ASC')
            ->where('product_id', $this->product_id)
            ->get();

    }
	
	function getUsedParts(){
        return ProductPartSize::partData()
             ->orderBy('num','ASC')
            ->where('product_id', $this->product_id)
            ->get();

    }
	
    function getDefaultSize(){
        return $this->getSizes(true);
    }

    function getDefaultPricePlus($textile){
        return $textile->extra_price;
    }

    public static function getBestellers($limit = 4){
        $now = new DateTime();
        $bestellers = $products = [];
        $products_order = Order::where('shop_id', getShopId())
            ->join('order_item','order_item.order_id','=','order.order_id')

            ->select(['product_id',DB::raw('count(product_id) as number')])
            ->where('created_at','>',$now->modify('-180 day')->format('Y-m-d H:i:s'))
            ->groupBy('product_id')
            ->orderBy('created_at','DESC')
            ->get();

        if (sizeof($products_order)) {
            $ids = [];
            foreach ($products_order as $item) {
                $ids[] = $item->product_id;
            }
            if ((sizeof($ids)*4) > $limit) {
                $products = Product::active()->lang()->where('type','scalable')
                    ->where('plannable', 'yes')
                    ->whereIn('products.product_id', $ids)
                    ->take($limit)->get();
            }
        }

        if(sizeof($products) < $limit){
            $products = Product::active()->lang()->where('type','scalable')
                ->where('plannable', 'yes')
                ->orderBy(DB::raw('rand()'))->take($limit)->get();
        }
        return $products;


    }

    public static function getPartSizes($product_id){
        $part_sizes = ProductPartSize::
            join('product_parts','product_parts.part_id','=','product_part_sizes.part_id')
            ->select('product_part_sizes.*','product_parts.admin_name')
            ->where('product_id', $product_id)
            ->get();
        $return = [];
        foreach ($part_sizes as $part) {
            $tmp = [
                'S' => $part->s,
                'M' => $part->m,
                'L' => $part->l,
                'XL' => $part->xl,
            ];
            $return[$part->admin_name] = $tmp;
        }
        return $return;
    }
    /**
     * getRelations function.
     * Kapcsolódó termékek
     *
     * @access public
     * @return void
     */
    function getRelations() {
		$result = [];
        $relations =  ProductRelated::lang()->where('product_related.product_id', $this->product_id)
			->leftjoin('products','products.product_id','=','product_related.rel_id')
            ->select('product_related.*','products.global_name')
			->orderBy('product_related.rel_size_id','ASC')
            ->get();
		//die(var_export($relations,true));
        if (sizeof($relations)) {
            foreach ($relations as $related) {
                $product = Product::lang()->where('products.product_id', $related->rel_id)->first();
				if ($product->category_id != Config::get('website.garancia_category')) {
					$size = ProductSize::find($related->rel_size_id);
					$related->size = $size;
					$related->product = $product;
					$result[] = $related;
				}
            }
        }
        return $result;
    }
	
	function getGarancia() {
		$result = [];
        $relations =  ProductRelated::lang()->where('product_related.product_id', $this->product_id)
			->leftjoin('products','products.product_id','=','product_related.rel_id')
            ->select('product_related.*','products.global_name')
			->orderBy('product_related.rel_size_id','ASC')
            ->get();
		//die(var_export($relations,true));
        if (sizeof($relations)) {
            foreach ($relations as $related) {
                $product = Product::lang()->where('products.product_id', $related->rel_id)->first();
				if ($product->category_id == Config::get('website.garancia_category')) {
					$size = ProductSize::find($related->rel_size_id);
					$related->size = $size;
					$related->product = $product;
					$result[] = $related;
				}
            }
        }
        return $result;
    }

	function isOutlet() {
		$outlet = ProductCategory::where('category_id',$this->category_id)->first();
		//hozzáadva új feltétel, a 156-os kategória is outletként kell hogy viselkedjen
		if (($outlet->parent_id == 131) or ($outlet->category_id == 131) or ($outlet->category_id == 156)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * getPrimaryCategory function.
	 * Visszaadja a főketóriát és egy kategória prefixet.
	 *
	 * @access public
	 * @param mixed $sizes
	 * @return void
	 */
	function getPrimaryCategory($sizes) {
    	$kategoria = 'egyenes';
        $kategoria_prefix = 'e';
        $pages = 1;

        if($this->type == 'scalable' and $this->plannable == 'yes'){
            foreach ($sizes as $size) {
        	    if ($size->bsize_y > 0 and $pages == 1){
    				$pages = 2;
    			}
    			if ($size->csize_y > 0 and $pages == 2){
    		    	$pages = 3;
                }

                break;
            }
    		$pages_s = ['1' => 'A', '2' => 'B', '3' => 'C'];
            if($pages == 2) {
    			$kategoria = "l-alaku-kanape";
                $kategoria_prefix = 'l';
            } elseif($pages == 3) {
        		$kategoria = "u-alaku-kanape";
    			$kategoria_prefix = 'u';
			}
        }
        //puffi
        if (in_array($this->category_id, [22, 122])) {
            $kategoria = 'puff';
            $kategoria_prefix = 'p';
        }
        //fotel
        if (in_array($this->category_id, [10, 110])) {
            $kategoria = 'fotel';
            $kategoria_prefix = 'f';
        }
        //fr ágy
        if (in_array($this->category_id, [11, 9])) {
            $kategoria = 'francia';
            $kategoria_prefix = 'fr';
        }
        return array('kategoria' => $kategoria, 'kategoria_prefix' => $kategoria_prefix, 'pages' => $pages);
	}
	
	function getFavoritesCount(){
        return Favorites::where('product_id', $this->product_id)->count();
    }
}
