<?php
use App\Database\R_DB;
class ProductTextile extends R_DB
{
    protected $table = 'product_textiles';
    protected $primaryKey = 'textile_id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;

    public static function getDefaultImageUrl() {
        return get404Image(400, 200);
    }	
    public static function getImageUrl($textile_id, $width = 400){		$textile = Cache::remember('textile_cache_'.$textile_id, 15, function() use($textile_id)
        {
            return \ProductTextile::find($textile_id);
        });				if (empty($textile)) {
            return get404Image(40, 20);
        }				if ($textile->colorcode == "") {			if (is_file(Config::get('website.textiles_image_path')."/".$textile->image)){				$image=imagecreatefromjpeg(Config::get('website.textiles_image_path')."/".$textile->image);				$thumb=imagecreatetruecolor(1,1); imagecopyresampled($thumb,$image,0,0,0,0,1,1,imagesx($image),imagesy($image));				$mainColor=strtoupper(dechex(imagecolorat($thumb,0,0)));				$textile->colorcode = $mainColor;				$textile->save();			}		}
        if ($width == 255) {			if (!is_file(Config::get('website.textiles_image_path')."/255/".$textile->image)) {
                if (is_file(Config::get('website.textiles_image_path')."/".$textile->image)){
    					$new_image = Image::make(Config::get('website.textiles_image_path')."/".$textile->image);
    					$new_image->resize(255,127);
    					$new_image->save(Config::get('website.textiles_image_path')."/255/".$textile->image);
    					return Config::get('website.textiles_image_url')."/255/".$textile->image;
    			}else{
    				return get404Image(255, 127);
    			}
            }else {				
                return Config::get('website.textiles_image_url')."/255/".$textile->image;
            }
		} elseif (!is_file(Config::get('website.textiles_image_path')."/thumb/".$textile->image)) {
			if (is_file(Config::get('website.textiles_image_path')."/".$textile->image)){
				if ($width == 40) {
					$new_image = Image::make(Config::get('website.textiles_image_path')."/".$textile->image);
					$new_image->resize(40,20);
					$new_image->save(Config::get('website.textiles_image_path')."/thumb/".$textile->image);
					return Config::get('website.textiles_image_url')."/thumb/".$textile->image;
				}
				return Config::get('website.textiles_image_url')."/".$textile->image;
			}else{
				return get404Image(40, 20);
			}
		} else {
			if ($width == 40) {
				return Config::get('website.textiles_image_url')."/thumb/".$textile->image;
			}elseif ($width == 255) {
				return Config::get('website.textiles_image_url')."/thumb/".$textile->image;
			} else {
				return Config::get('website.textiles_image_url')."/".$textile->image;
			}
		}
    }
		public static function getTextileName($textile_id){
		$t = ProductTextile::find($textile_id);
		if (isset($t)) {
			return ProductTextile::find($textile_id)->global_name;
		} else {
			return t('Ismeretlen');
		}
    }
    	public function scopeLang($query){
        return $query->join('product_textiles_lang', 'product_textiles_lang.textile_id', '=', 'product_textiles.textile_id')
        ->whereNull('product_textiles_lang.deleted_at')
        ->where('product_textiles_lang.shop_id', getShopId());
    }
    	public function scopeExtraPrice($query){
        return $query->join('textile_extraprices_lang', 'textile_extraprices_lang.extraprice_category_id', '=', 'product_textiles.extraprice_category_id')
        ->whereNull('textile_extraprices_lang.deleted_at')
        ->where('textile_extraprices_lang.shop_id', getShopId());
    }	
    public function scopeActive($query) {
        return $query->where('visible', 'yes');
    }	
    public static function getAll($paginate = false, $where = [], $orderField = 'product_textiles_lang.name', $orderOrder='ASC') {
		if (!isset($orderField)) $orderField='product_textiles_lang.name';
		if (!isset($orderOrder)) $orderOrder='ASC';
        if (empty($where)) {
            $textiles = Cache::remember('all_textiles_v5_'.($paginate ? Input::get('page') : '__1'), 15, function() use($paginate,$orderField,$orderOrder)
            {
            $t =  ProductTextile::lang()
                ->join('textile_extraprices_lang', 'textile_extraprices_lang.extraprice_category_id', '=', 'product_textiles.extraprice_category_id')
                ->join('textile_categories_lang', 'textile_categories_lang.category_id', '=', 'product_textiles.textile_category_id')
                ->where('image','!=','')
                ->where('product_textiles_lang.visible','!=','no')
				->orderBy($orderField,$orderOrder)
                //->orderBy('product_textiles_lang.name','ASC')
				->orderByRaw("$orderField $orderOrder, product_textiles_lang.name ASC")
                ->where('textile_extraprices_lang.shop_id', getShopId())
                ->where('textile_categories_lang.shop_id', getShopId())
                ->select('product_textiles_lang.*',
                        'textile_extraprices_lang.extra_price',
                        'product_textiles.on_egesz as on_butor',
                        'product_textiles.on_also',
                        'product_textiles.on_felso',
                        'product_textiles.on_parna',
						'product_textiles.on_elemes',
                        'product_textiles.just_pillow',
                        'product_textiles.textile_category_id',
                        'product_textiles.extraprice_category_id',
						'product_textiles.textile_type_id',
						'product_textiles.textile_pattern_id',
						'product_textiles.zoomx',
						'product_textiles.zoomy',
                        'textile_categories_lang.name as textile_catname'
                        );
                if ($paginate != false) {
                    return $t->paginate(10);
                }
                return $t->get();
            });
        }else{
                $t =  ProductTextile::lang()
                ->join('textile_extraprices_lang', 'textile_extraprices_lang.extraprice_category_id', '=', 'product_textiles.extraprice_category_id')
                ->join('textile_categories_lang', 'textile_categories_lang.category_id', '=', 'product_textiles.textile_category_id')
                ->where('image','!=','')
                ->where('product_textiles_lang.visible','!=','no')
                //->orderBy($orderField,$orderOrder)
				->orderByRaw("$orderField $orderOrder, product_textiles_lang.name ASC")
                ->where('textile_extraprices_lang.shop_id', getShopId())
                ->where('textile_categories_lang.shop_id', getShopId())
                ->select('product_textiles_lang.*',
                        'textile_extraprices_lang.extra_price',
                        'product_textiles.on_egesz as on_butor',
                        'product_textiles.on_also',
                        'product_textiles.on_felso',
                        'product_textiles.on_parna',
						'product_textiles.on_elemes',
                        'product_textiles.just_pillow',
                        'product_textiles.textile_category_id',
                        'product_textiles.extraprice_category_id',
						'product_textiles.textile_type_id',
						'product_textiles.textile_pattern_id',
						'product_textiles.zoomx',
						'product_textiles.zoomy',
                        'textile_categories_lang.name as textile_catname'
                        );
                if ($where) {
                    $t->where(function($query) use ($where){
                        foreach ($where as $operator => $where_items){
                            if (is_array($where_items)) {
                                foreach ($where_items as $field => $string){
                                    if ($operator == 'or'){
                                        $query->orWhere($field, $string);
                                    }else{
                                        $query->Where($field, $string);
                                    }
                                }
                            }
                        }
                        return $query;
                    });
                    foreach ($where as $k => $v) {
                        if (!is_array($v)) {
                            $t->where($k, $v);
                        }
                    }
                }
                if ($paginate != false) {
                    return $t->paginate(10);
                }
                return $t->get();
        }
        return $textiles;
    }
}
