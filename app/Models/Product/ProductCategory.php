<?php

use App\Database\R_DB;

class ProductCategory extends R_DB
{

    protected $table = 'product_categories';
    protected $primaryKey = 'category_id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;

    public function scopeLang($query) {
        return $query->join('product_categories_lang','product_categories_lang.category_id','=','product_categories.category_id')
            ->whereNull('product_categories_lang.deleted_at')
            ->where('product_categories_lang.shop_id', getShopId());
    }
    public function scopeActive($query) {
        return $query->where('product_categories_lang.visible','yes')->orderBy('num', 'ASC');
    }


    public static function getMenu($parent_id = 0){
        $categories = self::where('parent_id' , $parent_id)
            ->lang()
            ->active()
            ->orderBy('num', 'ASC')
            ->get();

        if (!sizeof($categories)) {
            return [];
        }

        foreach ($categories as $category) {
            $return[$category->admin_name]['data'] = $category;
            $child = self::getMenu($category->category_id);
            if (sizeof($child)) {
                $return[$category->admin_name]['child'] = $child;
            }
        }
        return $return;
    }

    public static function menuHighLight(){
        return self::where('menu_top' , 'yes')
            ->lang()
            ->active()
            ->orderBy('num', 'ASC')
            ->get();
    }

    /**
	 * getUrl function.
	 * Categória URL-je
	 *
	 * @access public
	 * @return void
	 */
	function getUrl($end_string = ''){

    	$url = $this->getUrlString();

    	return action('WebshopController@product', $url).($end_string ? '/'.$end_string : '');
	}

	function getUrlString($end_string = ''){
    	return $this->recursiveUrl().($end_string ? '/'.$end_string : '');
    }


	function recursiveUrl(){
    	if ($this->parent_id) {

        	$category = self::where('product_categories.category_id', $this->parent_id)
        	    ->lang()
        	    ->first();
        	if (empty($category)){
            	return $this->category_url;
        	}
        	return $category->recursiveUrl()."/".$this->category_url;
    	}
    	return $this->category_url;
	}

	/**
	 * getImageUrl function.
	 * Kép méret
	 *
	 * @access public
	 * @return void
	 */
	function getImageUrl($size = 'default'){
        	$sizes = Config::get('website.image_sizes');
    	$url = Config::get('website.category_image_url');
    	foreach($sizes as $width => $height){
        	if($size == 'default' or $size == $width){
            	if($this->image_url == ''){
                	return get404Image($width,$height);
            	}
        	    return $url."/".$width."/".$this->image_url;
            }
    	}

	}
	/**
	 * getBannerImageUrl function.
	 * banner méret
	 *
	 * @access public
	 * @return void
	 */
	function getBannerImageUrl($size = 'default'){

        $sizes = Config::get('website.banner_sizes');
    	$url = Config::get('website.banner_image_url');
    	foreach($sizes as $width => $height){
        	if($size == 'default' or $size == $width){
            	if($this->banner_url == ''){
                	return get404Image($width,$height);
            	}
        	    return $url."/".$width."/".$this->banner_url;
            }
    	}

	}
	/**
	 * getBreadcrumbs function.
	 * Webmorzsa generálása
	 *
	 * @access public
	 * @return void
	 */
	function getBreadcrumbs(){
    	$array = [];
    	$array =  $this->getRecursiveBreadcrumbs($array);
    	$current = [$this->getUrl() => $this->name];

    	return array_merge($array, $current);
    }
	/**
	 * getRecursiveBreadcrumbs function.
	 * Többszintű kategóriához recusive hívás.
	 *
	 * @access public
	 * @param mixed $array
	 * @return void
	 */
	function getRecursiveBreadcrumbs($array){
    	if ($this->parent_id) {
        	$category = self::lang()->active()->where('product_categories.category_id',$this->parent_id)->first();

        	if (empty($category)){
            	$array[$this->getUrl()] = $this->name;
                return $array;
        	}

        	$array[$category->getUrl()] = $category->name;
        	return $category->getRecursiveBreadcrumbs($array);
    	}
    	$array[$this->getUrl()] = $this->name;
        return $array;
	}





}
