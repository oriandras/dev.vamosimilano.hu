<?php

use App\Database\R_DB;

class ProductRelated extends R_DB
{

    protected $table = 'product_related';
    protected $primaryKey = 'related_id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;
    public static $rules = array(

        'rel_id'=>'required',
        'price'=>'required',
        'show_price'=>'required',


    );

    public static function getFieldNames()
    {
        return array(

            'rel_id' => t('Kapcsolódó termék'),
            'price' => t('Végleges ár'),
            'show_price' => t('Megjelenített ár'),
        );
    }

    public function scopeLang($query) {
        return $query->where('shop_id', getShopId());
    }

    public static function renderToCart($related_id) {
        $relation =  ProductRelated::lang()->where('related_id', $related_id)->first();

        if (!empty($relation)) {
            if ($relation->rel_id == -1) {
                return ($relation->name ? $relation->name : t('Kiegészítő nélkül kéri'));
            }
            $product = Product::lang()->where('products.product_id', $relation->rel_id)->first();
            $size = ProductSize::find($relation->rel_size_id);
			if (isset($related->size)) {
				return $product->name." - ".$size->size_name." - ".ProductSize::getSizeString($relation->rel_id, 'a', $size->size_name, false, true);
			} else {
				return $product->name;
			}
        }
        return '';
    }

}
