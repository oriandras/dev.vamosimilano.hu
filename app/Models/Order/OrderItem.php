<?php

use App\Database\R_DB;

class OrderItem extends R_DB  {

	protected $table = 'order_item';
	protected $primaryKey = 'order_item_id';
    public $timestamps = false;

	function __construct()
    {
        parent::__construct();
    }

    function getSizeString(){
        $feature = ProductSize::find($this->feature_id);
        return $feature->size_name.' - '.$feature->size_x.($feature->size_y ? "x".$feature->size_y : '').($feature->size_z ? "x".$feature->size_z : '');

    }



}
