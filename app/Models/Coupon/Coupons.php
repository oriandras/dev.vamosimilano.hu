<?php
use App\Database\R_DB;

class Coupons extends R_DB
{

    protected $table = 'coupons';
    protected $primaryKey = 'coupon_id';
    public $timestamps = true;
    use Illuminate\Database\Eloquent\SoftDeletes;

    public function validString(){
        $result = 'OK';

        $product = CouponProducts::where('coupon_id', $this->coupon_id)->get();

        if (sizeof($product)) {
            $cart_products = Cart::getProductIds();
            if (sizeof($cart_products)) {
                $tmp_ok = false;
                foreach ($product as $product_data) {
                    if (in_array($product_data->product_id, $cart_products)) {
                        $tmp_ok = true;
                    }
                }
                if ($tmp_ok == false) {
                    return 'PRODUCT';
                }
            }else{
                return 'PRODUCT';
            }
        }


        /*if (!Auth::user() and $this->quantity > 0) {
            return 'LOGIN';
        }*/

        $now = new DateTime();
        if ($this->valid_to < $now->format('Y-m-d H:i:s')){
            return 'DATE';
        }
        if ($this->valid_from > $now->format('Y-m-d H:i:s')){
            return 'DATE';
        }



        if ($this->quantity > 0) {
            $orders = Order::where('uid', getUid())
                ->where('coupon_code', $this->code)
                ->count();
            if ($orders >= $this->quantity) {
                return 'LIMIT';
            }
        }

        return $result;
    }
    /**
     * runCheck function.
     * Gyors ellenőrzés
     *
     * @access public
     * @static
     * @param mixed $code
     * @return void
     */
    public static function runCheck($code) {
        $coupon = self::where('code', $code)
            ->where('shop_id', getShopId())
            ->first();
        if (sizeof($coupon)) {
            return ($coupon->validString() == 'OK' ? true : false);
        }
        return false;
    }

    public static function validateCode($code) {
        $coupon = self::where('code', $code)
            ->where('shop_id', getShopId())
            ->first();
        if (sizeof($coupon)) {
            return $coupon->validString();
        }else{
            return 'NOTFOUND';
        }
    }

}
