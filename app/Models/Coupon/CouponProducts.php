<?php
use App\Database\R_DB;

class CouponProducts extends R_DB
{

    protected $table = 'coupons_products';
    protected $primaryKey = 'conn_id';
    public $timestamps = false;

}
