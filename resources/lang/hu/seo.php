<?php

return [
    'home_title' => 'Prémium bútorok',
    'default_title' => 'Modern, olasz formatervezés',
    'default_description' => 'Vamosi Milano, modern, olasz formatervezési hagyományok alapján itthon, kézzel készít design kanapékat és franciaágyakat. 2013 novemberétől nem csak a külföldi piacra exportálunk bútorokat, hanem lehetővé tettük magas minőségű bútoraink hazai értékesítését is.',

];
