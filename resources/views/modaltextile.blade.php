@if ($product->type != 'general' and sizeof($parts))
	@foreach ($custom_parts as $part_key => $parts_data)
		@foreach ($parts_data as $i => $part)
			@if ($part->admin_name==$partname)
				@include('webshop.block.product_textiles_modal', ['part' => $part, 'type' => $part->admin_name,'all_textiles' => $all_textiles])
			@endif
		@endforeach
	@endforeach
@endif