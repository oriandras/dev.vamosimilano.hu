<?php
$headversion = "newhead";
?>
@extends('layouts.default')
@section('content')
<?php
if( isset($headversion) && $headversion === "newhead" ) {
?>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-lg-6 mx-auto py-5">
			<form id="login-form" method="post" action="{{ action('AuthController@loginSubmit') }}" class="form">
			<div class="card">
				<div class="card-body pb-0">
					<h1 class="h4-responsive title-center text-uppercase">{{ t('Új felhasználó létrehozása') }}</h1>
					<p class="text-center text-uppercase pt-2"><strong>{{ t('Személyes adatok') }}</strong></p>
					<div class="row">
						<div class="col-md-6">
							<div class="md-form md-outline my-0">
								<input type="text" class="form-control" value="{{ (Input::old('lastname') ? Input::old('lastname') : '') }}" name="lastname" id="lastname" required autocomplete="off">
								<label for="lastname">{{ t('Vezetéknév') }}<strong class="ml-1">*</strong></label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="md-form md-outline mt-0">
								<input type="text" class="form-control" value="{{ (Input::old('firstname') ? Input::old('firstname') : '') }}" name="firstname" id="firstname" required autocomplete="off">
								<label for="firstname">{{ t('Keresztnév') }}<strong class="ml-1">*</strong></label>
							</div>
						</div>
					</div>
					<p class="text-center text-uppercase pt-2"><strong>{{ t('Belépéshez szükséges adatok') }}</strong></p>
					<div class="md-form md-outline">
						<input type="email" class="form-control" id="email_address" autocomplete="off" value="{{( Input::old('email') ? Input::old('email') : '')}}" name="email" required>
						<label for="email_address">{{ t('Email') }}<strong class="ml-1">*</strong></label>
					</div>
					<div class="md-form md-outline">
						<input type="password" class="form-control" id="password" name="password" required>
						<label for="password">{{ t('Jelszó') }}<strong class="ml-1">*</strong></label>
					</div>
					<div class="md-form md-outline">
						<input type="password" class="form-control" id="password-confirmation" name="password_confirmation" required>
						<label for="password-confirmation">{{ t('Jelszó újra') }}<strong class="ml-1">*</strong></label>
					</div>
					<div class="md-form">
						<div class="row">
							<div class="col-md-6 text-center text-md-left">
								<a href="{{ action('AuthController@login') }}" class="btn btn-flat">{{ t('Vissza a belépéshez') }}</a>
							</div>
							<div class="col-md-6 text-center text-md-right">
								<button type="submit" class="btn btn-primary">{{ t('Regisztráció') }}</button>
							</div>
						</div>
					</div>
				</div>
			</div>
				{{ csrf_field() }}
			</form>
		</div>
	</div>
</div>
<?php
}
else {
?>

<main class="page-main" id="maincontent"><a tabindex="-1" id="contentarea"></a>
<div class="page-title-wrapper">
    <h1 class="page-title">
        <span data-ui-id="page-title-wrapper" class="base">{{ t('Új felhasználó létrehozása') }}</span>    </h1>
    </div>
<div class="columns">
<div class="column main">



<form autocomplete="off" enctype="multipart/form-data" id="form-validate" method="post" action="{{ action('AuthController@regSubmit') }}" class="form create account form-create-account">
    <fieldset class="fieldset create info">
        <legend class="legend"><span>{{ t('Személyes adatok') }}</span></legend><br>




            <div class="field field-name-lastname required">
            <label for="lastname" class="label">
                <span>{{ t('Vezetéknév') }}</span>
            </label>

            <div class="control">
                <input type="text"class="input-text required-entry"  required="" value="{{ (Input::old('lastname') ? Input::old('lastname') : '') }}" name="lastname" id="lastname" autocomplete="off" aria-required="true">
            </div>
        </div>
        <div class="field field-name-firstname required">
            <label for="firstname" class="label">
                <span>{{ t('Keresztnév') }}</span>
            </label>

            <div class="control">
                <input type="text" class="input-text required-entry" required=""  name="firstname" id="firstname" value="{{ (Input::old('firstname') ? Input::old('firstname') : '') }}" autocomplete="off"  aria-required="true">
            </div>
        </div>
                    </fieldset>
        <fieldset  class="fieldset create account">
        <legend class="legend"><span>{{ t('Belépéshez szükséges adatok') }}</span></legend><br>
        <div class="field required">
            <label class="label" for="email_address"><span>{{ t('Email') }}</span></label>
            <div class="control">
                <input type="email"  class="input-text" equired=""  value="{{ (Input::old('email') ? Input::old('email') : '') }}" id="email_address" autocomplete="email" name="email" aria-required="true">
            </div>
        </div>
        <div class="field password required">
            <label class="label" for="password"><span>{{ t('Jelszó') }}</span></label>
            <div class="control">
                <input type="password" autocomplete="off" required=""  class="input-text"  id="password" name="password"  aria-required="true">
            </div>

        </div>
        <div class="field confirmation required">
            <label class="label" for="password-confirmation"><span>{{ t('Jelszó újra') }}</span></label>
            <div class="control">
                <input type="password" autocomplete="off" required="" class="input-text" id="password-confirmation"  name="password_confirmation"  aria-required="true">
            </div>
        </div>
            </fieldset>
    <div class="actions-toolbar">
        <div class="primary">
            <button  class="action submit primary" type="submit"><span>{{ t('Regisztráció') }}</span></button>
        </div>
        <div class="secondary">
            <a href="{{ action('AuthController@login') }}" class="action back" style="display: block;"><span>{{ t('Vissza a belépéshez') }}</span></a>
        </div>
    </div>
    {{ csrf_field() }}
</form>

</div></div></main>
@section('footer_js')
@append

<?php
}
?>
@stop