<?php
$headversion = "newhead";
?>
@extends('layouts.default')
@section('content')
<?php
if( isset($headversion) && $headversion === "newhead" ) {
?>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-lg-6 mx-auto py-5">
			<form id="login-form" method="post" action="{{ action('PasswordController@postReset') }}" class="form">
			<div class="card">
				<div class="card-body pb-0">
					<h1 class="h4-responsive title-center text-uppercase">{{ t('Új jelszó megadása') }}</h1>
					<div class="md-form md-outline">
						<input type="email" class="form-control" id="email_address" autocomplete="off" value="{{( Input::old('email') ? Input::old('email') : '')}}" name="email" required>
						<label for="email_address">{{ t('Email') }}<strong class="ml-1">*</strong></label>
					</div>
					<div class="md-form md-outline">
						<input type="password" class="form-control" id="password" name="password" required>
						<label for="password">{{ t('new_password') }}<strong class="ml-1">*</strong></label>
					</div>
					<div class="md-form md-outline">
						<input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
						<label for="password_confirmation">{{ t('new_password_confirmation') }}<strong class="ml-1">*</strong></label>
					</div>
					<div class="md-form">
						<div class="row">
							<div class="col-md-12 text-center">
								<button type="submit" class="btn btn-primary">{{ t('Új jelszó mentése') }}</button>
							</div>
							<div class="col-md-12 text-center">
								<a href="{{ action('AuthController@login') }}" class="btn btn-flat">{{ t('Eszembe jutott a jelszavam') }}</a>
							</div>
						</div>
					</div>
				</div>
			</div>
				{{ csrf_field() }}
			</form>
		</div>
	</div>
</div>
<?php
}
else {
?>
<main class="page-main" id="maincontent">
<div class="page-title-wrapper">
    <h1 class="page-title">
    <span data-ui-id="page-title-wrapper" class="base">{{ t('Új jelszó megadása') }}</span>    </h1>
</div>


<div class="columns">
    <div class="column main">
        <form id="form-validate" method="post" action="{{ action('PasswordController@postReset') }}" class="form password forget" >
            <fieldset  class="fieldset">
                <div class="field note">{{ t('Adja meg e-mail címét, majd új jelszavát!') }}</div>
                <div class="field email required">
                    <label class="label" for="email_address"><span>{{ t('E-mail') }}</span></label>
                    <div class="control">
                        <input type="email"  value="{{ (Input::old('email') ? Input::old('email') : '') }}" class="input-text" required="" id="email_address" name="email" aria-required="true">
                    </div>
                </div>
                <div class="field email required">
                    <label class="label" for="password"><span>{{ t('new_password') }}</span></label>
                    <div class="control">
                        <input type="password"  value="" class="input-text" required="" id="password" alt="email" name="password" aria-required="true">
                    </div>
                </div>
                <div class="field email required">
                    <label class="label" for="password_confirmation"><span>{{ t('new_password_confirmation') }}</span></label>
                    <div class="control">
                        <input type="password"  value="" class="input-text" required="" id="password_confirmation" name="password_confirmation" aria-required="true">
                    </div>
                </div>

                    </fieldset>
            <div class="actions-toolbar">
                <div class="primary">
                    <button class="action submit primary" type="submit"><span>{{ t('Új jelszó mentése') }}</span></button>
                </div>
                <div class="secondary">
                    <a href="{{ action('AuthController@login') }}" class="action back" style="display: block;"><span>{{ t('Eszembe jutott a jelszavam') }}</span></a>
                </div>
            </div>
            {{ csrf_field() }}
			    		<input type="hidden" name="token" value="{{ $token }}">
        </form>
    </div>
</div>

</main>
@section('footer_js')
@append

<?php
}
?>
@stop