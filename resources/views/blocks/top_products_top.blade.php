@if (sizeof($designerbottom))
<div class="single-images border-radius">
	<div class="container">
		<div class="row">
			@foreach ($designerbottom as $item)
			<div class="col-sm-3 col-xs-6" >
				<a class="image-link" href="{{$item->link}}">
					<img src="{{$item->getImageUrl()}}" alt="{{$item->title}}" />
				</a>
			</div>
			@endforeach
		</div>
	</div>
</div>
@endif