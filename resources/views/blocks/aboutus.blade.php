<div class="col-sm-12">
	<h2 class="filterproduct-title"><span class="content"><strong>{{ t('A VAMOSI-ról') }}</strong></span></h2>
</div>
<div class="col-md-12">
	@if(ismobile())
	<div class="col-md-4 text-center my-3">
		<img src="{{ asset('images/vamosilogo.png') }}" alt="{{ t('project_name','project') }}" class="img-responsive"/>
	</div>
	<div class="col-md-8">
		<p>A Vamosi Milano elhozza az igazi olasz stílust az otthonába. Kanapéink, franciaágyaink és kiegészítőink mind a trendi design, a praktikum és a prémium minőség megtestesítői. Hamisítatlan, olasz hangulatot árasztó bútoraink egyedi méretekben és színben rendelhetők, így azok tökéletesen személyre szabva ékesítik otthonát!</p>
		<p>Válassza ki az Önnek leginkább tetsző formát, válogasson sok száz, varázslatosan szép szövetünk közül és rendelje meg nappalija vagy hálószobája legújabb darabját most!</p>
	</div>
	@else
	<div class="col-md-9">
		<p class="text-medium">A Vamosi Milano elhozza az igazi olasz stílust az otthonába. Kanapéink, franciaágyaink és kiegészítőink mind a trendi design, a praktikum és a prémium minőség megtestesítői. Hamisítatlan, olasz hangulatot árasztó bútoraink egyedi méretekben és színben rendelhetők, így azok tökéletesen személyre szabva ékesítik otthonát!</p>
		<p class="text-medium">Válassza ki az Önnek leginkább tetsző formát, válogasson sok száz, varázslatosan szép szövetünk közül és rendelje meg nappalija vagy hálószobája legújabb darabját most!</p>
	</div>
	<div class="col-md-3 text-right">
		<img src="{{ asset('images/vamosilogo.png') }}" alt="{{ t('project_name','project') }}" class="img-responsive float-right"/>
	</div>
	@endif
</div>