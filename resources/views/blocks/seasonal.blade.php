
<div class="homepage-grid-banner desktop-view">
    <div class="container">
		<div class="row">
            <div class="col-sm-12">
				<h2 class="filterproduct-title"><span class="content"><strong>{{ t('Szezonális ajánlatok') }}</strong></span></h2>
            </div>
		</div>
        <div class="row">
            <div class="col-sm-6">
                <div class="grid1" style="position:relative;">
                    <img src="https://static.vamosimilano.hu/seasonal/grandioso/600x375.jpg" width="600" height="375" alt="{{ t('Grandioso kanapé - szezonális ajánlat') }}" />
                    <div class="ribbon theme-border-color" style="position:absolute;right:0;top:0;color:#fff;">
                        <div style="position:absolute;right:9%;top:10%;width:90%;text-align:right;">
                            <em style="font-weight:300;font-style:normal;color:#fff;margin-right:26%;">AKÁR</em>
                            <h4 style="font-weight:700;color:#fff;">50%</h4>
                            <h5 style="font-weight:600;color:#fff;">ÁRON</h5>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-3">
                <div class="grid2" style="position:relative;">
                    <img src="https://static.vamosimilano.hu/seasonal/grandioso/300x375.jpg" width="300" height="375" alt="{{ t('Grandioso kanapé - szezonális ajánlat') }}" />

                </div>
            </div>
            <div class="col-sm-3">
                <div class="grid3 col-sm-margin" style="position:relative;">
                    <img src="https://static.vamosimilano.hu/seasonal/grandioso/300x188_01.jpg" width="300" height="188" alt="{{ t('Grandioso kanapé - szezonális ajánlat') }}" />

                </div>
                <div class="grid4" style="position:relative;">
                    <img src="https://static.vamosimilano.hu/seasonal/grandioso/300x188_02.jpg" width="300" height="188" alt="{{ t('Grandioso kanapé - szezonális ajánlat') }}" />

                </div>
            </div>
        </div>
    </div>
</div>
