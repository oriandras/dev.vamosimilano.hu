<div class="clearfix"><br><br></div>
<div class="col-sm-12">
	<h2 class="filterproduct-title"><span class="content"><strong>{{ t('Miért válasszon minket?') }}</strong></span></h2>
</div>
<div class="col-sm-4">
	<figure class="figure">
		<img src="{{ asset('images/minoseg.jpg') }}" alt="Vamosi Minőség" class="img-responsive" />
	</figure>
	<h3 class="text-dark text-uppercase text-bold med_underline">{{t('Vamosi Minőség')}}</h3>
	<p>A Vamosi Olaszországban tervezett, uniós jogvédelemmel rendelkező bútorait magas minőséggel, az ISO szabványnak megfelelően gyártjuk Magyarországon és Portugáliában. A vadonatúj, high-end technológiával készített, különösen kényelmes ülőfelületeknek köszönhetően akár 10 év garanciát adunk a bútorok vázára és választástól függően akár 5 évet a szövetekre.</p>
</div>
<div class="col-sm-4">
	<figure class="figure">
		<img src="{{ asset('images/egyediseg.jpg') }}" alt="Vamosi Egyediség" class="img-responsive" />
	</figure>
	<h3 class="text-dark text-uppercase text-bold med_underline">{{t('Vamosi Egyediség')}}</h3>
	<p>A Vamosi közel minden bútorát igénye szerint rendelheti, egyedi méret és színkombinációban, akár 800 szövet közül választva. Később is friss megjelenést kölcsönözne bútorának? Vásárolja meg kiterjesztett jótállásunkat, így kérésére bármikor önköltségen átkárpitozzuk kanapéját vagy franciaágyát, ha új színekkel szeretné feldobni otthonát! Tervezgessen, játsszon a színekkel és rendelje meg valóban személyre szabott bútorát!</p>
</div>
<div class="col-sm-4">
	<figure class="figure">
		<img src="{{ asset('images/elmeny.jpg') }}" alt="Vamosi Élmény" class="img-responsive" />
	</figure>
	<h3 class="text-dark text-uppercase text-bold med_underline">{{t('Vamosi Élmény')}}</h3>
	<p>A Vamosi bútorokat akár 30-70%-os kedvezménnyel rendelheti meg úgy, hogy elegendő csupán 30%-ot befizetnie vagy igénybe veheti önerő nélküli, <a href="{{ action('CmsController@index', t('aruhitel', 'url')) }}">0% THM áruhitelünket is</a>!<a href="#csillag">*</a> Megrendelését Budapesten belül ingyen házhoz szállítjuk vagy térítésmentesen átveheti Győrben, Miskolcon, Debrecenben és Szegeden is. Természetesen bútorát külön kérésre össze is szereljük!</p>
</div>