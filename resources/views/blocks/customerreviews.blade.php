<?php
    $customer_reviews = Cms::getCustomerReviews(10);
?>
@if(sizeof($customer_reviews))
    <div class="slidercontainer">
            <div class="container">
               <div class="row">
                  <div class="col-sm-12">
                    <h2 class="filterproduct-title customerreview-title"><span class="content"><strong>{{ t('Vásárlóink küldték') }}</strong></span></h2>
                    <div id="brands-slider-demo-1" class="brands-slider">
                        <div class="owl-carousel">
                            @foreach ($customer_reviews as $review)
                            <div class="item itempaddingtop"><a href="{{ $review->getImageUrl(1024) }}" class="fancy" title="{{ $review->title }}"><img src="{{ $review->getImageUrl() }}" alt="{{ $review->title }}" /></a></div>
                            @endforeach

                    	</div>
                        <script type="text/javascript">
                            require([
                                'jquery',
                                'owl.carousel/owl.carousel.min'
                            ], function ($) {
                                $("#brands-slider-demo-1 .owl-carousel").owlCarousel({
                                    autoplay: true,
                                    autoplayTimeout: 5000,
                                    autoplayHoverPause: true,
                                    margin: 30,
                                    nav: false,
                                    dots: true,
                                    loop: true,
                                    responsive: {
                                        0: {
                                            items:2
                                        },
                                        640: {
                                            items:3
                                        },
                                        768: {
                                            items:4
                                        },
                                        992: {
                                            items:5
                                        },
                                        1200: {
                                            items:6
                                        }
                                    }
                                });
                            });
                        </script>
                    </div>

	</div>
	</div>
	</div>
	</div>
@endif
