<?php if(isset($_COOKIE["stat_cookie"]) && $_COOKIE["stat_cookie"]==="yes"): 
	$schecked="checked";
else:
	$schecked="unchecked";
endif;
if(!isset($_COOKIE["stat_cookie"])):
	$schecked="checked";
endif;
if(isset($_COOKIE["marketing_cookie"]) && $_COOKIE["marketing_cookie"]==="yes"): 
	$mchecked="checked";
else:
	$mchecked="unchecked";
endif;
if(isset($_COOKIE["stat_cookie"]) && isset($_COOKIE["marketing_cookie"])):
//$toggle='style="display: none; height:0px !important;"';
$toggle="hidden";
else:
$toggle="";
endif;
?>
<?php /*
<div class="container-fluid py-2" id="cookie_bar" {{$toggle}}>
	<div class="row">
		<div class="col-lg-12">
			<div class="card card-black shadow">
				<div class="card-body">
					<p>{{t("cookie_bar_text")}} <a href="javascript:void(0)" data-toggle="modal" data-target="#CookieModal">{{t("cookie_settings")}}hoz kattintson ide.</a></p>
					<p><label for="cookie_submit" class="btn btn-primary float-right"> {{t("elfogadom")}} </label></p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="CookieModal">
	<button type="button" class="close text-white" data-dismiss="modal">&times;</button>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<div class="container-fluid">
					<form action="/cookieaccept" method="post">
					<div class="row">
						<div class="col-md-3">
							<ul class="nav nav-pills">
								<li class="nav-item">
									<a class="nav-link" data-toggle="pill" href="#cookie">{{t("cookie_settings")}}</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="pill" href="#basic">{{t("cookie_basic")}}</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="pill" href="#stat">{{t("cookie_stat")}}</a>
								</li>
								<li class="nav-item">
									<a class="nav-link active" data-toggle="pill" href="#marketing">{{t("cookie_marketing")}}</a>
								</li>
							</ul>
						</div>
						<div class="col-md-9">
							<div class="tab-content">
								<div class="tab-pane container fade" id="cookie">
									<h3 class="h3-responsive">{{t("cookie_settings")}}</h3>
									{{t("cookie_bar_text")}}
									
								</div>
								<div class="tab-pane container fade" id="basic">
									<h3>{{t("cookie_basic")}}</h3>
									{{t("cookie_basic_text")}}
									<label class="disabled"><input type="checkbox" checked readonly disabled> {{t('engedélyezem')}}</label>
								</div>
								<div class="tab-pane container fade" id="stat">
									<h3>{{t("cookie_stat")}}</h3>
									{{t("cookie_stat_text")}}
									<label><input type="checkbox" name="stat_cookie" value="yes" {{$schecked}}> {{t('engedélyezem')}}</label>
								</div>
								<div class="tab-pane container active" id="marketing">
									<h3>{{t("cookie_marketing")}}</h3>
									{{t("cookie_marketing_text")}}
									<label><input type="checkbox" name="marketing_cookie" value="yes" {{$mchecked}}> {{t('engedélyezem')}}</label>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 text-right">
							{{csrf_field()}}
							<button type="submit" id="cookie_submit" class="btn btn-primary ">{{t("elfogadom")}}</button>
						</div>
					</div>
				</form>
				</div>
			</div>
		</div>
	</div>
</div>
*/ ?>
<div id="cookiecontainer" class="z-depth-1 bg-primary p-3 waves-effect {{$toggle}}">
	<form action="/cookieaccept" method="post">
		<div class="accordion md-accordion" id="c_settings" role="tablist" aria-multiselectable="true">
			<div class="row">
				<div class="col-md-12">
					<p class="mt-2">
						{{t("cookie_bar_text")}} <a data-toggle="collapse" data-parent="#c_settings" href="#cookiesettings" aria-expanded="true" aria-controls="cookiesettings" class="text-dark" style="text-decoration:underline !important;">További információkért kattintson ide</a>
					</p>
				</div>
			</div>
			<div class="row">
				<div id="cookiesettings" class="col-md-12 collapse" role="tabpanel" aria-labelledby="cookiecontent" data-parent="#c_settings">
					<ul style="max-height:30vh!important;overflow:auto;">
						<li>
							<p class="mb-0"><strong>{{t('cookie_basic')}}</strong></p>
							{{t("cookie_basic_text")}}
						</li>
						<li>
							<p class="mb-0"><strong>{{t('cookie_stat')}}</strong></p>
							{{t("cookie_stat_text")}}
						</li>
						<li>
							<p class="mb-0"><strong>{{t('cookie_marketing')}}</strong></p>
							{{t("cookie_marketing_text")}}
						</li>
					</ul>
				</div>
			</div>
			<div class="row text-center">
				<div class="col-md-3">
					<?php
					$inline="form-check-inline my-0";
					if( ismobile() ){
						$inline="my-1 text-left";
					}
					?>
					<div class="form-check md-form {{$inline}} disabled">
						<input type="checkbox" class="form-check-input" checked readonly disabled>
						<label class="form-check-label disabled" title="Nem változtatható">{{t('cookie_basic')}}</label>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-check md-form {{$inline}}">
						<input type="checkbox" name="stat_cookie" id="statcookie" value="yes" class="form-check-input" {{$schecked}}>
						<label class="form-check-label" for="statcookie">{{t('cookie_stat')}}</label>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-check md-form {{$inline}}">
						<input type="checkbox" id="marketingcookie" name="marketing_cookie" value="yes" class="form-check-input" {{$mchecked}}>
						<label class="form-check-label" for="marketingcookie">{{t('cookie_marketing')}}</label>
					</div>
				</div>
				<div class="col-md-3">
					<div class="md-form px-0 my-0 text-right">
						<button type="submit" id="cookie_submit" class="btn btn-sm btn-secondary ">{{t("elfogadom")}}</button>
					</div>
					{{csrf_field()}}
				</div>
			</div>
		</div>

	</form>
</div>