<div class="container-fluid my-4">
	<div class="row d-flex">
		<div class="col-lg-3 order-4 align-self-stretch">
			<div class="card h-100">
				<div class="view overlay">
					<img class="card-img-top" src="{{ asset("/media/card/shipping_708x472.jpg") }}" alt="Card image cap">
					<a href="{{ url("/hazhozszallitas") }}">
						<div class="mask rgba-white-slight"></div>
					</a>
				</div>
				<a class="btn-floating btn-action ml-auto mr-4 btn-primary" href="{{ url("/hazhozszallitas") }}"><i class="fal fa-truck-couch pl-1"></i></a>
				<div class="card-body">
					<h4 class="card-title">Élvezze az ingyenes szállítás kényelmét!</h4>
					<hr>
					<p class="card-text">Rendelését az egész ország területén ingyen kiszállítjuk! A Vamosi Milano bútorai a kényelem és elegancia megtestesítői, így Önnek nem kell foglalkoznia a szállítással és összeszereléssel.*</p>
				</div>
			</div>
		</div>
		<div class="col-lg-3 order-2 align-self-stretch">
			<div class="card h-100">
				<div class="view overlay">
					<img class="card-img-top" src="{{ asset("/media/card/textiles_708x472.jpg") }}" alt="Card image cap">
					<a>
						<div class="mask rgba-white-slight"></div>
					</a>
				</div>
				<a class="btn-floating btn-action ml-auto mr-4 btn-primary"><i class="fal fa-store"></i></a>
				<div class="card-body">
					<h4 class="card-title">Válasszon {{$textile_count}} kárpitfajta közül! </h4>
					<hr>
					<p class="card-text">Engedje szabadjára fantáziáját és valósítsa meg a legmerészebb színkombinációkat! A Vamosi Milano bútorait bármilyen színben megrendelheti, standard és prémium minőségű kárpitozással is.</p>
				</div>
			</div>
		</div>
		<div class="col-lg-3 order-3 align-self-stretch">
			<div class="card h-100">
				<div class="view overlay">
					<img class="card-img-top" src="{{ asset("/media/card/loan_708x472.jpg") }}" alt="Card image cap">
					<a href="{{ url("/aruhitel") }}">
						<div class="mask rgba-white-slight"></div>
					</a>
				</div>
				<a class="btn-floating btn-action ml-auto mr-4 btn-primary" href="{{ url("/aruhitel") }}"><i class="fal fa-euro-sign"></i></a>
				<div class="card-body">
					<h4 class="card-title">Vegye igénybe kedvezményes áruhitelünket!</h4>
					<hr>
					<p class="card-text">Legyen Öné már most álmai bútora kedvezményes áruhitelünkkel! A Vamosi Milano bútorait áruhitelre is megvásárolhatja, hitelbírálatát pedig akár egy finom kávé mellett is megvárhatja.</p>
				</div>
			</div>
		</div>
		<div class="col-lg-3 order-1 align-self-stretch">
			<div class="card h-100">
				<div class="view overlay">
					<img class="card-img-top" src="{{ asset("/media/card/scalable_708x472.jpg") }}" alt="Card image cap">
					<a>
						<div class="mask rgba-white-slight"></div>
					</a>
				</div>
				<a class="btn-floating btn-action ml-auto mr-4 btn-primary"><i class="fal fa-couch"></i></a>
				<div class="card-body">
					<h4 class="card-title">Méretezze bútorát tetszés szerint!</h4>
					<hr>
					<p class="card-text">Megtervezte álmai nappaliját? A Vamosi Milano bútorait egyedi elképzelése szerint, többféle méretkombinációban rendelheti meg - jobbos és balos kiszerelésben is.</p>
				</div>
			</div>
		</div>
	</div>
</div>