<footer class="page-footer">

            <div class="footer">

                <div class="footer-top">

                    <div class="container">

                        <div class="row">

                            <div class="col-md-12">

                                <div class="custom-block">

                                    <div class="homepage-bar">

                                        <div class="row">

                                            <div class="col-md-3 col-sm-6">

                                                <em class="fa fa-male white-text"></em>

                                                <div class="text-area">

                                                    <h3 class="white-text">{{ t('RENDELÉSRE') }}</h3>

                                                    <p>{{ t('Minden bútor minden darabja <br> kézzel készített.') }}</p>

                                                </div>

                                            </div>

                                            <div class="col-md-3 col-sm-6">

                                                <em class="fa fa-crop white-text"></em>

                                                <div class="text-area">

                                                    <h3 class="white-text">{{ t('EGYEDI') }}</h3>

                                                    <p>{{ t('Minden bútor egyedi, az Ön igényei szerint.') }}</p>

                                                </div>

                                            </div>

                                            <div class="col-md-3 col-sm-6 sm-bd-0">

                                                <em class="fa fa-bus white-text"></em>

                                                <div class="text-area">

                                                    <h3 class="white-text">{{ t('HÁZHOZSZÁLLÍTÁS') }}</h3>

                                                    <p>{{ t('Garatnált szállítás, ISO 9001 tanúsítvány.') }}</p>

                                                </div>

                                            </div>

                                            <div class="col-md-3 col-sm-6 sm-bd-0">

                                                <em class="fa fa-money white-text"></em>

                                                <div class="text-area">

                                                    <h3 class="white-text">{{ t('ÁRUHITEL') }}</h3>

                                                    <p>{{ t('Akár 0% THM áruhitel.') }}</p>

                                                </div>

                                            </div>


                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="footer-middle">

                    <div class="container">

                        <div class="row">

                            <div class="col-sm-3">

                                <div class="block">

									<div class="block-title"><strong>{{ t('Legnépszerűbb termékeink') }}</strong>

                                    </div>

                                    <div class="block-content">

                                        <ul class="links">

                                            <li><em class="porto-icon-right-dir theme-color"></em><a href="/termekek/kanape/l-alaku-kanapek">{{ t('L-alakú kanapék') }}</a>

                                            </li>

											<li><em class="porto-icon-right-dir theme-color"></em><a href="/termekek/kanape/u-alaku-kanapek">{{ t('U-alakú kanapék') }}</a>

                                            </li>

											<li><em class="porto-icon-right-dir theme-color"></em><a href="/termekek/kanape/franciaagyak">{{ t('Franciaágyak') }}</a>

                                            </li>

											<?php

											//<li><em class="porto-icon-right-dir theme-color"></em><a href="/termekek/matrac/dr-balm-matracok">{{ t('Matracok') }}</a>

                                            //</li>

											?>

											<li><em class="porto-icon-right-dir theme-color"></em><a href="/termekek/outlet">{{ t('Outlet bútorok') }}</a>

                                            </li>

                                        </ul>

                                    </div>

                                </div>

                            </div>

                            <div class="col-sm-3">

                                <div class="block">

                                    <div class="block-title"><strong>{{ t('Kapcsolat') }}</strong>

                                    </div>

                                    <div class="block-content">

                                        <ul class="contact-info">



											<li><em class="porto-icon-location"></em>

                                                <p><b>{{ t('Bemutatótermeink:') }}</b>

                                                    <br/><a href="/kapcsolat" title="{{ t('Bemmutatótermeink')." - ".t('project_name','project') }}">{{ t('Üzletkereső') }}</a>

                                                </p>

                                            </li>

											<li><em class="porto-icon-phone"></em>

                                                <p><b>{{ t('Központi telefonszám:') }}</b>

                                                    <br/><a href="tel:00361353941" title="{{ t('Hívás most!') }}">{{ t('phone_number') }}</a></p>

                                            </li>

                                            <li><em class="porto-icon-mail"></em>

                                                <p><b>{{ t('Email:') }}</b>

                                                    <br/><a href="mailto:{{ t('email_address') }}">{{ t('email_address') }}</a>

                                                </p>

                                            </li>

                                            <li><em class="porto-icon-clock"></em>

                                                <p><b>{{ t('Ügyfélszolgálat nyitvatartása') }}:</b>

                                                    <br/>{{ t('Hétfő-Szombat: 9:00-17:00') }}

													<br/>{{ t('Vasárnap: 11:00-18:00') }}

                                            </li>

                                        </ul>

                                    </div>

                                </div>

                            </div>

                            <div class="col-sm-3">

                                <div class="block">

                                    <div class="block-title"><strong>{{ t('Fontos tudnivalók') }}</strong>

                                    </div>

                                    <div class="block-content">

                                        <ul class="features">

											@if (App::getLocale()!='en')

                                            <li><em class="porto-icon-ok theme-color"></em><a target="_blank" href="{{ action('CmsController@index', t('aruhitel', 'url')) }}">{{ t('Áruhitel') }}</a>

                                            </li>

											@endif

                                            <li><em class="porto-icon-ok theme-color"></em><a target="_blank" href="{{ action('CmsController@index', t('hazhozszallitas', 'url')) }}">{{ t('Házhozszállítás') }}</a>

                                            </li>

                                            <li><em class="porto-icon-ok theme-color"></em><a target="_blank" href="{{ action('CmsController@index', t('aszf', 'url')) }}">{{ t('ÁSZF') }}</a>

                                            </li>

											<li><em class="porto-icon-ok theme-color"></em><a target="_blank" href="{{ action('CmsController@index', t('adatvedelem', 'url')) }}">{{ t('Adatvédelem') }}</a>

                                            </li>

											<li><em class="porto-icon-ok theme-color"></em><a target="_blank" href="{{ action('CmsController@index', t('impresszum', 'url')) }}">{{ t('Impresszum') }}</a>

                                            </li>
											
											<li><i class="fa fa-cogs" aria-hidden="true"></i> <a href="javascript:void(0)" class="modaltoggle">{{ t('Cookie beállítások') }}</a>

                                            </li>

                                        </ul>

                                    </div>

                                </div>

                            </div>

                            <div class="col-sm-3">

                                <div class="block">

                                    <div class="block-title"><strong>{{ t('Értesüljön akcióinkról!') }}</strong>

                                    </div>

                                    <div class="content">

                                        <p>{{ t('Értesüljön elsőként a meghirdetett akcióinkról') }}</p>

                                        <label for="footer_newsletter"></label>

										<?

										/*

                                        <form class="form subscribe" id="subscribe-footer" action="{{ action('SubscribeController@subscribe') }}" ajax-action="" method="post">

                                            <input type="hidden" name="url" value="{{ Request::url() }}">

                                            <div class="row form-group">

                                                <div class="col-md-6">



                                                    <div class="control">

                                                        <input type="text" name="lastname" autocomplete="off" placeholder="{{ t('Vezetéknév') }}" value="{{ old('lastname') }}" class="form-control" required=""  />

                                                    </div>

                                                </div>

                                                <div class="col-md-6">



                                                    <div class="control">

                                                        <input type="text"  name="firstname" autocomplete="off" placeholder="{{ t('Keresztnév') }}" value="{{ old('firstname') }}" class="form-control" required=""  />

                                                    </div>

                                                </div>

                                            </div>



                                            <div class="row form-group">

                                                <div class="col-md-12">



                                                    <div class="control">

                                                        <input name="email" autocomplete="off" placeholder="{{ t('E-mail') }}" value="{{ old('email') }}" type="email" class="form-control"  required=""  />

                                                    </div>

                                                </div>



                                            </div>



                                            <div class="clearfix"></div>



                                            <div class="actions">

                                                <button class="action subscribe primary" title="{{ t('Feliratkozás') }}" type="submit">

                                                    <span>{{ t('Feliratkozás') }}</span>

                                                </button>

                                            </div>

                                            {{ csrf_field() }}

                                        </form>

										*/ ?>

										@include('cms.subscribeGDPR')

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="footer-bottom">

                    <div class="container">

                        <strong class="logo">

                            <a href="{{ url('/') }}" rel="home" title="{{ t('Főoldal') }} - {{ t('project_name','project') }}"><img src="{{ asset('images/vamosilogo.png') }}" alt="{{ t('project_name','project') }}"/></a>

                        </strong>

                        <div class="custom-block">

                            <ul class="social-icons">

                                <li><a class="facebook-link" href="https://www.facebook.com/vamosimilano" target="_blank"><em class="fa fa-facebook"></em></a></li>

                                <li><a class="twitter-link" href="https://twitter.com/vamosimilano" target="_blank"><em class="fa fa-twitter"></em></a></li>

								<li><a class="linkedin-link" href="https://www.linkedin.com/company/vamosimilano" target="_blank"><em class="fa fa-linkedin"></em></a></li>

								<li><a class="youtube-link" href="https://www.youtube.com/channel/UC6ckSCubYM-M4fdOmXkfb1g" target="_blank"><em class="fa fa-youtube-play"></em></a></li>

								<li><a class="instagram-link" href="https://instagram.com/vamosimilano/" target="_blank"><em class="fa fa-instagram"></em></a></li>

								<li><a class="pinterest-link" href="https://www.pinterest.com/vamosimilano/" target="_blank"><em class="fa fa-pinterest"></em></a></li>

                            </ul>

                            <a href="{{ action('CmsController@index', t('bankkartyas-fizetes-tajekoztato', 'url')) }}"><img src="{{ asset('/images/cards/payments.png') }}" alt="{{ t('Bankkártyás fizetési tájékoztató') }}" /></a>

                        </div>

                    </div>

                </div>
				<div class="container"><a id="csillag"></a>
					<div class="row">
						<div class="col-lg-12">
							<p><small><strong>*</strong> A tájékoztatás nem teljeskörű, az áruhitel és az ingyenes házhoz szállítás részleteiről tájékozódjon weboldalunkon, illetve az ÁSZF-ben! Az egyes kedvezmények nem összevonhatók. Az akár 70% mértékű kedvezmény kizárólag az outlet bútorként forgalmazott bútorokra vonatkozik. Az outlet bútorokat a készlet erejéig lehet megvásárolni, az aktuális készlet folyamatosan változik.</small></p>
						</div>
					</div>
				</div>
            </div>

            <a href="javascript:void(0)" id="totop"><em class="fa fa-arrow-up"></em></a>

        </footer>

