<?php
if( sizeof($showroom) ){
?>
<nav id="showroom{{$sid}}" class="carousel slide carousel-fade" data-ride="carousel">
<?php
	if( sizeof($showroom) === 1) { //default slide
	?>
	<div class="carousel-inner">
	<?php
		foreach($showroom as $default){?>
		<div class="carousel-item active">
		<?php
		if( isset($default->title3) && $default->title3!=""){
		?>
			<a href="{{$default->link}}" title="{{$default->title3}}" class="v1">
				<img class="img-fluid fullwidth-img" src="{{$default->getImageUrl()}}" alt="{{$default->title}}">
				<div class="carousel-caption d-none d-md-block">
					<h2 class="text-uppercase lead"><strong>{{$default->title}}</strong></h2>
					<?php
					if( isset($default->title2) && $default->title2!=""){
					?>
					<p>{{$default->title2}}</p>
					<?php
					}//if
					?>
					<p><a href="{{$slide->link}}" title="{{$slide->title3}}" class="btn btn-sm btn-primary">{{$slide->title3}}</a></p>
				</div>
			</a>
		<?php
		}
		else {
		?>
			<a href="{{$default->link}}" title="{{$default->title}}" class="v2">
				<img class="img-fluid" src="{{$default->getImageUrl()}}" alt="{{$default->title}}">
				<div class="overlay">
					<div class="overlay-text">
						<h2 class="text-uppercase lead"><strong>{{$default->title}}</strong></h2>
						<?php
						if( isset($default->title2) && $default->title2!=""){
						?>
						<p>{{$default->title2}}</p>
						<?php
						}//if
						?>
					</div>
				</div>
			</a>
		<?php
		}
		?>
		</div>
		<?php
		}
		?>
	</div>
	<?php
	}
	else {
		//több slide van
		if( sizeof($showroom)>2 ){//ide jön a navigáló pöttyös cucc
	?>
	<ol class="carousel-indicators">
	<?php
	$indicatornum=0;
	foreach($showroom as $indicator){
		if($indicator->weight != 0) {
			if($indicatornum===0){
				$indicator_status="active";
			}
			else {
				$indicator_status="";
			}
		?>
		<li data-target="#showroom{{$sid}}" data-slide-to="{{$indicatornum}}" class="{{$indicator_status}}"></li>
		<?php
			$indicatornum++;
			}
		}
	?>
	</ol>
		<?php
		}
		?>
	<section class="carousel-inner">
	<?php
		$slidenum=0;
		foreach($showroom as $slide ) {
			if($slide->weight !== 0){
				//echo $slidenum;
			?>
		<article class="carousel-item {{$slidenum===0 ? 'active' : ''}} animated">
			<a href="{{$slide->link}}" title="{{$slide->title3}}" class="">
				<img class="img-fluid" src="{{$slide->getImageUrl()}}" alt="{{$slide->title}}">
			<div class="carousel-caption d-none d-md-block">
			</div>
			</a>
		</article><!-- //carousel-item {{$slide->weight}} {{$slidenum}}-->
		<?php
			$slidenum++;
			}//if nem a default
			?>
		<?php
		}//foreach
		?>
	</section><!-- //carousel-inner -->
	<a class="carousel-control-prev" href="#showroom{{$sid}}" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="carousel-control-next" href="#showroom{{$sid}}" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
	<?php
	}
	?>
</nav><!-- //carousel -->
<?php
}//if van showroom
?>