<div class="header content">

                <span class="action nav-toggle" data-action="toggle-nav"><span>{{ t('Navigation') }}</span></span>

                <strong class="logo">

                    <a href="{{ url('/') }}" title="{{ t('Főoldal') }}" rel="home"><img alt="{{ t('project_name', 'project') }} - Handmade Sofas & Beds" src="{{ asset('images/vamosilogo.png') }}">

                    </a>

                </strong>

                <div class="sections nav-sections">

                    <div  class="section-items nav-sections-items">

                        <div  class="section-item-title nav-sections-item-title" onclick="$('html').removeClass('nav-open');$('html').removeClass('nav-before-open');">

                            <a href="javascript:" data-action="toggle-nav" class="nav-sections-item-switch action toggle-nav"><i class="fa fa-bars menu-icon"></i></a>

                        </div>

                        <div data-role="content" id="store.menu" class="section-item-content nav-sections-item-content">

                            <nav role="navigation" class="navigation sw-megamenu">

                                <div  id="mobile-search" class="section-item-content nav-sections-item-content margintop40" style="display: none;">

                                <form method="get" action="{{ action('WebshopController@search') }}" class="form minisearch">

                                    <div class="field search">

                                        <div class="control">

                                            <input type="text" autocomplete="off" aria-autocomplete="both" aria-haspopup="false" aria-expanded="false" role="combobox" maxlength="128" class="input-text" placeholder="{{ t('Írja be a keresőszót') }}" value="" name="q">

                                        </div>

                                    </div>

                                </form>

                                </div>

                                 <div class="clearfix mobile-view"></div>

                                <ul>



                                    <li class="ui-menu-item level0 classic">

                                        <a class="level-top" rel="home" href="{{ url('/') }}" title="{{ t('project_name', 'project') }}"><span>{{ t('Főoldal') }}</span></a>

                                    </li>









                                    <li class="ui-menu-item level0 fullwidth parent ">

                                        <div class="open-children-toggle" id="one-item-click"></div><a class="level-top" href="{{ action('WebshopController@product', '') }}"><span>{{ t('Termékkategóriák') }}</span> <i class="fa fa-sort-desc desktop-view" aria-hidden="true"></i></a>

                                        <div class="level0 submenu">

                                            <div class="menu-top-block">

                                                <div class="menu-top-product">

                                                <strong >{{ t('Legkeresettebb') }}:</strong>

                                                    @foreach (ProductCategory::menuHighLight() as $top_category)

                                                            <a href="{{ $top_category->getUrl() }}" title="{{ $top_category->name }}">{{ $top_category->name }}</a>

                                                    @endforeach

                                                </div>

                                            </div>

                                            <div class="row">

                                                <ul class="subchildmenu col-sm-8 mega-columns columns3">



                                                    @foreach (ProductCategory::getMenu(0) as $category)

                                                        <li class="ui-menu-item level1 parent ">

                                                            <div class="open-children-toggle"></div>

                                                            <a href="{{ $category['data']->getUrl() }}"><span>{{ $category['data']->name }}</span></a>

                                                            @if (isset($category['child']))



                                                            <ul class="subchildmenu ">

                                                                @foreach ($category['child'] as $child)

                                                                 <li class="ui-menu-item level2 "><a href="{{ $child['data']->getUrl() }}"><span>{{ $child['data']->name }}</span></a>

                                                                </li>



                                                                @endforeach

                                                            </ul>

                                                            @endif

                                                        </li>



                                                    @endforeach









                                                </ul>



                                                @if (isset($highlighted_products) and sizeof($highlighted_products)>1)

                                                <div class="menu-right-block col-sm-4">

                                                    <?php

                                                        $highlighted_product = $highlighted_products[rand(0,(count($highlighted_products)-1))];

                                                    ?>

                                                    <div class="menu-right-block">

                                                    <a href="{{$highlighted_product->getUrl()}}" title="{{$highlighted_product->getName()}}"><img alt="{{$highlighted_product->getName()}}" src="{{ $highlighted_product->getDefaultImageUrl(600) }}"/></a>

                                                        <div class="div-container">

                                                            <h2>{{ t('A hónap bútora') }}</h2>

                                                            <a href="{{$highlighted_product->getUrl()}}" title="{{$highlighted_product->getName()}}" class="btn btn-default">{{ t('Hatalmas kedveménnyel') }} <em class="porto-icon-right-dir"></em></a>															

                                                        </div>

														<div class="highlight-desc">{{$highlighted_product->getName()}} 

															<span class="old-price athuzva">{{ money($highlighted_product->getDefaultFullPrice()) }}</span>

															<span class="price">{{ money($highlighted_product->getDefaultPrice()) }}</span>

														</div>

														@if ($highlighted_product->getPercentPrice())

														<div style="position:absolute;top:40px;right:10px;border-radius:15px;background-color:#CD7126;">

														<a href="{{$highlighted_product->getUrl()}}" title="{{$highlighted_product->getName()}}"><span style="font-wight:bold;font-size:25px;color:white">- {{ $highlighted_product->getPercentPrice() }} %</span></a>

														</div>

														@endif

                                                    </div>

                                                </div>
												@else
												<div class="col-sm-4" style="overflow: hidden;top:-40px;">
													<div class="div-container" style="overflow: hidden;padding: 0;">
														<img src="https://vamosimilano.hu/static/mar.jpg" style="width: 100%; max-width: 392px; height: auto; margin: 0px; padding: 0px;" alt="Sofa">
													</div>
												</div>
                                                @endif




                                            </div>

                                        </div>

                                    </li>
									@if(sizeof($highlighted_products)>0)
                                    <li class="ui-menu-item level0 fullwidth"><a class="level-top" href="{{ action('WebshopController@sales') }}" title="{{ t('A hónap Bútorai') }}"><span>{{ t('A hónap Bútorai') }}</span></a>

                                    </li>
									@endif;
									<? /*

									<li class="ui-menu-item level0 fullwidth"><a class="level-top" href="/products/outlet" title="{{ t('Outlet') }}"><span>{{ t('outlet') }}</span></a>

                                    </li>

									*/ ?>





                                    <li class="ui-menu-item level0 classic parent fl-right ">

                                        <div class="open-children-toggle"></div><a class="level-top" href="{{ action('CmsController@index', t('rolunk', 'url')) }}"><span>{{ t('Rólunk') }}</span> <i class="fa fa-sort-desc desktop-view" aria-hidden="true"></i></a>

                                        <div class="level0 submenu submenuleft">

                                            <div class="row">

                                                <ul class="subchildmenu ">

                                                    <li class="ui-menu-item level1 "><a href="{{ action('CmsController@index', t('rolunk', 'url')) }}" title="{{ t('A Bella Italia Bútorház') }}"><span>{{ t('A Bella Italia Bútorház') }}</span></a>

                                                    </li>

                                                    <li class="ui-menu-item level1 "><a href="{{ action('CmsController@index', t('bemutatotermeink', 'url')) }}" title="{{ t('Üzleteink') }}"><span>{{ t('Üzleteink') }}</span></a>

                                                    </li>

													@if (App::getLocale()!='en')

                                                    <li class="ui-menu-item level1 "><a href="http://karrier.vamosimilano.hu" title="{{ t('Karrier') }}"><span>{{ t('Karrier') }}</span></a>

                                                    </li>

													@endif

                                                </ul>

                                            </div>

                                        </div>

                                    </li>



									<?php /* <li class="ui-menu-item level0 fullwidth fl-right"><a class="level-top" href="{{ action('BlogController@index') }}" title="{{ t('Blog') }}"><span>{{ t('Blog') }}</span></a> */ ?>

									@if (App::getLocale()!='en')

									<li class="ui-menu-item level0 fullwidth fl-right"><a class="level-top" href="http://blog.vamosimilano.hu" title="{{ t('Blog') }}"><span>{{ t('Blog') }}</span></a>

                                    </li>

									@endif

                                    <li class="ui-menu-item level0 classic parent fl-right">

                                        <div class="open-children-toggle"></div><a class="level-top" href="{{ action('CmsController@index', t('hazhozszallitas', 'url')) }}"><span>{{ t('Fontos tudnivalók') }}</span> <i class="fa fa-sort-desc desktop-view" aria-hidden="true"></i></a>

                                        <div class="level0 submenu submenuleft">

                                            <div class="row">

                                                <ul class="subchildmenu">

                                                    <?php

                                                        /*<li class="ui-menu-item level1 "><a href="https://static.vamosimilano.hu/loancalculator/" title="{{ t('Hitelfeltételek') }}"><span>{{ t('Hitelfeltételek') }}</span></a>

                                                    </li>*/

                                                    ?>

                                                    <li class="ui-menu-item level1 "><a target="_blank" href="{{ action('CmsController@index', t('hazhozszallitas', 'url')) }}" title="{{ t('Szállítási feltételek') }}"><span>{{ t('Szállítási feltételek') }}</span></a>

                                                    </li>

                                                     <li class="ui-menu-item level1 "><a target="_blank" href="{{ action('CmsController@index', t('adatvedelem', 'url')) }}" title="{{ t('Áruhitel') }}"><span>{{ t('Adatvédelmi nyilatkozat') }}</span></a>

                                                    </li>

                                                     <li class="ui-menu-item level1 "><a target="_blank" href="{{ action('CmsController@index', t('aszf', 'url')) }}" title="{{ t('ÁSZF') }}"><span>{{ t('ÁSZF') }}</span></a>

                                                    </li>



                                                </ul>

                                            </div>

                                        </div>

                                    </li>

									@if (App::getLocale()!='en')

                                    <li class="ui-menu-item level0 fullwidth fl-right"><a class="level-top" href="{{ action('CmsController@credit') }}" title="{{ t('Áruhitel') }}"><span>{{ t('Áruhitel') }}</span></a>

                                    </li>

									@endif









                                  </ul>

                            </nav>

                            <script type="text/javascript">

                                require([

                                    'jquery',

                                    'js/sw_megamenu'

                                ], function($) {

                                    $(".sw-megamenu").swMegamenu();

                                });

                            </script>





                        </div>



                        <div data-role="collapsible" class="section-item-title nav-sections-item-title">

                            @if (Auth::user())

                                <a href="{{ action('ProfileController@index') }}" data-toggle="switch" class="nav-sections-item-switch"><i class="fa fa-user menu-icon"></i></a>

                            @else

                                <a href="{{ action('AuthController@login') }}" data-toggle="switch" class="nav-sections-item-switch"><i class="fa fa-user menu-icon"></i></a>

                            @endif

                        </div>



                        <div data-role="collapsible" class="section-item-title nav-sections-item-title">

                            <a onclick="$('#mobile-search').slideToggle();" data-toggle="switch" class="nav-sections-item-switch"><i class="fa fa-search menu-icon"></i></a>

                        </div>

                    </div>

                </div>

            </div>

