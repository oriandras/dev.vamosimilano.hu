<?php
/*
1. Megvizsgálom, hogy volt-e már beállított cookie preferenciája
	1.1 Ha volt, akkor annak megfelelően megvizsgálom az eltárolt cookie bállításokat és átadom a kapcsolók értékeit.
	1.2 Ha még nem volt (else ág), akkor minden kapcsoló értéke checked, hiszen az elrejtett beálltás panelen ez az alapértelmezett, ha kinyítja a beállítás panelt, akkor a check értékek üresre állnak (java scripttel van megoldva, lásd custom.js).
*/
if(isset($_COOKIE["cookie_accepted"]) && $_COOKIE["cookie_accepted"]=="yes"): 

	if(isset($_COOKIE["basic_cookie"]) && $_COOKIE["basic_cookie"]==="yes"):
		$basic_cookie_check = "checked";
	else:
		$basic_cookie_check = "checked";
	endif;
	if(isset($_COOKIE["stat_cookie"]) && $_COOKIE["stat_cookie"]==="yes"):
		$stat_cookie_check = "checked";
	else:
		$stat_cookie_check = "";
	endif;
	if(isset($_COOKIE["marketing_cookie"]) && $_COOKIE["marketing_cookie"]==="yes"):
		$marketing_cookie_check = "checked";
	else:
		$marketing_cookie_check = "";
	endif;
else: 
	$basic_cookie_check = "checked";
	$stat_cookie_check = "checked";
	$marketing_cookie_check = "checked";
endif;
/*
A felső cookie választó, értesítő csík csak akkor jelenik meg, ha még nem fogadtuk el (nem nyomta meg a user a sárga elfogadom gombot)
*/
if(!isset($_COOKIE["cookie_accepted"])):
?>
<div id="cookie_top" class="animated fadeInDown">
	<form method="post" action="/cookieaccept">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8">
				<p><strong>Kedves Látogató!</strong> Weboldalunkon – ahogy a weben legtöbben – cookie-kat használunk az alapvető működés biztosítására, különféle névtelen statisztikák gyűjtésére és a felhasználói élmény tökéletesítésére. <a href="/adatvedelem">Cookie szabályzat.</a></p> 
			</div>
			<div class="col-xs-6 col-md-2 text-left">
				<p><button type="button" class="btn primary btn-link white-text" id="ck-trigger"><span class="glyphicon glyphicon-cog"></span> Cookie beállítások</button></p>
			</div>
			<div class="col-xs-6 col-md-2 text-right">
				{{csrf_field()}}
				<p><button type="submit" class="btn btn-primary action primary" id="cookiebutton"><span class="glyphicon glyphicon-ok"></span> Elfogadom</button></p>
			</div>
			<div class="col-xs-12 collapse" id="cookie_settings">
				<div class="form-group">
					<input type="checkbox" id="basic" <?php echo $basic_cookie_check;?> name="basic_cookie" value="1" onchange="cookieButton()" disabled readonly> 
					<label for="basic" onclick="basicInfo()"><strong>ALAPVETŐ</strong> A weboldal alapvető működéséhez nélkülözhetetlenek, ezek hiányában Ön nem tudja használni a kosarat, az egyedi kárpitozás választót és nem tud vásárolni sem webáruházunkban.</label>	
				</div>
				<div class="form-group">
					<input type="checkbox" id="statistic" <?php echo $stat_cookie_check;?> name="stat_cookie" value="1">
					<label for="statistic"><strong>STATISZTIKAI</strong> Névtelen adatokat biztosít számunkra, hogy szolgáltatásainkat és az online felhasználói élményt folyamatosan javítani tudjuk weboldalunkon.</label>				
				</div>
				<div class="form-group">
					<input type="checkbox" id="marketing" <?php echo $marketing_cookie_check;?> name="marketing_cookie" value="1">
					<label for="marketing"><strong>HIRDETŐI (ajánlott)</strong> Lehetővé teszi számunkra, hogy személyre szabott ajánlatokkal és testreszabott hirdetésekkel érjük el Önt, így érdeklődésének megfelelő, tökéletes böngészési élményben lehet része!</label>
				</div>
			</div>
		</div>
	</div>
	</form>
</div>
<?php
endif;
?>