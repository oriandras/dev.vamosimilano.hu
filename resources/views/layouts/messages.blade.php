@section('footer_js')
	@if (Session::has('flash_error'))
	<script>
	toastr.warning("{{ Session::get('flash_error') }}")
	</script>
	@endif

	@if (Session::has('flash_success'))
	<script>
	toastr.success("{{ Session::get('flash_success') }}")
	</script>
	@endif

	@if (isset($errors) and $errors->all())
		@foreach($errors->all() as $error)
		<script>
			toastr.error("{{ $error }}")
		</script>
		@endforeach
	@endif
@append