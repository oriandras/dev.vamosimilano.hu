<div class="container mb-5">zzz
	<div class="row">
		<div class="col-md-6 px-0">
			<img class="img-fluid" src="https://vamosimilano.hu/images/1200x800.png" style="background:red;">
		</div>
		<div class="col-md-6 px-0">
			<img class="img-fluid" src="https://vamosimilano.hu/images/01.png" style="background:pink;">
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 px-0">
			<img class="img-fluid" src="https://vamosimilano.hu/images/1200x800.png" style="background:green;">
		</div>
		<div class="col-md-4 px-0">
			<img class="img-fluid" src="https://vamosimilano.hu/images/1200x800.png" style="background:lime;">
		</div>
		<div class="col-md-4 px-0">
			<img class="img-fluid" src="https://vamosimilano.hu/images/1200x800.png" style="background:blue;">
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 px-0">
			<img class="img-fluid" src="https://vamosimilano.hu/images/1200x800.png" style="background:navy;">
		</div>
		<div class="col-md-4 px-0">
			<img class="img-fluid" src="https://vamosimilano.hu/images/1200x800.png" style="background:lightblue;">
		</div>
		<div class="col-md-4 px-0">
			<img class="img-fluid" src="https://vamosimilano.hu/images/1200x800.png" style="background:yellow;">
		</div>
	</div>
</div>
<div class="container-fluid mb-5">
	<div class="row">
		<div class="col-md-6 px-0">
			<img class="img-fluid" src="https://vamosimilano.hu/images/1200x800.png" style="background:red;">
		</div>
		<div class="col-md-6 px-0">
			<img class="img-fluid" src="https://vamosimilano.hu/images/1200x800.png" style="background:pink;">
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 px-0">
			<img class="img-fluid" src="https://vamosimilano.hu/images/1200x800.png" style="background:green;">
		</div>
		<div class="col-md-4 px-0">
			<img class="img-fluid" src="https://vamosimilano.hu/images/1200x800.png" style="background:lime;">
		</div>
		<div class="col-md-4 px-0">
			<img class="img-fluid" src="https://vamosimilano.hu/images/1200x800.png" style="background:blue;">
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 px-0">
			<img class="img-fluid" src="https://vamosimilano.hu/images/1200x800.png" style="background:navy;">
		</div>
		<div class="col-md-4 px-0">
			<img class="img-fluid" src="https://vamosimilano.hu/images/1200x800.png" style="background:lightblue;">
		</div>
		<div class="col-md-4 px-0">
			<img class="img-fluid" src="https://vamosimilano.hu/images/1200x800.png" style="background:yellow;">
		</div>
	</div>
</div>
<br><br><br><br><br><br><br><br>
<div class="container-fluid px-0" id="welcome">
<?php
/*
	Full page carousel
	http://getbootstrap.com/docs/4.1/components/carousel/
*/
if( sizeof($carousel) ){
	$i_active=0;
?>
	<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<?php
			foreach($carousel as $item){
				//$titles = $item->getFieldNames();
			?>
			<div class="carousel-item{{( $i_active===0 ? ' active' : '')}}">
				<img class="img-full" src="{{ (isMobile() ? $item->getMobilImageUrl() :  $item->getImageUrl()) }}" alt="{{ $item->title }}">
			</div>
			<?php
				$i_active++;
			}//foreach
			?>
		</div>
		<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
<?php
}//if carousel
?>
</div>