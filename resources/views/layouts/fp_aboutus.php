<div class="container-fluid py-3">
	<div class="row">
		<div class="col-md-4">
			<img src="https://vamosimilano.hu/images/minoseg.jpg" alt="Vamosi minőség" class="img-fluid pb-2 mx-auto">
			<h2 class="h3-responsive title text-uppercase">Vamosi minőség</h2>
			<p>A Vamosi Olaszországban tervezett, uniós jogvédelemmel rendelkező bútorait magas minőséggel, az ISO szabványnak megfelelően gyártjuk Magyarországon és Portugáliában. A vadonatúj, high-end technológiával készített, különösen kényelmes ülőfelületeknek köszönhetően akár 10 év garanciát adunk a bútorok vázára és választástól függően akár 5 évet a szövetekre.</p>
		</div>
		<div class="col-md-4">
			<img src="https://vamosimilano.hu/images/egyediseg.jpg" alt="Vamosi egyediség" class="img-fluid pb-2 mx-auto">
			<h2 class="h3-responsive title text-uppercase">Vamosi egyediség</h2>
			<p>A Vamosi közel minden bútorát igénye szerint rendelheti, egyedi méret és színkombinációban, akár 800 szövet közül választva. Később is friss megjelenést kölcsönözne bútorának? Vásárolja meg kiterjesztett jótállásunkat, így kérésére bármikor önköltségen átkárpitozzuk kanapéját vagy franciaágyát, ha új színekkel szeretné feldobni otthonát! Tervezgessen, játsszon a színekkel és rendelje meg valóban személyre szabott bútorát!</p>
		</div>
		<div class="col-md-4">
			<img src="https://vamosimilano.hu/images/elmeny.jpg" alt="Vamosi élmény" class="img-fluid pb-2 mx-auto">
			<h2 class="h3-responsive title text-uppercase">Vamosi élmény</h2>
			<p>A Vamosi bútorokat akár 30-70%-os kedvezménnyel rendelheti meg úgy, hogy elegendő csupán 30%-ot befizetnie vagy igénybe veheti <a href="https://vamosimilano.hu/aruhitel">önerő nélküli áruhitelünket is</a>!<a href="#csillag">*</a> A bútorokra akár 10 év vázgaranciát vállalunk, megrendelését pedig az egész ország területén ingyen házhoz szállítjuk*. Természetesen bútorát külön kérésre össze is szereljük!</p>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<h2 class="h3-responsive title text-uppercase">A Vamosi-ról</h2>
		</div>
		<div class="col-md-9">
			<p>A Vamosi Milano elhozza az igazi olasz stílust az otthonába. Kanapéink, franciaágyaink és kiegészítőink mind a trendi design, a praktikum és a prémium minőség megtestesítői. Hamisítatlan, olasz hangulatot árasztó bútoraink egyedi méretekben és színben rendelhetők, így azok tökéletesen személyre szabva ékesítik otthonát!</p>
			<p>Válassza ki az Önnek leginkább tetsző formát, válogasson sok száz, varázslatosan szép szövetünk közül és rendelje meg nappalija vagy hálószobája legújabb darabját most!</p>
		</div>
		<div class="col-md-3">
			<img src="https://vamosimilano.hu/images/vamosilogo.png" alt="Vamosi Milano" class="img-fluid">
		</div>
	</div>
</div>