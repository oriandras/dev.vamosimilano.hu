<?php if(isset($headversion) && $headversion==="newhead"){ ?>
<?php /* Desktop design 992px felett */ ?>
<div class="container py-3 d-none d-lg-block">
	<div class="row">
		<div class="col-md-5">
			<p class="white-text mt-3 mb-0">
				<a href="javascript:void(0);" data-activates="slide-out" class="button-collapse" onClick="sideNavContent('menu')"><i class="fal fa-bars mr-3 white-text" aria-hidden="true"></i></a> 
				<a href="{{ url('/kiemelt-akciok') }}"><strong class="mx-lg-3 white-text">Akciók</strong></a> 
				<a href="{{ url('/termekek/outlet') }}"><strong class="mx-lg-3 white-text">Outlet</strong></a> 
				<a href="{{ url('/blog') }}"><strong class="mx-lg-3 white-text">Blog</strong></a> 
				<a href="{{ url('/kapcsolat') }}"><strong class="mx-lg-3 white-text">Kapcsolat</strong></a>
			</p>
		</div>
		<div class="col-md-2 brand-logo text-center px-0">
			<a href="{{ url('/') }}" title="{{ t('Főoldal') }}" rel="home">
				<img src="{{ asset('media/logo/w/vamosimilano-inline.png') }}" alt="{{ t('project_name', 'project') }}" class="img-fluid p-0">
			</a>
		</div>
		<div class="col-md-5 md-form text-right white-text form-inline my-0 active-primary d-flex justify-content-end">
			<form method="get" action="{{ url('/kereses') }}" class="w-50">
			<?php
				if( isset($_GET["q"]) && $_GET["q"]!=""){
					$toggle="vi";
					$value='value="'.$_GET['q'].'"';
				}
				else {
					$toggle="iv invisible";
					$value="";
				}
			?>
				<input class="form-control form-control-sm py-0 search-form white-text animated {{$toggle}}" type="text" name="q" id="searchform" {{$value}}>
			</form>
			<a href="javascript:void(0)" id="searchtoggle" class=""><i class="fa fa-search mx-1 mx-lg-3 white-text" aria-hidden="true"></i></a> 
			<a href="{{ url('/termekek/favorit') }}" class="white-text"><i class="fas fa-heart mx-1 mx-lg-3" aria-hidden="true"></i></a> 
			@if (Auth::user())
			<a href="{{ action('ProfileController@index') }}" class="white-text"><i class="fal fa-user-check mx-1 mx-lg-3"></i></a>
			@else
			<a href="{{ action('AuthController@login') }}" class="white-text"><i class="far fa-sign-in-alt mx-1 mx-lg-3" aria-hidden="true"></i></a>
			@endif
			<?php $content = Cart::getContent(); ?>
			<a href="javascript:void(0)" class="white-text button-collapse" data-activates="slide-out" onClick="sideNavContent('cart')">
				<i class="far fa-shopping-bag mx-1 ml-lg-3" aria-hidden="true"></i>
				@if(Cart::countItems())
				<span class="badge badge-pill orange darken-3 mb-3">{{Cart::countItems()}}</span>
				@endif
			</a>
		</div>
	</div>
	<nav class="row">
		<div class="w-10 text-center white-text ml-auto sofont-ribbon" data-tipus="egyenes">
			<a href="{{ url('/termekek/kanape/egyenes-kanapek') }}">
				<p class="white-text head-menu-item m-0"><i class="icon-vonal-egyenes"></i></p>
				<p class="white-text head-menu-caption">{{ t('Egyenes kanapék') }}</p>
			</a>
		</div>
		<div class="w-10 text-center white-text sofont-ribbon" data-tipus="lalak">
			<a href="{{ url('/termekek/kanape/l-alaku-kanapek') }}">
				<p class="white-text head-menu-item m-0"><i class="icon-vonal-lalak"></i></p>
				<p class="white-text head-menu-caption">{{ t('Sarokkanapék') }}</p>
			</a>
		</div>
		<div class="w-10 text-center white-text sofont-ribbon" data-tipus="ualak">
			<a href="{{ url('/termekek/kanape/u-alaku-kanapek') }}">
				<p class="white-text head-menu-item m-0"><i class="icon-vonal-ualak"></i></p>
				<p class="white-text head-menu-caption">{{ t('U-alakú kanapék') }}</p>
			</a>
		</div>
		<div class="w-10 text-center white-text sofont-ribbon" data-tipus="agy">
			<a href="{{ url('/termekek/kanape/franciaagyak') }}">
				<p class="white-text head-menu-item m-0"><i class="icon-vonal-agy"></i></p>
				<p class="white-text head-menu-caption">{{ t('Franciaágyak') }}</p>
			</a>
		</div>
		<div class="w-10 text-center white-text sofont-ribbon" data-tipus="matrac">
			<a href="{{ url('/termekek/matracok') }}">
				<p class="white-text head-menu-item m-0"><i class="icon-vonal-matrac"></i></p>
				<p class="white-text head-menu-caption">{{ t('Matracok') }}</p>
			</a>
		</div>
		<div class="w-10 text-center white-text sofont-ribbon" data-tipus="fotel">
			<a href="{{ url('/termekek/kiegeszito/fotel') }}">
				<p class="white-text head-menu-item m-0"><i class="icon-vonal-fotel"></i></p>
				<p class="white-text head-menu-caption">{{ t('Fotelek') }}</p>
			</a>
		</div>
		<div class="w-10 text-center white-text sofont-ribbon" data-tipus="puff">
			<a href="{{ url('/termekek/kiegeszito/puff') }}">
				<p class="white-text head-menu-item m-0"><i class="icon-vonal-puff"></i></p>
				<p class="white-text head-menu-caption">{{ t('Puffok') }}</p>
			</a>
		</div>
		<div class="w-10 text-center white-text sofont-ribbon" data-tipus="asztal">
			<a href="{{ url('/termekek/kiegeszito/dohanyzoasztal') }}">
				<p class="white-text head-menu-item m-0"><i class="icon-vonal-asztal"></i></p>
				<p class="white-text head-menu-caption">{{ t('Dohányzóasztal') }}</p>
			</a>
		</div>
		<div class="w-10 text-center white-text sofont-ribbon" data-tipus="eloszoba">
			<a href="{{ url('/termekek/kiegeszito/eloszobabutor') }}">
				<p class="white-text head-menu-item m-0"><i class="icon-vonal-eloszoba"></i></p>
				<p class="white-text head-menu-caption">{{ t('Előszobafal') }}</p>
			</a>
		</div>
		<div class="w-10 text-center white-text mr-auto sofont-ribbon" data-tipus="lampa">
			<a href="{{ url('/termekek/lampahaz') }}">
				<p class="white-text head-menu-item m-0"><i class="icon-vonal-lampa"></i></p>
				<p class="white-text head-menu-caption">{{ t('Lámpák') }}</p>
			</a>
		</div>
	</nav>
</div>
<?php /* Tablet és mobil design 992 px alatt */ ?>
<div class="row d-lg-none no-gutters">
	<div class="col col-6">
		<p class="pt-3">
		<a href="javascript:void(0);" data-activates="slide-out" class="button-collapse" onClick="sideNavContent('menu')">
			<i class="fal fa-bars mr-3 white-text" aria-hidden="true"></i>
		</a>
		<a class="mt-3" href="{{ url('/') }}">
			<img src="{{ asset('media/logo/w/vamosimilano-inline.png') }}" alt="{{ t('project_name', 'project') }}" class="img-fluid" style="max-height: 1rem;">
		</a>
		</p>
	</div>
	<div class="col col-6">
		<p class="text-right pt-3">
			<a href="{{ url('/termekek/favorit') }}" class="white-text">
				<i class="fas ml-2 fa-heart" aria-hidden="true"></i>
			</a>
			@if (Auth::user())
			<a href="{{ action('ProfileController@index') }}" class="white-text">
				<i class="far ml-2 fa-sign-out-alt "></i>
			</a>
			@else
			<a href="{{ action('AuthController@login') }}" class="white-text">
				<i class="far ml-2 fa-sign-in-alt" aria-hidden="true"></i>
			</a>
			@endif		
			<a href="javascript:void(0)" class="button-collapse white-text" data-activates="slide-out" onClick="sideNavContent('cart')">
				<i class="far ml-2 fa-shopping-bag" aria-hidden="true"></i>
				@if(Cart::countItems())
				<span class="badge badge-pill orange darken-3 mb-3">{{Cart::countItems()}}</span>
				@endif
			</a>
		</p>
	</div>
</div>
<?php } else { ?>
<div class="header content">
                <span class="action nav-toggle" data-action="toggle-nav"><span>{{ t('Navigation') }}</span></span>
                <strong class="logo">
                    <a href="{{ url('/') }}" title="{{ t('Főoldal') }}" rel="home"><img alt="{{ t('project_name', 'project') }}" src="{{ asset('images/vamosilogo.png') }}">
                    </a>
                </strong>
                <div class="sections nav-sections">
                    <div  class="section-items nav-sections-items">
                        <div  class="section-item-title nav-sections-item-title" onclick="$('html').removeClass('nav-open');$('html').removeClass('nav-before-open');">
                            <a href="javascript:" data-action="toggle-nav" class="nav-sections-item-switch action toggle-nav"><i class="fa fa-bars menu-icon"></i></a>
                        </div>
                        <div data-role="content" id="store.menu" class="section-item-content nav-sections-item-content">
                            <nav role="navigation" class="navigation sw-megamenu">
                                <div  id="mobile-search" class="section-item-content nav-sections-item-content margintop40" style="display: none;">
                                <form method="get" action="{{ action('WebshopController@search') }}" class="form minisearch">
                                    <div class="field search">
                                        <div class="control">
                                            <input type="text" autocomplete="off" aria-autocomplete="both" aria-haspopup="false" aria-expanded="false" role="combobox" maxlength="128" class="input-text" placeholder="{{ t('Írja be a keresőszót') }}" value="" name="q">
                                        </div>
                                    </div>
                                </form>
                                </div>
                                 <div class="clearfix mobile-view"></div>
                                <ul>

                                    <li class="ui-menu-item level0 classic">
                                        <a class="level-top" rel="home" href="{{ url('/') }}" title="{{ t('project_name', 'project') }}"><span>{{ t('Főoldal') }}</span></a>
                                    </li>




                                    <li class="ui-menu-item level0 fullwidth parent ">
                                        <div class="open-children-toggle" id="one-item-click"></div><a class="level-top" href="{{ action('WebshopController@product', '') }}"><span>{{ t('Termékkategóriák') }}</span> <i class="fa fa-sort-desc desktop-view" aria-hidden="true"></i></a>
                                        <div class="level0 submenu">
                                            <div class="menu-top-block">
                                                <div class="menu-top-product">
                                                <strong >{{ t('Legkeresettebb') }}:</strong>
                                                    @foreach (ProductCategory::menuHighLight() as $top_category)
                                                            <a href="{{ $top_category->getUrl() }}" title="{{ $top_category->name }}">{{ $top_category->name }}</a>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="row">
                                                <ul class="subchildmenu col-sm-8 mega-columns columns3">

                                                    @foreach (ProductCategory::getMenu(0) as $category)
                                                        <li class="ui-menu-item level1 parent ">
                                                            <div class="open-children-toggle"></div>
                                                            <a href="{{ $category['data']->getUrl() }}"><span>{{ $category['data']->name }}</span></a>
                                                            @if (isset($category['child']))

                                                            <ul class="subchildmenu ">
                                                                @foreach ($category['child'] as $child)
                                                                 <li class="ui-menu-item level2 "><a href="{{ $child['data']->getUrl() }}"><span>{{ $child['data']->name }}</span></a>
                                                                </li>

                                                                @endforeach
                                                            </ul>
                                                            @endif
                                                        </li>

                                                    @endforeach
													<li class="ui-menu-item level1 parent ">
													<a href="/szovetek"><span>Szövetek</span></a>
													</li>


                                                </ul>

                                                @if (isset($highlighted_products) and sizeof($highlighted_products)>0)
                                                <div class="menu-right-block col-sm-4">
                                                    <?php
                                                        $highlighted_product = $highlighted_products[rand(0,(count($highlighted_products)-1))];
                                                    ?>
                                                    <div class="menu-right-block">
                                                    <a href="{{$highlighted_product->getUrl()}}" title="{{$highlighted_product->getName()}}"><img alt="{{$highlighted_product->getName()}}" src="{{ $highlighted_product->getDefaultImageUrl(600) }}"/></a>
                                                        <div class="div-container">
                                                            <h2>{{ t('A hónap bútora') }}</h2>
                                                            <a href="{{$highlighted_product->getUrl()}}" title="{{$highlighted_product->getName()}}" class="btn btn-default">{{ t('Hatalmas kedveménnyel') }} <em class="porto-icon-right-dir"></em></a>															
                                                        </div>
														<div class="highlight-desc">{{$highlighted_product->getName()}} 
															<span class="old-price athuzva">{{ money($highlighted_product->getDefaultFullPrice()) }}</span>
															<span class="price">{{ money($highlighted_product->getDefaultPrice()) }}</span>
														</div>
														@if ($highlighted_product->getPercentPrice())
														<div style="position:absolute;top:40px;right:10px;border-radius:15px;background-color:#CD7126;">
														<a href="{{$highlighted_product->getUrl()}}" title="{{$highlighted_product->getName()}}"><span style="font-wight:bold;font-size:25px;color:white">- {{ $highlighted_product->getPercentPrice() }} %</span></a>
														</div>
														@endif
                                                    </div>
                                                </div>
												@else
												<div class="col-sm-4" style="overflow: hidden;top:-40px;">
													<div class="div-container" style="overflow: hidden;padding: 0;">
														<img src="https://vamosimilano.hu/static/mar.jpg" style="width: 100%; max-width: 392px; height: auto; margin: 0px; padding: 0px;" alt="Kanapé">
													</div>
												</div>
                                                @endif
                                            </div>
                                        </div>
                                    </li>
									@if(sizeof($highlighted_products)>0)
                                    <li class="ui-menu-item level0 fullwidth"><a class="level-top" href="{{ action('WebshopController@sales') }}" title="{{ t('A hónap Bútorai') }}"><span>{{ t('A hónap Bútorai') }}</span></a>
                                    </li>
									@endif
									<li class="ui-menu-item level0 fullwidth"><a class="level-top" href="/termekek/outlet" title="{{ t('Outlet') }}"><span>{{ t('outlet') }}</span></a>
                                    </li>


                                    <li class="ui-menu-item level0 classic parent fl-right ">
                                        <div class="open-children-toggle"></div><a class="level-top" href="{{ action('CmsController@index', t('rolunk', 'url')) }}"><span>{{ t('Rólunk') }}</span> <i class="fa fa-sort-desc desktop-view" aria-hidden="true"></i></a>
                                        <div class="level0 submenu submenuleft">
                                            <div class="row">
                                                <ul class="subchildmenu ">
                                                    <li class="ui-menu-item level1 "><a href="{{ action('CmsController@index', t('rolunk', 'url')) }}" title="{{ t('A Bella Italia Bútorház') }}"><span>{{ t('A Bella Italia Bútorház') }}</span></a>
                                                    </li>
                                                    <li class="ui-menu-item level1 "><a href="{{ action('CmsController@index', t('bemutatotermeink', 'url')) }}" title="{{ t('Üzleteink') }}"><span>{{ t('Üzleteink') }}</span></a>
                                                    </li>
													@if (App::getLocale()!='en')
                                                    <li class="ui-menu-item level1 "><a href="http://karrier.vamosimilano.hu" title="{{ t('Karrier') }}"><span>{{ t('Karrier') }}</span></a>
                                                    </li>
													@endif
                                                </ul>
                                            </div>
                                        </div>
                                    </li>

									<li class="ui-menu-item level0 fullwidth fl-right"><a class="level-top" href="{{ action('BlogController@index') }}" title="{{ t('Blog') }}"><span>{{ t('Blog') }}</span></a>
									@if (App::getLocale()!='en')
									<li class="ui-menu-item level0 fullwidth fl-right"><a class="level-top" href="http://blog.vamosimilano.hu" title="{{ t('Blog') }}"><span>{{ t('Blog') }}</span></a>
                                    </li>
									@endif
                                    <li class="ui-menu-item level0 classic parent fl-right">
                                        <div class="open-children-toggle"></div><a class="level-top" href="{{ action('CmsController@index', t('hazhozszallitas', 'url')) }}"><span>{{ t('Fontos tudnivalók') }}</span> <i class="fa fa-sort-desc desktop-view" aria-hidden="true"></i></a>
                                        <div class="level0 submenu submenuleft">
                                            <div class="row">
                                                <ul class="subchildmenu">
                                                    <?php
                                                        /*<li class="ui-menu-item level1 "><a href="https://static.vamosimilano.hu/loancalculator/" title="{{ t('Hitelfeltételek') }}"><span>{{ t('Hitelfeltételek') }}</span></a>
                                                    </li>*/
                                                    ?>
                                                    <li class="ui-menu-item level1 "><a target="_blank" href="{{ action('CmsController@index', t('hazhozszallitas', 'url')) }}" title="{{ t('Szállítási feltételek') }}"><span>{{ t('Szállítási feltételek') }}</span></a>
                                                    </li>
                                                     <li class="ui-menu-item level1 "><a target="_blank" href="{{ action('CmsController@index', t('adatvedelem', 'url')) }}" title="{{ t('Áruhitel') }}"><span>{{ t('Adatvédelmi nyilatkozat') }}</span></a>
                                                    </li>
                                                     <li class="ui-menu-item level1 "><a target="_blank" href="{{ action('CmsController@index', t('aszf', 'url')) }}" title="{{ t('ÁSZF') }}"><span>{{ t('ÁSZF') }}</span></a>
                                                    </li>

                                                </ul>
                                            </div>
                                        </div>
                                    </li>
									@if (App::getLocale()!='en')
                                    <li class="ui-menu-item level0 fullwidth fl-right"><a class="level-top" href="{{ action('CmsController@credit') }}" title="{{ t('Áruhitel') }}"><span>{{ t('Áruhitel') }}</span></a>
                                    </li>
									@endif




                                  </ul>
                            </nav>
                            <script type="text/javascript">
                                require([
                                    'jquery',
                                    'js/sw_megamenu'
                                ], function($) {
                                    $(".sw-megamenu").swMegamenu();
                                });
                            </script>


                        </div>

                        <div data-role="collapsible" class="section-item-title nav-sections-item-title">
                            @if (Auth::user())
                                <a href="{{ action('ProfileController@index') }}" data-toggle="switch" class="nav-sections-item-switch"><i class="fa fa-user menu-icon"></i></a>
                            @else
                                <a href="{{ action('AuthController@login') }}" data-toggle="switch" class="nav-sections-item-switch"><i class="fa fa-user menu-icon"></i></a>
                            @endif
                        </div>

                        <div data-role="collapsible" class="section-item-title nav-sections-item-title">
                            <a onclick="$('#mobile-search').slideToggle();" data-toggle="switch" class="nav-sections-item-switch"><i class="fa fa-search menu-icon"></i></a>
                        </div>
                    </div>
                </div>
            </div>
<?php } ?>