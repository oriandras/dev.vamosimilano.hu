<!doctype html>
<html lang="{{ getShopCode() }}">

<head>

    <meta charset="utf-8" />
    <meta name="keywords" content="vamosi milano" />
    <meta name="robots" content="INDEX,FOLLOW" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" />
    <title>{{ t('project_name','project') }} | {{ Seo::getTitle() }}</title>
    <meta name="description" content="{{ Seo::getDescription() }}" />
	<meta name="author" content="Vamosi Milano"/>
	<link rel="canonical" href="{{ Request::url() }}" />
    <meta property="og:type" content="{{ Seo::getType() }}" />
    <meta property="og:title" content="{{ Seo::getTitle() }}" />
    <meta property="og:image" content="{{ Seo::getImage() }}" />
    <meta property="og:image:secure_url" content="{{ Seo::getImage() }}" />
    <meta property="og:description" content="{{ Seo::getDescription() }}" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:site_name" content="{{ str_replace(['http://','https://'], "", Request::root()) }}" />



    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/styles-m.css') }}" />
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/bootstrap-theme.css') }}" />

    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/transitions.css') }}" />
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/porto-icons-codes.css') }}" />
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/animation.css') }}" />
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/custom.css') }}" />
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/magnific-popup.css') }}" />
        <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" media="screen and (min-width: 768px)" href="{{ asset('css/styles-l.css') }}" />
    <link rel="stylesheet" type="text/css" media="print" href="{{ asset('css/print.css') }}" />
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/theme.css') }}?v3" />


    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicons/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicons/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicons/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicons/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicons/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicons/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicons/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicons/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicons/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicons/android-icon-192x192.png') }}?v2">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicons/favicon-32x32.png') }}?v2">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicons/favicon-96x96.png') }}?v2">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicons/favicon-16x16.png') }}?v2">
    <link rel="manifest" href="{{ asset('favicons/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('favicons/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
	
	<?php if (isset($order->order_number) and isset($order->payable_price) and ($order->order_number > 0) and ($order->payable_price > 0)) { ?>
	<script>
	window.dataLayer = window.dataLayer || []
	dataLayer.push({
	   'transactionId': '{{ $order->order_number }}',
	   'transactionTotal': {{ $order->payable_price }}
	   <?php /*
	   //'transactionAffiliation': 'www.vamosimilano.hu', //partner or store - nem k�telez�
	   //'transactionTax': 1.29, //ad� tartalma - nem k�zlez�
	   //'transactionShipping': 5, //sz�ll�t�si k�lt�sg  - nem k�telez�
	   //eladott term�kek - nem k�telez�
	   'transactionProducts': [{
		   'sku': 'DD44',
		   'name': 'T-Shirt',
		   'category': 'Apparel',
		   'price': 11.99,
		   'quantity': 1
	   },{
		   'sku': 'AA1243544',
		   'name': 'Socks',
		   'category': 'Apparel',
		   'price': 9.99,
		   'quantity': 2
	   }]
	   */ ?>
	});
	</script>
	<?php } ?>

    <!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','{{ Config::get('website.tagmanager') }}');</script>
	<!-- End Google Tag Manager -->

    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Shadows+Into+Light" />
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2C400%2C400italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&amp;v1&amp;subset=latin%2Clatin-ext" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/design_demo11_en.css') }}">
    <link rel="stylesheet" type="text/css" media="all" href="{{ asset('css/settings_demo11_en.css') }}">


    <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>

    <script src="/unity{{(isMobile() ? '-mobile' : '')}}/TemplateData/UnityProgress.js?v4"></script>

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    </head>

<body data-container="body" class="{{ $body_class }}">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T5D89CZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
        <div id="fb-root"></div>
		<script>
	      window.fbAsyncInit = function() {
	        FB.init({
	          appId      : '167404730371647',
	          xfbml      : true,
	          version    : 'v2.1'
	        });
	      };

	      (function(d, s, id){
	         var js, fjs = d.getElementsByTagName(s)[0];
	         if (d.getElementById(id)) {return;}
	         js = d.createElement(s); js.id = id;
	         js.src = "//connect.facebook.net/hu_HU/sdk.js";
	         fjs.parentNode.insertBefore(js, fjs);
	       }(document, 'script', 'facebook-jssdk'));

	    </script>
    <div class="page-wrapper">





        @yield('content')






    </div>



     @yield('footer_js')



</body>

</html>
