<span id="searchtoggle"><i class="fa fa-search" aria-hidden="true"></i> {{ t('Keresés') }}</span>
<div class="search-form animated hidden">
	<form method="get" action="{{ action('WebshopController@search') }}">
	<input type="text" class="searchform" autocomplete="off" placeholder="{{ t('Írja be a keresőszót') }}" value="" maxlength="128" name="q" required>
		<button type="submit" class="searchbutton"><i class="fa fa-search" aria-hidden="true"></i></button>
	</form>
</div>