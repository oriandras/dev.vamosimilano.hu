<?php /* Új verzió */
if(isset($headversion) && $headversion==="newhead"){
?>
<!doctype html>
<html lang="{{ getShopCode() }}">
<head>
<!-- Google Tag Manager -->

<!-- End Google Tag Manager -->
	<meta charset="utf-8" />
	<meta name="robots" content="INDEX,FOLLOW" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" />
@if ($_SERVER["REQUEST_URI"]=="/")
	<title>{{ t('project_name','project') }} | {{ Seo::getTitle() }}</title>
@else
	<title>{{ Seo::getTitle() }} | {{ t('project_name','project') }}</title>
@endif
	<meta name="description" content="{{ Seo::getDescription() }}" />
	<meta name="author" content="Vamosi Milano"/>
	
	<link rel="canonical" href="{{ Request::url() }}" />
	<meta property="fb:app_id" content="279645012445636" />
	<meta property="og:type" content="{{ Seo::getType() }}" />
	<meta property="og:title" content="{{ Seo::getTitle() }}" />
	<meta property="og:image" content="{{ Seo::getImage() }}" />
	<meta property="og:image:secure_url" content="{{ Seo::getSSLImage() }}" />
	<meta property="og:description" content="{{ Seo::getDescription() }}" />
	<meta property="og:url" content="{{ Request::url() }}" />
	<meta property="og:site_name" content="{{ str_replace(['http://','https://'], "", Request::root()) }}" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	
	<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicons/apple-icon-57x57.png') }}">
	<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicons/apple-icon-60x60.png') }}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicons/apple-icon-72x72.png') }}">
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicons/apple-icon-76x76.png') }}">
	<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicons/apple-icon-114x114.png') }}">
	<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicons/apple-icon-120x120.png') }}">
	<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicons/apple-icon-144x144.png') }}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicons/apple-icon-152x152.png') }}">
	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicons/apple-icon-180x180.png') }}">
	<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicons/android-icon-192x192.png') }}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicons/favicon-32x32.png') }}">
	<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicons/favicon-96x96.png') }}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicons/favicon-16x16.png') }}">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="{{ asset('favicons/ms-icon-144x144.png') }}">
	<meta name="theme-color" content="#ffffff">
	<script>
		 var require = {
			"baseUrl": "{{ url('/') }}"
		 };
		var dataLayer=[];
	</script>
<?
/*
	Gyorsítási funkció, ha nem régi típusú böngésző, akkor a css fájlokat javascripttel aktiválom az oldaltöltés után
*/
$user_agent = '';
if (isset($_SERVER['HTTP_USER_AGENT'])) $user_agent = $_SERVER['HTTP_USER_AGENT'];
if (stripos( $user_agent, 'Chrome') !== false){ //Chrome
?>
	<link rel="preload" href="{{ asset('css2/bootstrap.min.css') }}" as="style" onload="this.rel='stylesheet'">
	<link rel="preload" href="{{ asset('css2/mdb.min.css') }}" as="style" onload="this.rel='stylesheet'">
	<link rel="preload" href="{{ asset('css2/custom.css') }}?refr={{date('Ymdhis')}}" as="style" onload="this.rel='stylesheet'">	
<?php
}
elseif (stripos( $user_agent, 'Safari') !== false) { //Safari
?>
	<link rel="stylesheet" type="text/css" media="all" href="{{ asset('css2/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" media="all" href="{{ asset('css2/mdb.min.css') }}">
	<link rel="stylesheet" type="text/css" media="all" href="{{ asset('css2/custom.css') }}?refr={{date('Ymdhis')}}">
<?php
}
elseif (stripos( $user_agent, 'Firefox') !== false) { //Safari
?>
	<link rel="stylesheet" type="text/css" media="all" href="{{ asset('css2/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" media="all" href="{{ asset('css2/mdb.min.css') }}">
	<link rel="stylesheet" type="text/css" media="all" href="{{ asset('css2/custom.css') }}?refr={{date('Ymdhis')}}">
<?php
}
else { //minden más
?>
	<link rel="preload" href="{{ asset('css2/bootstrap.min.css') }}" as="style" onload="this.rel='stylesheet'">
	<link rel="preload" href="{{ asset('css2/mdb.min.css') }}" as="style" onload="this.rel='stylesheet'">
	<link rel="preload" href="{{ asset('css2/custom.css') }}" as="style" onload="this.rel='stylesheet'">		

<?php
}
?>
<?php
/*
Kedvencek kezelése
*/
	if( !isset($_COOKIE["wishlist"]) ){
		$replacecharacters=array(" ", ".");
		$uid = "9".str_replace($replacecharacters, "", microtime());
		setcookie("wishlist", $uid, time()+(86400*365));
		$_SESSION["wishlist"] = $uid;
		//header("Location: ".$_SERVER['REQUEST_URI']);
	}
	else {
		$_SESSION["wishlist"]=$_COOKIE["wishlist"];
	}
/*
A tagmanager triggereket elindítom, ha elfogadta a cookiekat.
*/
if(isset($_COOKIE["stat_cookie"]) && $_COOKIE["stat_cookie"]==="no"){ ?>
	<script type="text/javascript">
	dataLayer.push({
		stat_cookie: "no"
	});	
	</script>
<?php } else { ?>
	<script type="text/javascript">
	dataLayer.push({
		stat_cookie: "yes"
	});	
	</script>
<?php } //endif
if(isset($_COOKIE["marketing_cookie"]) && $_COOKIE["marketing_cookie"]==="yes") { ?>
	<script type="text/javascript">
	dataLayer.push({
		marketing_cookie: "yes"
	});	
	</script>
<?php
} else {
?>
	<script type="text/javascript">
	dataLayer.push({
		marketing_cookie: "no"
	});	
	</script>
<?php } //endif ?>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','{{ Config::get("website.tagmanager") }}');</script>
	<!-- End Google Tag Manager -->
</head>
<!--<body data-container="body" class="{{ $body_class }}" onLoad="pageReady()">-->
<body data-container="body" class="{{ $body_class }}">
<?php //Google Tagmanager Noscript ?>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{ Config::get('website.tagmanager') }}" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<?php //Facebook követőkód ?>
<script>
  window.fbAsyncInit = function() {
	FB.init({
		appId	: '167404730371647',
		xfbml	: true,
		version	: 'v2.1'
	});
};
<?php $fblocale = strtolower(App::getLocale())."_".strtoupper(App::getLocale()); ?>
	(function(d, s, id){
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/{{ $fblocale }}/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
	<div class="page-wrapper">
	@include('layouts.header')
	@yield('content')
	@include('blocks.newsletter')
	@include('layouts.footer2_'.App::getLocale())
	
		<div class="preload"></div>
	<script type="text/javascript" src="{{ asset('js2/jquery-3.3.1.min.js') }}?v=<?php echo date('Ymdhis'); ?>"></script>
	<script type="text/javascript" src="{{ asset('js2/popper.min.js') }}?v=<?php echo date('Ymdhis'); ?>"></script>
	<script type="text/javascript" src="{{ asset('js2/bootstrap.min.js') }}?v=<?php echo date('Ymdhis'); ?>"></script>
	<script type="text/javascript" src="{{ asset('js2/mdb.min.js') }}?v=<?php echo date('Ymdhis'); ?>"></script>
	<script type="text/javascript" src="{{ asset('js2/custom.js') }}?v=<?php echo date('Ymdhis'); ?>"></script>
	@yield('footer_js')
	<div class="container-fluid">
		<div class="row" id="devtranslatecheck">
		@if (Session::has('not_found_lang') and Config::get('app.debug'))
			<div class="col-md-6 pt-3" style="background: red; color: white;">
				<p>
				{{ implode('<br>', Session::get('not_found_lang')) }},
				<?php 
				Session::forget('not_found_lang');
				?>
				</p>
			</div>
		@endif
		@if (Session::has('not_found_perm') and Config::get('app.debug'))
			<div class="col-md-6 pt-3" style="background: orange; color: black;">
				{{ implode('<br>', Session::get('not_found_perm')) }},
				<?php
				Session::forget('not_found_perm');
				?>
			</div>
		@endif
		</div>
	</div>
<input type="hidden" id="default_data" data-curr="{{ Config::get('shop.'.getShopCode().'.shop_currency') }}">
</body>
</html>
	<?php
/* Régi verzió*/
} else {
?>
<!doctype html>
<html lang="{{ getShopCode() }}">
<head>
	<meta charset="utf-8" />
	<meta name="robots" content="INDEX,FOLLOW" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" />
@if ($_SERVER["REQUEST_URI"]=="/")
	<title>{{ t('project_name','project') }} | {{ Seo::getTitle() }}</title>
@else
	<title>{{ Seo::getTitle() }} | {{ t('project_name','project') }}</title>
@endif
	<meta name="description" content="{{ Seo::getDescription() }}" />
	<meta name="author" content="Vamosi Milano"/>
	
	<link rel="canonical" href="{{ Request::url() }}" />
	<meta property="fb:app_id" content="279645012445636" />
	<meta property="og:type" content="{{ Seo::getType() }}" />
	<meta property="og:title" content="{{ Seo::getTitle() }}" />
	<meta property="og:image" content="{{ Seo::getImage() }}" />
	<meta property="og:image:secure_url" content="{{ Seo::getSSLImage() }}" />
	<meta property="og:description" content="{{ Seo::getDescription() }}" />
	<meta property="og:url" content="{{ Request::url() }}" />
	<meta property="og:site_name" content="{{ str_replace(['http://','https://'], "", Request::root()) }}" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<link rel="stylesheet" type="text/css" media="all" href="https://vamosimilano.hu/css/calendar.css" /> 
	<script>var dataLayer=[];</script>
<?php 
	/*
	A tagmanager triggereket elindítom, ha elfogadta a cookiekat.
	*/
	if(isset($_COOKIE["stat_cookie"]) && $_COOKIE["stat_cookie"]==="no"): ?>
		<script type="text/javascript">
		dataLayer.push({
			stat_cookie: "no"
		});	
		</script>
	<?php
	else:
	?>
		<script type="text/javascript">
		dataLayer.push({
			stat_cookie: "yes"
		});	
		</script>
	<?php
	endif;
	if(isset($_COOKIE["marketing_cookie"]) && $_COOKIE["marketing_cookie"]==="yes"): ?>
		<script type="text/javascript">
		dataLayer.push({
			marketing_cookie: "yes"
		});	
		</script>
	<?php
	else:
	?>
		<script type="text/javascript">
		dataLayer.push({
			marketing_cookie: "no"
		});	
		</script>
	<?php
	endif;
	?>
	@include('layouts.head')
	<script src="{{ asset('js/gtm.js') }}?v1"></script>
</head>
<body data-container="body" class="{{ $body_class }}">
<!-- Google Tag Manager (noscript) -->

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{ Config::get('website.tagmanager') }}"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<!-- End Google Tag Manager (noscript) -->

       <div id="fb-root"></div>

		<script>

	      window.fbAsyncInit = function() {

	        FB.init({

	          appId      : '167404730371647',

	          xfbml      : true,

	          version    : 'v2.1'

	        });

	      };

		<?

		$fblocale = strtolower(App::getLocale())."_".strtoupper(App::getLocale());

		?>

	      (function(d, s, id){

	         var js, fjs = d.getElementsByTagName(s)[0];

	         if (d.getElementById(id)) {return;}

	         js = d.createElement(s); js.id = id;

	         js.src = "//connect.facebook.net/{{ $fblocale }}/sdk.js";

	         fjs.parentNode.insertBefore(js, fjs);

	       }(document, 'script', 'facebook-jssdk'));



	    </script>

    <div class="page-wrapper">

		@include('layouts.cookietop')
        @include('layouts.header')



        @if (Request::path() == '/')

            <div class="container">

                @include('blocks.carousel')

            </div>

            <div class="container">







        @else

        <div class="container">

        @include('layouts.messages')

        </div>



        @endif




@if (Request::path() == 'aszf')
	<div class="container">
		<div class="row">
			<div class="col-md- 12">
	<?php if(date('YmdHis') > '20181125110000') { ?>
				<h4>A WORE HUNGARY KFT / Vamosi Milano</h4>
				<h1>Általános Szerződési Feltételei</h1>
				<p><small>érvényes: 2018. november 25. 11:00-tól</small></p>
				<ol>
					<li>A jelen szerződéses feltételek kerülnek alkalmazásra valamennyi, a Vamosi Milano / tulajdonos: WORE HUNGARY Kft/cégjegyzékszám: 01-09-944100, adószám: 22905192-2-43 székhely: 1119 Bp., Petzvál J. u. 46-48. /; továbbiakban: Eladó / által értékesített bútor vonatkozásában. Vevő aláírásával elfogadja az általános szerződési feltételeket.</li>
					<li>Vevő a bútort katalógusból, online felületekről / weboldal, közösségi portál /, illetve a Vamosi Milano bemutatótermében kiállított mintadarabok alapján rendeli meg. Vevőnek lehetősége van valamennyi rendelhető és a gyártás során felhasználható szövetminta személyes megtekintésére a Vamosi Milano bemutatótermében. Vevő tudomásul veszi, hogy a megrendelőlap benyújtását követően az általa kiválasztott szövetek színével, típusával, minőségével kapcsolatban kifogást nem emelhet. A weboldalon és a hirdetési felületeken látható képek illusztrációk.<br />
					<br />
					Vevő aláírásával elfogadja, hogy az elektronikus felületen megjelenő szín, ábra eltérhet a gyártáshoz ténylegesen felhasznált és a bemutatóteremben kiállított mintadarabtól. Ezen eltérések az elektronikus képmegjelenítő eszközök fajtájától is függhetnek és a megrendelni kívánt bútor kárpitozását ( színárnyalatait ), valamint a bútor arányait érinthetik ( a képen vagy bemutatóteremben feltűntetett daraboktól eltérő méretben történő megrendelés esetén ). A párnák darabszáma a megrendelt bútor méretétől függően változhat. Tekintettel arra, hogy bútoraink egyedi igényekre készülnek, egyes, a megrendelő által kiválasztott kárpitok vastagsága, valamint kárpitozási technikája és az ehhez szükséges gyártási technológia – figyelembe véve a bútor elemeinek számát is - eltérhet, így vevő aláírásával elfogadja, hogy a bútor mérete ezen eltérésekből adódóan, a megrendelőlapon szereplő mérettől összesen 1-10 cm-t eltérhet. Vevő jelen szerződés aláírásával igazolja, hogy a bútor típusát, színét, méretét, továbbá a kárpitot ellenőrizte, elfogadja, és a jelen szerződés mellékletét képező megrendelőlapon feltüntetett tulajdonságokkal kívánja a bútort megvásárolni. Vevő tudomásul veszi, hogy az általa meghatározott és a megrendelőlapon rögzített tulajdonságoktól a későbbiekben nem térhet el és az annak megfelelően előállított bútort köteles átvenni és a vételárat kiegyenlíteni.<br />
					<br />
					Vevő a megrendelőlap benyújtása előtt köteles ellenőrizni minden azon szereplő és a bútor gyártásához szükséges adatot / elérhetőség, számlázási cím, a bútor valamennyi tulajdonsága, ideértve típus, méretek, szövet, egyedi igények /. Vevő tudomásul veszi, hogy egyedi megrendelés esetén amennyiben nem határozza meg egyértelműen a bútor kiegészítő részeinek tulajdonságait, úgy Eladó jogosult azt saját hatáskörben – a termék elkészítéséhez szükséges mértékben – meghatározni.Vevő tudomásul veszi, hogy a megrendelőlap aláírásával és a 4. pontban részletezett vételárrészlet megfizetésével a megrendelés véglegessé válik, és azon ezt követően utólagos módosításra nincs lehetőség. A megrendelőlap benyújtását követően a Vevő részéről történt bármely tévedés vagy elírás miatti felelősséget Eladó kifejezetten kizárja.</li>
					<li>Amennyiben az Eladó minden gondossága ellenére hibás ár kerül a webáruház felületére, különös tekintettel a nyilvánvalóan téves, pl. a termék közismert, általánosan elfogadott vagy becsült árától jelentősen eltérő, esetleg rendszerhiba miatt megjelenő téves árra, akkor az Eladó nem köteles a terméket hibás áron szállítani, hanem felajánlhatja a helyes áron történő szállítást, amelynek ismeretében a Vevő elállhat vásárlási szándékától.</li>
					<li>Vevő tudomásul veszi, hogy a mindenkor változó akciókban meghirdetett ajándékokra kizárólag abban az esetben jogosult, amennyiben eleget tett az abban szereplő feltételeknek, továbbá, az ajándékra vonatkozó igényét a vásárlással egyidejűleg bejelentette és az a megrendelőlapon rögzítésre került. Az Eladó által havonta meghirdetett „Hónap bútora” elnevezésű akció keretében adott kedvezmény az adott hónap első napjától a hónap utolsó napjáig leadott, hatályos megrendeléssel történő vásárlás esetében érvényes. A hatályos megrendelés feltételeit az 5. pont tartalmazza.<br />
					<br />
					A mindenkor hatályos akciókban szereplő kedvezményes vételár és/vagy ajándékok és/vagy nyeremények igénybevételéhez szükséges feltételeket a meghirdetett akció szövege tartalmazza. Vevő kijelenti, hogy az akcióban szereplő feltételeket megismerte és azoknak hiánytalanul eleget tett.</li>
					<li>A megrendelés akkor hatályos, ha Vevő a bútor vételárának 30%-át, valamint az egyéb szolgáltatások - szövet, illetve méret felárak, házhozszállítás díja - 100 %-át kifizette, továbbá a megrendelőlapot aláírta. Ezen feltételtől való eltérésre kizárólag egyedi megállapodás alapján, továbbá bruttó 1.500.000,-Ft-ot elérő értékű megrendelés esetén van lehetőség. A jelen pontban rögzített, fenti előírás alól kivételt képeznek az áruhitel igénybevételével kezdeményezett megrendelések is. Áruhitel igénybevételével történő megrendelés esetén a megrendelés a hitelkérelem bank általi elfogadásával egyidejűleg válik hatályossá, és ezen időponttól kezdődően alkalmazandó valamennyi ehhez kapcsolódó és jelen ÁSZF-ben rögzített jogkövetkezmény /módosítási tilalom, kártérítési – és kötbérfizetési kötelezettség, stb./<br />
					<br />
					A vételárhátralékot Vevő a bútor átvételével egyidejűleg köteles megfizetni. Ez alól kivételt képez az ingyenes házhozszállítás igénybevétele. Ez esetben vevő a vételárhátralékot a 9. pontban foglaltak szerint köteles megfizetni a megrendelés leadásától számított 30 napon belül. Ez történhet átutalással, illetve bemutatótermeinkben készpénzes, illetve kártyás fizetéssel. &nbsp;Vevő a vételár hiánytalan megfizetésével teljesíti a szerződést. Eladó a vételárhátralék teljes megfizetéséig tulajdonjogát fenntartja.<br />
					<br />
					Vevő az Általános Szerződési Feltételek elfogadásával beleegyezik, hogy Eladó elektronikus számlát bocsát ki. Késedelmes fizetés esetén Vevő köteles megfizetni Eladó részére a Polgári Törvénykönyvről szóló 2013. évi V. törvény (a továbbiakban: Ptk.) szerinti késedelmi kamatot, továbbá a 8. pontban foglalt tárolási díjat.</li>
					<li>Elállási jog, kötbér<br />
					Tekintettel arra, hogy Eladó a megrendelt terméket Vevő kifejezett kérésére és utasításai alapján állítja elő, így Vevő a fogyasztó és a vállalkozás közötti szerződések részletes szabályairól szóló 45/2014. (II.26.) Korm. rendelet 20. §-ban foglalt elállási jogot nem gyakorolhatja a rendelet 29. § (1) bekezdés c. pontja értelmében.<br />
					<br />
					Amennyiben Vevő a megrendelést követően olyan okból áll el a vásárlástól, mely Eladónak nem felróható, Eladó – a megkezdett egyedi gyártás sajátosságaira tekintettel - kártérítésre tarthat igényt, melynek mértéke a megrendelt bútor vételárának 30%-a.<br />
					<br />
					Amennyiben Vevő a megrendelt bútort nem veszi át, vagy a vételár fennmaradó részét nem egyenlíti ki, az Eladó a 9. pontban rögzített módon jogosult eljárni. Vevő az Eladó – 9. pontban foglaltak szerinti - elállása esetén nemteljesítési kötbér fizetésére köteles, melynek mértéke a megrendelt bútor vételárának 30%-a, valamint az egyéb szolgáltatások 100 %-a. Eladó a kötbér összegének erejéig jogosult beszámítani a Vevő által teljesített vételárrészletet. Az Eladó a 7. pontban rögzített gyártási határidő be nem tartása esetén, a Vevő felé a megrendelt bútor alapára 1%-ának megfelelő összegű késedelmi kötbér fizetésére köteles a késedelemmel érintett naptári naponként, a késedelembe esés 5. munkanapjától kezdődően addig az időpontig, amikor Eladó a bútor elkészültéről igazolhatóan értesíti Vevőt. Az Eladó által fizetendő kötbér összege azonban nem haladhatja meg a bútor vételárának 30%-át. Eladó késedelmi kötbér fizetési kötelezettsége nem vonatkozik az egyedi, az Eladó által megadott általános tulajdonságokkal rendelkező bútoroktól eltérő vevői megrendelésekre.</li>
					<li>A gyártási idő a megrendelés 5. pont szerinti hatályossá válását követően kezdődik. Vevő tudomásul veszi, hogy a megrendelőlapon feltüntetett, várható gyártási idő általános, becsült intervallumot jelent, melytől eltérések előfordulhatnak.<br />
					<br />
					Eladó a végleges megrendelés benyújtásától és az 5. pontban részletezett vételárrészlet beérkezésétől számított 90 napon belül köteles a megrendelt termék elkészítésére. Vevő tudomásul veszi, hogy amennyiben a megrendelt bútor kárpitjaként egy magas kategóriájú szövetet ( 2-7 kategória ) választ, ez az egyedi megrendelésre tekintettel hosszabb gyártási időt eredményezhet. Eladó ezen esetekben fenntartja a jogot arra, hogy a jelen pontban rögzített gyártási határidőt további 60 nappal meghosszabbítsa. Ebben az esetben eladó késedelmi kötbér fizetésére vonatkozó kötelezettsége is a további 60 nap eltelte után ( 90+60 nap ) kezdődik.<br />
					<br />
					Eladó a Vevő által megadott elektronikus levelezési címre küldött e-maillel tájékoztatja Vevőt a megrendelt termék elkészültéről és ezáltal a megrendelés teljesítéséről. Kizárólag ezen e-mail minősül Eladó részéről értesítésnek. Vevő tudomásul veszi, hogy az értesítéstől a kiszállításig eltelt idő ezen határidőbe nem számít bele. Nem minősül a gyártási határidő Eladó részéről történő megszegésének, ha a megrendelésben, a megrendelés hatályosulását követően bármilyen változtatás történik, ez esetben a változtatás elfogadásától számítva a gyártási időtartam újrakezdődik.</li>
					<li>Eladó részéről a szerződés teljesítettnek minősül, amikor a bútort az előírásoknak és a megrendelésnek megfelelően előállította, azt átadásra előkészítette és erről a Vevőt értesítette. Az értesítés kizárólag a vevő által megrendeléskor megadott e-mail címére küldött elektronikus levél formájában minősül szabályosan megtettnek. Eladó a teljesítés során alvállalkozót vesz igénybe. Eladó jogosult közvetített szolgáltatás nyújtására.<br />
					Eladó kizárja felelősségét minden olyan kár vonatkozásában, amely a Vevő által helytelenül megadott értesítési címből/elérhetőségből származik.</li>
					<li>Az átadás-átvétel az értesítés kézhezvételétől számított 8 napon belül történik vagy Eladó telephelyén, vagy pedig Vevő kérése esetén házhozszállítással. A termék személyes átvételekor – az azonosítás érekében - &nbsp;Vevő köteles közölni a megrendelés számát és bemutatni az eredeti megrendelőlapot. A házhozszállítás külön díj ellenében történik Amennyiben vevő nem veszi igénybe a kiszállítási szolgáltatást, úgy személyes átvétel esetén vevő köteles gondoskodni a bútor elszállításáról és az ehhez szükséges rakodási munkák elvégzéséről. Eladó kizárólag budapesti átvétel esetén, bútoronként 10.000,-Ft+ÁFA rakodási díj megfizetése ellenében biztosítja az ehhez szükséges feltételeket, melynek díja a helyszínen fizetendő.<br />
					<br />
					Eladó – országosan - ingyenes házhozszállítást biztosít, amennyiben a vevő minimum 200.000.-Ft értékben kanapét vagy franciaágyat vásárol. Kisebb összegű kanapé vagy franciaágy vásárlása esetén lehetősége van kiegészítők ( puff, fotel, előszobafal ) vásárlásával elérnie ezen &nbsp;összeget. Új megrendelésű bútorhoz kizárólag új, outletes termékhez pedig kizárólag outletes kiegészítő vásárolható az összeghatár elérése érdekében. Az igénybevétel további feltétele, hogy vevő a megrendelése leadásának napjától számított 30 napon belül kiegyenlítse a megrendelése teljes vételárát. Vevő tudomásul veszi, hogy amennyiben a fenti fizetési határidőt elmulasztja, abban az esetben köteles a teljes szállítási díj megfizetésére.<br />
					<br />
					A kiszállítás igénybevétele esetén a szállítók a megadott címen lévő első zárható helyiségig kötelesek szállítani a terméket. Amennyiben a bútort emeletre kell szállítani, vevő erre irányuló – előzetes vagy helyszíni –kérése alapján 1000.-Ft/emelet összegű díjazás ellenében van lehetőség. A kiszállítás összeszerelési szolgáltatást nem tartalmaz, annak megrendelésére és igénybevételére külön díjazás alapján van lehetőség. Az összeszerelési szolgáltatás kizárólag az Eladó által kiszállított bútorok vonatkozásában igényelhető, díja 16.900.-Ft.&nbsp;<br />
					<br />
					A bútor átadásának feltétele a Vevő részéről az 5. pontban részletezett vételárrészlet megfizetését igazoló bizonylat bemutatása, továbbá a vételárhátralék hiánytalan megfizetése. Amennyiben Vevő az értesítéstől számított 8 napon belül a bútort nem veszi át, úgy Eladó a késedelem idejére jogosult tárolási költséget felszámítani, melynek összege a bútor teljes vételárának 1%-a/nap. Vevő erre irányuló kötelezettsége abban az esetben is fennáll, amennyiben eladó a megrendelőlapon feltűntetett, becsült gyártási határidőt megelőzően értesíti vevőt a termék elkészültéről. Eladó a bútort legfeljebb az értesítés Vevő általi kézhezvételétől számított 30 napig köteles tárolni. Ezen határidő elteltét követően Eladó jogosult a szerződéstől elállni és a bútort értékesíteni. Ebben az esetben Vevő által befizetett, 5. pontban részletezett vételárrész összegét Eladó jogosult a 6. pont szerinti kötbér összegébe beszámítani, ezáltal Vevő kötbérfizetési kötelezettsége teljesítettnek tekintendő. A bútor átvételénél vevő köteles megvizsgálni, hogy megfelel-e a megrendelésnek. Amennyiben megfelel, úgy köteles ezt az átvételkor írásban igazolni. A Vevő köteles továbbá a bútor felismerhető hibáit azonnal kifogásolni.</li>
					<li>A bútor Vevő részére történő átadásával, továbbá az értesítésétől számított 8 nap elteltével a bútorhoz fűződő kárveszély Vevőre száll át.</li>
					<li>Eladó egyes bútorok vázára 5 éves garanciát biztosít. A megnövelt időtartamú vázgaranciával értékesített bútorokról az értékesítők adnak tájékoztatást.<br />
					<br />
					Outletes bútorok esetén a vázra kizárólagosan 1 éves alapgarancia vonatkozik.<br />
					Kiterjesztett garancia: a vásárlóknak – meghatározott bútorok vásárlása esetén - lehetőségük van kiterjesztett garanciát vásárolni bútoruk mellé. A kiterjesztett garanciával aktuálisan megvásárolható bútorokról az értékesítők adnak tájékoztatást.<br />
					<br />
					A megvásárolt garancia az alábbiakat tartalmazza:<br />
					a.: 10 éves ( 5+5 éves ) időtartamú vázgarancia. Ez a többletszolgáltatás kizárólag azon esetekre vonatkozik, amennyiben a bútor rendeltetésszerű használatából eredően következik be a vázszerkezet törése. A garanciákból kizárt esetek ( példaszerűen, nem kizárólagosan ) : nem rendeltetésszerű&nbsp; használatból eredő sérülések, külső fizikai hatásból eredő sérülések, láb leszakadása, csavar kiszakadása vagy meglazulása folytán bekövetkezett károsodás, helytelen vagy többszöri összeszerelésből eredő sérülések, bútor elázása, stb.<br />
					A kiterjesztett garancia díja a bútor – szövetfelárral együtt számolt – vételárának 10%-a. Az erre vonatkozó igényt megrendeléskor jelezni kell és az kizárólag a díj&nbsp; megfizetését követően vehető igénybe<br />
					<br />
					b.: A szolgáltatás tartalmaz továbbá a vásárlástól számított 10 éven belül maximum 5 alkalommal igénybe vehető átkárpitozási lehetőséget az alábbiak szerint:<br />
					Vevő a szolgáltatás megvásárlásával jogosulttá válik arra, hogy igény szerinti időközzel – a maximális 5 alkalmat nem átlépve – az eladó által meghatározott önköltségi áron igényelje eladótól megvásárolt bútora átkárpitozását az igény bejelentésekor aktuálisan elérhető szövetválasztékból. A pontos díjat a minden esetben egyedileg készített árajánlat tartalmazza. Ezen szolgáltatás kizárólag kanapék és franciaágyak esetén vehető igénybe és csak a bútor teljes felülete tekintetében.<br />
					A bútor el- és visszaszállításának költségeit a szolgáltatás díja nem tartalmazza.</li>
					<li>Ha a kiszállított bútor hibás vagy hiányos, abban az esetben Vevő ezt a helyszínen jelezni köteles, melyről a szállítók jegyzőkönyvet vesznek fel. Utólagos, jegyzőkönyv nélküli reklamációt Eladónak nem áll módjában elfogadni. A Vevőt az Eladó hibás teljesítése esetén megilletik mindazok a szavatossági, illetve / amennyiben a Vevő a Ptk. vonatkozó rendelkezései alapján fogyasztónak minősül / jótállási jogok, amelyeket számára a Ptk., illetve bruttó 10.000,- Ft-ot meghaladó bútorvásárlás esetén az egyes tartós fogyasztási cikkekre vonatkozó kötelező jótállásról szóló 151/2003. (IX.22.) Korm. rendelet lehetővé tesznek. A Dr. BALM matracok belső szerkezetére, illetve a matrac belső magjára és alakjára Eladó az átvételtől számított 6 éves garanciát vállal. A matrac külső burkolatára / huzatára / 1 éves jótállás vonatkozik. Vevő szavatossági, illetve jótállási igényeit a bútor átvételekor átadott jótállási jeggyel, ennek hiányában az ellenérték megfizetését igazoló bizonylattal ( számla) érvényesítheti. A Vevő panaszát&nbsp; személyesen vagy írásban nyújthatja be. Írásban benyújtottnak minősül az ajánlott postai küldeményben megküldött és kézbesített panaszlevél, a bemutatótermekben kihelyezett, kereskedelmi hatóság által hitelesített vásárlók könyvébe írt bejegyzés, továbbá az <a href="mailto:ugyintezes@vamosimilano.hu">ugyintezes@vamosimilano.hu</a> címre megküldött elektronikus levél. A szavatosság, illetve a jótállás nem terjed ki olyan hibákra, melyek az alábbiakra vezethetők vissza:<br />
					-&nbsp;ha a károsodás rendeltetésellenes (használati kezelési útmutatótól eltérő) használat következménye<br />
					-&nbsp;valamely alkatrész /szövet stb./ a természetes kopás folytán tönkremegy/elhasználódik, vagy<br />
					-&nbsp;a vevő nem tett eleget a kárenyhítési kötelezettségének / pl. a felismert hibákat időben nem kifogásolta /<br />
					-&nbsp;ha a hiba káresemény következménye<br />
					-&nbsp;ha a terméken a vásárló, harmadik személy, illetve nem a jótállásra kötelezett által megjelölt javítószolgálat átalakítást, javítást végzett<br />
					-&nbsp;olyan hibákra, amelyet a vásárló már az eladáskor ismert, vagy amelyre árengedményt kapott.<br />
					<br />
					Vevő tudomásul veszi, hogy annak előzetes elbírálásához, hogy a bejelentett hiba a jótállás körébe tartozik-e, eladó jogosult fényképfelvételt kérni a kifogásolt termékről, illetve annak egy részéről és vállalja ennek csatolását a jótállási igénybejelentéshez. Vevő tudomásul veszi továbbá, hogy amennyiben panasza nem tartozik a jótállás körébe, úgy az esetlegesen felmerült szállítási költségeket köteles megfizetni Eladó részére. Ezen költségekbe nem számítanak bele a felmerülő hiba okának megállapításával járó, azzal kapcsolatban felmerülő többletkiadások. Eladó köteles a garanciális problémákat megfelelő határidőn belül orvosolni. A megfelelő határidőre a Polgári Törvénykönyv rendelkezései irányadóak. Vevő tudomásul veszi, és jelen ÁSZF aláírásával elfogadja, hogy a megrendelt bútor egyedi termék, ami a vevő által igényelt paramétereknek megfelelően kerül legyártásra. Az esetleges javítás/csere teljesítését eladó – figyelembe véve a termék fenti tulajdonságait - törekszik a Ptk. 6:159.§ (4) bekezdésében foglalt megfelelő határidőn belül elvégezni.</li>
					<li>Amennyiben Vevő a bútor átvétele előtt az értesítési címét megváltoztatja, köteles erről Eladót tájékoztatni. Eladó jognyilatkozatai a Vevővel szemben hatályosak, amennyiben azokat Eladó az utoljára megadott értesítési címre küldte ki.</li>
					<li>Jelen ÁSZF-ben nem szabályozott kérdésekben a Polgári Törvénykönyv és a Magyar Jog vonatkozó rendelkezéseit kell alkalmazni. Vevő jelen ÁSZF aláírásával kifejezetten kijelenti, hogy az általános szerződési feltételeket teljes terjedelmében megismerte, arról részletes tájékoztatást kapott, és az abban foglaltakat elfogadta, különös tekintettel a megrendelés hatályosulásával kapcsolatos 5. pontban rögzített rendelkezésekre, továbbá a szerződésszegés vonatkozásában a 6. és 9. pontban rögzített jogkövetkezmények (kötbér, tárolási díj) alkalmazására.</li>
				</ol>
	<?php } else { ?>
				<h4>A WORE HUNGARY KFT / Vamosi Milano</h4>
				<h1>Általános Szerződési Feltételei</h1>
				<ol>
					<li>A jelen szerződéses feltételek kerülnek alkalmazásra valamennyi, a Vamosi Milano / tulajdonos: WORE HUNGARY Kft/cégjegyzékszám: 01-09-944100, adószám: 22905192-2-43 székhely: 1119 Bp., Petzvál J. u. 46-48. /; továbbiakban: Eladó / által értékesített bútor vonatkozásában. Vevő aláírásával elfogadja az általános szerződési feltételeket.</li>
					<li>Vevő a bútort katalógusból, online felületekről / weboldal, közösségi portál /, illetve a Vamosi Milano bemutatótermében kiállított mintadarabok alapján rendeli meg. Vevőnek lehetősége van valamennyi rendelhető és a gyártás során felhasználható szövetminta személyes megtekintésére a Vamosi Milano bemutatótermében. Vevő tudomásul veszi, hogy a megrendelőlap benyújtását követően az általa kiválasztott szövetek színével, típusával, minőségével kapcsolatban kifogást nem emelhet. A weboldalon és a hirdetési felületeken látható képek illusztrációk.<br />
					<br />
					Vevő aláírásával elfogadja, hogy az elektronikus felületen megjelenő szín, ábra eltérhet a gyártáshoz ténylegesen felhasznált és a bemutatóteremben kiállított mintadarabtól. Ezen eltérések az elektronikus képmegjelenítő eszközök fajtájától is függhetnek és a megrendelni kívánt bútor kárpitozását ( színárnyalatait ), valamint a bútor arányait érinthetik ( a képen vagy bemutatóteremben feltűntetett daraboktól eltérő méretben történő megrendelés esetén ). A párnák darabszáma a megrendelt bútor méretétől függően változhat. Tekintettel arra, hogy bútoraink egyedi igényekre készülnek, egyes, a megrendelő által kiválasztott kárpitok vastagsága, valamint kárpitozási technikája és az ehhez szükséges gyártási technológia – figyelembe véve a bútor elemeinek számát is - eltérhet, így vevő aláírásával elfogadja, hogy a bútor mérete ezen eltérésekből adódóan, a megrendelőlapon szereplő mérettől összesen 1-10 cm-t eltérhet. Vevő jelen szerződés aláírásával igazolja, hogy a bútor típusát, színét, méretét, továbbá a kárpitot ellenőrizte, elfogadja, és a jelen szerződés mellékletét képező megrendelőlapon feltüntetett tulajdonságokkal kívánja a bútort megvásárolni. Vevő tudomásul veszi, hogy az általa meghatározott és a megrendelőlapon rögzített tulajdonságoktól a későbbiekben nem térhet el és az annak megfelelően előállított bútort köteles átvenni és a vételárat kiegyenlíteni.<br />
					<br />
					Vevő a megrendelőlap benyújtása előtt köteles ellenőrizni minden azon szereplő és a bútor gyártásához szükséges adatot / elérhetőség, számlázási cím, a bútor valamennyi tulajdonsága, ideértve típus, méretek, szövet, egyedi igények /. Vevő tudomásul veszi, hogy egyedi megrendelés esetén amennyiben nem határozza meg egyértelműen a bútor kiegészítő részeinek tulajdonságait, úgy Eladó jogosult azt saját hatáskörben – a termék elkészítéséhez szükséges mértékben – meghatározni.Vevő tudomásul veszi, hogy a megrendelőlap aláírásával és a 4. pontban részletezett vételárrészlet megfizetésével a megrendelés véglegessé válik, és azon ezt követően utólagos módosításra nincs lehetőség. A megrendelőlap benyújtását követően a Vevő részéről történt bármely tévedés vagy elírás miatti felelősséget Eladó kifejezetten kizárja.</li>
					<li>Amennyiben az Eladó minden gondossága ellenére hibás ár kerül a webáruház felületére, különös tekintettel a nyilvánvalóan téves, pl. a termék közismert, általánosan elfogadott vagy becsült árától jelentősen eltérő, esetleg rendszerhiba miatt megjelenő téves árra, akkor az Eladó nem köteles a terméket hibás áron szállítani, hanem felajánlhatja a helyes áron történő szállítást, amelynek ismeretében a Vevő elállhat vásárlási szándékától.</li>
					<li>Vevő tudomásul veszi, hogy a mindenkor változó akciókban meghirdetett ajándékokra kizárólag abban az esetben jogosult, amennyiben eleget tett az abban szereplő feltételeknek, továbbá, az ajándékra vonatkozó igényét a vásárlással egyidejűleg bejelentette és az a megrendelőlapon rögzítésre került. Az Eladó által havonta meghirdetett „Hónap bútora” elnevezésű akció keretében adott kedvezmény az adott hónap első napjától a hónap utolsó napjáig leadott, hatályos megrendeléssel történő vásárlás esetében érvényes. A hatályos megrendelés feltételeit az 5. pont tartalmazza.<br />
					<br />
					A mindenkor hatályos akciókban szereplő kedvezményes vételár és/vagy ajándékok és/vagy nyeremények igénybevételéhez szükséges feltételeket a meghirdetett akció szövege tartalmazza. Vevő kijelenti, hogy az akcióban szereplő feltételeket megismerte és azoknak hiánytalanul eleget tett.
					<br>
						Az a vásárló, aki Budapesten, illetve Pest Megyén kívüli lakcímmel rendelkezik, a megvásárolt termék kiszállítási címe is ezen területeken kívülre esik, valamint a megrendelése összege meghaladja a 200.000.-Ft-ot, 12.000.-Ft kedvezményt kap megrendelése végösszegéből. A kedvezmény utólagosan nem igényelhető, kizárólag a megrendeléssel egyidejűleg, mindhárom feltétel együttes fennállása esetén. A lakcímet a vásárlónak lakcimkártyája bemutatásával kell igazolnia. Ezen akció 2018.11.25-től visszavonásig érvényes. Az akció online vásárlásokra nem vonatkozik.
					</li>
					<li>A megrendelés akkor hatályos, ha Vevő a bútor vételárának 30%-át, valamint az egyéb szolgáltatások - szövet, illetve méret felárak, házhozszállítás díja - 100 %-át kifizette, továbbá a megrendelőlapot aláírta. Ezen feltételtől való eltérésre kizárólag egyedi megállapodás alapján, továbbá bruttó 1.500.000,-Ft-ot elérő értékű megrendelés esetén van lehetőség. A jelen pontban rögzített, fenti előírás alól kivételt képeznek az áruhitel igénybevételével kezdeményezett megrendelések is. Áruhitel igénybevételével történő megrendelés esetén a megrendelés a hitelkérelem bank általi elfogadásával egyidejűleg válik hatályossá, és ezen időponttól kezdődően alkalmazandó valamennyi ehhez kapcsolódó és jelen ÁSZF-ben rögzített jogkövetkezmény /módosítási tilalom, kártérítési – és kötbérfizetési kötelezettség, stb./<br />
					<br />
					A vételárhátralékot Vevő a bútor átvételével egyidejűleg köteles megfizetni. Ez alól kivételt képez az ingyenes házhozszállítás igénybevétele. Ez esetben vevő a vételárhátralékot a 9. pontban foglaltak szerint köteles megfizetni a megrendelés leadásától számított 30 napon belül. Ez történhet átutalással, illetve bemutatótermeinkben készpénzes, illetve kártyás fizetéssel. &nbsp;Vevő a vételár hiánytalan megfizetésével teljesíti a szerződést. Eladó a vételárhátralék teljes megfizetéséig tulajdonjogát fenntartja.<br />
					<br />
					Vevő az Általános Szerződési Feltételek elfogadásával beleegyezik, hogy Eladó elektronikus számlát bocsát ki. Késedelmes fizetés esetén Vevő köteles megfizetni Eladó részére a Polgári Törvénykönyvről szóló 2013. évi V. törvény (a továbbiakban: Ptk.) szerinti késedelmi kamatot, továbbá a 8. pontban foglalt tárolási díjat.</li>
					<li>Elállási jog, kötbér<br />
					Tekintettel arra, hogy Eladó a megrendelt terméket Vevő kifejezett kérésére és utasításai alapján állítja elő, így Vevő a fogyasztó és a vállalkozás közötti szerződések részletes szabályairól szóló 45/2014. (II.26.) Korm. rendelet 20. §-ban foglalt elállási jogot nem gyakorolhatja a rendelet 29. § (1) bekezdés c. pontja értelmében.<br />
					<br />
					Amennyiben Vevő a megrendelést követően olyan okból áll el a vásárlástól, mely Eladónak nem felróható, Eladó – a megkezdett egyedi gyártás sajátosságaira tekintettel - kártérítésre tarthat igényt, melynek mértéke a megrendelt bútor vételárának 30%-a.<br />
					<br />
					Amennyiben Vevő a megrendelt bútort nem veszi át, vagy a vételár fennmaradó részét nem egyenlíti ki, az Eladó a 9. pontban rögzített módon jogosult eljárni. Vevő az Eladó – 9. pontban foglaltak szerinti - elállása esetén nemteljesítési kötbér fizetésére köteles, melynek mértéke a megrendelt bútor vételárának 30%-a, valamint az egyéb szolgáltatások 100 %-a. Eladó a kötbér összegének erejéig jogosult beszámítani a Vevő által teljesített vételárrészletet. Az Eladó a 7. pontban rögzített gyártási határidő be nem tartása esetén, a Vevő felé a megrendelt bútor alapára 1%-ának megfelelő összegű késedelmi kötbér fizetésére köteles a késedelemmel érintett naptári naponként, a késedelembe esés 5. munkanapjától kezdődően addig az időpontig, amikor Eladó a bútor elkészültéről igazolhatóan értesíti Vevőt. Az Eladó által fizetendő kötbér összege azonban nem haladhatja meg a bútor vételárának 30%-át. Eladó késedelmi kötbér fizetési kötelezettsége nem vonatkozik az egyedi, az Eladó által megadott általános tulajdonságokkal rendelkező bútoroktól eltérő vevői megrendelésekre.</li>
					<li>A gyártási idő a megrendelés 5. pont szerinti hatályossá válását követően kezdődik. Vevő tudomásul veszi, hogy a megrendelőlapon feltüntetett, várható gyártási idő általános, becsült intervallumot jelent, melytől eltérések előfordulhatnak.<br />
					<br />
					Eladó a végleges megrendelés benyújtásától és az 5. pontban részletezett vételárrészlet beérkezésétől számított 90 napon belül köteles a megrendelt termék elkészítésére. Vevő tudomásul veszi, hogy amennyiben a megrendelt bútor kárpitjaként egy magas kategóriájú szövetet ( 2-7 kategória ) választ, ez az egyedi megrendelésre tekintettel hosszabb gyártási időt eredményezhet. Eladó ezen esetekben fenntartja a jogot arra, hogy a jelen pontban rögzített gyártási határidőt további 60 nappal meghosszabbítsa. Ebben az esetben eladó késedelmi kötbér fizetésére vonatkozó kötelezettsége is a további 60 nap eltelte után ( 90+60 nap ) kezdődik.<br />
					<br />
					Eladó a Vevő által megadott elektronikus levelezési címre küldött e-maillel tájékoztatja Vevőt a megrendelt termék elkészültéről és ezáltal a megrendelés teljesítéséről. Kizárólag ezen e-mail minősül Eladó részéről értesítésnek. Vevő tudomásul veszi, hogy az értesítéstől a kiszállításig eltelt idő ezen határidőbe nem számít bele. Nem minősül a gyártási határidő Eladó részéről történő megszegésének, ha a megrendelésben, a megrendelés hatályosulását követően bármilyen változtatás történik, ez esetben a változtatás elfogadásától számítva a gyártási időtartam újrakezdődik.</li>
					<li>Eladó részéről a szerződés teljesítettnek minősül, amikor a bútort az előírásoknak és a megrendelésnek megfelelően előállította, azt átadásra előkészítette és erről a Vevőt értesítette. Az értesítés kizárólag a vevő által megrendeléskor megadott e-mail címére küldött elektronikus levél formájában minősül szabályosan megtettnek. Eladó a teljesítés során alvállalkozót vesz igénybe.<br />
					Eladó kizárja felelősségét minden olyan kár vonatkozásában, amely a Vevő által helytelenül megadott értesítési címből/elérhetőségből származik.</li>
					<li>Az átadás-átvétel az értesítés kézhezvételétől számított 8 napon belül történik vagy Eladó telephelyén, vagy pedig Vevő kérése esetén házhozszállítással. A termék személyes átvételekor – az azonosítás érekében - ;Vevő köteles közölni a megrendelés számát és bemutatni az eredeti megrendelőlapot. A házhozszállítás külön díj ellenében történik Amennyiben vevő nem veszi igénybe a kiszállítási szolgáltatást, úgy személyes átvétel esetén vevő köteles gondoskodni a bútor elszállításáról és az ehhez szükséges rakodási munkák elvégzéséről. Eladó kizárólag budapesti átvétel esetén, bútoronként 10.000,-Ft+ÁFA rakodási díj megfizetése ellenében biztosítja az ehhez szükséges feltételeket, melynek díja a helyszínen fizetendő.<br />
					<br />
					Eladó – országosan - ingyenes házhozszállítást biztosít, amennyiben a vevő minimum 200.000.-Ft értékben kanapét vagy franciaágyat vásárol. Kisebb összegű kanapé vagy franciaágy vásárlása esetén lehetősége van kiegészítők ( puff, fotel, előszobafal ) vásárlásával elérnie ezen összeget. Új megrendelésű bútorhoz kizárólag új, outletes termékhez pedig kizárólag outletes kiegészítő vásárolható az összeghatár elérése érdekében. Az igénybevétel további feltétele, hogy vevő a megrendelése leadásának napjától számított 30 napon belül kiegyenlítse a megrendelése teljes vételárát. Vevő tudomásul veszi, hogy amennyiben a fenti fizetési határidőt elmulasztja, abban az esetben köteles a teljes szállítási díj megfizetésére.<br />
					<br />
					A kiszállítás igénybevétele esetén a szállítók a megadott címen lévő első zárható helyiségig kötelesek szállítani a terméket. Amennyiben a bútort emeletre kell szállítani, vevő erre irányuló – előzetes vagy helyszíni –kérése alapján 1000.-Ft/emelet összegű díjazás ellenében van lehetőség. A kiszállítás összeszerelési szolgáltatást nem tartalmaz, annak megrendelésére és igénybevételére külön díjazás alapján van lehetőség. Az összeszerelési szolgáltatás kizárólag az Eladó által kiszállított bútorok vonatkozásában igényelhető, díja 16.900.-Ft.<br />
					<br />
					A bútor átadásának feltétele a Vevő részéről az 5. pontban részletezett vételárrészlet megfizetését igazoló bizonylat bemutatása, továbbá a vételárhátralék hiánytalan megfizetése. Amennyiben Vevő az értesítéstől számított 8 napon belül a bútort nem veszi át, úgy Eladó a késedelem idejére jogosult tárolási költséget felszámítani, melynek összege a bútor teljes vételárának 1%-a/nap. Vevő erre irányuló kötelezettsége abban az esetben is fennáll, amennyiben eladó a megrendelőlapon feltűntetett, becsült gyártási határidőt megelőzően értesíti vevőt a termék elkészültéről. Eladó a bútort legfeljebb az értesítés Vevő általi kézhezvételétől számított 30 napig köteles tárolni. Ezen határidő elteltét követően Eladó jogosult a szerződéstől elállni és a bútort értékesíteni. Ebben az esetben Vevő által befizetett, 5. pontban részletezett vételárrész összegét Eladó jogosult a 6. pont szerinti kötbér összegébe beszámítani, ezáltal Vevő kötbérfizetési kötelezettsége teljesítettnek tekintendő. A bútor átvételénél vevő köteles megvizsgálni, hogy megfelel-e a megrendelésnek. Amennyiben megfelel, úgy köteles ezt az átvételkor írásban igazolni. A Vevő köteles továbbá a bútor felismerhető hibáit azonnal kifogásolni.</li>
					<li>A bútor Vevő részére történő átadásával, továbbá az értesítésétől számított 8 nap elteltével a bútorhoz fűződő kárveszély Vevőre száll át.</li>
					<li>Eladó egyes bútorok vázára 5 éves garanciát biztosít. A megnövelt időtartamú vázgaranciával értékesített bútorokról az értékesítők adnak tájékoztatást.<br />
					<br />
					Outletes bútorok esetén a vázra kizárólagosan 1 éves alapgarancia vonatkozik.<br />
					Kiterjesztett garancia: a vásárlóknak – meghatározott bútorok vásárlása esetén - lehetőségük van kiterjesztett garanciát vásárolni bútoruk mellé. A kiterjesztett garanciával aktuálisan megvásárolható bútorokról az értékesítők adnak tájékoztatást.<br />
					<br />
					A megvásárolt garancia az alábbiakat tartalmazza:<br />
					a.: 10 éves ( 5+5 éves ) időtartamú vázgarancia. Ez a többletszolgáltatás kizárólag azon esetekre vonatkozik, amennyiben a bútor rendeltetésszerű használatából eredően következik be a vázszerkezet törése. A garanciákból kizárt esetek ( példaszerűen, nem kizárólagosan ) : nem rendeltetésszerű&nbsp; használatból eredő sérülések, külső fizikai hatásból eredő sérülések, láb leszakadása, csavar kiszakadása vagy meglazulása folytán bekövetkezett károsodás, helytelen vagy többszöri összeszerelésből eredő sérülések, bútor elázása, stb.<br />
					A kiterjesztett garancia díja a bútor – szövetfelárral együtt számolt – vételárának 10%-a. Az erre vonatkozó igényt megrendeléskor jelezni kell és az kizárólag a díj&nbsp; megfizetését követően vehető igénybe<br />
					<br />
					b.: A szolgáltatás tartalmaz továbbá a vásárlástól számított 10 éven belül maximum 5 alkalommal igénybe vehető átkárpitozási lehetőséget az alábbiak szerint:<br />
					Vevő a szolgáltatás megvásárlásával jogosulttá válik arra, hogy igény szerinti időközzel – a maximális 5 alkalmat nem átlépve – az eladó által meghatározott önköltségi áron igényelje eladótól megvásárolt bútora átkárpitozását az igény bejelentésekor aktuálisan elérhető szövetválasztékból. A pontos díjat a minden esetben egyedileg készített árajánlat tartalmazza. Ezen szolgáltatás kizárólag kanapék és franciaágyak esetén vehető igénybe és csak a bútor teljes felülete tekintetében.<br />
					A bútor el- és visszaszállításának költségeit a szolgáltatás díja nem tartalmazza.</li>
					<li>Ha a kiszállított bútor hibás vagy hiányos, abban az esetben Vevő ezt a helyszínen jelezni köteles, melyről a szállítók jegyzőkönyvet vesznek fel. Utólagos, jegyzőkönyv nélküli reklamációt Eladónak nem áll módjában elfogadni. A Vevőt az Eladó hibás teljesítése esetén megilletik mindazok a szavatossági, illetve / amennyiben a Vevő a Ptk. vonatkozó rendelkezései alapján fogyasztónak minősül / jótállási jogok, amelyeket számára a Ptk., illetve bruttó 10.000,- Ft-ot meghaladó bútorvásárlás esetén az egyes tartós fogyasztási cikkekre vonatkozó kötelező jótállásról szóló 151/2003. (IX.22.) Korm. rendelet lehetővé tesznek. A Dr. BALM matracok belső szerkezetére, illetve a matrac belső magjára és alakjára Eladó az átvételtől számított 6 éves garanciát vállal. A matrac külső burkolatára / huzatára / 1 éves jótállás vonatkozik. Vevő szavatossági, illetve jótállási igényeit a bútor átvételekor átadott jótállási jeggyel, ennek hiányában az ellenérték megfizetését igazoló bizonylattal ( számla) érvényesítheti. A Vevő panaszát&nbsp; személyesen vagy írásban nyújthatja be. Írásban benyújtottnak minősül az ajánlott postai küldeményben megküldött és kézbesített panaszlevél, a bemutatótermekben kihelyezett, kereskedelmi hatóság által hitelesített vásárlók könyvébe írt bejegyzés, továbbá az <a href="mailto:ugyintezes@vamosimilano.hu">ugyintezes@vamosimilano.hu</a> címre megküldött elektronikus levél. A szavatosság, illetve a jótállás nem terjed ki olyan hibákra, melyek az alábbiakra vezethetők vissza:<br />
					-&nbsp;ha a károsodás rendeltetésellenes (használati kezelési útmutatótól eltérő) használat következménye<br />
					-&nbsp;valamely alkatrész /szövet stb./ a természetes kopás folytán tönkremegy/elhasználódik, vagy<br />
					-&nbsp;a vevő nem tett eleget a kárenyhítési kötelezettségének / pl. a felismert hibákat időben nem kifogásolta /<br />
					-&nbsp;ha a hiba káresemény következménye<br />
					-&nbsp;ha a terméken a vásárló, harmadik személy, illetve nem a jótállásra kötelezett által megjelölt javítószolgálat átalakítást, javítást végzett<br />
					-&nbsp;olyan hibákra, amelyet a vásárló már az eladáskor ismert, vagy amelyre árengedményt kapott.<br />
					<br />
					Vevő tudomásul veszi, hogy annak előzetes elbírálásához, hogy a bejelentett hiba a jótállás körébe tartozik-e, eladó jogosult fényképfelvételt kérni a kifogásolt termékről, illetve annak egy részéről és vállalja ennek csatolását a jótállási igénybejelentéshez. Vevő tudomásul veszi továbbá, hogy amennyiben panasza nem tartozik a jótállás körébe, úgy az esetlegesen felmerült szállítási költségeket köteles megfizetni Eladó részére. Ezen költségekbe nem számítanak bele a felmerülő hiba okának megállapításával járó, azzal kapcsolatban felmerülő többletkiadások. Eladó köteles a garanciális problémákat megfelelő határidőn belül orvosolni. A megfelelő határidőre a Polgári Törvénykönyv rendelkezései irányadóak. Vevő tudomásul veszi, és jelen ÁSZF aláírásával elfogadja, hogy a megrendelt bútor egyedi termék, ami a vevő által igényelt paramétereknek megfelelően kerül legyártásra. Az esetleges javítás/csere teljesítését eladó – figyelembe véve a termék fenti tulajdonságait - törekszik a Ptk. 6:159.§ (4) bekezdésében foglalt megfelelő határidőn belül elvégezni.</li>
					<li>Amennyiben Vevő a bútor átvétele előtt az értesítési címét megváltoztatja, köteles erről Eladót tájékoztatni. Eladó jognyilatkozatai a Vevővel szemben hatályosak, amennyiben azokat Eladó az utoljára megadott értesítési címre küldte ki.</li>
					<li>Jelen ÁSZF-ben nem szabályozott kérdésekben a Polgári Törvénykönyv és a Magyar Jog vonatkozó rendelkezéseit kell alkalmazni. Vevő jelen ÁSZF aláírásával kifejezetten kijelenti, hogy az általános szerződési feltételeket teljes terjedelmében megismerte, arról részletes tájékoztatást kapott, és az abban foglaltakat elfogadta, különös tekintettel a megrendelés hatályosulásával kapcsolatos 5. pontban rögzített rendelkezésekre, továbbá a szerződésszegés vonatkozásában a 6. és 9. pontban rögzített jogkövetkezmények (kötbér, tárolási díj) alkalmazására.</li>
				</ol>
				<?php } ?>
			</div>
		</div>
	</div>
@else
	@yield('content')
@endif


            @if (Request::path() != '/')

                <div class="container">

                <?php // @include('blocks.informations') ?>

                @include('blocks.customerreviews')

                </div>

            @else

                <?php // @include('blocks.informations') ?>

                @include('blocks.customerreviews')

            @endif



        @if (Request::path() == '/')

            </div>

        @endif



        @include('blocks.newsletter')
		@include('layouts.footer_'.App::getLocale())
        

    </div>




	<a href="#0" class="cd-top">{{ t('Fel') }}</a>
	<script type="text/javascript" src="{{ asset('js/custom.js') }}?v22" async></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	
	<script >reloadCartPreview();</script>
	 @yield('footer_js')

	<div class="row">
	@if (Session::has('not_found_lang') and Config::get('app.debug'))
		<div class="col-md-6">
			{{ implode('<br>', Session::get('not_found_lang')) }},
			<?php 
			Session::forget('not_found_lang');
			?>
		</div>
	@endif
	@if (Session::has('not_found_perm') and Config::get('app.debug'))
		<div class="col-md-6">
			{{ implode('<br>', Session::get('not_found_perm')) }},
			<?php
			Session::forget('not_found_perm');
			?>
		</div>
	@endif
	</div>
	<input type="hidden" id="default_data" data-curr="{{ Config::get('shop.'.getShopCode().'.shop_currency') }}">
</body>



</html>
<?php
}// régi vagy új verzió if vége
?>