<script>
	function computeLoan(){
		var amount = document.getElementById('amount').value;
		var interest_rate = document.getElementById('interest_rate').value;
		var months = document.getElementById('months').value;
		var interest = (amount * (interest_rate * .01)) / months;
		var payment = Math.round(((amount / months) + interest).toFixed(2));
		payment = payment.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		var onero = Math.round(amount*0.2);
		onero = onero.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		document.getElementById('payment').innerHTML = "Havonta fizetendo = "+payment+" Ft";
		if (+amount > 1000000 || +amount < 29999) {
			document.getElementById('hitelosszeg_error').style.display = 'block';
			document.getElementById('hitelosszeg_error').innerHTML = "A hitelösszeg minimum 30 000 Ft és maximum 500 000 Ft.";
		} else {
			document.getElementById('hitelosszeg_error').style.display = 'none';
			if (+amount > 300000) {
				document.getElementById('figyelmeztetes').style.display = 'none';
				document.getElementById('figyelmeztetes').innerHTML = "300 000 Forint hitelösszeg felett 20% önerő szükséges, ami jelen esetben "+onero+" Ft. ";
				document.getElementById('onero').innerHTML = "Önerő: "+onero+ "Ft.";
				var onero = Math.round(amount*0.2);
				var hitelosszegOnerovel = amount-onero;
				var torleszto = Math.round(((hitelosszegOnerovel / months) + interest).toFixed(2));
				torleszto = torleszto.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
				document.getElementById('payment').innerHTML = "Havonta fizetendő: "+torleszto+" Ft.";
			} else {
				document.getElementById('figyelmeztetes').style.display = 'none';
				document.getElementById('onero').innerHTML = "Önerő: 0 Ft.";
			}
		}
	}
</script>
<?php $oldal = "215"; ?>
<div class="row">
	<div class="col-md-12">
		<h1 class="h1-responsive title-center text-uppercase my-4">0% THM akció</h1>
		<div id="figyelmeztetes" style="display:none" class="alert alert-info"></div>
		<div class="md-form md-outline">
			<label for="amount">Hitelösszeg</label>
			<div id="hitelosszeg_error" style="display:none" class="alert alert-danger"></div>
			<input class="form-control" id="amount" type="number" min="30000" max="500000" placeholder="30 000 Ft - 500 000 Ft" onkeyup="computeLoan()">
		</div>
		<div class="md-form md-outline">
			<label for="interest_rate">THM (%)</label>
			<input class="form-control" id="interest_rate" type="number" min="0" max="100" value="0" step=".1" disabled> 
		</div>
		<div class="md-form md-outline">
			<label for="months">Futamidő (hónap)</label>
			<input class="form-control" id="months" type="number" min="1" max="10" value="10" step="1" onchange="computeLoan()" disabled>
		</div>
		<p class="lead" id="payment">Havonta fizetendő</p>
		<p class="lead" id="onero">Önerő</p>
		<br>
		<div class="alert alert-primary">
			<strong>Hitelösszeg:</strong> 30 000 Ft és 500 000 Ft között.<br>
			<strong>Futamidő</strong>: 10 hónap. <br><strong>Önerő</strong>: 0%. <br><strong>Kezelési költség</strong>: 0%. <br><strong>Éves ügyleti kamat</strong>: 0%. <br>
			<br>300 000 Ft-ig önerő nélkül, 300 000 Ft felett 20% önerő szükséges.
		</div>
	</div>
	<div class="col-md-6">
		<p>
			<a href="https://static.vamosimilano.hu/loancalculator/hirdetmenyek/20180629_akcio_215.pdf" target="_blank" class="btn btn-outline-primary"><i class="fal fa-file-pdf fa-fw mr-2"></i> Hirdetmény letöltése</a>
		</p>
	</div>
	<div class="col-md-6">
		<p class="mt-2">
			<a href="https://www.otpbank.hu/portal/hu/SzabadFelhasznalasuHitelek/Aruhitel" target="_blank">Áruhitelekkel kapcsolatban bővebb tájékoztatást az OTP weboldalán talál</a>
		</p>
	</div>
	<div class="col-md-12">
			<p class="text-muted"><small>Az extra kedvezményes hitelt az OTP Bank nyújtja. Hitelösszeg: 30 ezer Ft és 500 ezer Ft között. Futamidő: 10 hónap.  Saját erő 0%. Kezelési költség 0%. Éves Ügyleti Kamat: 0%. 300 000 Ft-ig saját erő nélkül, 300 000 Ft felett 20% saját erő szükséges. A hitel nyújtója az OTP Bank Nyrt. A WORE Hungary Kft az OTP Bank Nyrt. hitelközvetítője. Ez a hirdetés nem minősül ajánlattételnek, a Bank a hitelbírálat jogát fenntartja. A hitelkonstrukció a 2018. december 1.-31. között benyújtott hitelkérelmek esetén, a kijelölt áruházakban, meghatározott termékekre vehető igénybe és a készlet erejéig érvényes. Példa THM: 500.000 Ft hitelösszeg és 10 hónapos futamidő esetén 0 % THM. A THM meghatározása a 83/2010. (III.25.) Korm. R.9§(3) bekezdésében megjelölt szabályozáson alapul. A THM számítás során a folyósítás és az első esedékesség közötti lehetséges legrövidebb időszak - 31 nap - került figyelembe vételre. A tájékoztatás nem teljes körű, ezért további részletes információért kérjük, tájékozódjon az ügyintézőnél, vagy az áruházakban elhelyezett hatályos Hirdetményből, valamint az OTP Bank Nyrt. bankfiókjaiban és honlapján közzétett vonatkozó Üzletszabályzatokból. <a href="https://www.otpbank.hu" target="_blank">www.otpbank.hu</a></small>
		</p>
	</div>
</div>