<? echo '<?xml version="1.0"?>'; ?>
<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
	<channel>
		<title>{{ t('project_name','project') }}</title>
		<link>{{ Request::root() }}</link>
		<description>{{ t('project_name','project') }} - product feed</description>
    
    @foreach( $products as $product )
	<?
	if ($product->visible!='yes') {
		continue;
	}
	if ($product->productfeed!='yes') {
		continue;
	}
	$textiles = $product->getTextilePreviews();
	?>
		@foreach( $textiles as $textile )
		<?php
			$color='';
			if ($textile->textile_id) {
				if (ProductTextile::lang()->find($textile->textile_id)->visible == 'no') {
					continue;
				}
				$color=str_replace('/','-',ProductTextile::getTextileName($textile->textile_id));
			}
			if (($textile->textile2_id) and ($textile->textile_id != $textile->textile2_id)) {
				if (ProductTextile::lang()->find($textile->textile2_id)->visible == 'no') {
					continue;
				}
				$color.=' / '.str_replace('/','-',ProductTextile::getTextileName($textile->textile2_id));
			}
		
			$preview_url = base64image(ProductImage::getHoverImageUrl($textile->image_id),'jpeg');
		?>
		<item>
			<g:id>{{$textile->image_id}}</g:id> 
			<g:title>{{$product->getName()}}</g:title>
			<g:description><![CDATA[{{ htmlspecialchars(strip_tags($product->getDescription()."\n".t('Our handmade furniture can be ordered in different sizes, colours and upholsteries. Please call us at 0208 208 3808 for further information!')))}}]]></g:description>
			<g:link>{{$product->getUrl()}}</g:link>
			<g:image_link>{{$preview_url}}</g:image_link>
			<g:condition>new</g:condition>
			<g:availability>in stock</g:availability>
			<g:price>{{$product->getDefaultPrice()}} {{ Config::get('shop.'.getShopCode().'.currency_code') }}</g:price>
			<?/* <g:sale_price_effective_date>2018-09-01T16:00-08:00/2018-09-03T16:00-08:00</g:sale_price_effective_date>*/?>			
			<g:google_product_category><![CDATA[{{ t($product->getCategory()->admin_name,'google_category') }}]]></g:google_product_category>
			<g:product_type><![CDATA[{{ t($product->getCategory()->admin_name,'google_type') }}]]></g:product_type>
			<g:identifier_exists>no</g:identifier_exists>
			<g:item_group_id>{{$product->product_id}}</g:item_group_id>
			<g:color>{{ $color }}</g:color>
			<?/*
			<!-- <g:shipping></g:shipping>  Mivel a shipping-et a Google Merchant Center-en belül kell beállítani elsődlegesen, így ez csak az ottani feltételek felülírására való termékenként -->
			<!-- <g:tax></g:tax> Mivel a tax-ot a Google Merchant Center-en belül kell beállítani elsődlegesen, így ez csak az ottani feltételek felülírására való termékenként -->
			*/?>
		</item>
		@endforeach
    @endforeach
</channel>
</rss>