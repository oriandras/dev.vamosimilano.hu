@include('emails.header')
<div style="color: #646464;  font-family: Helvetica,Arial; max-width: 600px;">
Kedves {{ $order->firstname }},
<p>{{ t('Sikeres tranzakciót hajtott végre!') }}</p>
<p>{{ t('Rendelés azonosítója') }}: {{ $order->order_number }}</p>
<p>{{ t('Tranzakció azonosítója') }}: {{ $result['trans_id'] }}</p>

<p>{{ t('Rendelés státusza').": <b>" .$order->getStatus() }}</b></p>

<p><b>{{ t('Tranzakció részleti') }}</b>: {{ Unicredit::getResultString($result['result']) }}</p>

<p><b>{{ t('Összeg') }}</b>: {{ money($result['price']) }}</p>


<p>{{ t('Bármilyen kérdése van, keressen minket!') }}</p>




@include('emails.footer')

