@include('emails.header')
<div style="color: #646464;  font-family: Helvetica,Arial; max-width: 600px;">
 {{ t('Kedves').", ".$order->user_firstname }}!
<p>{{ t('Rendelése sikeresen megérkezett hozzánk!') }}</p>
<p>{{ t('Rendelés azonosítója') }}: {{ $order->order_number }}</p>


<h3  style="color: #646464; font-size: 22px; font-family: Helvetica,Arial">{{ t('Megrendelés adatai') }}</h3>
<table style="width: 100%">
<tr>
<td valign="top">
<p>{{ t('Szállítási mód') }}:<br><b> {{ t('shipping_mode_'.$order->delivery_mode) }}
						    </b>
</p>
</td>
<td valign="top">
<p>{{ t('Fizetési mód') }}: <br><b>{{ t('billing_mode_'.$order->payment) }}
						    </b>
</p>
</td>
</tr>
</table>
<h3 style="color: #646464; font-size: 22px; font-family: Helvetica,Arial">{{ t('Személyes adatok') }}</h3>
    <p>{{ t('Vezetéknév') }}: <b>{{ $order->user_lastname }}</b></p>
    <p>{{ t('Keresztnév') }}: <b>{{ $order->user_firstname }}</b></p>
    <p>{{ t('E-mail cím') }}: <b>{{ $order->email }}</b></p>
    <p>{{ t('Telefonszám') }}: <b>{{ $order->phone  }}</b></p>

<table style="width: 100%">
<tr>
<td valign="top">
    <h3  style="color: #646464; font-size: 22px; font-family: Helvetica,Arial">{{ t('Szállítási cím') }}</h3>
    <p>{{ t('Vezetéknév') }}: <b>{{ $order->lastname }}</b></p>
    <p>{{ t('Keresztnév') }}: <b>{{ $order->firstname }}</b></p>
    <p>{{ $order->zip.", ".$order->city."<br>".$order->address."<br>".$order->other_addr.($order->floor ? "<br>".t('Emelet').": ".$order->floor : '') }}</p>
    @if($order->message)
        <p><strong>{{ t('Megjegyzés') }}:</strong> {{ $order->message }}<br /></p>
    @endif
</td>
<td valign="top">
    <h3  style="color: #646464; font-size: 22px; font-family: Helvetica,Arial">{{ t('Számlázási adatok') }}</h3>
    <p>{{ $order->billing_name.", ".$order->billing_zip.", ".$order->billing_city."<br> ".$order->billing_address."<br> ".$order->billing_other_addr }}</p>
    @if($order->billing_tax_number)
    {{ t('Adószám') }}: {{$order->billing_tax_number}}
    @endif
</td>
</tr>
</table>
<h3  style="color: #646464; font-size: 22px; font-family: Helvetica,Arial"> {{ t('Megrendelt termékek') }}</h3>
<table style="width: 100%" cellpadding="0" cellspacing="0">
@foreach ($items as $item)
<?php
    $price_array = CartHelper::calculateItemPrice(unserialize($item->options),true);
?>
<tr>
    <td style="padding: 5px; border-bottom: 1px solid #CD7126;"><b>{{ $item->item_name }}</b>
    @if ($item->options)
        <br>{{ CartHelper::writeDataByOptions(unserialize($item->options)) }}
    @endif

    @if ($price_array['textile_price'])
        {{ t('Bútor alapár').": ".money($price_array['furniture_price']) }} + {{ t('Szövet felár').": ".money($price_array['textile_price']) }}
    @endif
    </td>
    <td style="padding: 5px;  border-bottom: 1px solid #CD7126;">{{ $item->qty." ".t('db') }}</td>
    <td align="right" style="padding: 5px;  border-bottom: 1px solid #CD7126;">{{ money($item->item_payable_price) }}</td>
    <td align="right" style="padding: 5px;  border-bottom: 1px solid #CD7126;">{{ money($item->item_payable_price*$item->qty) }}</td>
</tr>

@endforeach
<tr>
    <td colspan="3" align="right" style="padding: 5px;">{{ t('Részösszeg') }}</td>
    <td  style="padding: 5px;" align="right">{{ money($order->total_price) }}</td>

</tr>
<tr>
    <td colspan="3" align="right" style="padding: 5px;">{{ t('Szállítási költség') }}</td>
    <td  style="padding: 5px;" align="right">{{ money($order->delivery_price) }}</td>

</tr>
<tr>
    <td colspan="3" align="right" style="padding: 5px;">{{ t('Emelet költség') }}</td>
    <td  style="padding: 5px;" align="right">{{ money($order->cost_price) }}</td>

</tr>
<tr>
    <td colspan="3" align="right" style="padding: 5px;">{{ t('Kedvezmény') }}</td>
    <td  style="padding: 5px;" align="right">{{ ($order->discount_price ? '-'.money($order->discount_price) : '') }}</td>

</tr>
<tr>
    <td colspan="3" align="right" style="padding: 5px;">{{ t('Előleg') }}</td>
    <td  style="padding: 5px;" align="right">{{ money($order->advance_price) }}</td>
</tr>
<tr>
    <td colspan="3" align="right" style="padding: 5px;">{{ t('Fizetendő') }}</td>
    <td  style="padding: 5px;" align="right">{{ money($order->payable_price) }}</td>
</tr>
</table>

@if (($order->payment=='ADV_CARD') or ($order->payment=='FULL_CARD'))
	@if ($order->delivery_free > 0) 
		{{ t('Az ingyenes házhozszállítás feltételeit az ÁSZF tartalmazza. Tájékoztatjuk, hogy amennyiben a megrendelés leadásától számított 30 napon belül  nem egyenlíti ki a megrendelése teljes vételárát, úgy köteles a ').money($order->delivery_free).t(' összegű szállítási díj megfizetésére, legkésőbb a bútor átvételével egyidejűleg. A vételárhátralékot a megadott határidőn belül az alábbi számlaszámra történő utalással teljesítheti: WORE Hungary Kft 11712004-20350376.') }}
	@endif
	<p>{{ t('utalasszoveg_wore') }}</p>
@else
	@if ($order->delivery_free > 0) 
		{{ t('Az ingyenes házhozszállítás feltételeit az ÁSZF tartalmazza. Tájékoztatjuk, hogy amennyiben a megrendelés leadásától számított 30 napon belül  nem egyenlíti ki a megrendelése teljes vételárát, úgy köteles a ').money($order->delivery_free).t(' összegű szállítási díj megfizetésére, legkésőbb a bútor átvételével egyidejűleg. A vételárhátralékot a megadott határidőn belül az alábbi számlaszámra történő utalással teljesítheti: Dívány Investment Kft 11712004-22460127-00000000.') }}
	@endif
	<p>{{ t('utalasszoveg_divany') }}</p>
@endif

<p>{{ t('Köszönjük a rendelését, várjuk vissza máskor is!') }}</p>
</div>

@include('emails.footer')

