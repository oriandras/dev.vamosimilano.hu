@include('emails.header')
<div style="color: #646464;  font-family: Helvetica,Arial; max-width: 600px;">
Kedves {{ $order->firstname }},
<p>{{ t('Rendelése státusza megváltozott!') }}</p>
<p>{{ t('Rendelés azonosítója') }}: {{ $order->order_number }}</p>

<p>{{ t('Rendelés új státusza').": <b>" .$order->getStatus() }}</b></p>

<p><b>{{ t('Részletek') }}</b>:</p>
@if ($order->status == 'PENDING')
<p>{{ t('Rendelése sikeresn beérkezett hozzánk, csomagját összekészítjük, és értesítjük, ha elkészült') }}</p>
@elseif ($order->status == 'DONE')

    @if ($order->delivery_mode == 'PERSONAL')
        <p>{{ t('Csomagja elkészült, és átveheti az alábbi telephelyen.') }}</p>
    @elseif ($order->delivery_mode == 'PPP')
        <p>{{ t('Csomagja elkészült, és átveheti az alábbi átvevő ponton.') }}</p>
    @else
        <p>{{ t('Csomagja elkészült, és átadtuk a futárszolgálatnak') }}</p>
    @endif
    <p>{{ $order->zip.",".$order->city }}<br>{{$order->address}}<br>{{$order->message_courier}}</p>

@endif

<h3  style="color: #646464; font-size: 22px; font-family: Helvetica,Arial"> {{ t('Megrendelt termékek') }}</h3>
<table style="width: 100%" cellpadding="0" cellspacing="0">
@foreach ($items as $item)
<?php
    $price_array = CartHelper::calculateItemPrice(unserialize($item->options),true);
?>
<tr>
    <td style="padding: 5px; border-bottom: 1px solid #CD7126;"><b>{{ $item->item_name }}</b>
    @if ($item->options)
        <br>{{ CartHelper::writeDataByOptions(unserialize($item->options)) }}
    @endif

    @if ($price_array['textile_price'])
        {{ t('Bútor alapár').": ".money($price_array['furniture_price']) }} + {{ t('Szövet felár').": ".money($price_array['textile_price']) }}
    @endif
    </td>
    <td style="padding: 5px;  border-bottom: 1px solid #CD7126;">{{ $item->qty." ".t('db') }}</td>
    <td align="right" style="padding: 5px;  border-bottom: 1px solid #CD7126;">{{ money($item->item_payable_price) }}</td>
    <td align="right" style="padding: 5px;  border-bottom: 1px solid #CD7126;">{{ money($item->item_payable_price*$item->qty) }}</td>
</tr>

@endforeach

<tr>
    <td colspan="3" align="right" style="padding: 5px;">{{ t('Részösszeg') }}</td>
    <td  style="padding: 5px;" align="right">{{ money($order->total_price) }}</td>

</tr>
<tr>
    <td colspan="3" align="right" style="padding: 5px;">{{ t('Szállítási költség') }}</td>
    <td  style="padding: 5px;" align="right">{{ money($order->delivery_price) }}</td>

</tr>
<tr>
    <td colspan="3" align="right" style="padding: 5px;">{{ t('Emelet költség') }}</td>
    <td  style="padding: 5px;" align="right">{{ money($order->cost_price) }}</td>

</tr>
<tr>
    <td colspan="3" align="right" style="padding: 5px;">{{ t('Kedvezmény') }}</td>
    <td  style="padding: 5px;" align="right">{{ ($order->discount_price ? '-'.money($order->discount_price) : '') }}</td>

</tr>
<tr>
    <td colspan="3" align="right" style="padding: 5px;">{{ t('Előleg') }}</td>
    <td  style="padding: 5px;" align="right">{{ money($order->advance_price) }}</td>
</tr>
<tr>
    <td colspan="3" align="right" style="padding: 5px;">{{ t('Fizetendő') }}</td>
    <td  style="padding: 5px;" align="right">{{ money($order->payable_price) }}</td>
</tr>
</table>

<p>{{ t('Bármilyen kérdése van, keressen minket!') }}</p>




@include('emails.footer')

