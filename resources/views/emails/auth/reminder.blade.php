@include('emails.header')
		<h2>{{ t('Új jelszó igény') }}</h2>
        <p>
        {{ t('Új jelszót igényeltek az alábbi e-mail címmel') }}:
        <br>{{ Input::get('email') }}
        <br>

        </p>
		<div>
			{{ t('Új jelszó létrehozásához kattintson az alábbi linkre') }}: <br>
			{{ URL::to('password/reset', array($token)) }}<br/><br/>
			{{ t('Amennyiben nem Ön igényelte ezt az üzenetet, hagyja figyelmen kívül.') }}
			<br>
			{{ t('Ez a link') }} {{ Config::get('auth.reminder.expire', 60) }} {{ t('percig érvényes.') }}
		</div>
@include('emails.footer')
