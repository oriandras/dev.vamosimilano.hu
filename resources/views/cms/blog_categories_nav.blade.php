<?php
$uri = 'https://' . $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$uri_parts = explode('/', $_SERVER['REQUEST_URI'], 2);
//print_r($uri_parts);
if ( ismobile() ) {
?>
<div class="container-fluid my-3">
	<div class="row">
		<div class="col-6">
			<div class="dropdown">
				<button class="btn btn-sm btn-block btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fal fa-folder mr-1"></i>Kategóriák</button>
				<div class="dropdown-menu dropdown-primary">
				<?php
				$categories = DB::select("SELECT count(*) as db, category FROM `cms_content` WHERE cms_type='blog' and is_active=1 and deleted_at is null GROUP BY category ORDER BY category");
				foreach ($categories as $category) {
					if($uri == 'https://' . $_SERVER['HTTP_HOST'].'/blog/'.$category->category){
						$activetab = "active show";
					}
					else {
						$activetab = "";
					}
				?>
					<a class="dropdown-item" href="/blog/{{ $category->category }}">{{ t('cms_blogcategory_'.$category->category) }}<span class="badge badge-primary ml-2">{{$category->db}}</span></a>
				<?php
				}
				?>
				</div>
			</div>
		</div>
		<div class="col-6">
		<?php
		//echo $uri_parts[1];
		if( isset($uri_parts[1]) && $uri_parts[1]=="blog" ) {
			$back_url = "/";
			$back_label = '<i class="fal fa-home mr-1"></i>Webáruház';
			//echo $uri_parts[1];
		}
		else {
			$back_url = "/blog";
			$back_label = '<i class="fal fa-chevron-left mr-1"></i> Vissza';
		}
		?>
			<a href="{{$back_url}}" class="btn btn-sm btn-block btn-secondary">{{$back_label}}</a>
		</div>
	</div>
</div>
<?php
}
else {
?>
<div class="container-fluid">
	<div class="container">
		<nav class="classic-tabs mx-2">
			<ul class="nav tabs-primary" id="categories" role="tablist">
				<li class="nav-item">
					<?php
					if($uri == 'https://' . $_SERVER['HTTP_HOST'].'/blog'){
						$activetab = "active show";
					}
					else {
						$activetab = "";
					}
					?>
					<a href="/blog" class="nav-link waves-light {{$activetab}} py-2">
						Címlap
					</a>
				</li>
			<?php
			$categories = DB::select("SELECT count(*) as db, category FROM `cms_content` WHERE cms_type='blog' and is_active=1 and deleted_at is null GROUP BY category ORDER BY category");
			foreach ($categories as $category) {
				if($uri == 'https://' . $_SERVER['HTTP_HOST'].'/blog/'.$category->category){
					$activetab = "active show";
				}
				else {
					$activetab = "";
				}
			?>
				<li class="nav-item">
					<a href="/blog/{{ $category->category }}" class="nav-link waves-light {{$activetab}} py-2" role="tab" aria-controls="profile-classic-shadow" aria-selected="true">
						{{ t('cms_blogcategory_'.$category->category) }}<span class="badge badge-primary ml-2">{{$category->db}}</span>
					</a>
				</li>
			<?php
			}
			?>
			</ul>
		</nav>
	</div>
</div>
<?php } //else ?>