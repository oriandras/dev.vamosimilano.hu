@extends('layouts.default')

@section('content')
<div class="clearfix"></div>
<div class="breadcrumbs">
    <ul class="items">
        <li class="item home">
            <a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">
                    {{ t('Főoldal') }} </a>
                        </li>
                    <li class="item cms_page">
                            <strong>{{ t('Hírlevél feliratkozás') }}</strong>
                        </li>
            </ul>
</div>

<main class="page-main" id="maincontent">

<div class="entry-content min-height">


    <div class="row">
        <div class="row-wrapper container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="">{{ t('Hírlevél feliratkozás') }}</h3>
						<p>{{ t('Köszönjük, hogy feliratkozott hírlevelünkre!') }}</p>
                </div>
            </div>
        </div>
    </div>



</div>
</div></div>
</main>

@stop
@section('footer_js')


@append

