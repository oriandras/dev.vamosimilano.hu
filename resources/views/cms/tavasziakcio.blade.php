@extends('layouts.default')

@section('content')
<style>
	body {
		overflow-x: hidden;
		width: 100vw !important;
		margin: 0 !important;
		background-color: #ffe6e6;
		/*background-image: url(<?php echo asset('media/landing/tavasziakcio/spring-repeat-bg.jpg'); ?>);*/
	}
	.text-red {
		color: #2d333c;
	}
	.text-white {
		color: #2d333c;
	}
	.circle-red {
		border: solid 1px #f9f9f9;
		border-radius: 50%;
		width: 8rem;
		height: 8rem;
		line-height: 8rem;
		background-color: #2d333c;
		color: #ffe6e6;
	}
	.counter-num p {
		border: 3px double #2d333c;
		/*background: white;*/
		margin: 0 auto;
		width: 8rem;
		font-size: 5rem !important;
		color: #2d333c;
		
	}
	.mp {
		display: none !important;
	}
	.masodperc {
		display: inline !important;
	}
	@media screen and (max-width: 768px) {
		.showroom img, .showroom p, .showroom h2 {
			text-align: center !important;
		}
		.showroom .img-responsive {
			margin-left: auto !important;
			margin-right: auto !important;
		}
	}
	@media screen and (max-width: 600px) {
		.counter-num p {
			width: 4rem;
			font-size: 2rem !important;
		}
		.mp {
			display: inline !important;
		}
		.masodperc {
			display: none !important;
		}
	}
	@media screen and (max-width: 420px) {
		h2.text-white {
			font-size: 2rem;
		}
	}
	.slidercontainer, .footer-top {
		display: none;
	}
	.img-responsive + h2 {
		margin-top: 8px;
	}
	.logo2 {
		margin: 0px auto !important;
		z-index: 2 !important;
	}
	.social-icons.new li a {
		display: inline-block;
		width: 60px;
		height: 60px;
		line-height: 60px !important;
		background: #f9f9f9;
		text-align: center;
		border-radius: 50%;
		color: #2d333c;
		font-size: 30px !important;
		margin: 1rem 2rem;
		transition: .3s background-color;
	}
	.social-icons.new li a:hover {
		color: white;
	}
	.social-icons.new li {
		margin-bottom: 0;
	}
</style>
<div class="clearfix"></div>
<div class="breadcrumbs">
	<ul class="items">
		<li class="item home">
			<a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">{{ t('Főoldal') }} </a>
		</li>
		<li class="item item-active">
			Black Friday 2018
		</li>
	</ul>
</div>
<div class="container-fluid" style="margin: 0;">
	<div class="row">
		<div class="col-md-12 text-center">
			<img src="{{ asset('media/landing/tavasziakcio/cover.png') }}" alt="Black Friday 50-70% kedvezmény - Vamosi Milano" class="img-responsive logo2">
		</div>
	</div>
</div>
<main class="container-fluid">
<div class="container">
	<div class="row">
		<div class="col-md-12 lead" style="padding-top: 30px;">
			<p>A tavasz a kibontakozás évszaka: a napsütés betölti kedvenc nappalinkat, minden virágzik és életre kel, lassan előkerülnek a trendi ruhadarabok is. Miért ne öltöztetnéd fel otthonodat is stílusosan? A Vamosi Milano kanapéi és ülőgarnitúrái minden lakásba elhozzák a hamisítatlan olasz hangulatot, a modern, kozmopolita életérzést. Válassz egy új, egyedi darabot az otthonodhoz illő színekben és méretben – akár 10 év vázgaranciával és ingyenes házhozszállítással!</p>
			<p>Induljon a tavasz legnépszerűbb kanapéinkkal és ülőgarnitúráinkkal! Találd meg Te is új kedvenced és rendelj webshopunkon keresztül vagy bemutatótermünkben!</p>
		</div>
	</div>
	<div class="row">
		<div class="col col-md-12 text-center0">
			<p class="countdown text-center lead" style="margin: 5rem auto;"><strong id="countdown" style="color:#f9f9f9;font-size: 4rem;"></strong></p>
			<table class="text-white text-center lead" style="width:50%;margin: 3rem auto;">
				<tr>
					<td class="counter-num">
						<p id="day"></p>
					</td>
					<td class="counter-num">
						<p id="hour"></p>
					</td>
					<td class="counter-num">
						<p id="minute"></p>
					</td>
					<td class="counter-num">
						<p id="second"></p>
					</td>
				</tr>
				<tr>
					<td style="width:25%;">NAP</td>
					<td style="width:25%;">ÓRA</td>
					<td style="width:25%;">PERC</td>
					<td style="width:25%;"><span class="mp">MP</span> <span class="masodperc">MÁSODPERC</span></td>
				</tr>
			</table>
			<script>
			function n(n){
				return n > 9 ? "" + n: "0" + n;
			}
			// Set the date we're counting down to
			var countDownDate = new Date("Mar 8, 2019 00:00:01").getTime();

			// Update the count down every 1 second
			var x = setInterval(function() {

				// Get todays date and time
				var now = new Date().getTime();

				// Find the distance between now and the count down date
				var distance = countDownDate - now;

				// Time calculations for days, hours, minutes and seconds
				var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);

				// Output the result in an element with id="demo"
				//document.getElementById("countdown").innerHTML = n(days) + " nap " + n(hours) + " óra "  + n(minutes) + " perc " + n(seconds) + " másodperc ";
				document.getElementById("day").innerHTML = n(days);
				document.getElementById("hour").innerHTML = n(hours);
				document.getElementById("minute").innerHTML = n(minutes);
				document.getElementById("second").innerHTML = n(seconds);

				// If the count down is over, write some text 
				if (distance < 0) {
					clearInterval(x);
					document.getElementById("countdown").innerHTML = "A Black Friday akció elindult!";
				document.getElementById("day").innerHTML = n(00);
				document.getElementById("hour").innerHTML = n(00);
				document.getElementById("minute").innerHTML = n(00);
				document.getElementById("second").innerHTML = n(00);
				}
			}, 1000);
			</script>
		</div>
	</div>
	<hr style="margin: 2rem 0px; background-color:#f9f9f9; border: none;">
	<div class="row">
		<div class="col col-sm-12 text-center" style="margin-bottom: 3rem;">
			<h2 class="h3-responsive text-red">Bemutatótermünkben a vásárlás valódi élmény!</h2>
		</div>
		<div class="col col-sm-3 text-center">
			<p><i class="fa fa-smile-o fa-3x circle-red" aria-hidden="true"></i></p>
			<p class="text-red lead">Szakértő és kedves személyzet</p>
		</div>
		<div class="col col-sm-3 text-center">
			<p><i class="fa fa-child fa-3x circle-red fa-flip-horizontal" aria-hidden="true"></i></p>
			<p class="text-red lead">Játszósarok a gyerkőcöknek</p>
		</div>
		<div class="col col-sm-3 text-center">
			<p><i class="fa fa-hand-lizard-o fa-3x circle-red" aria-hidden="true"></i></p>
			<p class="text-red lead">Megtapintható szövetpaletták</p>
		</div>
		<div class="col col-sm-3 text-center">
			<p><i class="fa fa-shopping-bag fa-3x circle-red" aria-hidden="true"></i></p>
			<p class="text-red lead">Bútorainkból kiállítási és outlet darabok</p>
		</div>
	</div>
	<hr style="margin: 8rem 0px; background-color:#f9f9f9; border: none;">
	<div class="row productgrid">
		<?php
		$metacount=$product_i=0;
		$pl1 = Product::where('products.product_id', 226)->active()->lang()->first();
		$pl2 = Product::where('products.product_id', 259)->active()->lang()->first();
		$pl3 = Product::where('products.product_id', 321)->active()->lang()->first();
		$pl4 = Product::where('products.product_id', 290)->active()->lang()->first();
		$pl5 = Product::where('products.product_id', 324)->active()->lang()->first();
		$pl6 = Product::where('products.product_id', 224 )->active()->lang()->first();
		$pl7 = Product::where('products.product_id', 288 )->active()->lang()->first();
		$pl8 = Product::where('products.product_id', 257 )->active()->lang()->first();
		$pl9 = Product::where('products.product_id', 228 )->active()->lang()->first();
		$pl10 = Product::where('products.product_id', 256 )->active()->lang()->first();
		$pl11 = Product::where('products.product_id', 294 )->active()->lang()->first();
		$pl12 = Product::where('products.product_id', 231 )->active()->lang()->first();
		$pl13 = Product::where('products.product_id', 317 )->active()->lang()->first();
		$pl14 = Product::where('products.product_id', 253 )->active()->lang()->first();
		$pl15 = Product::where('products.product_id', 333 )->active()->lang()->first();
		?>
		@include('cms.tavasziakcio_pc', ['product'=>$pl1, 'metacount'=>$metacount, 'product_i'=>$product_i])
		@include('cms.tavasziakcio_pc', ['product'=>$pl2, 'metacount'=>$metacount, 'product_i'=>$product_i])
		@include('cms.tavasziakcio_pc', ['product'=>$pl3, 'metacount'=>$metacount, 'product_i'=>$product_i])
		@include('cms.tavasziakcio_pc', ['product'=>$pl4, 'metacount'=>$metacount, 'product_i'=>$product_i])
		@include('cms.tavasziakcio_pc', ['product'=>$pl5, 'metacount'=>$metacount, 'product_i'=>$product_i])
		@include('cms.tavasziakcio_pc', ['product'=>$pl6, 'metacount'=>$metacount, 'product_i'=>$product_i])
		@include('cms.tavasziakcio_pc', ['product'=>$pl7, 'metacount'=>$metacount, 'product_i'=>$product_i])
		@include('cms.tavasziakcio_pc', ['product'=>$pl8, 'metacount'=>$metacount, 'product_i'=>$product_i])
		@include('cms.tavasziakcio_pc', ['product'=>$pl9, 'metacount'=>$metacount, 'product_i'=>$product_i])
		@include('cms.tavasziakcio_pc', ['product'=>$pl10, 'metacount'=>$metacount, 'product_i'=>$product_i])
		@include('cms.tavasziakcio_pc', ['product'=>$pl11, 'metacount'=>$metacount, 'product_i'=>$product_i])
		@include('cms.tavasziakcio_pc', ['product'=>$pl12, 'metacount'=>$metacount, 'product_i'=>$product_i])
		@include('cms.tavasziakcio_pc', ['product'=>$pl13, 'metacount'=>$metacount, 'product_i'=>$product_i])
		@include('cms.tavasziakcio_pc', ['product'=>$pl14, 'metacount'=>$metacount, 'product_i'=>$product_i])
		@include('cms.tavasziakcio_pc', ['product'=>$pl15, 'metacount'=>$metacount, 'product_i'=>$product_i])
	</div>
</div>
</main>
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div id="big_map" style="width:100vw!important;height: 40vh;"></div>
		</div>
	</div>
</div>
@stop
@section('footer_js')
<script src="//maps.googleapis.com/maps/api/js?v=3.17&key=AIzaSyD-2e09J6RRvWiyp_p0PR_zpiQuzM0mWqk" type="text/javascript"></script>
<script src="https://vamosimilano.hu/js/contact_v9.js"></script>
<script type="text/javascript">google.maps.event.addDomListener(window, 'load', initBigMap);</script>
@append

