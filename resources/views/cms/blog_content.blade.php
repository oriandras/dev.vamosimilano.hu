<?php $headversion = "newhead"; 
if( isset($headversion) && $headversion === "newhead" ) {
?>
@extends('layouts.default')
@section('content')
<?php
$uri_parts = explode('/', $_SERVER['REQUEST_URI'], 2);
$uri = 'https://' . $_SERVER['HTTP_HOST'].$uri_parts[0];
//$uri = $_SERVER['REQUEST_URI'];
?>
@include('cms.blog_categories_nav')
<div class="container">
	<div class="row">
		<section class="col-md-8 my-md-5">
			<header class="px-0 mb-4">
				<hgroup class="view">
					<img src="{{$blog->getImageUrl(1920)}}" alt="{{$blog->title}}" class="img-fluid">
					<div class="mask rgba-black-slight d-flex align-items-center">
						<h1 class="h2-responsive text-uppercase text-white mx-auto">{{$blog->title}}</h1>
					</div>
				</hgroup>
			</header>
			<div class="mt-4 px-2 " id="blogcontent">
				<hr>
				<p>
					<i class="fal fa-calendar-alt fa-fw mr-2"></i>{{t((new DateTime($blog->published_at))->format('Y'))}}. <span class="text-lowercase">{{t((new DateTime($blog->published_at))->format('F'))}}</span> {{(new DateTime($blog->published_at))->format('d')}}.</span>
					<span class="float-right text-uppercase"><i class="fal fa-folder mr-2"></i>{{ t('cms_blogcategory_'.$blog->category) }}</span>
				</p>
				<hr>
				<p class="lead">{{strip_tags($blog->short_description)}}</p>
				{{$blog->description}}
			</div>
			<footer class="my-4">
				<hr>
				<div class="row pt-3">
					<div class="col-md-6 text-right">
						<?php /*
						<div class="d-inline">
							<a href="fb-messenger://share/?link=https://{{$_SERVER['HTTP_HOST'].'/'.$_SERVER['REQUEST_URI']}}&app_id=256831345084199" class="btn btn-sm btn-dark"><i class="fab fa-facebook-messenger"></i> Send In Messenger</a>
						</div>
						*/ ?>
					</div>
					<div class="col col-md-2 text-right">
						<div class="d-inline fb-like" data-href="https://{{$_SERVER['HTTP_HOST'].'/'.$_SERVER['REQUEST_URI']}}" data-width="200" data-layout="button" data-action="recommend" data-size="small" data-show-faces="false" data-share="false"></div>
					</div>
					<div class="col col-md-2 text-right">
						<div class="d-inline mr-2" style="top: -5px;">
							<a href="https://www.pinterest.com/pin/create/button/" target="_blank" data-pin-do="buttonBookmark"></a>
						</div>
					</div>
					<div class="col col-md-2 text-right">
						<div class="d-inline"><a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=@vamosimilano" data-size="default">Tweet</a></div>
					</div>
				</div>
			</footer>
		</section>
		@include('cms.sidebar', ['headversion' => 'newhead'])
	</div>
</div>
<input type="hidden" name="current_uri" value="{{$uri}}" id="catch_the_uri">
<?php }
else {  ?>

<div class="clearfix"></div>
<div class="breadcrumbs">
    <ul class="items">
        <li class="item home">
            <a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">
                    {{ t('Főoldal') }} </a>
                        </li>
                        <li class="item cms_page">
                            <a href="{{ action('BlogController@index') }}">{{ t('Blog') }}</a>
                        </li>
                    <li class="item cms_page">
                            <strong>{{ $blog->title }}</strong>
                        </li>
            </ul>
</div>

<main id="maincontent" class="page-main">

    <div class="columns">
        <div class="column main">
            <div class="post-view">
    <div class="post-holder post-holder-2">
        <div class="post-banner">
            <div id="post_image_gallery_banner">


                                    <img src="{{$blog->getImageUrl(1280)}}" alt="{{$blog->title}}">

            </div>
        </div>
        <div class="post-date">
            <span class="day">{{(new DateTime($blog->published_at))->format('d')}}</span>
                                <span class="month">{{t('shortmonth_'.(new DateTime($blog->published_at))->format('m'))}}</span>
        </div>
        <div class="post-header">

            <h2 class="post-title theme-color">
                {{$blog->title}}            </h2>


<div class="post-info clear">
    <div class="item post-posed-date">
        <span class="label"><em class="porto-icon-calendar"></em></span>
        <span class="value">{{(new DateTime($blog->published_at))->format('Y-m-d')}}</span>
    </div>

    </div>        </div>
        <div class="post-content">
            <div class="post-description clearfix">
                <div class="post-text-hld">
                    {{$blog->description}}
                <div class="clear"></div>
                <div class="fb-like" data-href="{{ $blog->getUrl() }}" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>

                </div>
            </div>
        </div>

    </div>
</div>





        </div>
        @include('cms.sidebar')

    </div>

</main>

<?php } ?>
@stop
@section('footer_js')
@append