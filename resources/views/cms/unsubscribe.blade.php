@extends('layouts.default')

@section('content')
<div class="clearfix"></div>
<div class="breadcrumbs">
    <ul class="items">
        <li class="item home">
            <a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">
                    {{ t('Főoldal') }} </a>
                        </li>
                    <li class="item cms_page">
                            <strong>{{ t('Leiratkozás') }}</strong>
                        </li>
            </ul>
</div>

<main class="page-main" id="maincontent">

<div class="entry-content min-height">


    <div class="row">
        <div class="row-wrapper container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="">{{ t('Leiratkozás') }}</h3>
                    @if ($contact != false)
                        <p>{{ t('Biztosan leiratkozik hírleveleinkről, az alábbi e-mail címmel?') }}
                        <br>
                        {{$email}}
                        </p>
                        <form class="form subscribe ajax-form" id="subscribe-footer" action="{{ action('SubscribeController@unSubscribePost', $slug) }}" ajax-action="" method="post">
                            <input type="submit" class="btn btn-primary" value="{{ t('Igen, leiratkozom') }}">

                            {{ csrf_field() }}
                        </form>


                    @else
                        <p>{{ t('Ön már nincs feliratkozva hírleveleinkre, az alábbi e-mail címmel:') }}
                        <br>
                        {{$email}}
                        </p>

                    @endif
                </div>
            </div>
        </div>
    </div>



</div>
</div></div>
</main>

@stop
@section('footer_js')


@append

