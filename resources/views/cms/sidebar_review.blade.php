<?php
if( isset($headversion) && $headversion === "newhead" ) {
?>
<aside class="col-md-4 col-lg-3 my-5 pr-lg-5">
	<div class="row">
		<div class="col-12 blog_search mb-4" data-bind="scope: 'blog-search'">
			<h3 class="h4-responsive title text-uppercase">Keresés</h3>
			<form class="md-form form-inline active-primary" id="blog_search_mini_form" action="{{ action('BlogController@butorszepsegverseny') }}" method="get">
				<input id="blog_search" class="form-control w-75" type="text" name="q" aria-label="Search" maxlength="128" placeholder="{{t('Keresés a bejegyzésekben')}}">
				<button class="btn btn-primary btn-sm float-right" type="submit"><i class="fal fa-search"></i></button>
			</form>
		</div>
		<?php
		if (isset($random_posts) && sizeof($random_posts)) {
		?>
		<div class="col-12 last5post mb-4">
			<h3 class="h4-responsive title text-uppercase">Bútorszépségverseny ajánló</h3>
			<div id="carousel-post" class="carousel slide carousel-fade" data-ride="carousel">
				<div class="carousel-inner" role="listbox">
			<?php
			$active = 0;
			foreach ($random_posts as $r_post) {
				$headers = @get_headers($r_post->getImageUrl('1920'));
				//print_r($headers);
				if( $headers[0] == 'HTTP/1.1 200 OK' ) {
					$img_url = $r_post->getImageUrl('1920');
				}
				else {
					$img_url = "https://static.vamosimilano.hu/carousel/full/147-morbi-eu-sem-at-est-egestas-venenatis-a-convallis-nulla-5600.jpg";
				}
				if ($active == 0) {
					$iactive = "active";
				}
				else {
					$iactive = "";
				}
			?>
					<div class="carousel-item {{$iactive}}">
						<div class="view">
							<img src="{{$img_url}}" alt="{{$blog->title}}" class="d-block w-100 img-fluid">
							<div class="mask rgba-black-slight"></div>
						</div>
						<div class="carousel-caption">
							<p class="text-center"><a href="{{$r_post->getUrl()}}" class="text-white">{{$r_post->title}}</a></p>
						</div>
					</div>
			<?php
				$active++;
			}//foreach
			?>
					<a class="carousel-control-prev" href="#carousel-post" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carousel-post" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
		<?php
		}//if
		?>
	<?php
	if ( isset($highlighted_products) ) {
	?>
		<div class="col-12 kiemelt-akciok mb-4">
			<h3 class="h4-responsive title text-uppercase">Kiemelt akcióink</h3>
			<section class="row products-listing mx-0 px-0">
			<?php
			foreach ($highlighted_products as $highlighted_product) {
				$p = Product::where('products.product_id', $highlighted_product->product_id)->active()->lang()->first();
			?>
			@include('webshop.product_card', ['product'=>$p, 'metacount'=>1, 'product_i'=>1, 'col'=>1])
			<?php
			}//foreach
			?>
			</section>
		</div>
	<?php
	}//if
	?>
	</div>
</aside>
<?php
}
else {
?>
<div class="sidebar sidebar-additional">
        <div class="widget block blog-search" data-bind="scope: 'blog-search'">
        	<div class="block-content">
        		<form class="form" id="blog_search_mini_form" action="{{ action('BlogController@butorszepsegverseny') }}" method="get">
                    <div class="field search">
                        <label class="label" for="blog_search">
                            <span>{{ t('Search') }}</span>
                        </label>
                        <div class="control">
                            <input id="blog_search" name="q" value="" placeholder="{{ t('Írjon be egy kulcsszót') }}" class="input-text" maxlength="128" role="combobox" aria-haspopup="false" aria-autocomplete="both" autocomplete="off" type="text">
                        </div>
                    </div>
                    <div class="actions">
                        <button type="submit"  title="{{ t('Search') }}" class="action search">
                            <span>{{ t('Search') }}</span>
                        </button>
                    </div>
                </form>
        	</div>
        </div>
        <div class="clearfix"></div>
        @if (isset($random_posts) and sizeof($random_posts))
            <div class="widget block block-recent-posts" >
                <div class="block-title">
                    <strong>{{ t('Bútorszépségverseny Ajánló') }}</strong>
                </div>
                <div class="block-content">
                    <ul>

                    @foreach ($random_posts as $r_post)
                        <li class="clearfix">
                            <div class="post-image">
                                <a href="{{$r_post->getUrl()}}" title="{{$r_post->title}}"><img src="{{$r_post->getImageUrl(150)}}" alt="{{$r_post->title}}"></a>
                            </div>
                            <a class="post-item-link" href="{{$r_post->getUrl()}}" title="{{$r_post->title}}">

                                {{$r_post->title}}                </a>
                            <div class="post-time">{{(new DateTime($r_post->published_at))->format('Y-m-d')}}</div>
                        </li>
                    @endforeach
                </ul>
                </div>
            </div>
        @endif
        @if (isset($highlighted_products))
            @foreach($highlighted_products as $highlighted_product)
             <div class="filter-options-item sidebar-highlighted">
                    <div class="filter-options-title" >{{ t('A hónap bútora')." - ".$highlighted_product->getName() }}</div>
                    <div class="filter-options-content nopadding text-center">

                        <a href="{{$highlighted_product->getUrl()}}" title="{{$highlighted_product->getName()}}"><img alt="{{$highlighted_product->getName()}}" src="{{ $highlighted_product->getDefaultImageUrl(600) }}"/></a>
                        <?php
                            $price_full = $highlighted_product->getDefaultFullPrice();
                            $price = $highlighted_product->getDefaultPrice();
                        ?>
                        @if ($price_full > $price)
                        <span class="old-price highlighted">{{money($price_full)}}</span>
                        @endif
                        <span class="price highlighted">{{money($price)}}</span>
                        <br>
                        <a href="{{$highlighted_product->getUrl()}}" title="{{$highlighted_product->getName()}}" class="btn btn-primary white-text btn-block">{{ t('Részletek') }}</a>


                    </div>
                </div>
            @endforeach
         @endif
        </div>
<?php } ?>