@extends('layouts.default')

@section('content')
<div class="clearfix"></div>
<div class="breadcrumbs">
    <ul class="items">
        <li class="item home">
            <a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">
                    {{ t('Főoldal') }} </a>
                        </li>
                    <li class="item cms_page">
                            <strong>{{ t('Feliratkozás') }}</strong>
                        </li>
            </ul>
</div>

<main class="page-main" id="maincontent">

<div class="entry-content min-height">


    <div class="row">
        <div class="row-wrapper container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="">{{ t('Feliratkozás') }}</h3>
                    @if (Input::get('ok'))
                    <p>{{ t('Köszönjük, hogy feliratkozott hírlevelünkre!') }}</p>
                    @else
                    <p>{{ t('Értesüljön elsőként a meghírdetett akcióinkról') }}</p>
                                        <label for="footer_newsletter"></label>
                                        <form class="form subscribe ajax-form" id="subscribe-footer" action="{{ action('SubscribeController@subscribe') }}" ajax-action="" method="post">
                                            <input type="hidden" name="url" value="{{ Request::url() }}">
                                            <div class="row form-group">
                                                <div class="col-md-6">

                                                    <div class="control">
                                                        <input type="text" name="lastname" autocomplete="off" placeholder="{{ t('Vezetéknév') }}" value="{{ Input::old('lastname') }}" class="form-control" required=""  />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">

                                                    <div class="control">
                                                        <input type="text"  name="firstname" autocomplete="off" placeholder="{{ t('Keresztnév') }}" value="{{ Input::old('firstname') }}" class="form-control" required=""  />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12">

                                                    <div class="control">
                                                        <input name="email" autocomplete="off" placeholder="{{ t('E-mail') }}" value="{{ Input::old('email') }}" type="email" class="form-control"  required=""  />
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="clearfix"></div>

                                            <div class="actions">
                                                <button class="action subscribe primary" title="{{ t('Feliratkozás') }}" type="submit">
                                                    <span>{{ t('Feliratkozás') }}</span>
                                                </button>
                                            </div>
                                            {{ csrf_field() }}
                                        </form>

                    @endif
                    </div>
            </div>
        </div>
    </div>



</div>
</div></div>
</main>

@stop
@section('footer_js')


@append

