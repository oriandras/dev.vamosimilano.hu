2
<form id="subForm" class="js-cm-form" action="https://www.createsend.com/t/subscribeerror?description=" method="post" data-id="{{t('newlettersubscribeid')}}">

    <p>

        <input id="fieldName" class="form-control" name="cm-name" type="text" placeholder="{{ t('Név') }}" />

    </p>

    <p>

        <input id="fieldEmail" class="js-cm-email-input form-control" name="{{t('newlettersubscribename')}}" type="email" placeholder="{{ t('E-mail') }}" required /> 

    </p>

	<div class="checkbox">

		<label><input id="fieldok" class="js-cm-email-input" type="checkbox" required />{{t('Feliratkozasengedélyszöveg')}}</label>

	</div>

    <p>

        <button class="action subscribe primary" type="submit">{{t('Feliratkozás')}}</button> 

    </p>

</form>

<script type="text/javascript" src="https://js.createsend1.com/javascript/copypastesubscribeformlogic.js"></script>

