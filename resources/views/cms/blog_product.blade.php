<?php 
$p = Product::where('products.product_id', $product->product_id)->active()->lang()->first();
if ( ismobile() ) {
	$col = 1;
}
elseif ( istablet() ) {
	$col = 2;
}
else {
	$col = 4;
}
?>
@include('webshop.product_card', ['product'=>$p, 'metacount'=>1, 'product_i'=>1, 'col'=>$col])