@extends('layouts.default')

@section('content')

<div class="clearfix"></div>
<div class="breadcrumbs">
    <ul class="items">
        <li class="item home">
            <a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">
                    {{ t('Főoldal') }} </a>
                        </li>
                    <li class="item cms_page">
                            <strong>{{ t('Hírlevél leiratkozás') }}</strong>
                        </li>
            </ul>
</div>
<main class="page-main" id="maincontent">
        <div class="columns">
    <div class="column main">
		<legend class="legend"><span>{{ t('Hírlevél leiratkozás') }}</span></legend><br>
		
	<form action="{{t('newletterunsubscribeid')}}" method="post">
		<p>
			<input id="fieldEmail" class="js-cm-email-input form-control" name="{{t('newlettersubscribename')}}" type="email" placeholder="{{ t('E-mail') }}" required /> 
		</p>
		<p>
			<button class="action subscribe primary" type="submit">{{t('Leiratkozás')}}</button> 
		</p>
	</form>
	<script type="text/javascript" src="https://js.createsend1.com/javascript/copypastesubscribeformlogic.js"></script>

	
    <div class="clearfix"></div>

</div>

    </div>
</main>




@stop



