<?php $headversion = "newhead" ?>
@extends('layouts.default')
@section('content')
<?php
if( isset($headversion) && $headversion == "newhead" ){
?>
<script>
function loadLoan(id) {
	if( id==235 ) {
		
	}
}
</script>
<div class="container">
	<div class="row">
		<div class="col-md-3">
		<?php
		$active1 = $active2 = $active3 = "btn-flat";
		if( !isset($_GET["loan"]) || $_GET["loan"]==235) {
			$active1="btn-primary";
		}
		elseif( $_GET["loan"] ==203) {
			$active2="btn-primary";
		}
		elseif( $_GET["loan"] ==208) {
			$active3="btn-primary";
		}
		?>
			<h2 class="h4-responsive title text-uppercase mt-4">Hitelkonstrukciók</h2>
			<ul class="list-unstyled">
				<li>
					<a href="{{ url('/aruhitel?loan=215') }}" class="btn btn-block {{$active1}}" data-loan="235" onClick="loadLoan('235')">O% THM</a>
				</li>
				<li>
					<a href="{{ url('/aruhitel?loan=203') }}" class="btn btn-block {{$active2}}" data-loan="203" onClick="loadLoan('203')">Fix futamidejű hitel</a>
				</li>
				<li>
					<a href="{{ url('/aruhitel?loan=208') }}" class="btn btn-block {{$active3}}" data-loan="208" onClick="loadLoan('208')">10 hónap futamidejű hitel</a>
				</li>
			</ul>
		</div>
		<div class="col-md-9" id="kalkulator">
		<?php
		if( !isset($_GET["loan"]) || $_GET["loan"]==235) {
		?>
		@include('calculators.credit_235')
		<?php
		}
		elseif( $_GET["loan"] ==203) {
		?>
		@include('calculators.credit_203')
		<?php
		}
		elseif( $_GET["loan"] ==208) {
		?>
		@include('calculators.credit_208')
		<?php
		}
		?>
		</div>
	</div>
</div>
<?php
}
else {
?>
<div class="clearfix"></div>
<div class="breadcrumbs">
    <ul class="items">
        <li class="item home">
            <a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">
                    {{ t('Áruhitel') }} </a>
                        </li>

            </ul>
</div>

<main class="page-main" id="maincontent">

<div class="entry-content min-height">


    <div class="row">
        <div class="row-wrapper container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="">{{ t('Áruhitel') }}</h3>
                    <iframe frameborder="0" width="100%;" height="1000" src="https://static.vamosimilano.hu/loancalculator/kalkulator1.php" scrolling="no" style="overflow-y: hidden;"></iframe>
                </div>
            </div>
        </div>
    </div>




</div>
</div></div>
</main>
<?php
}//else
?>
@stop
@section('footer_js')


@append

