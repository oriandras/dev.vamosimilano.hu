<?php $headversion = 'newhead'; ?>
@extends('layouts.default')
@section('content')
<style>
	.counter-num p {
		width: 4rem;
		margin: .5rem auto;
		padding: 0.4rem 0.6rem;
		font-size: 2rem;
		color: #fed000;
		background-color: #2e2e2e;
	}
	.jarallax {
		height: 350px;
		max-height: 350px !important;
		min-height: 350px !important;
	}
</style>
<div class="container-fluid">
	<div class="row" style="background-color: #fed000;" >
		<header class="col-12">
			<div class="view">
				<img src="{{asset('/media/landing/nyitasiakcio/transparent.png')}}" alt="Nyitási akció - Vamosi Milano" class="mx-auto wow jackInTheBox" style="height: 450px !important;">
				<div class="mask flex-center">
					<div class="row">
						<div class="col-md-12 mb-4 text-center">
							<table class="text-center lead w-75 mx-auto">
								<tr>
									<td colspan="4">
										<p class="lead"><strong>Lépjen be a Vamosi Milano exkluzív világába! Az új szegedi bemutatótermünk nyitása alkalmából országos akciót hírdetünk!</strong></p>
										<p>Egyedi, Olaszországban tervezett, prémium minőségű bútorok az Ön igényeire szabva</p>
									</td>
								</tr>
								<tr>
									<td class="counter-num wow fadeInLeft" width="25%">
										<p id="day" class="z-depth-1"></p>
									</td>
									<td class="counter-num wow tada" width="25%">
										<p id="hour" class="z-depth-1"></p>
									</td>
									<td class="counter-num wow rollIn" width="25%">
										<p id="minute" class="z-depth-1"></p>
									</td>
									<td class="counter-num wow fadeInRight" width="25%">
										<p id="second" class="z-depth-1"></p>
									</td>
								</tr>
								<tr class="text-dark">
									<td style="width:25%;">NAP</td>
									<td style="width:25%;">ÓRA</td>
									<td style="width:25%;">PERC</td>
									<td style="width:25%;">
										<?php if( ismobile() ) { ?>
										<span class="mp">MP</span>
										<?php } else { ?>
										<span class="masodperc">MÁSODPERC</span>
										<?php } ?>
									</td>
								</tr>
							</table>
							<?php if( date('Ymd') > "20190425" ) { ?>
							<p class="countdown text-center lead mt-2"><strong id="countdown">ér véget az akció</strong></p>
							<?php } else { ?>
							<p class="countdown text-center lead mt-2"><strong id="countdown">múlva indul az akció</strong></p>
							<?php } ?>
							<script>
							function n(n){
								return n > 9 ? "" + n: "0" + n;
							}
							// Set the date we're counting down to
							<?php if( date('Ymd') > "20190425" ) { ?>
							var countDownDate = new Date("Apr 28, 2019 23:59:59").getTime();
							<?php } else { ?>
							var countDownDate = new Date("Apr 26, 2019 00:00:00").getTime();
							<?php } ?>

							// Update the count down every 1 second
							var x = setInterval(function() {

								// Get todays date and time
								var now = new Date().getTime();

								// Find the distance between now and the count down date
								var distance = countDownDate - now;

								// Time calculations for days, hours, minutes and seconds
								var days = Math.floor(distance / (1000 * 60 * 60 * 24));
								var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
								var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
								var seconds = Math.floor((distance % (1000 * 60)) / 1000);

								// Output the result in an element with id="demo"
								//document.getElementById("countdown").innerHTML = n(days) + " nap " + n(hours) + " óra "  + n(minutes) + " perc " + n(seconds) + " másodperc ";
								document.getElementById("day").innerHTML = n(days);
								document.getElementById("hour").innerHTML = n(hours);
								document.getElementById("minute").innerHTML = n(minutes);
								document.getElementById("second").innerHTML = n(seconds);

								// If the count down is over, write some text 
								if (distance < 0) {
									clearInterval(x);
									//document.getElementById("countdown").innerHTML = "A Black Friday akció elindult!";
								document.getElementById("day").innerHTML = n(00);
								document.getElementById("hour").innerHTML = n(00);
								document.getElementById("minute").innerHTML = n(00);
								document.getElementById("second").innerHTML = n(00);
								}
							}, 1000);
							</script>
						</div>
					</div>
				</div>
			</div>
		</header>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-12 my-4 wow fadeInUp">
			<h1 class="h1-responsive text-uppercase title-center mt-4 mb-0">Válasszon az alábbi termékcsoportjainkból akciós áron</h1>
		</div>
		<div class="col-md-4 my-4 text-center wow fadeInLeft">
			<a href="{{ url('/termekek/kanape/l-alaku-kanapek') }}" class="">
				<img src="https://static.vamosimilano.hu/product/600/233_contarini5105.jpg" alt="Kanapék" class="img-fluid z-depth-1 my-3">
			</a>
			<a href="{{ url('/termekek/kanape/l-alaku-kanapek') }}" class="btn btn-amber amber accent-2 text-dark btn-sm">Sarokkanapék</a>
		</div>
		<div class="col-md-4 my-4 text-center wow fadeInLeft">
			<a href="{{ url('/termekek/kanape/u-alaku-kanapek') }}" class="">
				<img src="https://static.vamosimilano.hu/product/600/257_gadzone7049.jpg" alt="Kanapék" class="img-fluid z-depth-1 my-3">
			</a>
			<a href="{{ url('/termekek/kanape/u-alaku-kanapek') }}" class="btn btn-amber amber accent-2 text-dark btn-sm">U-alakú kanapék</a>
		</div>
		<div class="col-md-4 my-4 text-center wow fadeInLeft">
			<a href="{{ url('/termekek/kanape/egyenes-kanapek') }}" class="">
				<img src="https://static.vamosimilano.hu/product/600/eng_317_salvia2564.jpg" alt="Kanapék" class="img-fluid z-depth-1 my-3">
			</a>
			<a href="{{ url('/termekek/kanape/egyenes-kanapek') }}" class="btn btn-amber amber accent-2 text-dark btn-sm">Egyenes kanapék</a>
		</div>
		<div class="col-md-4 my-4 wow tada text-center">
			<a href="{{ url('/termekek/kanape/franciaagyak') }}" class="">
				<img src="https://static.vamosimilano.hu/product/full/eng_erizzo_florida6_kanape.jpg" alt="Kanapék" class="img-fluid z-depth-1 my-3">
			</a>
			<a href="{{ url('/termekek/kanape/franciaagyak') }}" class="btn btn-amber amber accent-2 text-dark btn-sm">Franciaágyak és heverők</a>
		</div>
		<div class="col-md-4 my-4 wow fadeInRight text-center">
			<a href="{{ url('/termekek/matrac') }}" class="">
				<img src="{{ asset('media/landing/nyitasiakcio/balm.jpg') }}" alt="Kanapék" class="img-fluid z-depth-1 my-3">
			</a>
			<a href="{{ url('/termekek/matrac') }}" class="btn btn-amber amber accent-2 text-dark btn-sm">Matracok</a>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-4 my-4 text-center wow fadeInLeft">
			<img src="{{asset('/media/landing/nyitasiakcio/szovetek-1x1.jpg')}}" alt="Prémium szövetek a Vamosi Milanonál" class="img-fluid z-depth-1 rounded-circle w-50 my-3 mx-auto">
			<h2 class="h4-responsive title-center text-uppercase mb-3">VÁLOGASSON csak ezen a hétvégén PRÉMIUM SZÖVETEINKBŐL FELÁR NÉLKÜL </h2>
			<p class="text-center">Tűzálló, foltálló, reach oeko tex gyártástechnológiájú, bolyhosodás elleni védelemmel ellátott, lélegző, extra magas kopásállóságú szöveteink felár nélkül.</p>
			<p class="text-center">Keresse az akció alatt a <strong>Lisbon, Dubai, Musa, Liverpool, Madeira és Uruguay</strong> szöveteinket több száz színben.</p>
		</div>
		<div class="col-md-4 my-4 text-center wow tada">
			<img src="{{asset('/media/landing/nyitasiakcio/garancia-1x1.jpg')}}" alt="1 év plusz garancia" class="img-fluid z-depth-1 rounded-circle w-50 my-3 mx-auto">
			<h2 class="h4-responsive title-center text-uppercase mb-3">Ajándékba adunk +1 év kiterjesztett garanciát</h2>
			<p class="text-center">Vásároljon új bútora mellé kiterjesztett garanciát, amelyből egy évet ezen a hétvégén ajándékba adunk, hogy az Ön igényeihez igazodva, hosszútávon is élvezhesse a Vamosi Milano nyújtotta biztonságot és kényelmet. Amennyiben a maximális, 10 év időtartamra igényli kiterjesztett garanciákat, akkor most önköltségi áron, akár öt alkalommal is újrakárpitoztathatja velünk bútorát.</p>
		</div>
		<div class="col-md-4 my-4 text-center wow fadeInRight">
			<img src="{{asset('/media/landing/nyitasiakcio/kiszallitas-1x1.jpg')}}" alt="Országos házhozszállítás" class="img-fluid z-depth-1 rounded-circle w-50 my-3 mx-auto">
			<h2 class="h4-responsive title-center text-uppercase mb-3">Ingyenes kiszállítás az ország egész területén</h2>
			<p class="text-center">Rendeljen kanapéja mellé most akciós áron puffot, fotelt, dohányzóasztalt vagy akár előszobafalat, és élvezze országosan ingyenes házhozszállítási szolgáltatásunkat!</p>
		</div>
		<div class="col-md-12 my-4">
			<div class="view">
				<?php if( ismobile() ) { ?>
				<img src="{{asset('/media/landing/nyitasiakcio/bg-utazastamogatas_m.jpg')}}" class="img-fluid" alt="">
				<?php } else { ?>
				<img src="{{asset('/media/landing/nyitasiakcio/bg-utazastamogatas.jpg')}}" class="img-fluid" alt="">
				<?php } ?>
				<div class="mask flex-center rgba-black-light">
					<div class="d-block text-center wow rubberBand">
						<h3 class="white-text text-uppercase mb-3 h3-responsive"><strong>Utiköltségéből 12000 forintot jóváírunk bemutatótermi rendelésénél!</strong></h3>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12 wow fadeInUp mt-2 mb-4">
			<p>Bemutatótermeink városain kívülről érkező vásárlóink <strong>utiköltségéből 12000 forintot jóváírunk bemutatótermi rendeléséből</strong>!</p>
			<p>Vásároljon az 50%-kal leértékelt termékeinkből bemutatótermünkben, kérjen házhozszállitást, és támogatjuk utazását! Mutassa be lakcímkártyáját és, ha nem Szegeden vagy Budapesten él, és nem is oda kéri a házhozszállitást, 12000 forintot jóváirunk vásárlásából.</p>
			<p>Amennyiben egy összegben fizeti ki bemutatótermünkben a bútorát <strong>további 3% kedvezmény igénybevételére is jogosult!</strong></p>
			<p class="text-center"><a href="/kapcsolat" class="btn btn-amber amber accent-2 text-dark">Válassza ki az Önhöz legközelebbi bemutatótermünket!</a></p>
		</div>
	</div>
</div>
<div class="container-fluid" style="background-color: #fed000;">
	<div class="container wow tada">
		<div class="row">
			<div class="col-12">
				<h2 class="h2-responsive text-uppercase my-4 text-center">Ízelítő akciós termékeinkből<br>
				Április 26-28. között kanapéink, franciaágyaink, heverőink és matracaink a jelenlegi kedvezményektől eltérő, akár 50-70%-os kedvezménnyel lesznek elérhetőek.
				</h2>
			</div>
		</div>
		<div class="row productgrid">
			<?php
			$metacount=$product_i=0;
			$pl1 = Product::where('products.product_id', 255)->active()->lang()->first();
			$pl2 = Product::where('products.product_id', 1874)->active()->lang()->first();
			$pl3 = Product::where('products.product_id', 1494)->active()->lang()->first();
			$pl4 = Product::where('products.product_id', 343)->active()->lang()->first();
			$pl5 = Product::where('products.product_id', 290)->active()->lang()->first();
			$pl6 = Product::where('products.product_id', 262 )->active()->lang()->first();
			$pl7 = Product::where('products.product_id', 249 )->active()->lang()->first();
			$pl8 = Product::where('products.product_id', 238 )->active()->lang()->first();
			?>
			@include('webshop.product_card', ['product'=>$pl1, 'metacount'=>$metacount, 'product_i'=>$product_i])
			@include('webshop.product_card', ['product'=>$pl2, 'metacount'=>$metacount, 'product_i'=>$product_i])
			@include('webshop.product_card', ['product'=>$pl3, 'metacount'=>$metacount, 'product_i'=>$product_i])
			@include('webshop.product_card', ['product'=>$pl4, 'metacount'=>$metacount, 'product_i'=>$product_i])
			@include('webshop.product_card', ['product'=>$pl5, 'metacount'=>$metacount, 'product_i'=>$product_i])
			@include('webshop.product_card', ['product'=>$pl6, 'metacount'=>$metacount, 'product_i'=>$product_i])
			@include('webshop.product_card', ['product'=>$pl7, 'metacount'=>$metacount, 'product_i'=>$product_i])
			@include('webshop.product_card', ['product'=>$pl8, 'metacount'=>$metacount, 'product_i'=>$product_i])
		</div>
		<div class="row">
			<div class="col-12 text-center">
				<a href="/termekek" class="btn btn-secondary btn-lg mb-4">Válogasson további termékeink közül</a>
			</div>
		</div>
	</div>
</div>
@stop
@section('footer_js')
<script>
// object-fit polyfill run
objectFitImages();

/* init Jarallax */
jarallax(document.querySelectorAll('.jarallax'));

jarallax(document.querySelectorAll('.jarallax-keep-img'), {
    keepImg: true,
});
</script>
<script src="//maps.googleapis.com/maps/api/js?v=3.17&key=AIzaSyD-2e09J6RRvWiyp_p0PR_zpiQuzM0mWqk" type="text/javascript"></script>
<script src="https://vamosimilano.hu/js/contact_v9.js"></script>
<script type="text/javascript">google.maps.event.addDomListener(window, 'load', initBigMap);</script>
@append
