@extends('layouts.default')
@section('content')
<?php
if (isset($headversion) && $headversion==="newhead"){
	if( isset($_COOKIE["filterlist"]) ){
		//setcookie("filterlist", $_COOKIE["filterlist"], time() - 3600);
		unset($_COOKIE["filterlist"]);
	}
?>
	@include('webshop.block.breadcrumbs_2')
	@include('webshop.product_listing')
<?php
}
else {
?>


@include('webshop.block.breadcrumbs')
<main class="page-main" id="maincontent">
<div class="columns">

<div class="column main{{(((isset($search) or isset($all_categories)) and !sizeof($products)) ? ' width-100percent' : '')}}" >
    @if (isset($current_category))
        <h1 id="page-title-heading" class="page-title">{{ $current_category->name }}</h1>
        @if ($current_category->description)
        <p >{{ $current_category->description }}</p>
        @endif

    @endif

    <div id="layer-product-list" itemscope itemtype="http://schema.org/ItemList">
            @if (!isset($current_category))
            <h1 id="page-title-heading" class="page-title">{{ (isset($search) ? t('Keresés').' - '.$search : '') }}</h1>
            @endif
            @if (sizeof($all_categories))
            <div class="single-images paddingtop30">
        		<div class="row">
        		    @foreach ($all_categories as $top_category)
            		    <div class="col-sm-4 paddingbottom15">
            				<a class="image-link" href="{{$top_category->getUrl()}}">
            					<img src="{{ $top_category->getImageUrl(370) }}" title="{{ $top_category->name }}" width="370" height="254" alt="{{ $top_category->name }}" />
            					<span class="category-title">{{ $top_category->name }}</span>
            				</a>
            			</div>

                    @endforeach

        		</div>
        	</div>
        	@endif
            @if (sizeof($products))
            <?php
                $textile_ids = [];
                $min = $max = $min_x = $max_x = $min_y = $max_y = $lining = 0;
            ?>
            <div class="toolbar toolbar-products">
                <p id="toolbar-amount" class="toolbar-amount">
                <span class="toolbar-number"><span class="current">{{ sizeof($products) }}</span><span> / </span><span itemprop="numberOfItems">{{ sizeof($products) }}</span></span> {{ t('termék') }}
                </p>
                <div class="pages">
                <strong id="paging-label" class="label pages-label">{{ t('Oldal') }}</strong>
                </div>
                <div class="toolbar-sorter sorter">
                    <label for="sorter" class="sorter-label">{{ t('Rendezés') }}</label>
                        <select class="sorter-options" data-role="sorter" onchange="shortProduct(this.value)" id="sorter">
                            <option selected="selected" value="null"> {{ t('Alapértelmezett') }}</option>
                            <option value="price"> {{ t('Ár szerint növekvő') }}</option>
                            <option value="price_desc"> {{ t('Ár szerint csökkenő') }}</option>

                            </select>

                </div>
            </div>
            <div class="products wrapper grid columns2  products-grid">

                    <ol class="products list items product-items">
						<?php $i=0; ?>
                        @foreach ($products as $lazy_counter => $product)
                            <?php
                                $textiles = $product->getTextilePreviews(9);
                                $price = $product->getDefaultPrice();
                                $price_full = $product->getDefaultFullPrice();
                                foreach ($textiles as $textile){
									if ($textile->textile_id) {
										if (ProductTextile::lang()->find($textile->textile_id)->visible == 'no') {
											continue;
										}
									}
									if ($textile->textile2_id) {
										if (ProductTextile::lang()->find($textile->textile2_id)->visible == 'no') {
											continue;
										}
									}
                                    $textile_ids[$textile->textile_id] = $textile->textile_id;
                                    if ($textile->textile2_id) {
                                        $textile_ids[$textile->textile2_id] = $textile->textile2_id;
                                    }
                                }
                                if ($product->lining != 'none') {
                                    $lining = 1;
                                }
                                $size_data = [];
                                $item_min_x = $item_min_y = $item_max_x = $item_max_y = 0;
                                $sizes = $product->getSizes();
                                if (sizeof($sizes)){
                                    foreach ($sizes as $size){

                                        $size->size_x = getUnit($size->size_x);
                                        $size->size_y = getUnit($size->size_y);

                                        if ($size->size_x < $min_x or $min_x == 0) {
                                            $min_x = $size->size_x;
                                        }
                                        if ($size->size_y < $min_y or $min_y == 0) {
                                            $min_y = $size->size_y;
                                        }
                                        if ($size->size_x > $max_x or $max_x == 0) {
                                            $max_x = $size->size_x;
                                        }
                                        if ($size->size_y > $max_y or $max_y == 0) {
                                            $max_y = $size->size_y;
                                        }

                                        if ($size->size_x < $item_min_x or $item_min_x == 0) {
                                            $item_min_x = $size->size_x;
                                        }
                                        if ($size->size_y < $item_min_y or $item_min_y == 0) {
                                            $item_min_y = $size->size_y;
                                        }
                                        if ($size->size_x > $item_max_x or $item_max_x == 0) {
                                            $item_max_x = $size->size_x;
                                        }
                                        if ($size->size_y > $item_max_y or $item_max_y == 0) {
                                            $item_max_y = $size->size_y;
                                        }

                                    }
                                }
                                $size_data = [
                                  'item_min_x' => $item_min_x,
                                  'item_min_y' => $item_min_y,
                                  'item_max_x' => $item_max_x,
                                  'item_max_y' => $item_max_y,
                                ];
                                if ($price < $min or $min == 0) {
                                    $min = $price;
                                }
                                if ($price > $max or $max == 0) {
                                    $max = $price;
                                }
								$i++;
                            ?>
                            @include('webshop.block.product_list', ['product' => $product, 'lazy_counter' => $lazy_counter, 'textiles' => $textiles, 'price' => $price, 'price_full' => $price_full, 'size_data' => $size_data])
                        @endforeach
                    </ol>
            </div>
            @elseif(isset($search))
                <p>{{ t('Sajnos nincs találata a keresésnek! Próbálkozzon más kulcsszóval!') }}</p>
            @endif
    </div>

</div>
 @if (sizeof($products))
    @include('webshop.sidebar')
@endif
</div>
</main>
@section('footer_js')
    @if (sizeof($products))
    <script type="text/javascript" src="{{ asset('js/isotope_custom.js') }}?v21"></script>
    <script>
        var $grid;
        $(function() {
            initIsotope();
            initRange({{$min}},{{$max}}, '{{Config::get('shop.'.getShopCode().'.shop_currency')}}');
            @if ($min_x > 0 and $max_x > 0)
            initWidthRange({{$min_x}},{{$max_x}});
            @endif
            @if ($min_y > 0 and $max_y > 0)
            initHeightRange({{$min_y}},{{$max_y}});
            @endif
        });
    </script>

    @endif


@append

<?php	
} //nem newhead else
?>
@stop
@section('header_js')
            <script src="https://connect.facebook.net/hu_HU/all.js" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('js/facebook_hover.js') }}"></script>
@append
