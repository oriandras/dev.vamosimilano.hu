<div class="sidebar sidebar-main">



     <div id="layered-filter-block" class="block filter">
        <div class="block-title filter-title" >
            <strong id="filter-openlink">{{ t('Szűrő') }}</strong>
        </div>

            @if ($min > 0 and $max > 0)
            <div class="filter-options-item">
                <div class="filter-options-title" >{{ t('Ár') }}
                (<span id="amount">{{ money($min).' - '.money($max) }}</span>)</div>
                <div class="filter-options-content">
                    <div id="slider-range"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
            @endif

            @if ($min_x > 0 and $max_x > 0)

            <div class="filter-options-item">
                <div class="filter-options-title" >{{ t('Hosszúság') }}
                (<span id="max-x">{{ $min_x.' - '.$max_x }}</span>)</div>
                <div class="filter-options-content">
                    <div id="slider-range-x"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
            @endif
            @if ($min_y > 0 and $max_y > 0)

            <div class="filter-options-item">
                <div class="filter-options-title" >{{ t('Szélesség') }}
                (<span id="max-y">{{ $min_y.' - '.$max_y }}</span>)</div>
                <div class="filter-options-content">
                    <div id="slider-range-y"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
            @endif

            @if ($lining)
            <div class="filter-options-item">
                <div class="filter-options-title" >{{ t('Bélés') }}</div>
                <div class="filter-options-content nopadding">
                    <div class="btn-group" id="filter-lining" data-toggle="buttons">
				   <input type="submit" id="spring" class="btn btn-sm col-lg-6 active btn-primary btn-filter"
				    onclick="if($(this).hasClass('active')){$(this).removeClass('active');}else{$(this).addClass('active');} filterNowProducts()"
				   value="{{ t('Rugó') }}"
				   >
				   <input type="submit" id="foam" class="btn btn-sm col-lg-6 active btn-primary btn-filter"
				     onclick="if($(this).hasClass('active')){$(this).removeClass('active');}else{$(this).addClass('active');} filterNowProducts()"
				   value="{{ t('Hab') }}"
				   >
				</div>
                    <div class="clearfix"></div>
                </div>
            </div>
            @endif

@include('webshop.sidebar_properties')



    </div>

    @if (isset($highlighted_products))
		<?php $i=1; ?>
        @foreach($side_products as $highlighted_product)
			<?php 
				$pclick = "gtmProductClick('Highlighted list','".$highlighted_product->getName()."','".$highlighted_product->product_id."','".$highlighted_product->getDefaultPrice()."','".$highlighted_product->getParentCategoryAdminName().$highlighted_product->getCategory()->admin_name."','".$i."');";
				$i++;
			?>
			<div class="desktop-view">
			<div class="filter-options-item sidebar-highlighted">
                <div class="filter-options-title" >{{ t('A hónap bútora')." - ".$highlighted_product->getName() }}</div>
                <div class="filter-options-content nopadding text-center">
					
                    <a onclick="{{$pclick}}" href="{{$highlighted_product->getUrl()}}" title="{{$highlighted_product->getName()}}"><img alt="{{$highlighted_product->getName()}}" src="{{ $highlighted_product->getDefaultImageUrl(600) }}"/></a>
                    <?php
                        $price_full = $highlighted_product->getDefaultFullPrice();
                        $price = $highlighted_product->getDefaultPrice();
                    ?>
                    @if ($price_full > $price)
                    <span class="old-price highlighted">{{money($price_full)}}</span>
                    @endif
                    <span class="price highlighted">{{money($price)}}</span>
                    <br>
                    <a onclick="{{$pclick}}" href="{{$highlighted_product->getUrl()}}" title="{{$highlighted_product->getName()}}" class="btn btn-primary white-text btn-block">{{ t('Részletek') }}</a>
                </div>
				@if ($highlighted_product->getPercentPrice())
				<div style="position:relative;width:43px;top:-230px;left:213px;border-radius:15px;background-color:#CD7126;"><a onclick="{{$pclick}}" href="{{$highlighted_product->getUrl()}}" title="{{$highlighted_product->getName()}}" style="text-decoration: none;"><span style="font-weight:bold;font-size:16px;color:white">&nbsp;-{{ $highlighted_product->getPercentPrice() }}%&nbsp;</span></a></div>
				@endif
            </div>
            </div>
			
        @endforeach
     @endif

</div>
