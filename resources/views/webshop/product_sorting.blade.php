<?php
/*
	Itt található a listázott termékek darabszám kijelzése és a rendezés (név szerint, ár szerint)
	Mobil nézet esetén itt található a sidebar lenyitására szolgáló gomb is.
*/
if(sizeof($products)){
?>
<nav class="col-12">
	<div class="row mb-3">
		<?php
		if( ismobile() ) {
			$prod_id=[];
			foreach($products as $x) {
				array_push($prod_id, $x->product_id);
			}//foreach
			$y = implode(", ",$prod_id);
			$structure=DB::select('SELECT DISTINCT `property` as `title`, `value` as `item` FROM `product_properties` WHERE `product_id` IN('.$y.') AND `deleted_at` IS NULL GROUP BY `property`, `value` ORDER BY `property` ASC');
		?>
		<div class="col-6 text-left">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#filterModal"><i class="fa fa-sliders" aria-hidden="true"></i> {{ t('Szűrők') }}</button>
		</div>
		<?php
		}//if
		else { ?>
		<div class="col-6 text-left"><span id="current-product-number" data-originalproductnumber="{{ sizeof($products) }}">{{ sizeof($products) }}</span> / <span itemprop="numberOfItems">{{ sizeof($products) }}</span> {{t('termék')}}</div>
		<?php } ?>
		<div class="col-6 text-right">
			<select class="mdb-select md-form" data-role="sorter" onchange="sortProduct(this.value)" id="sorter">
				<option selected="selected" value="basic"> {{ t('Rendezés: alapértelmezett') }}</option>
				<option value="asc"> {{ t('Ár szerint növekvő') }}</option>
				<option value="desc"> {{ t('Ár szerint csökkenő') }}</option>
			</select>
		</div>
	</div>
</nav>
<?php
}
?>