        <div>
            <em class="fa fa-crop"></em>
            <h3>{{ t('RENDELÉSRE') }}</h3>
            <p>{{ t('Minden bútor minden darabja kézzel készített.') }}</p>
        </div>
        <div>
            <em class="fa fa-male"></em>
            <h3>{{ t('Egyedi') }}</h3>
            <p>{{ t('Minden bútor egyedi, az Ön igényei szerint.') }}</p>
        </div>
        <div>
            <em class="fa fa-bus"></em>
            <h3>{{ t('HÁZHOZSZÁLLÍTÁS') }}</h3>
            <p>{{ t('Garantált szállítás, ISO 9001 tanúsítvány.') }}</p>
        </div>
