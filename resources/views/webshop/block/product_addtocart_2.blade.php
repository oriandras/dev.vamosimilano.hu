<?php
setcookie("active_product", $product->product_id, time() + (86400 * 90));
$sizes = $product->getSizes();
$standard = [];
foreach ($sizes as $size) {
	if ($size->is_standard){
		$standard = $size;
	}//if
}//foreach

if (empty($standard)) {
	if (sizeof($sizes)) {
		$standard = $sizes[0];
		$sizes[0]->is_standard = 1;
	}//if
}//if

if (sizeof($textiles)) {
	$default_textile =  $textiles[0];
	$price_plus =  $product->getDefaultPricePlus(($textiles[0]));
	$tmp_default_textile_2 = $tmp_default_textile_1 = 0;
	$tmp_default_parts = [];
	if (sizeof($textiles)){
		foreach ($textiles as $key => $textile){
			if ($textile->textile_id) {
				if (ProductTextile::lang()->find($textile->textile_id)->visible == 'no') {
					continue;
				}//if
			}//if
			if ($textile->textile2_id) {
				if (ProductTextile::lang()->find($textile->textile2_id)->visible == 'no') {
					continue;
				}//if
			}//if
			if ($tmp_default_textile_1 == 0){
				$tmp_default_textile_1 = $textile->textile_id;
				$tmp_default_textile_2 = ($textile->textile2_id ? $textile->textile2_id : $textile->textile_id);
			}//if
		}//foreach
		foreach ($parts as $i => $part) {
			if ($i == 1) {
				$tmp_default_parts[$part->admin_name] = $tmp_default_textile_1;
			}//if
			else {
				$tmp_default_parts[$part->admin_name] = $tmp_default_textile_2;
			}//else
		}//foreach
	}//if
	$tmp_array = [
		'product_id' => $product->product_id,
		'sizes' => [
			'a' => $standard,
		],
		'textile' => $tmp_default_parts,
	];
	$price_array = CartHelper::calculateItemPrice($tmp_array, true);
	$price_plus = $price_array['textile_price'];
}//if
else {
	$price_plus = $default_textile = 0;
}//else
?>
<form id="form-product-{{$product->product_id}}" class="product-form" action="{{ action('CartController@putToCart') }}" method="POST">
	<div class="product-sizes" itemprop="offers" itemscope itemtype="http://schema.org/Offer" >
		<?php $alapar=0;
		if ($product->type == 'non-scalable' and sizeof($sizes) == 1) {
			foreach ($sizes as $size) {
				if ($size->getPrice()>$alapar) {
					$alapar=$size->getPrice();
				}//if
		?>
		<div class="col-12 mx-0 px-0">
			<p class="mx-0">
				<strong>{{ t('Termék ára') }}</strong>
				<span class="float-right pr-1">
				<?php
				if ($size->list_price > $size->getPrice()) {
				?>
					<em class="original-price mr-3">{{ money($size->list_price) }}</em>
				<?php
				}//if 
				?>
					<span class="price text-right"><strong>{{ money($size->getPrice()) }}</strong></span>
				</span>
			</p>
		</div>
		<input type="hidden" name="feature_id" data-size-id="{{$size->size_id}}" value="{{ $size->size_id }}">
		<?php
			}//foreach
		}//if
		else {
		?>
		<div class="form-check" id="equalsizes">
		<?php 
		$mt="";
		if(ismobile()){
			$mt="mt-3";
		} ?>
			<p class="{{$mt}} text-uppercase"><strong>Válassza ki bútora méretét:</strong></p>
		<?php
			foreach ($sizes as $size) {
				if ($size->getPrice()>$alapar) {
					$alapar=$size->getPrice();
				}//if
					//echo "size-a-".$product->product_id;
				//echo $size->size_name;
				$cookiename_a = 'size-a-'.$product->product_id;
				$cookiename_b = 'size-b-'.$product->product_id;
				$cookiename_c = 'size-c-'.$product->product_id;
				//echo $_COOKIE[$cookiename];
				//echo $cookiename;
				if($category_data['kategoria_prefix'] === "e" || $category_data['kategoria_prefix'] === "fr") {
					if(isset($_COOKIE[$cookiename_a]) && $_COOKIE[$cookiename_a] === $size->size_name) {
						$size->is_standard = 1;
					}
					else {
						if( $size->is_standard!=1 ) {
							$size->is_standard = 0;
						}
					}
				}
				elseif($category_data['kategoria_prefix'] === "l") {
					if(isset($_COOKIE[$cookiename_a]) && isset($_COOKIE[$cookiename_b]) && $_COOKIE[$cookiename_a] === $size->size_name && $_COOKIE[$cookiename_b] === $size->size_name) {
						$size->is_standard = 1;
					}
					else {
						if( $size->is_standard!=1 ) {
							$size->is_standard = 0;
						}
					}
				}
				elseif($category_data['kategoria_prefix'] === "u") {
					if(isset($_COOKIE[$cookiename_a]) && isset($_COOKIE[$cookiename_b]) && isset($_COOKIE[$cookiename_c]) && $_COOKIE[$cookiename_a] === $size->size_name && $_COOKIE[$cookiename_b] === $size->size_name && $_COOKIE[$cookiename_c] === $size->size_name) {
						$size->is_standard = 1;
					}
					else {
						if( $size->is_standard!=1 ) {
							$size->is_standard = 0;
						}
					}
				}
				if(!isset($_COOKIE[$cookiename_a])){
					//$size->is_standard = 1;
				}
				//ez fontos, ne bántsd!
				if($size->is_standard == 1){
					$GLOBALS['default_size']=$size->size_id;
					$GLOBALS['default_sizename']=$size->size_name;
				}//if
				else {
					//$GLOBALS['default_size']=$size->size_id;
				}
				if($category_data['kategoria_prefix'] === "l"){
					$onchange = "blueprint('a', '".ProductSize::getSizeString($product->product_id,'a',$size->size_name ,false ,true )."'); blueprint('b', '".ProductSize::getSizeString($product->product_id,'b',$size->size_name ,false ,true )."')";
				}
				elseif($category_data['kategoria_prefix'] === "u"){
					$onchange = "blueprint('a', '".ProductSize::getSizeString($product->product_id,'a',$size->size_name ,false ,true )."'); blueprint('b', '".ProductSize::getSizeString($product->product_id,'b',$size->size_name ,false ,true )."'); blueprint('c', '".ProductSize::getSizeString($product->product_id,'c',$size->size_name ,false ,true )."')";
				}
				else {
					$onchange="";
				}
			?>
			<input type="radio" class="form-check-input sizelabel-input" name="feature_id" value="{{ $size->size_id }}" id="{{ $size->size_id }}" {{ ( $size->is_standard == 1 ? 'checked' : '' ) }} onClick="sizeChange('{{ $size->size_id }}', '{{ $size->size_name }}', 'full', '{{$category_data['kategoria_prefix']}}', {{ $product->product_id }})" onChange="{{$onchange}}">
			<label for="{{ $size->size_id }}" class="form-check-label w-100 {{($size->is_standard == 1 ? ' active' : '')}} sizelabel size-{{ $size->size_name }}" id="label-{{ $size->size_id }}">
				
				{{ $size->size_name }}
				<?php
				if ($size->size_x) {
				?>
				<span class="size">{{ ($size->size_x ? ' - '. getUnit($size->size_x) : '').($size->size_y ? "x".getUnit($size->size_y) : '').($size->size_z ? "x".getUnit($size->size_z) : '') }}cm</span>
				<?php
				}//if
				else {
				?>
				<span class="size">{{ ($size->asize_x ? ' - '. getUnit($size->asize_x) : '').($size->asize_y ? "x".getUnit($size->asize_y) : '') }}cm</span>
				<?php
				}//else
				?>
				<span class="price float-right">
					<span class="price mr-1">
				<?php
				if ($size->list_price > $size->getPrice()) {
				?>
					<em class="original-price mr-3 text-muted">{{ money($size->list_price) }}</em>
				<?php
				}//if 
				?>
					{{ money($size->getPrice()) }}
					</span>
				</span>
				@section('footer_js')
				<script>
					if( $("#{{ $size->size_id }}").is(":checked") ) {
						//alert('{{ $size->size_name }}');
					}
					else {
						$("#label-{{ $size->size_id }}").removeClass("active");
					}
					<?php
					if(isset($_COOKIE[$cookiename_a])){
						if(!isset($_COOKIE[$cookiename_b]) || !isset($_COOKIE[$cookiename_c])){
							$_COOKIE[$cookiename_b] = $_COOKIE[$cookiename_a];
							$_COOKIE[$cookiename_c] = $_COOKIE[$cookiename_a];
						}//if
						if($_COOKIE[$cookiename_a]!=$_COOKIE[$cookiename_b] || $_COOKIE[$cookiename_a]!=$_COOKIE[$cookiename_c] || $_COOKIE[$cookiename_b]!=$_COOKIE[$cookiename_c]) { ?>
						$("#label-{{ $size->size_id }}").removeClass("active");
						$("#{{ $size->size_id }}").prop("checked", false);
					<?php
						}//if
					}//if
					?>
				</script>
				@append
			</label>
		<?php
			}//foreach
		?>
		<?php
			if(sizeof($standard)) {
				if(isset($_COOKIE[$cookiename_a])){
					$active_a = $_COOKIE[$cookiename_a];
				}
				else {
					$active_a="";
				}
				if(isset($_COOKIE[$cookiename_b])){
					$active_b = $_COOKIE[$cookiename_b];
				}
				else {
					$active_b="";
				}
				if(isset($_COOKIE[$cookiename_c])){
					$active_c = $_COOKIE[$cookiename_c];
				}
				else {
					$active_c="";
				}
				if( ($active_a==$active_b && $active_a!=$active_c) || ($active_a==$active_c && $active_a!=$active_b) || ($active_b==$active_c && $active_a!=$active_b) || ($active_a!=$active_b && $active_a!=$active_c && $active_b!=$active_c)){
					$toggleswitch = " show";
				}
				else {
					$toggleswitch = "";
				}
				if ($standard->bsize_y) {
		?>
		<div class="row">
			<div class="col-12" style="padding-left: 1rem;">
				<a href="#other-sizes" class="othersize-toggle text-dark py-1 d-block w-100" data-toggle="collapse" role="button" aria-expanded="{{ ($toggleswitch == ' show' ? 'true' : 'false') }}" aria-controls="other-sizes"><i class="fa fa-fw fa-chevron-circle-down" aria-hidden="true" style="font-size:16px;margin-right: .5rem;"></i> {{ t('Egyéni méretek választásához kattintson ide') }}</a>
			</div>
		</div>
		<div class="collapse my-3 {{$toggleswitch}} cursor-pointer" id="other-sizes">
			<?php
					if ($standard->asize_y) {
			?>
			<div class="row mx-0 shadow">
				<div class="col-12 text-center border">
					<p class="my-0"><strong>A {{ t('oldal') }}</strong></p>
				</div>
				<?php
						if(ProductSize::getSizeString($product->product_id,'a','S',false,false)!='') {
							if($active_a==="S"){
								$a_checked = " active";
							}
							else {
								$a_checked = "";
							}
				?>
				<div data-rel="S" data-side="A" class="col border text-center mixedsizes mixed-a{{$a_checked}}" id="AS" onClick="sizeChange({{ $product->product_id }}, 'S', 'A', '{{$category_data['kategoria_prefix']}}', {{ $product->product_id }}); blueprint('a', '{{ ProductSize::getSizeString($product->product_id,'a', 'S' ,false ,true )}}')">
					<p class="my-0">S<br><?=ProductSize::getSizeString($product->product_id,'a','S',false,true)?></p>
				</div>
				<?php
						}//if
						if(ProductSize::getSizeString($product->product_id,'a','M',false,false)!='') {
							if($active_a==="M"){
								$a_checked = " active";
							}
							else {
								$a_checked = "";
							}
				?>
				<div data-rel="M" data-side="A" class="col border text-center mixedsizes mixed-a{{$a_checked}}" id="AM" onClick="sizeChange({{ $product->product_id }}, 'M', 'A', '{{$category_data['kategoria_prefix']}}', {{ $product->product_id }}); blueprint('a', '{{ ProductSize::getSizeString($product->product_id,'a', 'M' ,false ,true )}}')">
					<p class="my-0">M<br><?=ProductSize::getSizeString($product->product_id,'a','M',false,true)?></p>
				</div>
				<?php
						}//if
						if(ProductSize::getSizeString($product->product_id,'a','L',false,false)!='') {
							if($active_a==="L"){
								$a_checked = " active";
							}
							else {
								$a_checked = "";
							}
				?>
				<div data-rel="L" data-side="A" class="col border text-center mixedsizes mixed-a{{$a_checked}}" id="AL" onClick="sizeChange({{ $product->product_id }}, 'L', 'A', '{{$category_data['kategoria_prefix']}}', {{ $product->product_id }}); blueprint('a', '{{ ProductSize::getSizeString($product->product_id,'a', 'L' ,false ,true )}}')">
					<p class="my-0">L<br><?=ProductSize::getSizeString($product->product_id,'a','L',false,true)?></p>
				</div>
				<?php
						}//if
						if(ProductSize::getSizeString($product->product_id,'a','XL',false,false)!='') {
							if($active_a==="XL"){
								$a_checked = " active";
							}
							else {
								$a_checked = "";
							}
				?>
				<div data-rel="XL" data-side="A" class="col border text-center mixedsizes mixed-a{{$a_checked}}" id="AXL" onClick="sizeChange({{ $product->product_id }}, 'XL', 'A', '{{$category_data['kategoria_prefix']}}', {{ $product->product_id }}); blueprint('a', '{{ ProductSize::getSizeString($product->product_id,'a', 'XL' ,false ,true )}}')">
					<p class="my-0">XL<br><?=ProductSize::getSizeString($product->product_id,'a','XL',false,true)?></p>
				</div>
				<?php
						}//if
				?>
			</div>
			<?php
					}//if
					if ($standard->bsize_y) {
			?>
			<div class="row mx-0 mt-3 shadow">
				<div class="col-12 text-center border">
					<p class="my-0"><strong>B {{ t('oldal') }}</strong></p>
				</div>
				<?php
						if(ProductSize::getSizeString($product->product_id,'b','S',false,false)!='') {
							if($active_b==="S"){
								$b_checked = " active";
							}
							else {
								$b_checked = "";
							}
				?>
				<div data-rel="S" data-side="B" class="col border text-center mixedsizes mixed-b{{$b_checked}}" id="BS" onClick="sizeChange({{ $product->product_id }}, 'S', 'B', '{{$category_data['kategoria_prefix']}}', {{ $product->product_id }}); blueprint('b', '{{ ProductSize::getSizeString($product->product_id,'b', 'S' ,false ,true )}}')">
					<p class="my-0">S<br><?=ProductSize::getSizeString($product->product_id,'b','S',false,true)?></p>
				</div>
				<?php
						}//if
						if(ProductSize::getSizeString($product->product_id,'b','M',false,false)!=''){
							if($active_b==="M"){
								$b_checked = " active";
							}
							else {
								$b_checked = "";
							}
				?>
				<div data-rel="M" data-side="B" class="col border text-center mixedsizes mixed-b{{$b_checked}}" id="BM" onClick="sizeChange({{ $product->product_id }}, 'M', 'B', '{{$category_data['kategoria_prefix']}}', {{ $product->product_id }}); blueprint('b', '{{ ProductSize::getSizeString($product->product_id,'b', 'M' ,false ,true )}}')">
					<p class="my-0">M<br><?=ProductSize::getSizeString($product->product_id,'b','M',false,true)?></p>
				</div>
				<?php
						}//if
						if(ProductSize::getSizeString($product->product_id,'b','L',false,false)!=''){
							if($active_b==="L"){
								$b_checked = " active";
							}
							else {
								$b_checked = "";
							}
				?>
				<div data-rel="L" data-side="B" class="col border text-center mixedsizes mixed-b{{$b_checked}}" id="BL" onClick="sizeChange({{ $product->product_id }}, 'L', 'B', '{{$category_data['kategoria_prefix']}}', {{ $product->product_id }}); blueprint('b', '{{ ProductSize::getSizeString($product->product_id,'b', 'L' ,false ,true )}}')">
					<p class="my-0">L<br><?=ProductSize::getSizeString($product->product_id,'b','L',false,true)?></p>
				</div>
				<?php
						}//if
						if(ProductSize::getSizeString($product->product_id,'b','XL',false,false)!='') {
							if($active_b==="XL"){
								$b_checked = " active";
							}
							else {
								$b_checked = "";
							}
				?>
				<div data-rel="XL" data-side="B" class="col border text-center mixedsizes mixed-b{{$b_checked}}" id="BXL" onClick="sizeChange({{ $product->product_id }}, 'XL', 'B', '{{$category_data['kategoria_prefix']}}', {{ $product->product_id }}); blueprint('b', '{{ ProductSize::getSizeString($product->product_id,'b', 'XL' ,false ,true )}}')">
					<p class="my-0">XL<br><?=ProductSize::getSizeString($product->product_id,'b','XL',false,true)?></p>
				</div>
				<?php 
						}//if
				?>
			</div>
			<?php 
					}//if
					if ($standard->csize_y) {
			?>
			<div class="row mx-0 mt-3 shadow">
				<div class="col-12 text-center border">
					<p class="my-0"><strong>C {{ t('oldal') }}</strong></p>
				</div>
				<?php 
						if(ProductSize::getSizeString($product->product_id,'c','S',false,false)!='') { 
							if($active_c==="S"){
								$c_checked = " active";
							}
							else {
								$c_checked = "";
							}
				?>
				<div data-rel="S" data-side="C"  class="col border text-center mixedsizes mixed-c{{$c_checked}}" id="CS" onClick="sizeChange({{ $product->product_id }}, 'S', 'C', '{{$category_data['kategoria_prefix']}}', {{ $product->product_id }}); blueprint('c', '{{ ProductSize::getSizeString($product->product_id,'c', 'S' ,false ,true )}}')">
					<p class="my-0">S<br><?=ProductSize::getSizeString($product->product_id,'c','S',false,true)?></p>
				</div>
				<?php
						}//if
						if(ProductSize::getSizeString($product->product_id,'c','M',false,false)!='') {
							if($active_c==="M"){
								$c_checked = " active";
							}
							else {
								$c_checked = "";
							}
				?>
				<div data-rel="M" data-side="C"  class="col border text-center mixedsizes mixed-c{{$c_checked}}" id="CM" onClick="sizeChange({{ $product->product_id }}, 'M', 'C', '{{$category_data['kategoria_prefix']}}', {{ $product->product_id }}); blueprint('c', '{{ ProductSize::getSizeString($product->product_id,'c', 'M' ,false ,true )}}')">
					<p class="my-0">M<br><?=ProductSize::getSizeString($product->product_id,'c','M',false,true)?></p>
				</div>
				<?php 
						}//if
						if(ProductSize::getSizeString($product->product_id,'c','L',false,false)!='') { 
							if($active_c==="L"){
								$c_checked = " active";
							}
							else {
								$c_checked = "";
							}
				?>
				<div data-rel="L" data-side="C" class="col border text-center mixedsizes mixed-c{{$c_checked}}" id="CL" onClick="sizeChange({{ $product->product_id }}, 'L', 'C', '{{$category_data['kategoria_prefix']}}', {{ $product->product_id }}); blueprint('c', '{{ ProductSize::getSizeString($product->product_id,'c', 'L' ,false ,true )}}')">
					<p class="my-0">L<br><?=ProductSize::getSizeString($product->product_id,'c','L',false,true)?></p>
				</div>
				<?php 
						}//if
						if(ProductSize::getSizeString($product->product_id,'c','XL',false,false)!=''){
							if($active_c==="XL"){
								$c_checked = " active";
							}
							else {
								$c_checked = "";
							}
				?>
				<div data-rel="XL" data-side="C" class="col border text-center mixedsizes mixed-c{{$c_checked}}" id="CXL" onClick="sizeChange({{ $product->product_id }}, 'XL', 'C', '{{$category_data['kategoria_prefix']}}', {{ $product->product_id }}); blueprint('c', '{{ ProductSize::getSizeString($product->product_id,'c', 'XL' ,false ,true )}}')">
					<p class="my-0">XL<br><?=ProductSize::getSizeString($product->product_id,'c','XL',false,true)?></p>
				</div>
				<?php
						}//if 
				?>
			</div>
			<?php
					}//if
			?>
		</div>
		<?php
				}//if
			}//if 
		}//else
		?>
		<?php
			$mounting_check = "";
			if ($product->mounting == 'yes') {
				if( isset($_COOKIE["lastusedmounting"]) ) {
					$mounting_check=$_COOKIE["lastusedmounting"];
				}
				else {
					$mounting_check="right";
				}
		?>
		</div>
		<div class="form-check">
			<p class="mt-3 text-uppercase"><strong>Válassza ki a fekvőfelület tájolását:</strong></p>
			<input type="radio" class="form-check-input" id="mounting_left" name="mounting" value="left" onClick="mountingChange('left')"{{($mounting_check == 'left' ? ' checked' : '')}}>
			<label class="form-check-label w-100 change-mounting{{($mounting_check == 'left' ? ' active' : '')}}" id="mounting-left" for="mounting_left">
				{{ t('Balos elrendezés') }} 
			</label>
			<input type="radio" class="form-check-input float-right" id="mounting_right" name="mounting" value="right" onClick="mountingChange('right')"{{($mounting_check == 'right' ? ' checked' : '')}}>
			<label class="form-check-label w-100 change-mounting{{($mounting_check == 'right' ? ' active' : '')}}" id="mounting-right" for="mounting_right">
				{{ t('Jobbos elrendezés') }}
			</label>
		</div>
		<?php
			}//if
		?>
		<?php
			if ($product->seat == 'yes'){
		?>
		<div class="form-check form-check-inline row mx-0 mt-0">
			<div class="list-group list-group-flush col-6 p-0">
				<label class="col-sm-12 list-group-item change-seat active" id="sponge" >
					<input type="radio" class="form-check-input float-left mt-1 mr-2" id="seat-sponge" name="seat" value="szivacs" checked  onClick="seatChange('sponge')"> {{ t('Szivacsos ülőfelület') }}
				</label>
			</div>
			<div class="list-group list-group-flush col-6 p-0">
				<label class="col-sm-12 text-right list-group-item change-seat" id="sprung" >
					<input type="radio" class="form-check-input float-right ml-2 mt-1" id="seat-sprung" name="seat" value="rugo" onClick="seatChange('sprung')"> {{ t('Rugós ülőfelület') }}
				</label>
			</div>
		</div>
		<?php
			}//if
		?>
		<?php
		if(!sizeof($standard) || $product->seat != 'yes' || $product->mounting != 'yes') {
			//ha nincsenek a méret és a kiegészítők között egyéb elemek (mounting, seating), akkor nem kell új container divet letenni, egyébként sajnos igen
			//echo "zzz";
		?>
		
		@include('webshop.block.product_right2')
		<?php
		}//if
		else { 
		?>
			@include('webshop.block.product_right2')
		<?php
		}//else
		?>
		<div class="col-12 mx-0 px-0 mt-3">
			<p class="text-uppercase"><strong>Összesen:</strong></p>
		</div>
		
		<?php
		if (sizeof($textiles)) {
			/*  Textil felár számolás */
		?>
		<div class="col-12 mx-0 px-0 mt-2">
			<p><strong>{{ t('Szövet felár') }}:</strong><span id="priceplus-target" data-pricetextil="{{$price_plus}}" class="float-right pr-1">{{ money($price_plus) }}</span></p>
		</div>
		<?php
		}//if
		
		/*Kiegészítő felár számolás*/
		$price_feature=0;
		//$feature_string = $_COOKIE["productfeatures"];
		?>
		<div class="col-12 mx-0 px-0 mt-2" id="priceother-div">
			<p><strong>{{ t('Kiegészítők') }}:</strong><span id="priceother-target" class="float-right pr-1">{{ money($price_feature) }}</span></p>
		</div>
		<div class="col-12 mx-0 px-0 mt-2">
			<p><strong>{{ t('Vételár') }}:</strong><span id="priceall-target" data-priceall="{{ $price + $price_plus + $price_feature }}" class="float-right pr-1 font-weight-bold">{{ money($price + $price_plus + $price_feature) }}</span></p>
		</div>
		<div class="clearfix"></div>
		<div class="row col-12 mt-3 mx-0 px-0">
		<?php
		$nincskosargomb_category=array(119,157,127,130,129);
		if (in_array($product->category_id, $nincskosargomb_category)) {
			//kosárgomb le van tiltva
		?>
			<button class="btn btn-disabled">Ez a termék önmagában nem vásárolható meg</button>
		<?php
		} //if
		else {
			if ($alapar) { 
				$s="";$p="";
				if(sizeof($standard)) {
					if ($standard->asize_y) {
						$s.="$('#product_size_a').val()+' '";
						$p="+";
					}//if
					if ($standard->bsize_y) {
						$s.=$p."$('#product_size_b').val()+' '";
						$p="+";
					}//if
					if ($standard->csize_y){
						$s.=$p."$('#product_size_c').val()+' '";
						$p="+";
					}//if
				}//if
				else {
					$s.="$('#product_size_a').val()+' '";
				}//else
				if ($product->mounting == 'yes') {
					$s.=$p."$('input[name=mounting]:checked', '#form-product-".$product->product_id."').val()";
				}
				if($s=="") {
					//ezt meg kell nézni ha már vége az őrületnek
					$gtm="gtmAddToCart('".App::getLocale()."','".$product->product_id."','".Config::get('shop.'.getShopCode().'.currency_code')."','".$product->name."','".$price + $price_plus."','".$product->getParentCategoryAdminName()."".$product->getCategory()->admin_name ."',".$s.",1);";
				}
		?>
			<a href="javascript:;" onclick="$('#form-product-{{$product->product_id}}').submit();"  id="add-to-cart" class="btn btn-primary col">{{ t('Kosárba rakom') }}</a>
		<?php
			}//if
			else {
		?>
				{{ t('Árazás alatt!') }}
		<?php
			}//else
		}//else
		if ($kategoria){
			$prodcatid = $product->category_id;
			//echo $prodcatid;
			if ( isset($mounting_check) && $mounting_check!="" ) {
		?>
			<a href="javascript:void(0);" data-toggle="modal" data-target="#mounting-modal" class="btn btn-outline-secondary w-50">{{ t('Bútor tervrajz') }}</a> 
			<div class="modal fade" tabindex="-1" role="dialog" id="mounting-modal" aria-labelledby="mounting-modal" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
						<?php
						//echo $GLOBALS['default_sizename']."<br>";
						$sides = $product->getSizes();
						foreach ($sides as $side) {
							if ($side->is_standard){
								$standard = $side;
							}
						}
						//echo $standard->bsize_y."<br>";
						//echo $standard->csize_y."<br>";
						$tipus = "egyenes";
						$x_full = 0;
							if(isset($standard->bsize_y) && $standard->bsize_y>0){
								$tipus = "lalak";
								$a_sides = explode("x", ProductSize::getSizeString($product->product_id,'a',$GLOBALS['default_sizename'],false,true));
								$b_sides = explode("x", ProductSize::getSizeString($product->product_id,'b',$GLOBALS['default_sizename'],false,true));
								$x_full = $a_sides[0]+$b_sides[0];
							}
							if(isset($standard->csize_y) && $standard->csize_y>0) {
								$tipus = "ualak";
								$a_sides = explode("x", ProductSize::getSizeString($product->product_id,'a',$GLOBALS['default_sizename'],false,true));
								$b_sides = explode("x", ProductSize::getSizeString($product->product_id,'b',$GLOBALS['default_sizename'],false,true));
								$c_sides = explode("x", ProductSize::getSizeString($product->product_id,'c',$GLOBALS['default_sizename'],false,true));
								$x_full = $a_sides[0]+$b_sides[0]+$c_sides[0];
							}
							//echo $tipus."<br>";
						if($tipus!="egyenes"){
						?>
						<p class="pt-3 text-center fullwidthsize"><span>{{$x_full}}</span>cm teljes szélesség</p>
						<div class="pb-3 d-flex flex-row xoverflow" style="min-width: {{$x_full+20}}px !important;">
							@if($tipus == "lalak" || $tipus == "ualak")
							<?php
							if(isset($_COOKIE['lastusedmounting']) && $_COOKIE['lastusedmounting']=="left"){
								$order="order-0";
							}
							else {
								$order="order-2";
							}
							?>
							<div class="d-inline border border-dark bside {{$order}}" style="height:{{$b_sides[1]}}px; width:{{$b_sides[0]}}px">
								<table border="0px" width="{{$b_sides[0]}}px" height="{{$b_sides[1]}}px">
									<tr height="1rem">
										<td valign="top"><div class="bw text-center" data-w="{{$b_sides[0]}}"><span>{{$b_sides[0]}}</span>cm</div></td>
									</tr>
									<tr class="">
										<td class="m-0 p-0"><div class="bh h text-center">{{$b_sides[1]}}cm</div></td>
									</tr>
									<tr height="1rem">
										<td valign="bottom">&nbsp;</td>
									</tr>
								</table>
							</div>
							@endif
							<?php 
							if($tipus == "ualak"){
								$borders = " border-left-0 border-right-0";
							}
							elseif($tipus == "lalak") {
								if(isset($_COOKIE['lastusedmounting']) && $_COOKIE['lastusedmounting']=="left"){
									$borders = " border-left-0";
								}
								else {
									$borders = " border-right-0";
								}
							}
							?>
							<div class="d-inline border border-dark aside {{$borders}} order-1" style="height:{{$a_sides[1]}}px; width:{{$a_sides[0]}}px">
								<table border="0px" width="{{$a_sides[0]}}px" height="{{$a_sides[1]}}px">
									<tr height="1rem">
										<td valign="top"><div class="aw text-center" data-w="{{$a_sides[0]}}"><span>{{$a_sides[0]}}</span>cm</div></td>
									</tr>
									<tr class="">
										<td class="m-0 p-0"><div class="ah h text-center">{{$a_sides[1]}}cm</div></td>
									</tr>
									<tr height="1rem">
										<td valign="bottom">&nbsp;</td>
									</tr>
								</table>
							</div>
							@if($tipus == "ualak")
							<?php
							if(isset($_COOKIE['lastusedmounting']) && $_COOKIE['lastusedmounting']=="left"){
								$order="order-2";
							}
							else {
								$order="order-0";
							}
							?>
							<div class="d-inline border border-dark cside {{$order}}" style="height:{{$c_sides[1]}}px; width:{{$c_sides[0]}}px">
								<table border="0px" width="{{$c_sides[0]}}px" height="{{$c_sides[1]}}px">
									<tr height="1rem">
										<td valign="top"><div class="cw text-center" data-w="{{$c_sides[0]}}"><span>{{$c_sides[0]}}</span>cm</div></td>
									</tr>
									<tr class="">
										<td class="m-0 p-0"><div class="ch h text-center">{{$c_sides[1]}}cm</div></td>
									</tr>
									<tr height="1rem">
										<td valign="bottom">&nbsp;</td>
									</tr>
								</table>
							</div>
							@endif
						</div>
						<?php }//if ?>
							<img src="{{Config::get('website.static_root_url')}}/product/szerelhetoseg/{{$kategoria}}-{{($mounting_check == 'left' ? 'balos' : 'jobbos')}}.png" class="img-fluid mounting-example hidden" alt="" data-url="{{Config::get('website.static_root_url')}}/product/szerelhetoseg/{{$kategoria}}-"/>
						</div>
					</div>
				</div>
			</div>
		<?php
			}//if
		}//if
		if ((isset($product->outlet_parent_id)) && ($product->outlet_parent_id)) {
			$oproduct = Product::lang()->where('products.product_id', $product->outlet_parent_id)->first();
		}//if
		if (isset($oproduct)) { 
		?>
		<a href="{{ $oproduct->getUrl() }}" class="btn btn-outline-secondary w-50"><span>{{ t('Új termék vásárlása *') }}</span></a>
		<?php
		}//if
		if( strlen($product->outlet_location)>0 ) {
		?>
		<div class="col-12">
			@if($product->getPercentPrice()>0 && isset($oproduct))
			<p class="mt-3"><strong>*</strong> Amennyiben szeretné a terméket más méretben vagy szövetezéssel megrendelni kattintson az „Új termék vásárlása” gombra</p>
			@endif
			@if($product->outlet_location>0)
			<div class="alert alert-primary shadow mt-2" role="alert">
				<i class="fa fa-3x float-left mt-2 mr-3 fa-map-marker" aria-hidden="true"></i> {{t('outlet_'.$product->outlet_location)}}
			</div>
			@endif
		</div>
		<?php
		}
		?>
			<meta itemprop="price" content="{{ $price }}">
			<meta itemprop="priceCurrency" content="{{Config::get('shop.'.getShopCode().'.currency_code')}}" />
			<meta itemprop="availability" content="http://schema.org/InStock" />
			<meta itemprop="itemCondition" itemtype="http://schema.org/OfferItemCondition" content="http://schema.org/NewCondition" />
		</div>
	</div>
	<input type="hidden" name="product_id" id="product_id_" value="{{$product->product_id}}">
	<input type="hidden" name="product_id" value="{{$product->product_id}}">
	<input type="hidden" name="qty" value="1">
	<input type="hidden" name="default_price_" id="default_price_" value="{{$price}}">
	<input type="hidden" name="priceplus_price_" id="priceplus_price_" value="{{$price_plus}}">
	<input type="hidden" name="priceplus_price_2_" id="priceplus_price_2_" value="{{0}}">
	<input type="hidden" name="related" id="related_product" value="0" onChange="calcProductDetailPrice()">
	<?php
	foreach ($product->getUsedParts() as $part_item) {
		if( $part_item->admin_name==="butor" ) {
			$default_part_value = "";
		}
		else {
			//ez a sor kiakasztja a fejtámlát
			//$default_part_value =  (isset($tmp_default_parts[$part_item->admin_name]) ? $tmp_default_parts[$part_item->admin_name] : $tmp_default_parts['also']);
			$default_part_value =  (isset($tmp_default_parts[$part_item->admin_name]) ? $tmp_default_parts[$part_item->admin_name] : '');		}
	?>
	<input type="hidden" name="textile[{{$part_item->admin_name}}]" id="textile_{{$part_item->admin_name}}" class="resetfield" data-default="{{ $default_part_value }}" value="{{ $default_part_value }}" onchange="clearIfNotMixed('{{$part_item->admin_name}}');">
	<? 
	}//foreach
	if(sizeof($standard)) {
		if ($standard->asize_y) {
			if( isset($_COOKIE["size-a-".$product->product_id]) ){
				$size_value_a = $_COOKIE["size-a-".$product->product_id];
			}
			else {
				$size_value_a = $standard->size_name;
			}
	?>
	<input type="hidden" name="sizes[a]" id="product_size_a" data-default="{{$standard->size_name}}" value="{{$size_value_a}}" class="sizes_input" onChange="refreshPayment();">
	<?php
		}//if
		if ($standard->bsize_y) {
			if( isset($_COOKIE["size-b-".$product->product_id]) ){
				$size_value_b = $_COOKIE["size-b-".$product->product_id];
			}
			else {
				$size_value_b = $standard->size_name;
			}
	?>
	<input type="hidden" name="sizes[b]" id="product_size_b" data-default="{{$standard->size_name}}" value="{{$size_value_b}}" class="sizes_input" onChange="refreshPayment();">
	<?php 
		}//if
		if ($standard->csize_y) { 
			if( isset($_COOKIE["size-c-".$product->product_id]) ){
				$size_value_c = $_COOKIE["size-c-".$product->product_id];
			}
			else {
				$size_value_c = $standard->size_name;
			}
	?>
	<input type="hidden" name="sizes[c]" id="product_size_c" data-default="{{$standard->size_name}}" value="{{$size_value_c}}" class="sizes_input" onChange="refreshPayment();">
	<?php
		}//if
	}//if
	?>
	{{ csrf_field() }}
<input type="hidden" id="relatedfdata" name="relatedfdata" value="">
<input type="hidden" id="relatedfprice" name="relatedfprice" value="0">
<input type="hidden" id="relatedgarprice" name="relatedgarprice" value="0">
</form>