<?php if(!ismobile()): ?>
<?php if (!isset($i)): $i=0; endif; ?>
<div class="d-none d-md-block" id="breadcrumb">
	<div class="container-fluid mx-0 px-0 bg-dark">
		<nav aria-label="breadcrumb">
		<ol class="breadcrumb transparent bg-dark rounded-0 container" itemscope itemtype="http://schema.org/BreadcrumbList">
			<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="/" itemprop="item"><span itemprop="name">{{ t('Főoldal') }}</span></a><meta itemprop="position" content="1" /></li>
			<?php /*
			<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="{{ action('WebshopController@product', '') }}"><span itemprop="name">{{ t('Termékek') }}</span></a><meta itemprop="position" content="2" /></li>
			*/ ?>
	@if((isset($current_category) and sizeof($current_category)))
		<?php
		$breadcrumbs = $current_category->getBreadcrumbs();
		$i=2;
		?>
		@if (isset($breadcrumbs))
			@foreach($breadcrumbs as $url => $breadcrumb)
			<?php $i++; 
			if (($i<=sizeof($breadcrumbs)+1) or (isset($product))): ?>
			<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="{{$url}}"><span itemprop="name">{{ $breadcrumb }}</span></a><meta itemprop="position" content="{{ $i }}" /></li>
			<?php else: ?>
			<li class="breadcrumb-item active" aria-current="page"><a href="{{$url}}">{{ $breadcrumb }}</a></li>	
			<?php endif; ?>
			@endforeach
		@endif
	@endif
	@if (isset($product))
			<?php $i++; ?>
			@if (isset($textileditor))
				<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="{{ $product->getUrl() }}" title="{{ $product->getName() }}"><span itemprop="name">{{ $product->getName() }}</span></a><meta itemprop="position" content="{{ $i }}" /></li>
			@else
				<li class="breadcrumb-item active" aria-current="page"><a href="{{ $product->getUrl() }}" title="{{ $product->getName() }}">{{ $product->getName() }}</a></li>
			@endif
		@endif
	    @if (isset($search))
			<?php $i++; ?>
        <li class="breadcrumb-item active" aria-current="page">{{ t('Keresés').' - '.$search }}</li>
	    @endif
	    @if (isset($textileditor))
	    	<?php $i++; ?>
        <li class="breadcrumb-item active" aria-current="page">{{ t('Szövet választás') }}</li>
		@endif
			</ol>
		</nav>
	</div>
</div>
<?php endif; ?>