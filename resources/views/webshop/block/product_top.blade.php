<?php
    $category_data = $product->getPrimaryCategory($sizes);
    $kategoria = $category_data['kategoria'];
    $kategoria_prefix = $category_data['kategoria_prefix'];
    $pages = $category_data['pages'];

?>
<div class="columns">
    <div class="column main">
        <div class="product-info-main">
            <div class="page-title-wrapper product">
                <h1 class="page-title">
                <span class="base" data-ui-id="page-title-wrapper" itemprop="name">{{ $product->getName() }}</span>    </h1>
            </div>
            <?php
                $price = $product->getDefaultPrice();
                $price_full = $product->getDefaultFullPrice();
            ?>
            @if ($product->type == 'general')


            <div class="product-info-price">
            <div class="price-box price-final_price" data-role="priceBox" data-product-id="1049">




            <div class="price-box price-final_price">
                @if ($price_full > $price)
                <span class="old-price old-price-custom1">
                    <span class="price-container price-final_price tax weee" >
                        <span class="price-label"></span>
                        <span class="price-wrapper">
                            <span class="price">{{ ($price_full > $price ? money($price_full) : '') }}</span>
                        </span>
                    </span>
                </span>
                @endif
                <span class="special-price special-price-custom1">
                    <span class="price-container price-final_price tax weee" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                        <span class="price-label">{{ t('A termék ára') }}</span>
                        <span class="price-wrapper " data-price-type="finalPrice">
                            <span class="price">{{ money($price) }}</span>
                        </span>
						<meta itemprop="price" content="{{ $price }}">
						<meta itemprop="priceCurrency" content="{{Config::get('shop.'.getShopCode().'.currency_code')}}" />
						<meta itemprop="availability" content="http://schema.org/InStock" />
						<meta itemprop="itemCondition" itemtype="http://schema.org/OfferItemCondition" content="http://schema.org/NewCondition" />
                    </span>
                </span>
            </div>


            </div>
            </div>
@endif



<div class="product-social-links">
    <div class="col-lg-12 overflowhidden">
    <div class="fb-like" data-href="{{ $product->getUrl() }}" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
    </div>
    <?php
        /*
    <div class="col-lg-3">
    <a data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url={{ $product->getUrl() }}&media={{ $product->getDefaultImageUrl(600) }}&description={{ $product->getName() }}" style="display: inline-block; padding-top:10px;">
        <img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" style="display: none;" />
    </a>
    </div>*/
    ?>
</div>

<div class="product-add-form">
    @if ($product->type == 'general')
        <?php /* <form class="ajax-form" id="form-product-{{$product->product_id}}" ajax-action="{{ action('CartController@addToCart') }}"> */ ?>
		<form id="form-product-{{$product->product_id}}" action="{{ action('CartController@putToCart') }}" method="POST">
                            <input type="hidden" name="product_id" value="{{$product->product_id}}">
                            <input type="hidden" name="qty" value="1">
                            <input type="hidden" name="feature_id" value="0">
                            {{ csrf_field() }}
        </form>
		<a href="javascript:;" onclick="gtmAddToCart('{{ App::getLocale() }}','{{ $product->product_id }}','{{Config::get('shop.'.getShopCode().'.currency_code')}}','{{ $product->name }}','{{ $price }}','{{ $product->getParentCategoryAdminName() }}{{ $product->getCategory()->admin_name }}','',1);$('#form-product-{{$product->product_id}}').submit();" class="action tocart primary"><span>{{ t('Kosárba rakom') }}</span></a>
		@if ((isset($product->outlet_parent_id)) and ($product->outlet_parent_id))
			<?php 
				$oproduct = Product::lang()->where('products.product_id', $product->outlet_parent_id)->first();
			?>
			@if (isset($oproduct))
			<a href="{{ $oproduct->getUrl() }}" class="action tocart primary"><span>{{ t('Új termék vásárlása') }}</span></a>
			@endif
		@endif
		<div class="clearfix"><br></div>
		<p class="text-left">{{t('outlet_'.$product->outlet_location)}}</p>
    @else
        @include('webshop.block.product_addtocart', ['kategoria' => $kategoria])

    @endif
</div>


</div>
<div class="product media">
<div class="gallery-placeholder {{($product->menu_top == 'yes' ? 'item-highlighted' : '')}}" >
<div class="product photo product-item-photo">
    <a id="productimagehref" href="{{ $product->getDefaultImageUrl('full') }}" title="{{ $product->getName() }}" class="fancy">
        <img itemprop="image" alt="{{ $product->getName() }} - {{ $product->getCategory()->name }}" src="{{ $product->getDefaultImageUrl(600) }}" id="product-image-{{ $product->product_id }}" class="product-image-photo default_image">
    </a>
</div>
@if (sizeof($textiles))
	<? /*
    @if (($product->type == 'scalable' or in_array($product->category_id, array(22, 10, 11, 9, 110, 122)) and sizeof($parts) > 1) ) */ ?>
        @include('webshop.block.textile_tab', ['new_version' => true])
	<? /*
    @else  
         @include('webshop.block.textile_tab')
    @endif */ ?>
@endif
</div>

@if($product->planner == 'yes')
<?
$col = 'pull-left';
?>
<div style="margin-top: 20px;">
	@if (isMobile())
	<a id="product-page-3dplanner-btn" href="https://play.google.com/store/apps/details?id=com.VamosiMilano.VamosiDesigner&hl={{ strtolower(App::getLocale()) }}" target="_blank" style="display: block;">
	@else
	<a id="product-page-3dplanner-btn" href="/unity/index.html/?{{ $product->product_id}}_{{ strtolower(App::getLocale()) }}" target="_blank" style="display: block;">
	@endif
	<img src="/images/planner_button_{{ strtolower(App::getLocale()) }}.jpg">
	</a>
	<div class="clearfix"></div>
</div>
@endif

</div>
</div>

@include('webshop.block.product_right')
</div>
<? /* //takaritas
@if ($product->type != 'general' and sizeof($parts) and !isMobile())
    @foreach ($custom_parts as $part_key => $parts_data)
            @foreach ($parts_data as $i => $part)
            @include('webshop.block.product_textiles_slider', ['part' => $part, 'type' => $part->admin_name,'all_textiles' => $all_textiles])
        @endforeach
    @endforeach
@endif
*/ ?>

<div class="product info detailed detailed-container">
    @if ($product->type == 'scalable' and $product->plannable == 'yes')
    <div class="col-lg-{{ ($kategoria != '' ? 8 : 12) }} mobile-product-page-bottom">
    @endif
        <div class="product data items" role="tablist">
          <ul class="nav nav-tabs" role="tablist">
            @if ($product->getDescription() != '')
            <li role="presentation" class="data item title active">
                <a href="#{{ t('termek-leiras', 'url') }}" aria-controls="home" class="data switch" role="tab" data-toggle="tab">{{ t('Leírás') }}</a>
            </li>
            @endif
            @if ($product->features)
            <li role="presentation" class="data item title{{ $product->getDescription() == '' ? ' active' : '' }}">
                <a href="#{{ t('jellemzok', 'url') }}" aria-controls="profile" class="data switch" role="tab" data-toggle="tab">{{ t('Jellemzők') }}</a>
            </li>
            @endif
            @if ($product->howto)
            <li role="presentation" class="data item title">
                <a href="#{{ t('hasznalati-utmutato', 'url') }}" aria-controls="profile" class="data switch" role="tab" data-toggle="tab">{{ t('Használati útmutató') }}</a>
            </li>
			@endif
			@if (sizeof($familyproducts))
			<li role="presentation" class="data item title">
                <a href="#{{ t('kapcsolodotermekek', 'url') }}" aria-controls="profile" class="data switch" role="tab" data-toggle="tab">{{ t('Kapcsolódó termékek') }}</a>
            </li>
            @endif
          </ul>

          <div class="tab-content">
            @if ($product->getDescription() != '')
            <div role="tabpanel" class="tab-pane active" id="{{ t('termek-leiras', 'url') }}" itemprop="description" >
                {{ $product->getDescription() }}
            </div>
            @endif
            @if ($product->features)
            <div role="tabpanel" class="tab-pane{{ $product->getDescription() == '' ? ' active' : '' }}" id="{{ t('jellemzok', 'url') }}">
                {{ $product->features }}
            </div>
            @endif
            @if ($product->howto)
            <div role="tabpanel" class="tab-pane" id="{{ t('hasznalati-utmutato', 'url') }}">
                {{ $product->howto }}
            </div>
            @endif
			@if (sizeof($familyproducts))
            <div role="tabpanel" class="tab-pane container" id="{{ t('kapcsolodotermekek', 'url') }}">
                @foreach ($familyproducts as $item)
					@if ($item->product_id != $product->product_id)
					<div class="col-sm-4 {{$kategoria ? 'small-item' : 'big-item'}}">
						<div class="filter-options-title">{{ $item->getName() }}</div>
						<div class="filter-options-content nopadding text-center">
							<a href="{{$item->getUrl()}}" title="{{$item->getName()}}" class="related-prod-image"><img alt="{{$item->getName()}}" src="{{ $item->getDefaultImageUrl(600) }}"/></a>
							<?php
								$price_full = $item->getDefaultFullPrice();
								$price = $item->getDefaultPrice();
							?>
							@if ($price_full > $price)
							<span class="old2-price">{{money($price_full)}}</span>
							@endif
							<span class="price highlighted">{{money($price)}}</span>
							<br>
							<a href="{{$item->getUrl()}}" title="{{$item->getName()}}" class="btn btn-primary white-text btn-block">{{ t('Részletek') }}</a>
						</div>
						<br>
					</div>
					@endif
				@endforeach
				<div class="clearfix"></div>
            </div>
            @endif

          </div>
        </div>
    @if ($product->type == 'scalable' and $product->plannable == 'yes')
    </div>


    @if (($kategoria != '') and (($product->category_id==12) or ($product->category_id==24)))
	<div class="col-lg-4 mobile-product-page-bottom">
		<div class="row">
            <div class="col-sm-12">
				<img style='width:auto' id="helpImageData" data-src="{{Config::get('website.static_root_url')}}/product/szerelhetoseg/" data-left="{{$kategoria}}-balos.PNG" data-right="{{$kategoria}}-jobbos.PNG" src='{{Config::get('website.static_root_url')}}/product/szerelhetoseg/{{$kategoria}}-jobbos.PNG' alt=''>
			</div>
		</div>
	</div>
    @endif
    <?php
        /*
    <div class="col-lg-4 mobile-product-page-top hidden">
        <p>{{ t('butortervezo_text') }}</p>
        <div id="builder-form-controller">

        <a href="javascript:;" onclick="$(this).fadeOut(); $('#goto-builder').animate({top:0});" id="builder-form-start" class="btn btn-primary action tocart primary"><span>{{ t('Bútortervező') }}</span></a>

        <form action="{{action('WebshopController@productAddToCart', $product->url)}}" method="get" id="goto-builder">
        <?php

            if ($pages == 2) {
                unset($pages_s[3]);
            }
            foreach ($pages_s as $c => $a) {

                $html_data = '';
                foreach ($sizes as $size) {
                    $inner_html = '';
                    if ($a == 'A' and $size->asize_x > 0) {
                        $inner_html = getUnit($size->asize_x, true)." x ".getUnit($size->asize_y, true);
                    }
                    if ($a == 'B' and $size->bsize_x > 0) {
                        $inner_html = getUnit($size->bsize_x, true)." x ".getUnit($size->bsize_y, true);
                    }
                    if ($a == 'C' and $size->csize_x > 0) {
                        $inner_html =  getUnit($size->csize_x, true)." x ".getUnit($size->csize_y, true);
                    }
                    if ($inner_html){
                        $html_data.='<option value="'.$size->size_name.'">'.$size->size_name.' (';
                        $html_data.= $inner_html;
                        $html_data.= ')</option>';
                    }
                }
                if ($html_data) {
                    echo '<b>'.t('oldal_merete', 'global', ['page' => $a]).'</b>';
                    echo '<select name="page_'.$a.'">';
                    echo $html_data;
                    echo '</select><br><br>';
                }
            }
        ?>
        <a href="javascript:;" onclick="$('#goto-builder').submit()" class="btn btn-primary btn-block action tocart primary"><span>{{ t('Tervezés indítása') }}</span></a>
        </form>
        </div>
    </div>
    */
    ?>
    @endif
</div>
</div>