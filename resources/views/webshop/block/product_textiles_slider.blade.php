
@if (sizeof($all_textiles))
    <div class="slidercontainer textileslider" id="slider_of_{{$type}}" data-slider-type="{{$type}}" style="display: none;">
            <div class="container">
               <div class="row">
                  <div class="col-sm-12">
                    <h2 class="filterproduct-title">
                    <span class="content"><strong>{{ $part->name }}</strong>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="switch-carousel" data-target="destroy_{{$type}}_Carousel()">{{ t('Válasszon több száz féle szövetünk közül') }}</i>
                    <i class="switch-carousel fa fa-th-list active hidden" data-target="init_{{$type}}_Carousel()"></i>
                    <i class='fa fa-times' onclick="$('#slider_of_{{$type}}').slideUp()"></i>
                    </span>

                    </h2>
                    <div class="row category-filter" style="display: none;">
                         <div class="col-lg-4 text-center" onclick="sliderFilterCategory('featured_product_textiles_{{$type}}','3')"><span class="block">{{ t('Minden szövet') }}</span></div>
                         <div class="col-lg-4 text-center" onclick="sliderFilterCategory('featured_product_textiles_{{$type}}','2')"><span class="block">{{ t('Szövetek') }}</span></div>
                         <div class="col-lg-4 text-center" onclick="sliderFilterCategory('featured_product_textiles_{{$type}}','1')"><span class="block">{{ t('Műbőrők') }}</span></div>
					</div>
					<div class="row category-filter" style="display: none;">
						 <div class="col-lg-3 text-center" onclick="sliderFilterPrice('featured_product_textiles_{{$type}}','1')"><span class="block">{{ t('1. kategória') }}</span></div>
						 <div class="col-lg-3 text-center" onclick="sliderFilterPrice('featured_product_textiles_{{$type}}','2')"><span class="block">{{ t('2. kategória') }}</span></div>
						 <div class="col-lg-3 text-center" onclick="sliderFilterPrice('featured_product_textiles_{{$type}}','3')"><span class="block">{{ t('3. kategória') }}</span></div>
						 @if (App::getLocale()!='en')
						 <div class="col-lg-3 text-center" onclick="sliderFilterPrice('featured_product_textiles_{{$type}}','4')"><span class="block">{{ t('4. kategória') }}</span></div>
						 <div class="col-lg-3 text-center" onclick="sliderFilterPrice('featured_product_textiles_{{$type}}','5')"><span class="block">{{ t('5. kategória') }}</span></div>
						 <div class="col-lg-3 text-center" onclick="sliderFilterPrice('featured_product_textiles_{{$type}}','6')"><span class="block">{{ t('6. kategória') }}</span></div>
						 <div class="col-lg-3 text-center" onclick="sliderFilterPrice('featured_product_textiles_{{$type}}','7')"><span class="block">{{ t('7. kategória') }}</span></div>
						 <div class="col-lg-3 text-center" onclick="sliderFilterPrice('featured_product_textiles_{{$type}}','9')"><span class="block">{{ t('Bőr') }}</span></div>
						 <div class="col-lg-3 text-center" onclick="sliderFilterPrice('featured_product_textiles_{{$type}}','10')"><span class="block">{{ t('Easy to clean only with water I') }}</span></div>
						 <div class="col-lg-3 text-center" onclick="sliderFilterPrice('featured_product_textiles_{{$type}}','11')"><span class="block">{{ t('Easy to clean only with water II') }}</span></div>
						 <div class="col-lg-3 text-center" onclick="sliderFilterPrice('featured_product_textiles_{{$type}}','12')"><span class="block">{{ t('Easy to clean only with water III') }}</span></div>
						 @else
						 <div class="col-lg-3 text-center" onclick="sliderFilterPrice('featured_product_textiles_{{$type}}','5')"><span class="block">{{ t('Bőr') }}</span></div>
						 <div class="col-lg-3 text-center" onclick="sliderFilterPrice('featured_product_textiles_{{$type}}','4')"><span class="block">{{ t('Easy to clean only with water') }}</span></div>
						 @endif
						 <br><hr>
                    </div>

                     <div id="featured_product_textiles_{{$type}}" class="owl-top-narrow">
                        <div class="products wrapper grid products-grid">
                           <div class="filterproducts products list items product-items owl-carousel">
                             <?php
                                 $slider_items = [];
                             ?>
                             @foreach ($all_textiles as $textile)
                             <?php
                                 $field = "on_".$type;
                                 if ($type == 'kisparna'){
                                     $field = "on_parna";
                                 }
                                 if (!isset($textile['original'][$field])) {
                                     $field = "on_butor";
                                 }
								 if (strpos(" ".$type,"elemes")) {
									 $field = "on_elemes";
								 }
                             ?>

                             @if ($textile->$field == 'yes')

							  <div data-category="{{$textile->textile_category_id}}" data-price="{{$textile->extraprice_category_id}}" data-textile-target="{{$type}}-{{$part->type}}-{{$textile->textile_id}}" class="item product product-item change_textile_data {{$part->type}}_textile_change_{{$textile->textile_id}}{{$i}}" data-target="{{$part->type}}{{$i}}" onclick="rewriteProductDetailsPrice('textile_{{$type}}', {{$textile->textile_id}},{{$product->product_id}});if(iklikk==0){scrollToElement('#textilimage1');}">

                                    <div class="product photo slider-max-height-overflow extra-price-container{{($textile->extra_price>0 ? ' extra-price' : '' )}}">
                                       <img class="product-image-photo default_image lazyOwl owl-lazy"  data-src="{{ (ProductTextile::getImageUrl($textile->textile_id)) }}" alt="{{ $textile->name }}"/>
                                    </div>
                                    <div class="product details product-item-details">
                                       <strong class="product name product-item-name">
                                       <b>{{ $textile->name }}</b>
                                       @if($textile->extra_price)
                                       <span class="pull-right">
                                            (+ {{money($textile->extra_price)}}/fm) &nbsp;&nbsp;
                                       </span>
                                       @endif
                                       <br>
                                       {{ $textile->textile_catname }}

                                       </strong>


                                    </div>

                              </div>
                              @endif
                              @endforeach
                              <div class="clearfix"></div>



							</div>
                        </div>
                     </div>
                     <script type="text/javascript">
                        var slider_{{$type}};
                        var slider_{{$type}}_status = 0;
                        var slider_{{$type}}_config = {
                                        autoplayHoverPause: true,
                                        loop: true,
                                        lazyLoad : true,
                                        scrollPerPage: true,
                                        navRewind: false,
                                        margin: 10,
                                        nav: true,
                                        navText: ["<em class='fa fa-arrow-left' ></em>","<em class='fa fa-arrow-right'></em>"],
                                        dots: false,
                                        responsive: {
                                            0: {
                                                items:2
                                            },
                                            768: {
                                                items:3
                                            },
                                            992: {
                                                items:4
                                            },
                                            1200: {
                                                items:5
                                            }
                                        }
                                    };

                        function init_{{$type}}_Carousel(){
                            if (slider_{{$type}}_status == 1){
                                return;
                            }
                            if (slider_{{$type}}_status == 2) {
                                return;
                                $('#featured_product_textiles_{{$type}} .wrapper').addClass('products-grid');
                                $('#featured_product_textiles_{{$type}} .filterproducts').addClass('owl-carousel');
                                $('#featured_product_textiles_{{$type}} .product-item').removeClass('col-lg-3').removeAttr('style');
                                slider_{{$type}}_status = 1;
                            }
                            slider_{{$type}}_status = 1;
                            require([
                                'jquery',
                                'owl.carousel/owl.carousel.min'
                            ], function ($) {
                                slider_{{$type}} =  $("#featured_product_textiles_{{$type}} .owl-carousel").owlCarousel(slider_{{$type}}_config);


                            });
                        }
                        function destroy_{{$type}}_Carousel(){
                            if (slider_{{$type}}_status == 2) {
                                return;
                            }
                            slider_{{$type}}.data('owlCarousel').destroy();
                            $('#slider_of_{{$type}}').find('.category-filter').show();
                            $('#featured_product_textiles_{{$type}} .wrapper').removeClass('products-grid');
                            $('#featured_product_textiles_{{$type}} .filterproducts').removeClass('owl-carousel');
                            $('#featured_product_textiles_{{$type}} .product-item').addClass('col-lg-3').css('float','left');
                            $('#featured_product_textiles_{{$type}} .product-item img').each(function(){
                                if ($(this).data('src')) {
                                    $(this).attr('src', $(this).data('src'));
                                }
                            })
                            slider_{{$type}}_status = 2;
                        }


                     </script>
                  </div>
               </div>
            </div>
         </div>
@endif

