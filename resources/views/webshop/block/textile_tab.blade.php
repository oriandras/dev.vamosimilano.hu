  <div class="change-image">

                <div class="swatch-attribute color">

                    <div class="clearfix text-center">

                        <?php

                            $default_textile_1 = $default_textile_2 = 0;

							$firsttextile=-1;

							$firsttextilecounter=0;

                        ?>

                        @foreach ($textiles as $textile)

							<?php

								if ($textile->textile_id) {

									if (ProductTextile::lang()->find($textile->textile_id)->visible == 'no') {

										$firsttextilecounter++;

										continue;

									}

								}

								if ($textile->textile2_id) {

									if (ProductTextile::lang()->find($textile->textile2_id)->visible == 'no') {

										$firsttextilecounter++;

										continue;

									}

								}

								if ($firsttextile==-1) {

									$firsttextile = $firsttextilecounter;

								}

								if (($textile->textile2_id>0) and ($textile->extra_price==0)) {

									$textile->extra_price = DB::table('product_textiles')

									->join('textile_extraprices','product_textiles.extraprice_category_id','=','textile_extraprices.extraprice_category_id')

									->join('textile_extraprices_lang','textile_extraprices.extraprice_category_id','=','textile_extraprices_lang.extraprice_category_id')

									->where('product_textiles.textile_id',$textile->textile2_id)

									->where('textile_extraprices_lang.shop_id',getShopId())

									->whereNull('textile_extraprices.deleted_at')

									->whereNull('textile_extraprices_lang.deleted_at')

									->value('textile_extraprices_lang.extra_price');



								}

							?>

                            <div class="image-option{{(isset($textile_editor) ? ' hidden' : '')}}{{($textile->extra_price ? ' extra-price' : '' )}}" 

							<?

							

							//takaritas eredeti:

							//onclick="iklikk=1;findAndRewriteTextileData('textile_change_{{($textile->textile2_id ? $textile->textile2_id : $textile->textile_id)}}','textile_change_{{$textile->textile_id}}');iklikk=0;"

							?>

							@if (sizeof($parts)==1) 

								onclick="iklikk=1;if(disable_short_change==false){rewriteProductDetailsPrice('textile_butor', {{($textile->textile2_id ? $textile->textile2_id : $textile->textile_id)}},{{$product->product_id}});}iklikk=0;"

							@else

								onclick="iklikk=1;if(disable_short_change==false){rewriteProductDetailsPrice('textile_felso', {{($textile->textile2_id ? $textile->textile2_id : $textile->textile_id)}},{{$product->product_id}});rewriteProductDetailsPrice('textile_also', {{$textile->textile_id}},{{$product->product_id}});}iklikk=0;"

							@endif

							>

                                <?php

                                $preview_url = ProductImage::getHoverImageUrl($textile->image_id);

								$full_url = ProductImage::getHoverImageUrl($textile->image_id,'full');

                                if ($default_textile_1 == 0){

                                    $default_textile_1 = $textile->textile_id;

                                }

                                if ($default_textile_2 == 0 and $textile->textile2_id){

                                    $default_textile_2 = $textile->textile2_id;

                                }

                                ?>

								@if ($textile->textile2_id and !isset($textile_editor))

                                    <img onmouseover="$('#productimagehref').attr('href','{{ $full_url }}');$('#textilimage1').attr('title','{{ (ProductTextile::getTextileName($textile->textile2_id)) }}');$('#textilimage2').attr('title','{{ (ProductTextile::getTextileName($textile->textile_id)) }}');$('#textilimage1').attr('src','{{ ProductTextile::getImageUrl($textile->textile2_id) }}');$('#textilimage2').attr('src','{{ ProductTextile::getImageUrl($textile->textile_id) }}');" src="{{ ProductTextile::getImageUrl($textile->textile2_id,40) }}" data-target-image="#product-image-{{ $product->product_id }}" data-image-hover="{{ $preview_url }}" title="{{ (ProductTextile::getTextileName($textile->textile2_id)) }}">

									<img onmouseover="$('#productimagehref').attr('href','{{ $full_url }}');$('#textilimage1').attr('title','{{ (ProductTextile::getTextileName($textile->textile2_id)) }}');$('#textilimage2').attr('title','{{ (ProductTextile::getTextileName($textile->textile_id)) }}');$('#textilimage1').attr('src','{{ ProductTextile::getImageUrl($textile->textile2_id) }}');$('#textilimage2').attr('src','{{ ProductTextile::getImageUrl($textile->textile_id) }}');" src="{{ ProductTextile::getImageUrl($textile->textile_id,40) }}" class="last" data-priceplus="{{$textile->extra_price}}" data-priceplus-string="{{money($textile->extra_price)}}" data-target-image="#product-image-{{ $product->product_id }}" data-image-hover="{{ $preview_url }}" title="{{ (ProductTextile::getTextileName($textile->textile_id)) }}">

                                @elseif (!isset($textile_editor))

                                    <img onmouseover="$('#productimagehref').attr('href','{{ $full_url }}');$('#textilimage1').attr('title','{{ (ProductTextile::getTextileName($textile->textile_id)) }}');$('#textilimage2').attr('title','{{ (ProductTextile::getTextileName($textile->textile_id)) }}');$('#textilimage1').attr('src','{{ ProductTextile::getImageUrl($textile->textile_id) }}');$('#textilimage2').attr('src','{{ ProductTextile::getImageUrl($textile->textile_id) }}');" src="{{ ProductTextile::getImageUrl($textile->textile_id,40) }}" data-priceplus="{{$textile->extra_price}}" data-priceplus-string="{{money($textile->extra_price)}}" data-target-image="#product-image-{{ $product->product_id }}" data-image-hover="{{ $preview_url }}" title="{{ (ProductTextile::getTextileName($textile->textile_id)) }}">

									<img onmouseover="$('#productimagehref').attr('href','{{ $full_url }}');$('#textilimage1').attr('title','{{ (ProductTextile::getTextileName($textile->textile_id)) }}');$('#textilimage2').attr('title','{{ (ProductTextile::getTextileName($textile->textile_id)) }}');$('#textilimage1').attr('src','{{ ProductTextile::getImageUrl($textile->textile_id) }}');$('#textilimage2').attr('src','{{ ProductTextile::getImageUrl($textile->textile_id) }}');" src="{{ ProductTextile::getImageUrl($textile->textile_id,40) }}" class="last" data-priceplus="{{$textile->extra_price}}" data-priceplus-string="{{money($textile->extra_price)}}" data-target-image="#product-image-{{ $product->product_id }}" data-image-hover="{{ $preview_url }}" title="{{ (ProductTextile::getTextileName($textile->textile_id)) }}">

                                @endif



                            </div>

                        @endforeach

                    </div>

                    @if (!isset($textile_editor))

                    <div class="text-center">

                    <small>{{ t('A csillaggal jelölt szövetekre felár érvényes!') }}</small>

                    </div>

                    @endif

                </div>



            </div>



			<div class="center" id="changed-textile-target">

				<?php

					$default_textile_1 = $default_textile_2 = 0;

					if ($firsttextile != -1) {

						$textile=$textiles[$firsttextile];

					} else {

						$textile=$textiles[0];

					}

				?>



						<?php

						$preview_url = ProductImage::getHoverImageUrl($textile->image_id);

						if ($default_textile_1 == 0){

							$default_textile_1 = $textile->textile_id;

						}

						if ($default_textile_2 == 0 and $textile->textile2_id){

							$default_textile_2 = $textile->textile2_id;

						}

						?>

						@if ($textile->textile2_id and !isset($textile_editor))

							<div style="height:50px;overflow: hidden; margin-top: 5px;"><a id="textilimage1link" href="#" onclick="$('#textilimage1link').attr('href',$('#textilimage1').attr('src'));return true;" class="fancy"><img id="textilimage1" width="100%" src="{{ (ProductTextile::getImageUrl($textile->textile2_id)) }}" title="{{ (ProductTextile::getTextileName($textile->textile2_id)) }}"/></a></div>

						@elseif (!isset($textile_editor))

							<div style="height:50px;overflow: hidden; margin-top: 5px;"><a id="textilimage1link" href="#" onclick="$('#textilimage1link').attr('href',$('#textilimage1').attr('src'));return true;" class="fancy"><img id="textilimage1" width="100%" src="{{ (ProductTextile::getImageUrl($textile->textile_id)) }}" title="{{ (ProductTextile::getTextileName($textile->textile_id)) }}"/></a></div>

						@endif

						@if (!isset($textile_editor))

						<div style="height:50px;overflow: hidden; margin-bottom: 5px;"><a id="textilimage2link" href="#" onclick="$('#textilimage2link').attr('href',$('#textilimage2').attr('src'));return true;" class="fancy"><img id="textilimage2" width="100%" src="{{ (ProductTextile::getImageUrl($textile->textile_id)) }}" title="{{ (ProductTextile::getTextileName($textile->textile_id)) }}"/></a></div>

						@endif





			</div>

            @if ($product->type != 'general')

                @if (isset($new_version))

                        <a href="{{ action('WebshopController@productSelectTextile', str_replace(url('/').'/'.t('products', 'url').'/', '', $product->getUrl())) }}" class="btn btn-primary btn-block textileSelect text-center color-white" id="changeTextileBtn">{{ t('Szövet választása') }}</a>

					    <a href="#" onclick="setCookie('selected{{$product->product_id}}', '', -1);location.reload();" class="btn btn-primary btn-block textileSelect text-center color-white hidden" id="changeTextileReset">{{ t('Szövet választás törlése') }}</a>

                @endif

                <div id="changeTextile" style="margin-top: 4px; {{isset($new_version) ? 'display: none;': ''}}">

                    <ul class="nav nav-tabs" role="tablist">

                        <?php

                            $c = 0;

                            $active_sheet = 0;

                            if ($product->plannable  == 'yes'){

								if (sizeof($custom_parts)>1) {

									$active_sheet = 1;

								} else {

									$active_sheet = 0;

								}

                            }

                        ?>

                        @foreach ($custom_parts as $key_part => $values_)

							@if ( isset($textile_editor) and $textile_editor )

								<li role="presentation" class="data item title{{ ($c == $active_sheet ? ' active': '') }}" onclick="switchTabTextileselectpage()">

							@else

								<li role="presentation" class="data item title{{ ($c == $active_sheet ? ' active': '') }}" onclick="switchTabTextile()">

							@endif

                                <a href="#{{$key_part}}" aria-controls="home" class="data switch" data-part-key="{{$key_part}}" role="tab" data-toggle="tab">{{ t('parts_of_'.$key_part) }}</a>

                            </li>

                            <?php

                                $c++;

                            ?>

                        @endforeach

                    </ul>

                    <div class="tab-content">

                    <?php

                            $c = 0;

                        ?>

                    @foreach ($custom_parts as $key_part => $values_)



                        <div role="tabpanel" class="tab-pane{{ ($c == $active_sheet ? ' active': '') }}" id="{{$key_part}}">

                            <div class="clearfix"></div>

                            @foreach ($values_ as $i => $part)

                                <?php

                                    $textile = ProductTextile::lang()->where('product_textiles.textile_id', ($i == 0 ? ($default_textile_2 ? $default_textile_2 : $default_textile_1) : $default_textile_1))->first();

                                    if (empty($textile)){

                                        continue;

                                    }

                                ?>



                                <div class="form-group">



                                    <label for="ownTextile{{$key_part.$i}}">{{ $part->name }}</label>

                                    <div class="input-group">

                                        <span class="input-group-addon v1"><a id="ownTextileImglink{{$key_part.$i}}" href="#" onclick="$('#ownTextileImglink{{$key_part.$i}}').attr('href',$('#ownTextileImg{{$key_part.$i}}').attr('src'));return true;" class="fancy"><img src="{{ ProductTextile::getImageUrl($textile->textile_id) }}" id="ownTextileImg{{$key_part.$i}}" style="max-width:80px;height:26px;"></a></span>

                                        <input type="text" name="ownTextile{{$key_part.$i}}Name" id="ownTextile{{$key_part.$i}}" readonly="" class="form-control textileContainer" value="{{ $textile->name }}" data-part="{{ $part->admin_name }}" data-fmprice="0">

                                        @if (isset($new_version))

                                        <span class="input-group-btn">

                                            <a href="{{ action('WebshopController@productSelectTextile', $product->getUrl()) }}?type={{$part->admin_name}}" class="btn btn-primary textileSelect">{{ t('Kiválasztás') }}</a>

                                            <button class="btn btn-primary textileSelect hidden" onclick="closeSliders('#slider_of_{{ $part->admin_name }}'); $('#slider_of_{{ $part->admin_name }}').slideToggle(200, function(){ init_{{$part->admin_name }}_Carousel(); scrollToElement('#{{isset($new_version_link) ? 'set-textiles' : 'changeTextile'}}')}); " type="button" data-id="ownTextile{{$key_part.$i}}" data-subid="textile_{{$part->admin_name}}" data-part="{{ $part->admin_name }}" data-partname="{{ $part->name }}" data-alapszovet="{{ $textile->textile_id }}" style="border-radius: 0;">{{ t('Kiválasztás') }}</button>

                                        </span>

                                        @else

                                        <span class="input-group-btn">

                                            <button class="btn btn-primary textileSelect" onclick="closeSliders('#slider_of_{{ $part->admin_name }}'); $('#slider_of_{{ $part->admin_name }}').slideToggle(200, function(){ init_{{$part->admin_name }}_Carousel(); scrollToElement('#{{isset($new_version_link) ? 'set-textiles' : 'changeTextile'}}')}); " type="button" data-subid="textile_{{$part->admin_name}}" data-id="ownTextile{{$key_part.$i}}" data-part="{{ $part->admin_name }}" data-partname="{{ $part->name }}" data-alapszovet="{{ $textile->textile_id }}" style="border-radius: 0;">{{ t('Kiválasztás') }}</button>

                                        </span>

                                        @endif

                                    </div>

                                </div>

                            @endforeach

                        </div>

                        <?php

                                $c++;

                            ?>

                    @endforeach

                    </div>

                </div>

<? /*

 @if ($product->type != 'general' and sizeof($parts) and isMobile())

                @foreach ($custom_parts as $part_key => $parts_data)

                    @foreach ($parts_data as $i => $part)

                        @include('webshop.block.product_textiles_slider', ['part' => $part, 'type' => $part->admin_name,'all_textiles' => $all_textiles])

                    @endforeach

                @endforeach

            @endif

	*/

?>

            @endif

