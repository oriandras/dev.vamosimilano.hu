<?php



    $sizes = $product->getSizes();



    $standard = [];



    foreach ($sizes as $size) {

        if ($size->is_standard){

            $standard = $size;

        }

    }



    if (empty($standard)) {

        if (sizeof($sizes)) {

            $standard = $sizes[0];

            $sizes[0]->is_standard = 1;

        }

    }



    if (sizeof($textiles)) {

        $default_textile =  $textiles[0];

        $price_plus =  $product->getDefaultPricePlus(($textiles[0]));





        $tmp_default_textile_2 = $tmp_default_textile_1 = 0;

        $tmp_default_parts = [];



            if (sizeof($textiles)){

                foreach ($textiles as $key => $textile){

					if ($textile->textile_id) {

						if (ProductTextile::lang()->find($textile->textile_id)->visible == 'no') {

							continue;

						}

					}

					if ($textile->textile2_id) {

						if (ProductTextile::lang()->find($textile->textile2_id)->visible == 'no') {

							continue;

						}

					}

                    if ($tmp_default_textile_1 == 0){

                        $tmp_default_textile_1 = $textile->textile_id;

                        $tmp_default_textile_2 = ($textile->textile2_id ? $textile->textile2_id : $textile->textile_id);

                    }

                }

                foreach ($parts as $i => $part)

                {

                        if ($i == 1) {

                            $tmp_default_parts[$part->admin_name] = $tmp_default_textile_1;

                        }else{

                            $tmp_default_parts[$part->admin_name] = $tmp_default_textile_2;

                        }

                }



            }



        $tmp_array = [

            'product_id' => $product->product_id,

            'sizes' => [

                'a' => $standard,

            ],

            'textile' => $tmp_default_parts,

        ];

        $price_array = CartHelper::calculateItemPrice($tmp_array, true);

        $price_plus = $price_array['textile_price'];







    }else{

        $price_plus = $default_textile = 0;



    }





//<form class="ajax-form" id="form-product-{{$product->product_id}}" ajax-action="{{ action('CartController@addToCart') }}">

?>

<form id="form-product-{{$product->product_id}}" action="{{ action('CartController@putToCart') }}" method="POST">







<div class="product-sizes" itemprop="offers" itemscope itemtype="http://schema.org/Offer" >

<?php $alapar=0; ?>

@if ($product->type == 'non-scalable' and sizeof($sizes) == 1)

    @foreach ($sizes as $size)

		<?php if ($size->getPrice()>$alapar) $alapar=$size->getPrice(); ?>

    <div class="row">

        <div class="col-sm-4">{{ t('Alapár') }}</div>

        <div class="col-sm-8"><span class="price pull-right">{{ money($size->getPrice()) }}</span></div>

    </div>

    <input type="hidden" name="feature_id" data-size-id="{{$size->size_id}}" value="{{ $size->size_id }}">

    @endforeach



@else



    @foreach ($sizes as $size)

		<?php if ($size->getPrice()>$alapar) $alapar=$size->getPrice(); ?>

    <div class="row size{{ ($size->is_standard == 1 ? ' active' : '') }} animated zoomInDown sizecontainer-{{$size->size_name}}">

        <div class="col-sm-12" id="meret-{{$size->size_name}}">

            <input type="radio" class="radio" style="display: none;" data-size-id="{{$size->size_id}}" data-size-name="{{$size->size_name}}" data-product_id="{{$product->product_id}}" data-price="{{$size->price}}" name="feature_id" {{ ($size->is_standard == 1 ? 'checked' : '') }} value="{{ $size->size_id }}">

            {{ $size->size_name }}

            @if ($size->size_x)

            <span class="size">{{ ($size->size_x ? ' - '. getUnit($size->size_x) : '').($size->size_y ? "x".getUnit($size->size_y) : '').($size->size_z ? "x".getUnit($size->size_z) : '') }}</span>

            @else

            <span class="size">{{ ($size->asize_x ? ' - '. getUnit($size->asize_x) : '').($size->asize_y ? "x".getUnit($size->asize_y) : '') }}</span>



            @endif

            <span class="price pull-right">





            <span class="price">

            @if ($size->list_price > $size->getPrice())

            <i class="old2-price">{{ money($size->list_price) }}</i>

            @endif

            {{ money($size->getPrice()) }}</span>



            </span>

        </div>



    </div>



    @endforeach

    @if ($product->mounting == 'yes')

    <div class="row change-mounting-parent">

        <div class="col-sm-6 text-center change-mounting active-item active" id="mt-right" onclick="$('#helpImageData').attr('src',$('#helpImageData').data('src')+$('#helpImageData').data('right'));setCookie('lastusedmounting', 'right', 10);">

            {{ t('Jobbos') }} <input type="radio" class="mounting" style="display: none;" checked="" id="mounting_right" name="mounting" checked value="right">



        </div>

         <div class="col-sm-6 text-center change-mounting" id="mt-left" onclick="$('#helpImageData').attr('src',$('#helpImageData').data('src')+$('#helpImageData').data('left'));setCookie('lastusedmounting', 'left', 10);">

            {{ t('Balos') }} <input type="radio" class="mounting" style="display: none;" id="mounting_left" name="mounting" value="left">

        </div>



    </div>

    @endif

	

	@if ($product->seat == 'yes')

    <div class="row change-seat-parent">

        <div class="col-sm-6 text-center change-seat active-item active" onclick="setCookie('lastusedseat', 'szivacs', 10);">

            {{ t('Szivacsos ülőfelület') }} <input type="radio" class="seat" style="display: none;" checked="" id="seat_szivacs" name="seat" checked value="szivacs">



        </div>

         <div class="col-sm-6 text-center change-seat" onclick="setCookie('lastusedseat', 'rugo', 10);">

            {{ t('Rugós ülőfelület') }} <input type="radio" class="seat" style="display: none;" id="seat_rugo" name="seat" value="rugo">

        </div>



    </div>

    @endif

	

    @if(sizeof($standard))

		@if ($standard->bsize_y)

        <div id="other-sizes" style="display: none;">

        @if ($standard->asize_y)

            <div class="row">

                <div class="col-lg-3 text-center"><b>A</b><br>{{ t('oldal') }}</div>

                @if(ProductSize::getSizeString($product->product_id,'a','S',false,false)!='')

					<div data-rel="S" data-side="A" class="col-lg-2 animated clicked text-center{{($standard->size_name == 'S' ? ' active':'')}}" id="AS">S <?=ProductSize::getSizeString($product->product_id,'a','S',false,false)?></div>

				@endif

				@if(ProductSize::getSizeString($product->product_id,'a','M',false,false)!='')

					<div data-rel="M" data-side="A" class="col-lg-2 animated clicked text-center{{($standard->size_name == 'M' ? ' active':'')}}" id="AM">M <?=ProductSize::getSizeString($product->product_id,'a','M',false,false)?></div>

				@endif

				@if(ProductSize::getSizeString($product->product_id,'a','L',false,false)!='')

					<div data-rel="L" data-side="A" class="col-lg-2 animated clicked text-center{{($standard->size_name == 'L' ? ' active':'')}}" id="AL">L <?=ProductSize::getSizeString($product->product_id,'a','L',false,false)?></div>

				@endif

				@if(ProductSize::getSizeString($product->product_id,'a','XL',false,false)!='')

					<div data-rel="XL" data-side="A" class="col-lg-2 animated clicked text-center{{($standard->size_name == 'XL' ? ' active':'')}}" id="AXL">XL <?=ProductSize::getSizeString($product->product_id,'a','XL',false,false)?></div>

				@endif

            </div>

        @endif

        @if ($standard->bsize_y)

            <div class="row">

                    <div class="col-lg-3 text-center"><b>B</b><br>{{ t('oldal') }}</div>

					@if(ProductSize::getSizeString($product->product_id,'b','S',false,false)!='')

						<div data-rel="S" data-side="B" class="col-lg-2 animated clicked text-center{{($standard->size_name == 'S' ? ' active':'')}}" id="BS">S <?=ProductSize::getSizeString($product->product_id,'b','S',false,false)?></div>

					@endif

					@if(ProductSize::getSizeString($product->product_id,'b','M',false,false)!='')

						<div data-rel="M" data-side="B" class="col-lg-2 animated clicked text-center{{($standard->size_name == 'M' ? ' active':'')}}" id="BM">M <?=ProductSize::getSizeString($product->product_id,'b','M',false,false)?></div>

					@endif

					@if(ProductSize::getSizeString($product->product_id,'b','L',false,false)!='')

						<div data-rel="L" data-side="B" class="col-lg-2 animated clicked text-center{{($standard->size_name == 'L' ? ' active':'')}}" id="BL">L <?=ProductSize::getSizeString($product->product_id,'b','L',false,false)?></div>

					@endif

					@if(ProductSize::getSizeString($product->product_id,'b','XL',false,false)!='')

						<div data-rel="XL" data-side="B" class="col-lg-2 animated clicked text-center{{($standard->size_name == 'XL' ? ' active':'')}}" id="BXL">XL <?=ProductSize::getSizeString($product->product_id,'b','XL',false,false)?></div>

					@endif

                </div>

        @endif

        @if ($standard->csize_y)

                    <div class="row">

                        <div class="col-lg-3 text-center"><b>C</b><br>{{ t('oldal') }}</div>

						@if(ProductSize::getSizeString($product->product_id,'c','S',false,false)!='')

							<div data-rel="S" data-side="C"  class="col-lg-2 animated clicked text-center{{($standard->size_name == 'S' ? ' active':'')}}" id="CS">S <?=ProductSize::getSizeString($product->product_id,'c','S',false,false)?></div>

						@endif

						@if(ProductSize::getSizeString($product->product_id,'c','M',false,false)!='')

							<div data-rel="M" data-side="C"  class="col-lg-2 animated clicked text-center{{($standard->size_name == 'M' ? ' active':'')}}" id="CM">M <?=ProductSize::getSizeString($product->product_id,'c','M',false,false)?></div>

						@endif

						@if(ProductSize::getSizeString($product->product_id,'c','L',false,false)!='')

							<div data-rel="L" data-side="C" class="col-lg-2 animated clicked text-center{{($standard->size_name == 'L' ? ' active':'')}}" id="CL">L <?=ProductSize::getSizeString($product->product_id,'c','L',false,false)?></div>

						@endif

						@if(ProductSize::getSizeString($product->product_id,'c','XL',false,false)!='')

							<div data-rel="XL" data-side="C" class="col-lg-2 animated clicked text-center{{($standard->size_name == 'XL' ? ' active':'')}}" id="CXL">XL <?=ProductSize::getSizeString($product->product_id,'c','XL',false,false)?></div>

						@endif

                    </div>

        @endif

        </div>

        <div class="row">

            <div class="text-center cursor-pointer" id="other-size-shange-on" style="display: none;" onclick="otherSizeHide();">

                <small>{{ t('További méretek elrejtése') }}</small>



            </div>

            <div class="text-center cursor-pointer" id="other-size-shange-off" onclick="otherSizeShow(); ">

                <small>{{ t('További méretek megjelenítése') }}</small>

            </div>



        </div>

		@endif

    @endif





    @foreach ($sizes as $size)



    @endforeach







@endif







    @if (sizeof($textiles))

    <div class="row">

        <div class="col-sm-4">{{ t('Szövet felár') }}</div>

        <div class="col-sm-8"><span class="price pull-right" id="priceplus-target">{{ money($price_plus) }}</span></div>

    </div>

    @endif

	<div class="row" id="priceother-div" style="display:none">

        <div class="col-sm-4">{{ t('Kiegészítők') }}</div>

        <div class="col-sm-8"><span class="price pull-right"><span class="price" id="priceother-target">&nbsp;</span></span></div>

    </div>

    <div class="row">

        <div class="col-sm-4">{{ t('Vételár') }}</div>

        <div class="col-sm-8"><span class="price-wrapper pull-right"><span class="price" id="priceall-target">{{ money($price + $price_plus) }}</span></span></div>

    </div>

	

    <div class="clearfix"></div>

    <br>

    <?php

        $col = 'pull-right';

        if ($product->type == 'scalable' and $product->plannable == 'yes' and $kategoria) {

            $col = 'col-lg-6';

        }elseif ($product->type == 'scalable' and $product->plannable == 'yes') {

            $col = 'col-lg-6';

        }elseif ($kategoria) {

            $col = 'col-lg-6';

        }

    ?>

		@if ((App::getLocale()=='en') and ($product->category_id==128))

			<? //angol oldalon a matrac vásárlása letiltva! ?>

		@else

			@if ($alapar)
				@if ($product->category_id!=153)

						<a href="javascript:;" onclick="gtmAddToCart('{{ App::getLocale() }}','{{ $product->product_id }}','{{Config::get('shop.'.getShopCode().'.currency_code')}}','{{ $product->name }}','{{ $price + $price_plus }}','{{ $product->getParentCategoryAdminName() }}{{ $product->getCategory()->admin_name }}',<?php

					$s="";$p="";

					if(sizeof($standard)) {

						if ($standard->asize_y) {

							$s.="$('#product_size_a').val()+' '";

							$p="+";

						}

						if ($standard->bsize_y) {

							$s.=$p."$('#product_size_b').val()+' '";

							$p="+";

						}

						if ($standard->csize_y){

							$s.=$p."$('#product_size_c').val()+' '";

							$p="+";

						}

					} else {

						$s.="$('#product_size_a').val()+' '";

					}

					if ($product->mounting == 'yes') {

						$s.=$p."$('input[name=mounting]:checked', '#form-product-".$product->product_id."').val()";

					}
				
					?>{{ $s }},1);$('#form-product-{{$product->product_id}}').submit()"  id="product-page-addtocart-btn" class="btn btn-primary action tocart primary {{$col}}"><span>{{ t('Kosárba rakom') }}</span></a>
				@else
					<p>{{t('matrac_franciaagy_csak')}}</p>
				@endif
			@else

				{{ t('Árazás alatt!') }}

			@endif

		@endif

    @if ($kategoria)

		@if ($product->category_id!=153)

			<a href="javascript:;" onclick="openHelpImage()"  class="btn btn-primary action tocart primary gray {{$col}}"><span>{{ t('Segítség') }}</span></a>

		@endif

	@endif

	

	<meta itemprop="price" content="{{ $price }}">

	<meta itemprop="priceCurrency" content="{{Config::get('shop.'.getShopCode().'.currency_code')}}" />

	<meta itemprop="availability" content="http://schema.org/InStock" />

	<meta itemprop="itemCondition" itemtype="http://schema.org/OfferItemCondition" content="http://schema.org/NewCondition" />

</div>

<input type="hidden" name="product_id" id="product_id_" value="{{$product->product_id}}">

<input type="hidden" name="qty" value="1">

<input type="hidden" name="default_price_" id="default_price_" value="{{$price}}">

<input type="hidden" name="priceplus_price_" id="priceplus_price_" value="{{$price_plus}}">

<input type="hidden" name="priceplus_price_2_" id="priceplus_price_2_" value="{{0}}">

<input type="hidden" name="related" id="related_product" value="0">





@foreach (ProductPart::all() as $part_item)

    <?php

        $default_part_value =  (isset($tmp_default_parts[$part_item->admin_name]) ? $tmp_default_parts[$part_item->admin_name] : '');	

	?>

	<input type="hidden" name="textile[{{$part_item->admin_name}}]" id="textile_{{$part_item->admin_name}}" class="reset_click_" data-default="{{ $default_part_value }}" value="{{ $default_part_value }}">

    <?

	//takaritas

	//ez a teszt sor a fenti helyett

	//{{ $part_item->admin_name }}:<input type="text" name="textile[{{$part_item->admin_name}}]" id="textile_{{$part_item->admin_name}}" class="reset_click_" data-default="{{ $default_part_value }}" value="{{ $default_part_value }}">

	?>

@endforeach

@if(sizeof($standard))

    @if ($standard->asize_y)

    <input type="hidden" name="sizes[a]" id="product_size_a" value="{{$standard->size_name}}">

    @endif

    @if ($standard->bsize_y)

    <input type="hidden" name="sizes[b]" id="product_size_b" value="{{$standard->size_name}}">

    @endif

    @if ($standard->csize_y)

    <input type="hidden" name="sizes[c]" id="product_size_c" value="{{$standard->size_name}}">

    @endif

@else

    <input type="hidden" name="sizes[a]" id="product_size_a" value="S">

@endif





{{ csrf_field() }}

</form>

<input type="hidden" id="relatedfdata" name="relatedfdata" value="">

<input type="hidden" id="relatedfprice" name="relatedfprice" value="0">