<div class="container-fluid">
	<div class="row">
<?php
$category_data = $product->getPrimaryCategory($sizes);
$kategoria = $category_data['kategoria'];
$kategoria_prefix = $category_data['kategoria_prefix'];
$pages = $category_data['pages'];		
$default_textile_1 = $default_textile_2 = 0;
$firsttextile=-1;
$firsttextilecounter=0;
$j=0;
$pixcounter = 0;
foreach($textiles as $textile) {
	if($textile->textile_id){
		if (ProductTextile::lang()->find($textile->textile_id)->visible == 'no') {
			$firsttextilecounter++;
		}
	}
	if ($textile->textile2_id) {
		if (ProductTextile::lang()->find($textile->textile2_id)->visible == 'no') {
			$firsttextilecounter++;
			continue;
		}
	}
	if (($textile->textile2_id>0) and ($textile->extra_price==0)) {
		$textile->extra_price = DB::table('product_textiles')
			->join('textile_extraprices','product_textiles.extraprice_category_id','=','textile_extraprices.extraprice_category_id')
			->join('textile_extraprices_lang', 'textile_extraprices.extraprice_category_id', '=', 'textile_extraprices_lang.extraprice_category_id')
			->where('product_textiles.textile_id',$textile->textile2_id)
			->where('textile_extraprices_lang.shop_id',getShopId())
			->whereNull('textile_extraprices.deleted_at')
			->whereNull('textile_extraprices_lang.deleted_at')
			->value('textile_extraprices_lang.extra_price');
	}
	if ($default_textile_1 == 0){
		$default_textile_1 = $textile->textile_id;
	}
	if ($default_textile_2 == 0 and $textile->textile2_id){
		$default_textile_2 = $textile->textile2_id;
	}
	$preview_url = ProductImage::getHoverImageUrl($textile->image_id);
	$full_url = ProductImage::getHoverImageUrl($textile->image_id,'full');
	$product_image_prev = ProductImage::getHoverImageUrl($textile->image_id);
	if(isset($textile_editor)) {
		//echo "textile_editor: van<br>";
	}
	else {
		//echo "textile_editor: nincs<br>";
	}
	/*echo $product_image_prev."<br>";
	echo "textile1: ".$textile->textile_id."<br>";
	echo "textile2: ".$textile->textile2_id."<br>";
	echo "xtraprice: ".$textile->extra_price."<br>";
	echo "def textile1: ".$default_textile_1."<br>";
	echo "def textile2: ".$default_textile_2."<br>";
	echo "textile counter: ".$firsttextilecounter."<br><hr>";*/
	if( ($textile->textile_id==$default_textile_1) && ($textile->textile2_id==$default_textile_2) ){
		$def_textile_checker = " border-primary active default_selection";
	}//if
	else {
		$def_textile_checker = "";
	}//else
	if($textile->extra_price>0) {
		$extra_price_checker = " extra-price";
		$has_extra_price = 1;
		$xtraprice = '<em>'.money($textile->extra_price).' Ft/fm</em>';
	}
	else {
		$extra_price_checker = "";
		$xtraprice = "";
	}
	if(ismobile()) {
		$breakpoint = 3;
		$colsize = 4;
	}
	else {
		$breakpoint = 6;
		$colsize = 2;
	}
	if($j===$breakpoint){
		$j=0; 
	?>
	</div>
	<div class="row">
	<?php
	}//if
	if($textile->textile_id != $textile->textile2_id) {
		?>
		<div class="col-{{$colsize}} text-center px-0" onmouseover="changeProductImage('{{ $preview_url }}', '{{$full_url}}')" onClick="textileTabSwitch('{{$textile->textile2_id}}', '{{$textile->textile_id}}', '{{$pixcounter}}')">
			<div class="textilepicker text-center shadow{{$def_textile_checker}}{{$extra_price_checker}} pix-{{$pixcounter}}" onClick="refreshTextilePreview('{{ ProductTextile::getImageUrl($textile->textile2_id,40) }}', '{{ ProductTextile::getImageUrl($textile->textile2_id) }}', 'felso', '{{(ProductTextile::getTextileName($textile->textile2_id))}}', '{{$xtraprice}}'); refreshTextilePreview('{{ ProductTextile::getImageUrl($textile->textile_id,40) }}', '{{ ProductTextile::getImageUrl($textile->textile_id) }}', 'also', '{{(ProductTextile::getTextileName($textile->textile_id))}}', '{{$xtraprice}}'); setTextile('felso', '{{$textile->textile2_id}}');setTimeout( setTextile('also', '{{$textile->textile_id}}'), 1000);">
				<span class="textilepix tp-top" style="background-image: url({{ ProductTextile::getImageUrl($textile->textile2_id,40) }});" title="Felső rész: {{ (ProductTextile::getTextileName($textile->textile2_id)) }}"></span>
				<span class="textilepix tp-down" style="background-image: url({{ ProductTextile::getImageUrl($textile->textile_id,40) }});" title="Alsó rész: {{ (ProductTextile::getTextileName($textile->textile_id)) }}"></span>
			</div>
		</div>
	<?php
	}//if
	else {
		//ha egyszínű a választás máshogy kell futtatni a scripteket
	?>
		<div class="col-{{$colsize}} text-center px-0" onmouseover="changeProductImage('{{ $preview_url }}', '{{$full_url}}')" onClick="textileTabSwitch('{{$textile->textile2_id}}', '{{$textile->textile_id}}', '{{$pixcounter}}')">
			<div class="textilepicker text-center shadow{{$def_textile_checker}}{{$extra_price_checker}} pix-{{$pixcounter}}" onClick="refreshTextilePreview('{{ ProductTextile::getImageUrl($textile->textile2_id,40) }}', '{{ ProductTextile::getImageUrl($textile->textile2_id) }}', 'butor', '{{(ProductTextile::getTextileName($textile->textile2_id))}}', '{{$xtraprice}}'); setTextile('butor', '{{$textile->textile2_id}}')">
				<span class="textilepix tp-top" style="background-image: url({{ ProductTextile::getImageUrl($textile->textile2_id,40) }});" title="Felső rész: {{ (ProductTextile::getTextileName($textile->textile2_id)) }}"></span>
				<span class="textilepix tp-down" style="background-image: url({{ ProductTextile::getImageUrl($textile->textile_id,40) }});" title="Alsó rész: {{ (ProductTextile::getTextileName($textile->textile_id)) }}"></span>
			</div>
		</div>
	<?php	
	}//else
	$j++;
	$pixcounter++;
}//endforeach
?>
		<?php
		if(sizeof($photos)) {
			foreach($photos as $photo){
		?>
		<div class="col-{{$colsize}} text-center px-0" onmouseover="changeProductImage('{{url($photo->getImageUrl())}}', '{{url($photo->getImageUrl())}}')">
			<img src="{{url($photo->getImageUrl())}}" class="image-fluid textilepicker" alt="{{ t('Vendégágy funkció') }}" title="{{ t('Vendégágy funkció') }}" style="max-width: 100%; height: 43px;">
		</div>
		<?php
			}//foreach photos
		}//endif photos
		?>
	</div>
	<?php
	if(isset($has_extra_price)) {
	?>
	<div class="row">
		<div class="col-12 text-right">
			<p><strong>*</strong> {{ t('A csillaggal jelölt szövetekre felár érvényes!') }}</p>
		</div>
	</div>
	<?php
	}//endif
	if ($product->plannable  == 'yes'){
		if (sizeof($custom_parts)>1) {
			$active_tab = 1;
		}
		if (sizeof($custom_parts)>2) {
			$active_tab = 2;
		}
		else {
			$active_tab = 0;
		}
	}
	?>
<?php if( ismobile() ) { ?>
	<div class="row">
		<div class="col-12">
			<p>A szövetek képére kattintva változtathatja meg az adott bútorrész szöveteit:</p>
		</div>
	</div>
<?php } ?>
	<div class="row">
		<ul class="nav nav-tabs col-12" id="textile_changer_tabs">
			<?php
			$c = 0;
			$active_sheet = 0;
			if ($product->plannable  == 'yes'){
				if (sizeof($custom_parts)>1) {
					$active_sheet = 1;
				}
				else {
					$active_sheet = 0;
				}
			}
			?>
			@foreach ($custom_parts as $key_part => $values_)
				<?php $counter_i=0; ?>
				@foreach ($values_ as $i => $part)
				<?php $counter_i++; ?>
				@endforeach
			<li class="nav-item">
				<a href="#{{$key_part}}" aria-controls="home" class="textiletypetabs nav-link{{ ($c == $active_sheet ? ' active shadow': '') }}" id="tab-{{$key_part}}" role="tab" data-toggle="tab" aria-selected="{{ ($c == $active_sheet ? 'true': 'false') }}" onClick="resetTextile()">{{ t('parts_of_'.$key_part) }} {{ ($counter_i>2 ? '<span class="badge badge-primary">'.$counter_i.'</span>' : '') }}</a>
			</li>
			<?php
				$counter_i=0;
			$c++;
			?>
			@endforeach
		</ul>
		<div class="tab-content col-12 shadow" id="textiles-content">
			<?php
			$c = 0;
			?>
			@foreach ($custom_parts as $key_part => $values_)
			<div role="tabpanel" class="tab-pane{{ ($c == $active_sheet ? ' fade show active': '') }} pt-3" id="{{$key_part}}" aria-labelledby="{{$key_part}}-tab">
				@foreach ($values_ as $i => $part)
				<?php
				$textile = ProductTextile::lang()->where('product_textiles.textile_id', ($i == 0 ? ($default_textile_2 ? $default_textile_2 : $default_textile_1) : $default_textile_1))->first();
				if (empty($textile)){
					continue;
				}
				if($textile->extra_price>0){
					$textile_price = $textile->extra_price;
				}
				else {
					$textile_price = 0;
				}
				
				?>
				<p class="mb-1"><a class="a-{{$part->admin_name}}-bigimage bigimgopen" href="javascript:void(0)" data-toggle="modal" data-target="#textilepixmodal" data-textilefullpix="{{ ProductTextile::getImageUrl($textile->textile_id) }}"><i class="fa fa-external-link" aria-hidden="true"></i></a> <strong>{{ $part->name }}:</strong> <span id="textil-{{$part->admin_name}}-span">{{ $textile->name }} {{ ($textile_price > 0 ? money($textile_price) : '') }}</span></p>
				<div class="textile_preview textil-{{$part->admin_name}}-preview col-12 m-0 mb-3 p-0 row" style="background-image: url({{ ProductTextile::getImageUrl($textile->textile_id) }});" onClick="setTextile('{{$part->admin_name}}', '{{$textile->textile_id}}')" data-originalpix="{{ ProductTextile::getImageUrl($textile->textile_id) }}" data-originalid="{{$textile->textile_id}}" data-part="{{$part->admin_name}}">
					<div class="d-flex col-8 align-items-center cursor-pointer visible-onhover" data-toggle="modal" data-target="#textileselectmodal" data-parttype="{{ $part->admin_name }}">
						<p class="text-center m-0 align-self-center p-2">{{t('Ide kattintva választhat új textilt')}}</p>
					</div>
					<div class="col-4 p-0 m-0 bg-white text-right">
						<img class="img-fluid border border-white" id="item-textile-preview" alt="{{ $part->name }}" src="{{ asset('images/textileditor/'. $kategoria_prefix .'-'.$part->admin_name.'.png') }}" style="border-width: 5px;">
					</div>
				</div>
				@endforeach
			</div>
			<?php
			$c++;
			?>
			@endforeach
		</div>
	</div>
</div>
<?php
$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
$uri = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'].$uri_parts[0];
//$uri = $_SERVER['REQUEST_URI'];
?>
<input type="hidden" name="current_uri" value="{{$uri}}" id="catch_the_uri">
<div class="modal fade" id="textileselectmodal" tabindex="-1" role="dialog" aria-labelledby="textileselectmodal" aria-hidden="true">
	<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content h-100">
			<div class="modal-body" id="textilecontent">
				<p class="text-center" id="what">
					<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
					<span class="sr-only">{{t('Töltődik...')}}</span>
				</p>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="textilepixmodal" tabindex="-1" role="dialog" aria-labelledby="textilepixmodal" aria-hidden="true">
	<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content h-100">
			<div class="modal-body">
				<img src="" class="img-fluid">
			</div>
		</div>
	</div>
</div>