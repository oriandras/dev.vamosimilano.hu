<?php if (!isset($i)) $i=0; ?>
<div class="breadcrumbs">
    <ul class="items" itemscope itemtype="http://schema.org/BreadcrumbList">
        <li class="item home" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="/"><span itemprop="name">{{ t('Főoldal') }}</span></a><meta itemprop="position" content="1" /></li>
        <li class="item category3" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="{{ action('WebshopController@product', '') }}"><span itemprop="name">{{ t('Termékek') }}</span></a><meta itemprop="position" content="2" /></li>
        @if((isset($current_category) and sizeof($current_category)))
		    <?php
		        $breadcrumbs = $current_category->getBreadcrumbs();
				$i=2;
		    ?>
		    @if (isset($breadcrumbs))
		    @foreach($breadcrumbs as $url => $breadcrumb)
			<?php $i++; ?> 
			@if (($i<=sizeof($breadcrumbs)+1) or (isset($product)))
		    <li class="item category3" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="{{$url}}"><span itemprop="name">{{ $breadcrumb }}</span></a><meta itemprop="position" content="{{ $i }}" /></li>
		    @else
			<li class="item category3"><a href="{{$url}}">{{ $breadcrumb }}</a></li>	
			@endif
			@endforeach
		    @endif
	    @endif
	    @if (isset($product))
			<?php $i++; ?>
			@if (isset($textileditor))
				<li class="item category3" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="{{ $product->getUrl() }}" title="{{ $product->getName() }}"><span itemprop="name">{{ $product->getName() }}</span></a><meta itemprop="position" content="{{ $i }}" /></li>
			@else
				<li class="item category3"><a href="{{ $product->getUrl() }}" title="{{ $product->getName() }}">{{ $product->getName() }}</a></li>
			@endif
		@endif
	    @if (isset($search))
			<?php $i++; ?>
        <li class="item category3">{{ t('Keresés').' - '.$search }}</li>
	    @endif
	    @if (isset($textileditor))
	    	<?php $i++; ?>
        <li class="item category3">{{ t('Szövet választás') }}</li>

	    @endif
    </ul>
</div>
