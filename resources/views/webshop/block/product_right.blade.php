<div class="sidebar sidebar-additional{{ (sizeof($related_products) ? '' : ' related-container')}}">


    <div class="custom-block custom-block-1">
    @if (sizeof($related_products))
    <div class="page-title-wrapper product related-title">
        <span class="base"  >{{ t('Választható kiegészítők') }}</span>
    </div>
    <div class="row">
        @foreach ($related_products as $related)
            <div class="related-row animated bounceIn">
                <label>
				@if ($related->size_id == -1)
					<input type="checkbox" name="related"  {{ ($related->default == 1 ? 'data-default="1"' : '') }} data-product_id="{{ $related->product_id }}" class="related" value="{{ $related->related_id }}" data-size="{{ $related->size_id }}" data-priceplus2="{{ $related->product_price }}">
				@else
				<input type="radio" name="related"  {{ ($related->default == 1 ? 'data-default="1"' : '') }} data-product_id="{{ $related->product_id }}" class="related" value="{{ $related->related_id }}" data-size="{{ $related->size_id }}" data-priceplus2="{{ $related->product_price }}">
				@endif

                @if ($related->rel_id == -1)
                     {{ $related->name }}
                @else
					@if (isset($related->size))
						{{ $related->product->getName().' - '.$related->size->size_name }} <?php /* - {{ ProductSize::getSizeString($related->rel_id, 'a', $related->size->size_name, false, true) }} */ ?>
					@else
						{{ $related->product->getName() }}
					@endif
                @endif
                @if ($related->product->category_id != 128)
					({{ ($related->show_price > 0 ? '+' : '').money($related->show_price) }})
				@endif
				</label>
            </div>

        @endforeach
            @if ($product->related_btn)
            <div class="animated bounceIn text-center">
                <label onclick="uncheckRelated()">{{ t('Kijelölés kivétele') }}</label>
            </div>
            @endif


    </div>
    @endif


@if ($product->type!='general')
        @include('webshop.block.features')


@endif
    </div>
</div>
