<?php

if (!isset($i)) $i=1;

$pclick = "gtmProductClick('Product list','".$product->getName()."','".$product->product_id."','".$product->getDefaultPrice()."','".$product->getParentCategoryAdminName().$product->getCategory()->admin_name."','".$i."');";

$tol1="";
$tol2="";
if (($product->type == "scalable") or ($product->plannable == "yes")) {
	$tol1=t('From');
	$tol2=t('-tól');
}
?>
<?php	
	/*Új szűrők felvétele*/
$replace = [','=>'', ' '=>'-', '"'=>'', '&lt;' => '', '&gt;' => '', '&#039;' => '', '&amp;' => '', '&quot;' => '', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'Ae', '&Auml;' => 'A', 'Å' => 'A', 'Ā' => 'A', 'Ą' => 'A', 'Ă' => 'A', 'Æ' => 'Ae','Ç' => 'C', 'Ć' => 'C', 'Č' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C', 'Ď' => 'D', 'Đ' => 'D', 'Ð' => 'D', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ē' => 'E', 'Ę' => 'E', 'Ě' => 'E', 'Ĕ' => 'E', 'Ė' => 'E', 'Ĝ' => 'G', 'Ğ' => 'G', 'Ġ' => 'G', 'Ģ' => 'G',	'Ĥ' => 'H', 'Ħ' => 'H',	'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ī' => 'I', 'Ĩ' => 'I', 'Ĭ' => 'I', 'Į' => 'I',     'İ' => 'I', 'Ĳ' => 'IJ', 'Ĵ' => 'J', 'Ķ' => 'K', 'Ł' => 'K', 'Ľ' => 'K',     'Ĺ' => 'K', 'Ļ' => 'K', 'Ŀ' => 'K', 'Ñ' => 'N', 'Ń' => 'N', 'Ň' => 'N','Ņ' => 'N', 'Ŋ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O','Ö' => 'Oe', '&Ouml;' => 'Oe', 'Ø' => 'O', 'Ō' => 'O', 'Ő' => 'O', 'Ŏ' => 'O','Œ' => 'OE', 'Ŕ' => 'R', 'Ř' => 'R', 'Ŗ' => 'R', 'Ś' => 'S', 'Š' => 'S','Ş' => 'S', 'Ŝ' => 'S', 'Ș' => 'S', 'Ť' => 'T', 'Ţ' => 'T', 'Ŧ' => 'T','Ț' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'Ū' => 'U','&Uuml;' => 'Ue', 'Ů' => 'U', 'Ű' => 'U', 'Ŭ' => 'U', 'Ũ' => 'U', 'Ų' => 'U','Ŵ' => 'W', 'Ý' => 'Y', 'Ŷ' => 'Y', 'Ÿ' => 'Y', 'Ź' => 'Z', 'Ž' => 'Z','Ż' => 'Z', 'Þ' => 'T', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a','ä' => 'ae', '&auml;' => 'ae', 'å' => 'a', 'ā' => 'a', 'ą' => 'a', 'ă' => 'a','æ' => 'ae', 'ç' => 'c', 'ć' => 'c', 'č' => 'c', 'ĉ' => 'c', 'ċ' => 'c','ď' => 'd', 'đ' => 'd', 'ð' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e','ë' => 'e', 'ē' => 'e', 'ę' => 'e', 'ě' => 'e', 'ĕ' => 'e', 'ė' => 'e','ƒ' => 'f', 'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g', 'ĥ' => 'h','ħ' => 'h', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ī' => 'i','ĩ' => 'i', 'ĭ' => 'i', 'į' => 'i', 'ı' => 'i', 'ĳ' => 'ij', 'ĵ' => 'j','ķ' => 'k', 'ĸ' => 'k', 'ł' => 'l', 'ľ' => 'l', 'ĺ' => 'l', 'ļ' => 'l','ŀ' => 'l', 'ñ' => 'n', 'ń' => 'n', 'ň' => 'n', 'ņ' => 'n', 'ŉ' => 'n','ŋ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'oe','&ouml;' => 'oe', 'ø' => 'o', 'ō' => 'o', 'ő' => 'o', 'ŏ' => 'o', 'œ' => 'oe','ŕ' => 'r', 'ř' => 'r', 'ŗ' => 'r', 'š' => 's', 'ù' => 'u', 'ú' => 'u','û' => 'u', 'ü' => 'ue', 'ū' => 'u', '&uuml;' => 'ue', 'ů' => 'u', 'ű' => 'u','ŭ' => 'u', 'ũ' => 'u', 'ų' => 'u', 'ŵ' => 'w', 'ý' => 'y', 'ÿ' => 'y','ŷ' => 'y', 'ž' => 'z', 'ż' => 'z', 'ź' => 'z', 'þ' => 't', 'ß' => 'ss','ſ' => 'ss', 'ый' => 'iy', 'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G','Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I','Й' => 'Y', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O','П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F','Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SCH', 'Ъ' => '','Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA', 'а' => 'a','б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo','ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l','м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's','т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch','ш' => 'sh', 'щ' => 'sch', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e','ю' => 'yu', 'я' => 'ya'];
$query='SELECT DISTINCT `property`, `value` FROM `product_properties` WHERE `product_id` = '.$product->product_id.' AND `deleted_at` IS NULL GROUP BY `property`, `value` ORDER BY `property` ASC';
	//echo $query;
$prop_filters=DB::select($query);
$new_value=[];
foreach ($prop_filters as $prop){
	$title_slug=str_replace(array_keys($replace), $replace, strtolower($prop->property));
	$item_slug=str_replace(array_keys($replace), $replace, strtolower($prop->value));
	$pushvalue=$title_slug.'_'.$item_slug;
	array_push($new_value, $pushvalue);
	//echo $new_value;
}
$props=implode(" ",$new_value);
if (sizeof($textiles)) {
	foreach($textiles as $textile) {
		if ($textile->textile_id) {
			if (ProductTextile::lang()->find($textile->textile_id)->visible == 'no') {
				continue;
			}
		}
		if ($textile->textile2_id) {
			if (ProductTextile::lang()->find($textile->textile2_id)->visible == 'no') {
				continue;
			}
		}
		$props .= ' textile_'.$textile->textile_id;
        if ($textile->textile2_id) {
			$props .= ' textile_'.$textile->textile2_id;
		}
		$textile_ids[$textile->textile_id] = $textile->textile_id;
        }//foreach
}//if
/*
@if (isset($lazy_counter) and $lazy_counter > 3)
lazy-hover-container
@endif
*/
$datakeys = "";
foreach ($size_data as  $key => $size_d) {
	$datakeys .= ' data-'.$key.'="'.$size_d.'"';
}
?>
<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="d-flex align-items-stretch item col-sm-12 col-md-6 col-xl-4 mt-5 {{$props}} product product-item product-{{$product->product_id}} lining-{{$product->lining}}" id="product-{{$product->product_id}}" {{$datakeys}} data-price="{{round($price)}}">
	<meta itemprop="position" content="{{ $i }}" />
    <div data-container="product-grid" class="card shadow product-item-info">
		<a itemprop="url" tabindex="-1" onclick="{{$pclick}}" href="{{ $product->getUrl() }}" title="{{ $product->getName() }} - {{ t('project_name', 'project') }}" {{($product->menu_top == 'yes' ? 'class="item-highlighted"' : '')}}>
			<img alt="{{ $product->getName() }} - {{ $product->getCategory()->name }}" src="{{ $product->getDefaultImageUrl(600) }}" id="product-image-{{ $product->product_id }}" class="img-fluid card-img-top">
		</a>
			@if ($product->getPercentPrice()>0)
			<div style="position:absolute; top:10px; right:10px; border-radius:15px; background-color:#CD7126;">
			<a onclick="{{$pclick}}" href="{{$product->getUrl()}}" title="{{$product->getName()}}" style="text-decoration: none;"><span style="font-weight:bold; font-size:17px; color:white;">&nbsp;-{{ $product->getPercentPrice() }}%&nbsp;</span></a>
			</div>
			@endif
        <div class="product details product-item-details">
            <strong class="product name product-item-name">
                <a onclick="{{$pclick}}" href="{{ $product->getUrl() }}" class="product-item-link original-order"><span>{{ $product->getName() }}</span></a>
            </strong>
            <div class="price-box price-final_price">
				
                @if ($price_full > $price)
                <div class="row">
                    <div class="col-lg-6">
                        <span class="old-price align-right">
                            <span class="price-container price-final_price tax weee">
                                <span class="price-label"></span>
                                <span class="price-wrapper">
                                    <span class="price normal-size">{{ ($price_full > $price ? money($price_full) : '&nbsp;') }}</span>
                                </span>
                            </span>
                        </span>
                    </div>
                    <div class="col-lg-6">
                        <span class="special-price align-left">
                            <span class="price-container price-final_price tax weee">
                                <span class="price-label">{{ t('A termék ára') }}</span>
                                <span class="price-wrapper " data-price-type="finalPrice"  id="product-price-{{ $product->product_id }}">
                                    <span class="price">{{ $tol1 }}{{ money($price) }}{{ $tol2 }}</span>
                                </span>
                            </span>
                        </span>
                    </div>
                </div>
                @else
                <span class="special-price">
                    <span class="price-container price-final_price tax weee">
                        <span class="price-label">{{ t('A termék ára') }}</span>
                        <span class="price-wrapper " data-price-type="finalPrice"  id="product-price-{{ $product->product_id }}">
                            <span class="price">{{ $tol1 }}{{ money($price) }}{{ $tol2 }}</span>
                        </span>
                    </span>
                </span>

                @endif
            </div>
            <span class="price-order hidden">{{ $price }}</span>

            @if (sizeof($textiles))
            <div class="change-image">
                <div class="swatch-attribute color">
                    <div class="clearfix text-center">
                        @foreach ($textiles as $textile)
							<?php
								if ($textile->textile_id) {
									if (ProductTextile::lang()->find($textile->textile_id)->visible == 'no') {
										continue;
									}
								}
								if ($textile->textile2_id) {
									if (ProductTextile::lang()->find($textile->textile2_id)->visible == 'no') {
										continue;
									}
								}
                            
                                $preview_url = base64image(ProductImage::getHoverImageUrl($textile->image_id),'jpeg');
                            ?>
                            <div class="image-option">
								<?php /*
                                @if ($textile->textile2_id)
                                    <img {{(isset($lazy_counter) and $lazy_counter > 3) ? 'data-':''}}src="{{ (ProductTextile::getImageUrl($textile->textile2_id,40)) }}" width="40" height="20" class="lazy hover-lazy" data-target-image="#product-image-{{ $product->product_id }}" data-image-hover="{{ $preview_url }}">
                                @else
                                    <img {{(isset($lazy_counter) and $lazy_counter > 3) ? 'data-':''}}src="{{ (ProductTextile::getImageUrl($textile->textile_id,40)) }}" width="40" height="20" class="lazy hover-lazy"  data-target-image="#product-image-{{ $product->product_id }}" data-image-hover="{{ $preview_url }}">
                                @endif
                                <img {{(isset($lazy_counter) and $lazy_counter > 3) ? 'data-':''}}src="{{ (ProductTextile::getImageUrl($textile->textile_id,40)) }}" width="40" height="20" class="last hover-lazy" data-target-image="#product-image-{{ $product->product_id }}" data-image-hover="{{ $preview_url }}">
								*/ ?>
								@if ($textile->textile2_id)
                                    <img {{(isset($lazy_counter) and $lazy_counter > 3) ? 'data-':''}}src="{{ (ProductTextile::getImageUrl($textile->textile2_id,40)) }}" width="40" height="20" class="lazy hover-lazy" data-target-image="#product-image-{{ $product->product_id }}" data-image-hover="{{ $preview_url }}" alt="{{ (ProductTextile::getTextileName($textile->textile2_id)) }}" title="{{ (ProductTextile::getTextileName($textile->textile2_id)) }}">
                                @else
                                    <img {{(isset($lazy_counter) and $lazy_counter > 3) ? 'data-':''}}src="{{ (ProductTextile::getImageUrl($textile->textile_id,40)) }}" width="40" height="20" class="lazy hover-lazy"  data-target-image="#product-image-{{ $product->product_id }}" data-image-hover="{{ $preview_url }}" alt="{{ (ProductTextile::getTextileName($textile->textile_id)) }}" title="{{ (ProductTextile::getTextileName($textile->textile_id)) }}">
                                @endif
                                <img {{(isset($lazy_counter) and $lazy_counter > 3) ? 'data-':''}}src="{{ (ProductTextile::getImageUrl($textile->textile_id,40)) }}" width="40" height="20" class="last hover-lazy" data-target-image="#product-image-{{ $product->product_id }}" data-image-hover="{{ $preview_url }}" alt="{{ (ProductTextile::getTextileName($textile->textile_id)) }}" title="{{ (ProductTextile::getTextileName($textile->textile_id)) }}">
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>

            @else
            <div class="change-image clearf"></div>
            @endif
            <div class="product-item-inner">
                <div class="product actions product-item-actions">
                    <div class="actions-primary">
						<?php /*
                        @if ($product->type == 'general')
                        <form class="ajax-form" id="form-product-{{$product->product_id}}-list" ajax-action="{{ action('CartController@addToCart') }}">
                            <input type="hidden" name="product_id" value="{{$product->product_id}}">
                            <input type="hidden" name="qty" value="1">
                            <input type="hidden" name="feature_id" value="0">
                            {{ csrf_field() }}

                        </form>
                        <a href="javascript:;" onclick="$('#form-product-{{$product->product_id}}-list').submit();" class="btn btn-primary action tocart primary"><span>{{ t('Tovább a termékhez') }}</span></a>
                        @else
							*/ ?>
                        <a href="{{ $product->getAddToCartUrl(); }}" class="btn btn-primary action tocart primary"><span>{{ t('Tovább a termékhez') }}</span></a>
                        <?php /* @endif */ ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="fb-likers"  id="target-fb-{{$product->product_id}}">
                        <p><i class="icon-heart"></i><span><strong>0</strong> {{ t('kedvelő') }}</span></p>
                        <ul class="friends-list small clearfix"></ul>
                    </div>

    </div>	
</div>