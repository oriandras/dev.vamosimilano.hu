<?php
$category_data = $product->getPrimaryCategory($sizes);
$kategoria = $category_data['kategoria'];
$kategoria_id = $category_data['kategoria'];
$kategoria_prefix = $category_data['kategoria_prefix'];
$pages = $category_data['pages'];
$price = $product->getDefaultPrice();
$price_full = $product->getDefaultFullPrice();
$price_array = array(
	'furniture_price' => $price,
	'textile_price' => 0
);
?>
<input type="hidden" value="{{$category_data['kategoria_prefix']}}" name="butortipus" id="butortipus">
<div class="container">
	<div class="row">
		<?php
		if( date('Ymd') > "20190430" && date('Ymd') < "20190601" ) {
			$hb = array(1452,324);
			if( in_array($product->product_id, $hb) ) {
		?>
		<div class="col-12">
			<div class="alert alert-primary mt-2">
				<p class="lead">
					<?php if(!ismobile()) { ?>
					<i class="fal fa-piggy-bank fa-4x float-left mr-3"></i>
					<?php } ?>
					<strong class="text-uppercase">Hónap bútora ajánlatban!</strong><br>
					Május 1-31. között a(z) {{ $product->getName() }} {{ money($price) }} helyett, 50% kedvezménnyel {{ money($price/2) }}-ért elvihető! Vegye igénybe az országosan ingyenes házhozsszállítási ajánlatunkat és az akár 10 év vázgaranciát is!
				</p>
			</div>
		</div>
		<?php
			}//HB termék
		}//hónap dátum
		?>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-title{{ (ismobile()==true ? ' text-center' : '') }}"><span itemprop="name">{{ $product->getName() }}</span></h1>
		</div>
		<div class="col-lg-6 product-media">
			<div class="d-block" >
				<figure class="figure shadow {{($product->menu_top == 'yes' ? 'item-highlighted' : 'item-normal')}} my-3">
					<a id="imagelink" href="javascript:void(0)" title="{{ $product->getName() }}" data-toggle="modal" data-target="#productImageModal">
						<img id="product-image-{{ $product->product_id }}" class="img-fluid the-product-image" src="{{ $product->getDefaultImageUrl(600) }}" alt="{{ $product->getName() }} - {{ $product->getCategory()->name }}" itemprop="image">
					</a>
				</figure>
				<div class="modal fade" id="productImageModal" tabindex="-1" role="dialog" aria-labelledby="productImageModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg" role="dialog">
						<div class="modal-content bg-dark">
							<div class="row">
								<div class="col-12 text-center pt-3">
									<img id="modal-image-{{ $product->product_id }}" class="img-fluid the-product-image mx-auto" src="{{ $product->getDefaultImageUrl('full') }}" alt="{{ $product->getName() }} - {{ $product->getCategory()->name }}" style="height:auto !important;">
								</div>
								<div class="col-12 pt-3 text-center">
									<p class="text-white">{{ $product->getName() }}</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				@if (sizeof($textiles))
					@include('webshop.block.textile_tab_2', ['textile_editor' => true, 'new_version' => true])
				@endif
			</div>
		</div>
		<div class="col-lg-6">
		<?php
			/*
			Súlytól függően megerősített matracokra felhívó szöveg.
			
			1. a $matractomb fogja tárolni a product id számokat, amik részt vesznek a folyamatban
			2. input fieldben adjuk meg a súlyt - javascript válaszol
				2.1 100 kg felett a plusz opciót ajánljuk
				2.2 110 kg felett az extra opciót ajánljuk
			3. opcionális fejlesztés: kattintsa ki egy ok gombbal az ajánlott kiegészítő terméket
			*/
			$matractomb = array(2405, 2408, 2409);
			if (in_array($product->product_id, $matractomb)){
		?>
			<div class="row">
				<div class="col-md-12">
					<h3 class="h3-responsive">Segítünk kiválasztani a tökéletes matracot</h3>
					<p>A megfelelő termék ajánlásához kérjük, adja meg a matrac használójának magasságát és testsúlyát!</p>
					<div class="row">
						<div class="input-group col-md-5">
							<input type="number" min="40" max="230" class="form-control" id="cm" placeholder="Magasság (cm)">
						</div>
						<div class="input-group col-md-5">
							<input type="number" min="80" class="form-control" id="kg" placeholder="Testsúly (kg)">
						</div>
						<div class="col-md-2">
							<a class="btn btn-sm btn-primary mt-0" onClick="chooseProduct('{{$product->product_id}}')"><i class="fa fa-calculator fa-2x" aria-hidden="true"></i></a>
						</div>
					</div>
					<div class="row hidden" id="offer">
						<div class="col-12 text-center">
							<div class="alert alert-primary shadow" role="alert" id="offertext"></div>
						</div>
					</div>
				</div>
			</div>
			@section ("footer_js")
			<script>
				function chooseProduct(x){
					var cm = $("#cm").val();
					var kg = $("#kg").val();
					
					if(cm == "" || kg == ""){
						alert("Kérem töltsön ki minden mezőt.");
						$("#offer").addClass("hidden");
					}
					else {
						$("#offer").removeClass("hidden");
						if(x==1824) {
							if(kg<=99){
								//alert("Az alábbi lehetőségek közül választhat.");
								$("#offertext").text("A kalkulátor számításai alapján válasszon a Plusz és Extra megerősítésű matracok közül.");
							}
							if(kg>=100 && kg<111){
								//alert("A tökéletes alvása érdekében plusz megerősítéssel ajánljuk a terméket.");
								$("#offertext").text("A kalkulátor számításai alapján válasszon a Plusz megerősítésű matracok közül.");
							}
							if (kg>=111) {
								//alert("A tökéletes alvása érdekében extra megerősítéssel ajánljuk a terméket.");
								$("#offertext").text("A kalkulátor számításai alapján válasszon az Extra megerősítésű matracok közül.");
							}
						}
						else {
							if(kg<=99){
								//alert("Az alábbi lehetőségek közül választhat.");
								$("#offertext").text("A kalkulátor számításai alapján válasszon a Plusz és Extra megerősítésű matracok közül.");
							}
							if(kg>=100){
								//alert("A tökéletes alvása érdekében plusz megerősítéssel ajánljuk a terméket.");
								$("#offertext").text("A kalkulátor számításai alapján válasszon a Plusz megerősítésű matracok közül.");
							}
						}
					}
				}
			</script>
			@append
		<?php
			}//if
		?>
			
			@if ($product->type == 'general')
			<div class="product-info-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
				<div class="row my-3">
					@if ($price_full > $price)
					<div class="col-4 d-flex flex-wrap align-items-center">
						<p>{{t('A termék ára: ')}}</p>
					</div>
					<div class="col-8 d-flex flex-wrap align-items-center">
						<p class="original-price pb-0" itemprop="highPrice">{{ ($price_full > $price ? money($price_full) : '') }}</p>
					</div>
					<div class="col-4 d-flex flex-wrap align-items-center">
						<p class="pb-0">{{t('A termék akciós ára: ')}}</p>
					</div>
					<div class="col-8 d-flex flex-wrap align-items-center">
						<p class="advanced-price pb-0" itemprop="lowPrice">{{ money($price) }}</p>
					</div>
				<meta itemprop="price" content="{{ $price }}">
					@else
					<div class="col-4 d-flex flex-wrap align-items-center">
						<p>{{t('A termék ára: ')}}</p>
					</div>
					<div class="col-8">
						<p class="price pb-0" itemprop="price" content="{{ $price }}">{{ money($price) }}</p>
					</div>
					@endif
				<meta itemprop="priceCurrency" content="{{ Config::get('shop.'.getShopCode().'.currency_code') }}" />
				<meta itemprop="availability" content="http://schema.org/InStock" />
				<meta itemprop="itemCondition" itemtype="http://schema.org/OfferItemCondition" content="http://schema.org/NewCondition" />
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="fb-like col-12 my-2" data-href="{{ $product->getUrl() }}" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
				<div class="col-12 text-center my-3">
					<form id="form-product-{{$product->product_id}}" action="{{ action('CartController@putToCart') }}" method="POST">
						<input type="hidden" name="product_id" value="{{$product->product_id}}">
						<input type="hidden" name="product_id" value="{{$product->product_id}}">
						<input type="hidden" name="qty" value="1">
						<input type="hidden" name="feature_id" value="0">
						{{ csrf_field() }}
					</form>
					<?php
					$nincskosargomb_category=array(119,157,127,130,129);
					if (in_array($product->category_id, $nincskosargomb_category)) {
						//kosárgomb le van tiltva
					?>
						<button class="btn btn-primary disabled">Ez a termék önmagában nem vásárolható meg</button>
					<?php
					} //if nincs kosár gomb
					else {
					?>
					<a href="javascript:;" onclick="gtmAddToCart('{{ App::getLocale() }}','{{ $product->product_id }}','{{Config::get('shop.'.getShopCode().'.currency_code')}}','{{ $product->name }}','{{ $price }}','{{ $product->getParentCategoryAdminName() }}{{ $product->getCategory()->admin_name }}','',1);$('#form-product-{{$product->product_id}}').submit();" class="btn btn-lg btn-primary"><span>{{ t('Kosárba rakom') }}</span></a>
					<?php
					}//van kosár gomb
					if ((isset($product->outlet_parent_id)) && ($product->outlet_parent_id)) {
						$oproduct = Product::lang()->where('products.product_id', $product->outlet_parent_id)->first();
					}//if
					if (isset($oproduct)) { 
					?>
					<a href="{{ $oproduct->getUrl() }}" class="btn btn-lg btn-secondary"><span>{{ t('Új termék vásárlása *') }}</span></a>
					<?php
					}//if
					?>
				</div>
				<?php
				//echo $product->getPercentPrice();
				if( strlen($product->outlet_location)>0 ) {
				?>
				<div class="col-12">
					@if($product->getPercentPrice()>0 && isset($oproduct))
					<p class="mt-3"><strong>*</strong> Amennyiben szeretné a terméket más méretben vagy szövetezéssel megrendelni kattintson az „Új termék vásárlása” gombra</p>
					@endif
					@if($product->outlet_location>0)
					<div class="alert alert-primary shadow" role="alert">
						<i class="fa fa-3x float-left mt-2 mr-3 fa-map-marker" aria-hidden="true"></i> {{t('outlet_'.$product->outlet_location)}}
					</div>
					@endif
				</div>
				<?php
				}
				?>
			</div>

			@else
			<span id="addtocart">
			@include('webshop.block.product_addtocart_2', ['kategoria' => $kategoria])
			</span>
			<hr>
			<div class="row noverflow">
				<div class="col-12">
					<div class="fb-like" data-href="{{ $product->getUrl() }}" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
				</div>
			</div>
			@endif
			<?php
			if($product->planner == 'yes') {
			?>
			<div class="row mt-3">
				<?php
				if (isMobile()){
					$planner_url = "https://play.google.com/store/apps/details?id=com.VamosiMilano.VamosiDesigner&hl=".strtolower(App::getLocale());
				}//if
				else {
					$planner_url = "/unity/index.html/?".$product->product_id."_".strtolower(App::getLocale());
				}//else
				?>
				<div class="col">
					<a href="{{$planner_url}}" target="_blank">
						<img src="https://vamosimilano.hu/images/planner_button_{{strtolower(App::getLocale())}}.jpg" alt="{{t('Vamosi Bútortervező App')}}" title="{{t('Megnyitás a Vamosi Bútortervezőben')}}" class="img-fluid">
					</a>
				</div>
			<?php
				}//endif
			?>
			</div>

		</div>
	</div>
	<?php
	//echo $kategoria;
	$expected_categories = array("kiegeszitok", "agynemu", "asztalok", "eloszobabutor", "eloszobafal", "fejtamla", "fotelek", "lampa", "mechanika", "paplan", "puff", "szonyeg", "takaro", "tarolok", "lampahaz", "allolampa", "falilampa", "fenyforras", "fuggesztek", "mennyezetvilagitas", "olvasolampa", "spotlampa", "egyenes", "fotel", "u-alaku-kanape", "l-alaku-kanape");
	if( in_array($kategoria, $expected_categories) ) {
	?>
	<div class="container">
	<?php
	}//if
	?>
	<div class="row mt-4">
		<div class="col-md-12">
			<ul class="nav nav-tabs col-12" id="productinfo_tabs" role="tablist">
				@if ($product->getDescription() != '')
				<li class="nav-item">
					<a href="#productdescription" aria-controls="home" class="nav-link active show" id="tab-productdescription" role="tab" data-toggle="tab" aria-selected="true">{{ t('Leírás') }}</a>
				</li>
				@endif
				@if ($product->features)
				<li class="nav-item">
					<a href="#productfeatures" aria-controls="features" class="nav-link" id="tab-productfeatures" role="tab" data-toggle="tab" aria-selected="false">{{ t('Jellemzők') }}</a>
				</li>
				@endif
				@if ($product->howto)
				<li class="nav-item">
					<a href="#producthowto" aria-controls="howto" class="nav-link" id="tab-producthowto" role="tab" data-toggle="tab" aria-selected="false">{{ t('Használati útmutató') }}</a>
				</li>
				@endif
				@if (sizeof($familyproducts))
				<li class="nav-item">
					<a href="#productfamilyproducts" aria-controls="familyproducts" class="nav-link" id="tab-productfamilyproducts" role="tab" data-toggle="tab" aria-selected="false">{{ t('Kapcsolódó termékek') }}</a>
				</li>
				@endif
			</ul>
			<div class="tab-content col-12 shadow" id="productinfo-content">
				@if ($product->getDescription() != '')
				<div role="tabpanel" class="tab-pane fade show active py-3" id="productdescription" aria-labelledby="productdescription">
					{{ $product->getDescription() }}
					@if (sizeof($photos))
						@foreach ($photos as $photo)
						<p class="text-justify">
							<img src="{{url($photo->getImageUrl())}}" class="image-fluid" alt="Vamosi Milano">
						</p>
						@endforeach
					@endif
				</div>
				@endif
				@if ($product->features)
				<div role="tabpanel" class="tab-pane fade py-3" id="productfeatures" aria-labelledby="productfeatures">
					{{ $product->features }}
				</div>
				@endif
				@if ($product->howto)
				<div role="tabpanel" class="tab-pane fade py-3" id="producthowto" aria-labelledby="producthowto">
					{{ $product->howto }}
				</div>
				@endif
				@if (sizeof($familyproducts))
				<div role="tabpanel" class="tab-pane fade py-3" id="productfamilyproducts" aria-labelledby="productfamilyproducts">
					<div class="row">
					@foreach ($familyproducts as $item)
						@if ($item->product_id != $product->product_id)
						<div class="col-sm-4 p-3">
							<div class="card">
								<a href="{{$item->getUrl()}}"><img class="card-img-top" alt="{{$item->getName()}}" src="{{ $item->getDefaultImageUrl(600) }}"></a>
								<div class="card-body">
									<p><strong>{{ $item->getName() }}</strong></p>
									<p class="text-right"><?php
									$price_full = $item->getDefaultFullPrice();
									$price = $item->getDefaultPrice();
									?>
									@if ($price_full > $price)
										<span class="original-price">{{money($price_full)}}</span>
									@endif
										<span class="price">{{money($price)}}</span>
									</p>
									<a href="{{$item->getUrl()}}" title="{{$item->getName()}}" class="btn btn-primary btn-block">{{ t('Részletek') }}</a>
								</div>
							</div>
						</div>
						@endif
					@endforeach
					</div>
				</div>
				@endif
			</div>
		</div>

	</div>
	<?php
		if( in_array($kategoria, $expected_categories) ) {
	?>
	</div>
	<?php
	}//if
	?>
</div>
<style>
	#productinfo-content img {
		max-width: 100%;
		height: auto;
	}
</style>