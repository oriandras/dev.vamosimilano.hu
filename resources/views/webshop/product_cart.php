<?php
if( sizeof($product) ) {
	$textiles = $product->getTextilePreviews(9);
	foreach ($textiles as $textile){
		if ($textile->textile_id) {
			if (ProductTextile::lang()->find($textile->textile_id)->visible == 'no') {
				continue;
			}//if
		}//if
		if ($textile->textile2_id) {
			if (ProductTextile::lang()->find($textile->textile2_id)->visible == 'no') {
				continue;
			}//if
		}//if
		$textile_ids[$textile->textile_id] = $textile->textile_id;
		if ($textile->textile2_id) {
			$textile_ids[$textile->textile2_id] = $textile->textile2_id;
		}//if
	}//foreach
if ($product->lining != 'none') {
	$lining = 1;
}
$size_data = [];
$item_min_x = $item_min_y = $item_max_x = $item_max_y = 0;
$sizes = $product->getSizes();
if (sizeof($sizes)){
	foreach ($sizes as $size){
		$size->size_x = getUnit($size->size_x);
		$size->size_y = getUnit($size->size_y);
		if ($size->size_x < $min_x or $min_x == 0) {
			$min_x = $size->size_x;
		}
		if ($size->size_y < $min_y or $min_y == 0) {
			$min_y = $size->size_y;
		}
		if ($size->size_x > $max_x or $max_x == 0) {
			$max_x = $size->size_x;
		}
		if ($size->size_y > $max_y or $max_y == 0) {
			$max_y = $size->size_y;
		}
		if ($size->size_x < $item_min_x or $item_min_x == 0) {
			$item_min_x = $size->size_x;
		}
		if ($size->size_y < $item_min_y or $item_min_y == 0) {
			$item_min_y = $size->size_y;
		}
		if ($size->size_x > $item_max_x or $item_max_x == 0) {
			$item_max_x = $size->size_x;
		}
		if ($size->size_y > $item_max_y or $item_max_y == 0) {
			$item_max_y = $size->size_y;
		}
	}
}
$size_data = [
'xmin' => $item_min_x,
'ymin' => $item_min_y,
'xmax' => $item_max_x,
'ymax' => $item_max_y,
];
if ($price < $min or $min == 0) {
$min = $price;
}
if ($price > $max or $max == 0) {
$max = $price;
}
if( !isset($metacount) ) {
	$metacount=1;
}
$pclick = "gtmProductClick('Product list','".$product->getName()."','".$product->product_id."','".$product->getDefaultPrice()."','".$product->getParentCategoryAdminName().$product->getCategory()->admin_name."','".$metacount."');";
$tol1="";
$tol2="";
if (($product->type == "scalable") or ($product->plannable == "yes")) {
	$tol1=t('From');
	$tol2=t('-tól');
}
$replace = [','=>'', ' '=>'-', '"'=>'', '&lt;' => '', '&gt;' => '', '&#039;' => '', '&amp;' => '', '&quot;' => '', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'Ae', '&Auml;' => 'A', 'Å' => 'A', 'Ā' => 'A', 'Ą' => 'A', 'Ă' => 'A', 'Æ' => 'Ae','Ç' => 'C', 'Ć' => 'C', 'Č' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C', 'Ď' => 'D', 'Đ' => 'D', 'Ð' => 'D', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ē' => 'E', 'Ę' => 'E', 'Ě' => 'E', 'Ĕ' => 'E', 'Ė' => 'E', 'Ĝ' => 'G', 'Ğ' => 'G', 'Ġ' => 'G', 'Ģ' => 'G',	'Ĥ' => 'H', 'Ħ' => 'H',	'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ī' => 'I', 'Ĩ' => 'I', 'Ĭ' => 'I', 'Į' => 'I',     'İ' => 'I', 'Ĳ' => 'IJ', 'Ĵ' => 'J', 'Ķ' => 'K', 'Ł' => 'K', 'Ľ' => 'K',     'Ĺ' => 'K', 'Ļ' => 'K', 'Ŀ' => 'K', 'Ñ' => 'N', 'Ń' => 'N', 'Ň' => 'N','Ņ' => 'N', 'Ŋ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O','Ö' => 'Oe', '&Ouml;' => 'Oe', 'Ø' => 'O', 'Ō' => 'O', 'Ő' => 'O', 'Ŏ' => 'O','Œ' => 'OE', 'Ŕ' => 'R', 'Ř' => 'R', 'Ŗ' => 'R', 'Ś' => 'S', 'Š' => 'S','Ş' => 'S', 'Ŝ' => 'S', 'Ș' => 'S', 'Ť' => 'T', 'Ţ' => 'T', 'Ŧ' => 'T','Ț' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'Ū' => 'U','&Uuml;' => 'Ue', 'Ů' => 'U', 'Ű' => 'U', 'Ŭ' => 'U', 'Ũ' => 'U', 'Ų' => 'U','Ŵ' => 'W', 'Ý' => 'Y', 'Ŷ' => 'Y', 'Ÿ' => 'Y', 'Ź' => 'Z', 'Ž' => 'Z','Ż' => 'Z', 'Þ' => 'T', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a','ä' => 'ae', '&auml;' => 'ae', 'å' => 'a', 'ā' => 'a', 'ą' => 'a', 'ă' => 'a','æ' => 'ae', 'ç' => 'c', 'ć' => 'c', 'č' => 'c', 'ĉ' => 'c', 'ċ' => 'c','ď' => 'd', 'đ' => 'd', 'ð' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e','ë' => 'e', 'ē' => 'e', 'ę' => 'e', 'ě' => 'e', 'ĕ' => 'e', 'ė' => 'e','ƒ' => 'f', 'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g', 'ĥ' => 'h','ħ' => 'h', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ī' => 'i','ĩ' => 'i', 'ĭ' => 'i', 'į' => 'i', 'ı' => 'i', 'ĳ' => 'ij', 'ĵ' => 'j','ķ' => 'k', 'ĸ' => 'k', 'ł' => 'l', 'ľ' => 'l', 'ĺ' => 'l', 'ļ' => 'l','ŀ' => 'l', 'ñ' => 'n', 'ń' => 'n', 'ň' => 'n', 'ņ' => 'n', 'ŉ' => 'n','ŋ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'oe','&ouml;' => 'oe', 'ø' => 'o', 'ō' => 'o', 'ő' => 'o', 'ŏ' => 'o', 'œ' => 'oe','ŕ' => 'r', 'ř' => 'r', 'ŗ' => 'r', 'š' => 's', 'ù' => 'u', 'ú' => 'u','û' => 'u', 'ü' => 'ue', 'ū' => 'u', '&uuml;' => 'ue', 'ů' => 'u', 'ű' => 'u','ŭ' => 'u', 'ũ' => 'u', 'ų' => 'u', 'ŵ' => 'w', 'ý' => 'y', 'ÿ' => 'y','ŷ' => 'y', 'ž' => 'z', 'ż' => 'z', 'ź' => 'z', 'þ' => 't', 'ß' => 'ss','ſ' => 'ss', 'ый' => 'iy', 'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G','Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I','Й' => 'Y', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O','П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F','Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SCH', 'Ъ' => '','Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA', 'а' => 'a','б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo','ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l','м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's','т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch','ш' => 'sh', 'щ' => 'sch', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e','ю' => 'yu', 'я' => 'ya'];
$query='SELECT DISTINCT `property`, `value` FROM `product_properties` WHERE `product_id` = '.$product->product_id.' AND `deleted_at` IS NULL GROUP BY `property`, `value` ORDER BY `property` ASC';
$prop_filters=DB::select($query);
$new_value=[];
foreach ($prop_filters as $prop){
	$title_slug=str_replace(array_keys($replace), $replace, strtolower($prop->property));
	$item_slug=str_replace(array_keys($replace), $replace, strtolower($prop->value));
	//$pushvalue=$title_slug.'_'.$item_slug;
	$pushvalue=strtolower($item_slug);
	array_push($new_value, $pushvalue);
}
$props=implode(" ",$new_value);
if (sizeof($textiles)) {
	foreach($textiles as $textile) {
		if ($textile->textile_id) {
			if (ProductTextile::lang()->find($textile->textile_id)->visible == 'no') {
				continue;
			}
		}
		if ($textile->textile2_id) {
			if (ProductTextile::lang()->find($textile->textile2_id)->visible == 'no') {
				continue;
			}
		}
		$props .= ' textile_'.$textile->textile_id;
		if ($textile->textile2_id) {
			$props .= ' textile_'.$textile->textile2_id;
		}
		$textile_ids[$textile->textile_id] = $textile->textile_id;
		}//foreach
}//if
$datakeys = "";
foreach ($size_data as  $key => $size_d) {
	$datakeys .= ' data-'.$key.'="'.$size_d.'"';
}
$metacount++;
$product_i++; 
?>

?>
<article class="col-sm-6 col-md-4 col-lg-3">
	<div class="card card-ecommerce">
		<div class="view overlay ">
			<img src="<?php echo $product->getDefaultImageUrl('full'); ?>" id="product-image-<?php echo  $product->product_id; ?>" alt="<?php echo $product->global_name; ?>" class="img-fluid card-img-top">
			<a>
				<div class="mask rgba-white-slight"></div>
			</a>
		</div>
		<?php
		if ( sizeof($textiles) ) {
		?>
		<div class="card-body text-center mb-0 pb-0">
		<?php
		foreach ($textiles as $textile) {
			if ($textile->textile_id) {
				if (ProductTextile::lang()->find($textile->textile_id)->visible == 'no') {
					continue;
				}//if
			}//if
			if ($textile->textile2_id) {
				if (ProductTextile::lang()->find($textile->textile2_id)->visible == 'no') {
					continue;
				}//if
			}//if
		?>
			<div class="textile-circle rounded-circle">
				<?php
				$preview_url = base64image(ProductImage::getHoverImageUrl($textile->image_id),'jpeg');
				if( $textile->textile2_id ) {
				?>
				<img src="<?php echo (ProductTextile::getImageUrl($textile->textile2_id,40)); ?>" class="textile-circle-top" alt="<?php echo (ProductTextile::getTextileName($textile->textile2_id)); ?>" title="<?php echo (ProductTextile::getTextileName($textile->textile2_id)); ?>" onmouseover="productItemImagePreview('<?php echo $product->product_id; ?>', '<?php echo $preview_url; ?>')">
				<img src="<?php echo  (ProductTextile::getImageUrl($textile->textile_id,40)) ; ?>" class="textile-circle-bottom"  alt="<?php echo  (ProductTextile::getTextileName($textile->textile_id)) ; ?>" title="<?php echo  (ProductTextile::getTextileName($textile->textile_id)) ; ?>" onmouseover="productItemImagePreview('<?php echo  $product->product_id ; ?>', '<?php echo  $preview_url ; ?>')">
				<?php
				}//if
				else {
				?>
				<img src="<?php echo  (ProductTextile::getImageUrl($textile->textile_id,40)) ; ?>" class="textile-circle-full"  alt="<?php echo  (ProductTextile::getTextileName($textile->textile_id)) ; ?>" title="<?php echo  (ProductTextile::getTextileName($textile->textile_id)) ; ?>" onmouseover="productItemImagePreview('<?php echo  $product->product_id ; ?>', '<?php echo  $preview_url ; ?>')">
				<?php
				}//else
				?>
			</div>
			<?php
			}//foreach
		?>
		</div>
		<?php
		}//if
		?>
		<div class="card-body text-center">
			<h2 class="h4-responsive text-uppercase"><strong><?php echo  $product->getName() ; ?></strong></h2>
		</div>
		<div class="card-body">
			<p class=""></p>
		</div>
	</div>
</article>
<?php
}
else {
	echo "nincs ilyen termék.";
}
?>
