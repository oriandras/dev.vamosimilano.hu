@extends('layouts.editor')

@section('content')

@include('webshop.block.breadcrumbs')


<main class="page-main" id="maincontent">

<div class="entry-content min-height">


    <div class="row nofullscreen">
        <div class="row-wrapper container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="">{{ $product->getName() }} - {{ t('Tervezése') }}
                    <a href="{{ url('/') }}" title="{{ t('Főoldal') }}" rel="home" class="pull-right desktop-view"><img height="50" width="109" alt="{{ t('project_name', 'project') }}" src="{{ asset('images/vamosilogo.png') }}">
                    </a>

                    </h3>
                    <a href="{{ url('/') }}" title="{{ t('Főoldal') }}" rel="home" class="mobile-view text-center" style="margin-bottom: 10px;"><img height="50" width="109" alt="{{ t('project_name', 'project') }}" src="{{ asset('images/vamosilogo.png') }}"></a>
                </div>
            </div>
        </div>
    </div>
    <div id="scaled"></div>
    <div style="width:100%; display: block;">

        <canvas class="emscripten" id="canvas" oncontextmenu="event.preventDefault()" height="641px" width="1140px"></canvas>
        <div class="text-center nofullscreen">
        <a href="javascript:;" onclick="setFullScreen()" class="btn btn-primary action primary"><span>{{ t('Teljes képernyő') }}</span></a>
        </div>
        <a href="javascript:;" onclick="exitFullScreen()" class="btn btn-primary action primary" id="exit_full" style="display: none; z-index: 10; position: fixed; right: 5px; top: 5px;"><span>{{ t('Kilépés a teljes képernyős módból') }}</span></a>
    </div>
</div>


	<script type='text/javascript'>
    	function LoadUrl(url)
		{
			window.location.href = url;
		}
		function GetSessionId()
		{
		    var unity_data = {uid:"{{getUid()}}", session_id:"{{ Session::getId() }}", product_id: "{{ $product->product_id }}", size_id:"0", size_a:'{{Input::get('page_A')}}',size_b:'{{Input::get('page_B')}}',size_c:'{{Input::get('page_C')}}' };
			SendMessage("Manager", "Initialize", JSON.stringify(unity_data));

		}



	</script>

    <script type='text/javascript'>
  var Module = {
    TOTAL_MEMORY: 268435456,
    errorhandler: null,			// arguments: err, url, line. This function must return 'true' if the error is handled, otherwise 'false'
    compatibilitycheck: null,
    dataUrl: "{{ asset('unity'.((isMobile()) ? '-mobile' : '').'/Release/WebGIBuild_v18.data') }}",
    codeUrl: "{{ asset('unity'.((isMobile()) ? '-mobile' : '').'/Release/WebGIBuild_v18.js') }}",
    memUrl: "{{ asset('unity'.((isMobile()) ? '-mobile' : '').'/Release/WebGIBuild_v18.mem') }}",

  };
</script>


</main>

    <script src="//code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
    <script >
            $( window ).on( "orientationchange", function( event ) {

                respondCanvas();
            });
            $( window ).resize(function() {
                //document.location.reload();
                respondCanvas();
            });
            var init = 0;
            var nega = parseInt($('#maincontent').css('padding-left'));
                nega = nega*1.5;


                var current_width = $('#canvas').width();
                var current_height = $('#canvas').height();
            function respondCanvas(){

                var max_width = $(window).width()-nega;

                if (max_width > 1140) {

                    writeUnityJs();
                    return false;
                }



                var scale = (max_width / current_width);

                $('#canvas').width((current_width-nega) * scale).height((current_height-nega)*scale);

                writeUnityJs();



             }
             respondCanvas();

             function setFullScreen(){

                 var i = document.getElementById("canvas");

                // go full-screen
                if (i.requestFullscreen) {
                	i.requestFullscreen();
                } else if (i.webkitRequestFullscreen) {
                	i.webkitRequestFullscreen();
                } else if (i.mozRequestFullScreen) {
                	i.mozRequestFullScreen();
                } else if (i.msRequestFullscreen) {
                	i.msRequestFullscreen();
                }

                return;

                  max_width = $(window).width();
                  var scale = (max_width / current_width);
                  if ($(window).height() < (current_height * scale)){
                      var scale = ($(window).height() / current_height);
                  }

                  var left = 0;
                  if ((current_width * scale) < max_width) {
                      left = (max_width - (current_width * scale)) / 2;
                  }

                      $('#canvas').css('position','fixed').animate({
                          'left': left,
                          'top':0,
                          'width':(current_width * scale),
                          'height':(current_height * scale)
                      });
                      $('.nofullscreen').hide();
                      $('#exit_full').show();

             }

             function exitFullScreen(){


                      $('#canvas').css('position','relative').animate({
                          'left': 0,
                          'top':0,
                          'width': current_width,
                          'height':current_width
                      });
                      respondCanvas();
                      $('.nofullscreen').show();
                      $('#exit_full').hide();

             }

             function writeUnityJs(){
                 if (init == 0){
                    var my_awesome_script = document.createElement('script');
                my_awesome_script.setAttribute('src','{{ asset('unity'.(isMobile() ? '-mobile' : '').'/Release/UnityLoader.js') }}?v5');

                document.getElementById('maincontent').appendChild(my_awesome_script);
                }
                init++;
             }


    </script>

@stop




