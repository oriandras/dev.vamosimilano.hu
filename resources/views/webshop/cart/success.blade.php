@extends('layouts.default')
@section('content')
<?php
$headversion="newhead";
if(isset($headversion) && $headversion==="newhead"){
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
		<?php
		if( ismobile() ) {
		?>
			<h1 class="h1-responsive text-uppercase title-center my-4">{{ t('Személyes adatok') }</h1>
		<?php
		}
		else {
		?>
			<ul class="stepper stepper-horizontal" id="cart-stepper">
				<li class="">
					<a href="javascript:void(0)">
						<span class="circle"><i class="fal fa-check"></i></span>
						<span class="label text-uppercase">Kosár tartalma</span>
					</a>
				</li>
				<li class="">
					<a href="javascript:void(0)">
						<span class="circle"><i class="fal fa-check"></i></span>
						<span class="label text-uppercase">Személyes adatok</span>
					</a>
				</li>
				<li class="active">
					<a href="javascript:void(0)">
						<span class="circle">3</span>
						<span class="label text-uppercase">Fizetés és befejezés</span>
					</a>
				</li>
			</ul>
		<?php
		}
		?>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="h1-responsive title-center text-uppercase">{{ t('Sikeres rendelés feladás') }}</h1>
			</div>
			<div class="col-md-6 mx-auto my-4">
				<p class="lead">{{ t('Kedves')." ".$order->firstname }}!</p>
				<p>{{ t('Rendelése sikeresen megérkezett hozzánk!') }}</p>
				<p>{{ t('Rendelés azonosítója') }}: <strong>{{ $order->order_number }}</strong></p>
				@if (sizeof($payment))
				<div class="alert alert-success" role="alert">
					<p><strong>{{ t('Sikeres tranzakciót hajtott végre!') }}</strong></p>
					<p>{{ t('Tranzakció azonosítója') }}: {{ $payment['trans_id'] }}</p>
					<p>{{ t('Rendelés státusza').": <strong>" .$order->getStatus() }}</strong></p>
					<p><strong>{{ t('Tranzakció részleti') }}</strong>: {{ UniCredit::getResultString($payment['result']) }}</p>
					<p><strong>{{ t('Összeg') }}</strong>: {{ money($payment['price']) }}</p>
				</div>
				@endif
				<p>{{ t('A rendelés státuszának változásáról e-mailben értesítjük, valamit nyomon követheti rendeléseit a "Rendelések" menüpontban') }}:
				</p>
				<p class="text-center">
				<a href="{{ action('ProfileController@orders') }}" class="btn btn-outline-primary">Rendelések</a>
				</p>
				@if (Session::get('payment_success'))
				<div style="border: 5px solid green; padding: 5px;">
					<p><strong>{{ t('Sikeresen kifizette a rendelését') }}:</strong></p>
					<p>{{ Session::get('payment_success') }}</p>
				</div>
				@endif
			</div>
		</div>
	</div>
</div>
<?php
}
else {
?>
<div class="clearfix"></div>
<div class="breadcrumbs">
    <ul class="items">
        <li class="item home">
            <a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">
                    {{ t('Főoldal') }} </a>
                        </li>
                    <li class="item cms_page">
                            <strong>{{ t('Sikeres rendelés feladás') }}</strong>
                        </li>
            </ul>
</div>

<main class="page-main" id="maincontent">

<div class="entry-content min-height">


    <div class="row">
        <div class="row-wrapper container">
            <div class="row">
                <div class="col-md-12">
                    <h3>{{ t('Kedves')." ".$order->firstname }}!</h3>
			<p>{{ t('Rendelése sikeresen megérkezett hozzánk!') }}</p>
            <p>{{ t('Rendelés azonosítója') }}: {{ $order->order_number }}</p>
            @if (sizeof($payment))
                        <div class="alert alert-success" role="alert">
                        <p><b>{{ t('Sikeres tranzakciót hajtott végre!') }}</b></p>

                        <p>{{ t('Tranzakció azonosítója') }}: {{ $payment['trans_id'] }}</p>

                        <p>{{ t('Rendelés státusza').": <b>" .$order->getStatus() }}</b></p>

                        <p><b>{{ t('Tranzakció részleti') }}</b>: {{ UniCredit::getResultString($payment['result']) }}</p>

                        <p><b>{{ t('Összeg') }}</b>: {{ money($payment['price']) }}</p>
                        </div>
                    @endif

            <p>{{ t('A rendelés státuszának változásáról e-mailben értesítjük, valamit nyomon követheti rendeléseit a "Korábbi rendelések" menüpontban, ide kattintva') }}:
            <br>
            <a href="{{ action('ProfileController@orders') }}">{{ action('ProfileController@orders') }}</a>
            </p>


            @if (Session::get('payment_success'))
            <div style="border: 5px solid green; padding: 5px;"><p><b>{{ t('Sikeresen kifizette a rendelését') }}:</b></p>
                <p>{{ Session::get('payment_success') }}</p>
                           </div>
            <br>
            @endif
                </div>
            </div>
        </div>
    </div>



</div>
</div></div>
</main>
<?php
}
?>
@stop
@section('footer_js')
@append