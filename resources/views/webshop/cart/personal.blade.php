@extends('layouts.default')
@section('content')
<?
$mplenabled = Shipping::MPLOK();
$headversion="newhead";
if(isset($headversion) && $headversion==="newhead"){
?>
@section('footer_js')
<script>
function loadOverViewBlock(){
	//alert("load");
	if ($('#extradata').is(":visible")==false){
		$('#extradata input:checkbox').each(function() {
			$(this).prop('checked',false); 
			$(this).removeAttr('checked');
		});
	}
	$.ajax({
			dataType: "json",
			type: "post",
			url: '/cart/loadoverviewblock',
			data: $('#carform').serialize()
		}).done(function(data) {
		if(data.fn){
			var tmpFunc = new Function(data.fn);
			tmpFunc();
		}
		$('#cart-right-sidebar').html(data.html);
		});
}
function copyBillingAddress(){
	if($('#billing_exist').prop('checked')){
		$('#billing_name').val($('#lastname').val()+" "+$('#firstname').val())
		$('.billing-copy').each(function(){
			var id = $(this).data('target');
			$('#'+id).val($(this).val());
			$('#'+id).focus();
		});
	}
	return '';
}
$('.copy-personal').click(function(){
	$('#lastname').val($('#user_lastname').val());
	$('#firstname').val($('#user_firstname').val());
	$('#lastname').focus();
	$('#firstname').focus();
});
function searchCity(zip){
	if (zip == '') {
		return '';
	}
	$('#city').prop('readonly', true);
	console.log("Irányítószám: "+zip);
	$.ajax({
		dataType: "json",
		type: "post",
		data: {zip:zip},
		url: '/cart/searchcity',
	}).done(function(data) {
		$('#city').removeAttr('readonly');
		if (data.fn){
			var tmpFunc = new Function(data.fn);
			tmpFunc();
		}
		$('#city').val(data.city);
		$("#city").focus();
		console.log("Város: "+data.city);
	});
}
function aszfCheck() {
	var checksum = 0;
	if( $("#courier_free").is(':checked') ) {
		checksum = checksum +1;
	}
	if( $("#check_aszf").is(':checked') ) {
		checksum = checksum +1;
	}
	if( checksum == 2 ) {
		$("#gotoCartButton").prop("disabled", false);
	}
	else {
		$("#gotoCartButton").prop("disabled", true);
	}
}
	function checkCartRequired(){
  var empty_flds = 0;
  $('#carform input,textarea,select').filter('[required]:visible').each(function() {
    if(!$.trim($(this).val())) {

        empty_flds++;
    }
  });

  if (empty_flds == 0) {
    $('#gotoCartButton').removeAttr('disabled');
  } else {
     $('#gotoCartButton').attr('disabled','disabled');

  }
}
</script>
@append
@include('layouts.messages')
	<?php
	if($content->count()) {
	?>
<form action="{{ action('CartController@store') }}" id="carform">
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
		<?php
		if( ismobile() ) {
		?>
			<h1 class="h1-responsive text-uppercase title-center my-4">{{ t('Személyes adatok') }</h1>
		<?php
		}
		else {
		?>
			<ul class="stepper stepper-horizontal" id="cart-stepper">
				<li class="active">
					<a href="{{ action('CartController@index') }}">
						<span class="circle"><i class="fal fa-check"></i></span>
						<span class="label text-uppercase">Kosár tartalma</span>
					</a>
				</li>
				<li class="active">
					<a href="{{ action('CartController@personal') }}">
						<span class="circle">2</span>
						<span class="label text-uppercase">Személyes adatok</span>
					</a>
				</li>
				<li class="">
					<a href="!#">
						<span class="circle">3</span>
						<span class="label text-uppercase">Fizetés és befejezés</span>
					</a>
				</li>
			</ul>
		<?php
		}
		?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-6">
					<p class="lead title text-uppercase">{{ t('Szállítási mód') }}</p>
					<p>{{ t('Kérjük válasszon szállítási módot, hogy hol veszi át a rendelését!') }}</p>
					<?php 
					if (!isset($default_data['delivery_mode'])) {
						$modok=getConfig('shipping_mode');
						$default_data['delivery_mode'] = $modok[0];
						$default_data['COURIER_FREE'] = 1;
					}
					//if ((CartHelper::getAdvPrice()>0) and ($default_data['delivery_mode'] == 'MPL')) {
					if ((!$mplenabled) and ($default_data['delivery_mode'] == 'MPL')) {
						$modok=getConfig('shipping_mode');
						$default_data['delivery_mode'] = $modok[0];
						echo "<script>loadOverViewBlock();</script>";
					}
					if (($mplenabled) and ($default_data['delivery_mode'] == 'COURIER')) {
						$modok=getConfig('shipping_mode');
						$default_data['delivery_mode'] = $modok[1];
						echo "<script>loadOverViewBlock();</script>";
					}
					$szallitas_counter=1;
					foreach (getConfig('shipping_mode') as $mode) {
					?>
						<div class="form-check">
						<?php
						if (($mode == 'COURIER') or ($mode == 'MPL')) {
							if ((($mode == 'COURIER') and (!$mplenabled))  or (($mplenabled) and ($mode == 'MPL'))) {
							?>
							<input type="radio" onclick="{{ ( $mode == 'COURIER' ? '$(\'#extradata\').show();' : '') }}$('#deliveridata').show()" onchange="loadOverViewBlock()" name="delivery_mode" {{ ($default_data['delivery_mode'] == $mode ? 'checked' : '') }} value="{{ $mode }}" class="form-check-input" id="szallitascounter{{$szallitas_counter}}">
							<label class="form-check-label" for="szallitascounter{{$szallitas_counter}}">{{ t('shipping_mode_'.$mode) }}</label>
							<?php
							}//if
						}//if
						else {
							?>
							<input type="radio" onclick="$('#extradata').hide();$('#deliveridata').hide()" onchange="loadOverViewBlock()" name="delivery_mode" {{ ($default_data['delivery_mode'] == $mode ? 'checked' : '') }} value="{{ $mode }}" class="form-check-input" id="szallitascounter{{$szallitas_counter}}">
							<label class="form-check-label" for="szallitascounter{{$szallitas_counter}}">{{ t('shipping_mode_'.$mode) }}</label>
							<?php
						}
						?>
						</div>
					<?php
						$szallitas_counter++;
					}//foreach
					?>
				</div>
				<div class="col-md-6">
					<p class="lead title text-uppercase">{{ t('Fizetési mód') }}</p>
					<p>{{ t('Kérjük válasszon fizetési módot, amellyel kiegyenlíti a rendelését!') }}</p>
				<?php
				$fizetes_counter=1;
				foreach (getConfig('billing_mode') as $mode) {
					if (!isset($default_data['payment'])) {
						$default_data['payment']=$mode;
					}//if
					if (strstr($mode, 'ADV') and CartHelper::isEnableAdv()) {
					?>
					<div class="form-check">
						<input type="radio" name="payment" onchange="loadOverViewBlock()" {{ ($default_data['payment'] == $mode ? 'checked' : '') }} value="{{ $mode }}" class="form-check-input" id="fizetescounter{{$fizetes_counter}}">
						<label class="form-check-label" for="fizetescounter{{$fizetes_counter}}">{{ t('billing_mode_'.$mode) }}</label>
					</div>
					<?php
					}//if
					elseif (!strstr($mode, 'ADV')) {
						if ($mode == 'COD') {
							if ((CartHelper::getAdvPrice()==0) and ($mplenabled)) {
					?>
					<div class="form-check">
						<input type="radio" name="payment" onchange="loadOverViewBlock()" {{ ($default_data['payment'] == $mode ? 'checked' : '') }} value="{{ $mode }}" class="form-check-input" id="fizetescounter{{$fizetes_counter}}">
						<label class="form-check-label" for="fizetescounter{{$fizetes_counter}}">{{ t('billing_mode_'.$mode) }}</label>
					</div>
					<?php
							}//if
						}//if
					?> 
					<div class="form-check">
						<input type="radio" name="payment" onchange="loadOverViewBlock()" {{ ($default_data['payment'] == $mode ? 'checked' : '') }} value="{{ $mode }}" class="form-check-input" id="fizetescounter{{$fizetes_counter}}">
						<label class="form-check-label" for="fizetescounter{{$fizetes_counter}}">{{ t('billing_mode_'.$mode) }}</label>
					</div>
					<?php
					}//elseif
					$fizetes_counter++;
				}//foreach
				?>
				</div>
				<div class="col-12">
					<p class="lead text title text-uppercase">{{ t("Személyes adatok") }}</p>
					<div class="row">
						<div class="col-md-6">
							<div class="md-form md-outline">
								<input name="user_lastname" id="user_lastname" class="form-control" onchange="copyBillingAddress()" value="{{ (isset($default_data['user_lastname']) ? $default_data['user_lastname'] : '') }}" type="text">
								<label for="user_lastname">{{ t('Vezetéknév') }} *</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="md-form md-outline">
								<input name="user_firstname" id="user_firstname" class="form-control"  onchange="copyBillingAddress()" value="{{ (isset($default_data['user_firstname']) ? $default_data['user_firstname'] : '') }}" type="text">
								<label for="user_firstname">{{ t('Keresztnév') }} *</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="md-form md-outline">
								<input name="email" id="user_email" class="form-control" onchange="loadOverViewBlock()" value="{{ (isset($default_data['email']) ? $default_data['email'] : '') }}" type="email">
								<label for="user_email">{{ t('E-mail cím') }} *</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="md-form md-outline">
								<input name="phone" id="user_phone" class="form-control" onchange="loadOverViewBlock()" value="{{ (isset($default_data['phone']) ? $default_data['phone'] : '') }}" type="text">
								<label for="user_phone">{{ t('Telefonszám') }} *</label>
							</div>
						</div>
					</div>
				</div>
				<?php 
					if (($default_data['delivery_mode'] == 'COURIER')) {
						$sdisplay=true;
					} else {
						$sdisplay=false;
					}
				?>
				
                @if (sizeof($extra_prices))
                <div id="extradata" class="block-content" {{ ($sdisplay ? '' : 'style="display: none;"') }}>
                    <div class="box">
                        <strong class="box-title">
                            <span>{{ t('További szolgáltatások') }}</span>
                        </strong>
                        <div class="box-content no-min-height">
                            @foreach ($extra_prices as $extra)
                            <div class="col-lg-12">
                                <div class="form-group required">
                                    <label><input type="checkbox" name="extra_prices[]" onchange="loadOverViewBlock()"  {{ ((isset($default_data['extra_prices']) and in_array($extra->id, $default_data['extra_prices'])) ? 'checked' : '') }} value="{{ $extra->id }}"> {{ $extra->name }} (+{{money($extra->price)}})</label>
                                </div>
                            </div>
                            @endforeach
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
				@else

			
                @endif

				<div class="col-md-6">
				<?php 
					if (($default_data['delivery_mode'] == 'COURIER') or ($default_data['delivery_mode'] == 'MPL')) {
						$sdisplay=true;
					} else {
						$sdisplay=false;
					}
				?>
					
					<div id="deliveridata" class="row" {{ ($sdisplay ? '' : 'style="display: none;"') }}>
						<div class="col-12">
							<p class="lead text title text-uppercase">{{ t("Szállítási adatok") }}</p>
							<div class="form-check pl-0">
								<input type="checkbox" id="atmasol1" class="form-check-input">
								<label for="atmasol1" class="form-check-label copy-personal">Szállítási név megegyezik a személyes adatokkal</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="md-form md-outline">
								<input name="lastname" id="lastname" class="form-control" onblur="copyBillingAddress()" value="{{ (isset($default_data['lastname']) ? $default_data['lastname'] : '') }}"  type="text">
								<label for="lastname">{{ t('Vezetéknév') }} *</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="md-form md-outline">
								<input name="firstname" id="firstname" onchange="loadOverViewBlock()" class="form-control" onblur="copyBillingAddress()" value="{{ (isset($default_data['firstname']) ? $default_data['firstname'] : '') }}" type="text">
								<label for="firstname">{{ t('Keresztnév') }} *</label>
							</div>
						</div>
						<div class="col-md-4">
							<div class="md-form md-outline">
								<input name="zip" id="zip" onchange="searchCity(this.value)" data-target="billing_zip" class="form-control billing-copy" value="{{ (isset($default_data['zip']) ? $default_data['zip'] : '') }}" type="number">
								<label for="zip">{{ t('Irsz.') }}</label>
							</div>
						</div>
						<div class="col-md-8">
							<div class="md-form md-outline">
								<input type="text" name="city" id="city" onchange="loadOverViewBlock()" data-target="billing_city" class="form-control billing-copy" value="{{ (isset($default_data['city']) ? $default_data['city'] : '') }}">
								<label for="city">{{ t('Város') }}</label>
							</div>
						</div>
						<div class="col-md-9">
							<div class="md-form md-outline">
								<input type="text" name="address" id="address" data-target="billing_address" onblur="loadOverViewBlock()" class="form-control billing-copy"  value="{{ (isset($default_data['address']) ? $default_data['address'] : '') }}">
								<label for="address">{{ t('Utca, házszám.') }}</label>
							</div>
						</div>
						<div class="col-md-3 select-outline">
							<select name="floor" onchange="loadOverViewBlock()" class="mdb-select md-form md-outline colorful-select dropdown-primary">
								<option value="" disabled>{{ t('Emelet') }}</option>
								<?php
								for ($i = 0; $i <= 20; $i++) {
								?>
								<option value="{{ $i }}" {{ ((isset($default_data['floor']) and $default_data['floor'] == $i) ? 'selected' : '') }}>{{ ($i == '0' ? t('földszint') : $i.'. '.t('emelet')) }}</option>
								<?php
								}
								?>
							</select>
							<label>Emelet</label>
						</div>
						<div class="col-md-12">
							<div class="form-check pl-0">
								<input type="checkbox" id="elevator" name="elevator" value="1" onchange="loadOverViewBlock()" class="form-check-input">
								<label for="elevator" class="form-check-label">Van lift az épületben (emeletre szállítás esetén fontos)</label>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-12">
							<p class="lead text title text-uppercase">{{ t("Számlázási adatok") }}</p>
							<div class="form-check pl-0">
								<input type="checkbox" name="billing_exist" id="billing_exist" {{ (isset($default_data['billing_exist']) ? 'checked' : '') }} checked="" onchange="if($(this).prop('checked')){copyBillingAddress()}" value="1" class="form-check-input">
								<label for="billing_exist" class="form-check-label">{{ t('Számlázási adatok megegyeznek a szállítási adatokkal') }}</label>
							</div>
						</div>
						<div class="col-12">
							<div class="md-form md-outline" >
								<input type="text" name="billing_name" onblur="loadOverViewBlock()" id="billing_name" class="form-control" value="{{ (isset($default_data['billing_name']) ? $default_data['billing_name'] : '') }}" required>
								<label for="billing_name">{{ t('Számlázási név') }} *</label>
							</div>
						</div>
						<div class="col-12">
							<div class="md-form md-outline">
								<input type="text" name="billing_tax_number" class="form-control" onblur="loadOverViewBlock()" value="{{ (isset($default_data['billing_tax_number']) ? $default_data['billing_tax_number'] : '') }}">
								<label for="vat_number">{{ t('Adószám') }}</label>
							</div>
						</div>
						<div class="col-md-3">
							<div class="md-form md-outline">
								<input name="billing_zip" id="billing_zip" onblur="loadOverViewBlock()" placeholder="{{ t('Irsz.') }}" class="form-control" value="{{ (isset($default_data['billing_zip']) ? $default_data['billing_zip'] : '') }}" type="number">
								<label for="billing_zip">Irsz.</label>
							</div>
						</div>
						<div class="col-md-9">
							<div class="md-form md-outline">
								<input name="billing_city" id="billing_city" onblur="loadOverViewBlock()" placeholder="{{ t('Város') }}" class="form-control" value="{{ (isset($default_data['billing_city']) ? $default_data['billing_city'] : '') }}" type="text">
								<label for="billing_city">Város</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="md-form md-outline">
								<input name="billing_address" id="billing_address" onblur="loadOverViewBlock()" placeholder="{{ t('Utca, Hsz.') }}" class="form-control" value="{{ (isset($default_data['billing_address']) ? $default_data['billing_address'] : '') }}" type="text">
								<label for="billing_address">Utca, házszám</label>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12">
					<p class="lead text title text-uppercase mt-3">{{ t("Megjegyzés") }}</p>
					<p>Ha kérése vagy üzenete van a rendeléssel kapcsolatban itt jelezheti kollégáinknak.</p>
					<div class="form-group">
						<textarea id="note" class="form-control description" onblur="loadOverViewBlock()" name="description" rows="7">
							{{ ((isset($default_data['description']) and $default_data['description']) ? $default_data['description'] : '') }}
						</textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			@include('webshop.cart.blocks.personal_right')
		</div>
	</div>
</div>
{{ csrf_field() }}
<input type="hidden" name="country" value="{{$default_data['country']}}">
<input type="hidden" name="coupon" value="{{(isset($default_data['coupon']) ? $default_data['coupon'] : '')}}">
<input type="hidden" name="billing_country" value="{{$default_data['billing_country']}}">
</form>
	<?php
	}//if
	else {
	?>
<div class="container">
	<div class="col-12 h-75 text-center">
		<h1>Hoppá! Nincs termék a kosárban!</h1>
	</div>
</div>
	<?php
	}//else 
}// új design vége
else {
?>
<div class="clearfix"></div>
<div class="breadcrumbs">
    <ul class="items">
        <li class="item home">
            <a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">
                    {{ t('Főoldal') }} </a>
                        </li>
                    <li class="item cms_page">
                        <a href="{{ action('CartController@index') }}">{{ t('Kosár tartalma') }}</a>
                    </li>
                    <li class="item cms_page">
                        <strong>{{ t('Személyes adatok') }}</strong>
                    </li>
            </ul>
</div>

<main class="page-main" id="maincontent">

<div class="page-title-wrapper">
    <h1 class="page-title">
        <span class="base" data-ui-id="page-title-wrapper">{{ t('Személyes adatok') }}</span>    </h1>
</div>

<div class="columns">
<div class="column main">


    @if($content->count())
        <form action="" id="carform">
        <div id="checkout" class="checkout-container">
        @include('webshop.cart.blocks.top', ['step' => 2])
        <div class="opc-wrapper">

        <div class="block block-dashboard-info">
                <div class="col-lg-7 nopaddingleft">
                    <div class="block-content">
                        <div class="box">
                            <strong class="box-title">
                                <span>{{ t('Szállítási mód') }}</span>
                            </strong>
                            <div class="box-content">
                                <p>{{ t('Kérjük válasszon szállítási módot, hogy hol veszi át a rendelését!') }}</p>
								<?php 
								if (!isset($default_data['delivery_mode'])) {
									$modok=getConfig('shipping_mode');
									$default_data['delivery_mode'] = $modok[0];
									$default_data['COURIER_FREE'] = 1;
								}
								//if ((CartHelper::getAdvPrice()>0) and ($default_data['delivery_mode'] == 'MPL')) {
								if ((!$mplenabled) and ($default_data['delivery_mode'] == 'MPL')) {
									$modok=getConfig('shipping_mode');
									$default_data['delivery_mode'] = $modok[0];
									echo "<script>loadOverViewBlock();</script>";
								}
								if (($mplenabled) and ($default_data['delivery_mode'] == 'COURIER')) {
									$modok=getConfig('shipping_mode');
									$default_data['delivery_mode'] = $modok[1];
									echo "<script>loadOverViewBlock();</script>";
								}
								?>
                                @foreach (getConfig('shipping_mode') as $mode)
                                    <div class="col-lg-12 margin-bottom-rem">
										@if (($mode == 'COURIER') or ($mode == 'MPL'))
											<?php /* @if (((CartHelper::getAdvPrice()>0) and ($mode == 'COURIER')) or ((CartHelper::getAdvPrice()==0) and ($mode == 'MPL'))) 
											@if (($mode == 'COURIER') or ((CartHelper::getAdvPrice()==0) and ($mode == 'MPL'))) */ ?>
											@if ((($mode == 'COURIER') and (!$mplenabled))  or (($mplenabled) and ($mode == 'MPL')))
												<label><input type="radio" onclick="{{ ( $mode == 'COURIER' ? '$(\'#extradata\').show();' : '') }}$('#deliveridata').show()" onchange="loadOverViewBlock()" name="delivery_mode" {{ ($default_data['delivery_mode'] == $mode ? 'checked' : '') }} value="{{ $mode }}"> {{ t('shipping_mode_'.$mode) }}</label>
											@endif
										@else
											<label><input type="radio" onclick="$('#extradata').hide();$('#deliveridata').hide()" onchange="loadOverViewBlock()" name="delivery_mode" {{ ($default_data['delivery_mode'] == $mode ? 'checked' : '') }} value="{{ $mode }}"> {{ t('shipping_mode_'.$mode) }}</label>
										@endif
                                    </div>
                                @endforeach
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 nopaddingleft nopaddingright">
                    <div class="block-content">
                        <div class="box">
                            <strong class="box-title">
                                <span>{{ t('Fizetési mód') }}</span>
                            </strong>
                            <div class="box-content">
                                <p>{{ t('Kérjük válasszon fizetési módot, amellyel kiegyenlíti a rendelését!') }}</p>
                                @foreach (getConfig('billing_mode') as $mode)
									<?php if (!isset($default_data['payment'])) $default_data['payment']=$mode; ?>
                                    @if (strstr($mode, 'ADV') and CartHelper::isEnableAdv())
                                    <div class="col-lg-12 margin-bottom-rem">
                                    <label><input type="radio" name="payment" onchange="loadOverViewBlock()" {{ ($default_data['payment'] == $mode ? 'checked' : '') }} value="{{ $mode }}"> {{ t('billing_mode_'.$mode) }}</label>
                                    </div>
                                    @elseif (!strstr($mode, 'ADV'))
									@if ($mode == 'COD')
										@if ((CartHelper::getAdvPrice()==0) and ($mplenabled))
											<div class="col-lg-12 margin-bottom-rem">
											<label><input type="radio" name="payment" onchange="loadOverViewBlock()" {{ ($default_data['payment'] == $mode ? 'checked' : '') }} value="{{ $mode }}"> {{ t('billing_mode_'.$mode) }}</label>
											</div>
										@endif
									@else 
										<div class="col-lg-12 margin-bottom-rem">
										<label><input type="radio" name="payment" onchange="loadOverViewBlock()" {{ ($default_data['payment'] == $mode ? 'checked' : '') }} value="{{ $mode }}"> {{ t('billing_mode_'.$mode) }}</label>
										</div>
									@endif
                                    @endif
                                @endforeach
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
				
				<?php 
					if (($default_data['delivery_mode'] == 'COURIER')) {
						$sdisplay=true;
					} else {
						$sdisplay=false;
					}
				?>
				
                @if (sizeof($extra_prices))
                <div id="extradata" class="block-content" {{ ($sdisplay ? '' : 'style="display: none;"') }}>
                    <div class="box">
                        <strong class="box-title">
                            <span>{{ t('További szolgáltatások') }}</span>
                        </strong>
                        <div class="box-content no-min-height">
                            @foreach ($extra_prices as $extra)
                            <div class="col-lg-12">
                                <div class="form-group required">
                                    <label><input type="checkbox" name="extra_prices[]" onchange="loadOverViewBlock()"  {{ ((isset($default_data['extra_prices']) and in_array($extra->id, $default_data['extra_prices'])) ? 'checked' : '') }} value="{{ $extra->id }}"> {{ $extra->name }} (+{{money($extra->price)}})</label>
                                </div>
                            </div>
                            @endforeach
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                @endif
                <div class="block-content">
                    <div class="box">
                        <strong class="box-title">
                            <span>{{ t('Személyes adatok') }}</span>
                        </strong>
                        <div class="box-content">
                            <div class="col-lg-6">
                                <div class="form-group required">
                                    <label for="" class="control-label"><span>{{ t('Vezetéknév') }}*: </span></label>
                                    <div>
                                        <input name="user_lastname" id="user_lastname" class="input-text form-control input-sm required-input" onblur="copyBillingAddress()" value="{{ (isset($default_data['user_lastname']) ? $default_data['user_lastname'] : '') }}"  type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group required">
                                    <label for="" class="control-label"><span>{{ t('Keresztnév') }}*: </span></label>
                                    <div>
                                        <input name="user_firstname" id="user_firstname" class="input-text form-control input-sm required-input"  onblur="copyBillingAddress()" value="{{ (isset($default_data['user_firstname']) ? $default_data['user_firstname'] : '') }}"  type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="form-group required">
                                    <label for="" class="control-label"><span>{{ t('E-mail cím') }}*: </span></label>
                                    <div>
                                        <input name="email" class="input-text form-control input-sm required-input" onblur="loadOverViewBlock()" value="{{ (isset($default_data['email']) ? $default_data['email'] : '') }}"  type="email">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group required">
                                    <label for="" class="control-label"><span>{{ t('Telefonszám') }}*: </span></label>
                                    <div>
                                        <input name="phone" class="input-text form-control input-sm required-input" onblur="loadOverViewBlock()" value="{{ (isset($default_data['phone']) ? $default_data['phone'] : '') }}"  type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
				<?php 
					if (($default_data['delivery_mode'] == 'COURIER') or ($default_data['delivery_mode'] == 'MPL')) {
						$sdisplay=true;
					} else {
						$sdisplay=false;
					}
				?>
                <div id="deliveridata" class="block-content" {{ ($sdisplay ? '' : 'style="display: none;"') }}>
                    <div class="box">
                        <strong class="box-title">
                            <span>{{ t('Szállítási adatok') }}</span>
                        </strong>
                        <div class="box-content">
                            <div class="col-lg-6">
                                <div class="form-group required">
                                    <label for="" class="control-label"><span>{{ t('Vezetéknév') }}*: </span></label>
                                    <div>
                                        <input name="lastname" id="lastname" class="input-text form-control input-sm" onblur="copyBillingAddress()" value="{{ (isset($default_data['lastname']) ? $default_data['lastname'] : '') }}"  type="text">
                                        <small class="copy-personal">{{ t('Személyes adatok másolása') }}</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group required">
                                    <label for="" class="control-label"><span>{{ t('Keresztnév') }}*: </span></label>
                                    <div>
                                        <input name="firstname" id="firstname" onblur="loadOverViewBlock()" class="input-text form-control input-sm"  onblur="copyBillingAddress()" value="{{ (isset($default_data['firstname']) ? $default_data['firstname'] : '') }}"  type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group addressGroup">
                                    <label for="" class="control-label">{{ t('Cím') }}*:</label>
                                    <div class="input-group">
                                    <div class="col-sm-2 mobil-margin-bottom" >
                                        <input name="zip" id="zip" onblur="searchCity(this.value)" data-target="billing_zip" placeholder="{{ t('Irsz.') }}" class="form-control input-sm billing-copy"  value="{{ (isset($default_data['zip']) ? $default_data['zip'] : '') }}" type="{{Config::get('shop.'.getShopCode().'.ziptype')}}">
                                    </div>
                                    <div class="col-sm-2 mobil-margin-bottom" >
                                        <input name="city" id="city" onblur="loadOverViewBlock()" data-target="billing_city" placeholder="{{ t('Város') }}" class="form-control input-sm billing-copy"  value="{{ (isset($default_data['city']) ? $default_data['city'] : '') }}" type="text">
                                    </div>
                                    <div class="col-sm-6 mobil-margin-bottom" >
                                        <input name="address" id="address" data-target="billing_address" onblur="loadOverViewBlock()" placeholder="{{ t('Utca, Hsz.') }}" class="form-control input-sm billing-copy"  value="{{ (isset($default_data['address']) ? $default_data['address'] : '') }}" type="text">
                                    </div><?php /*
                                    <div class="col-sm-2 mobil-margin-bottom" >
                                        <input name="other_addr" id="other_addr" data-target="billing_other_addr" placeholder="{{ t('Ajtó') }}" class="form-control input-sm billing-copy" value="{{ (isset($default_data['other_addr']) ? $default_data['other_addr'] : '') }}" type="text">
                                    </div>
									*/ ?>
                                    <div class="col-sm-2 mobil-margin-bottom" >
                                        <select name="floor" onchange="loadOverViewBlock()">
                                            <option value="">{{ t('Emelet') }}</option>
                                            @for ($i = 0; $i < 20; $i++)

                                            <option value="{{ $i }}" {{ ((isset($default_data['floor']) and $default_data['floor'] == $i) ? 'selected' : '') }}>{{ ($i == '0' ? t('Fsz') : $i.' '.t('Emelet')) }}</option>
                                            @endfor
                                        </select>
										<p align="center"><label><input type="checkbox" name="elevator" value="1" onchange="loadOverViewBlock()"> {{ t('Lift van') }}</label></p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group required">
								<?php /* <p><label><input type="checkbox" name="billing_exist" id="billing_exist" {{ (isset($default_data['billing_exist']) ? 'checked' : '') }} checked="" onchange="if($(this).prop('checked')){$('#billing_data').hide()}else{$('#billing_data').show()}" value="1"> {{ t('Számlázási adatok megegyeznek a szállítási adatokkal') }}</label></p> */ ?>
								<p><label><input type="checkbox" name="billing_exist" id="billing_exist" {{ (isset($default_data['billing_exist']) ? 'checked' : '') }} checked="" onchange="if($(this).prop('checked')){copyBillingAddress()}" value="1"> {{ t('Számlázási adatok megegyeznek a szállítási adatokkal') }}</label></p>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="block-content" id="billing_data" style="">
                    <div class="box">
                        <strong class="box-title">
                            <span>{{ t('Számlázási adatok') }}</span>
                        </strong>
                        <div class="box-content">
                            <div class="col-lg-12">
                                <div class="form-group required">
                                    <label for="billing_name" class="control-label"><span>{{ t('Számlázási név') }}: </span></label>
                                    <div>
                                        <input name="billing_name" onblur="loadOverViewBlock()" id="billing_name" class="input-text form-control input-sm" value="{{ (isset($default_data['billing_name']) ? $default_data['billing_name'] : '') }}"  type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group required">
                                    <label for="vat_number" class="control-label"><span>{{ t('Adószám') }}: </span></label>
                                    <div>
                                        <input name="billing_tax_number" class="input-text form-control input-sm" onblur="loadOverViewBlock()" value="{{ (isset($default_data['billing_tax_number']) ? $default_data['billing_tax_number'] : '') }}"  type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group addressGroup">
                                    <label for="" class="control-label">{{ t('Cím') }}:</label>
                                    <div class="input-group">
                                    <div class="col-sm-2 mobil-margin-bottom" >
                                        <input name="billing_zip" id="billing_zip" onblur="loadOverViewBlock()" placeholder="{{ t('Irsz.') }}" class="form-control input-sm"  value="{{ (isset($default_data['billing_zip']) ? $default_data['billing_zip'] : '') }}" type="{{Config::get('shop.'.getShopCode().'.ziptype')}}">
                                    </div>
                                    <div class="col-sm-2 mobil-margin-bottom" >
                                        <input name="billing_city" id="billing_city" onblur="loadOverViewBlock()" placeholder="{{ t('Város') }}" class="form-control input-sm"  value="{{ (isset($default_data['billing_city']) ? $default_data['billing_city'] : '') }}" type="text">
                                    </div>
                                    <div class="col-sm-6 mobil-margin-bottom" >
                                        <input name="billing_address" id="billing_address" onblur="loadOverViewBlock()" placeholder="{{ t('Utca, Hsz.') }}" class="form-control input-sm"  value="{{ (isset($default_data['billing_address']) ? $default_data['billing_address'] : '') }}" type="text">
                                    </div><?php /*
                                    <div class="col-sm-2 mobil-margin-bottom" >
                                        <input name="billing_other_addr" id="billing_other_addr" placeholder="{{ t('Em./Ajtó') }}" class="form-control input-sm" value="{{ (isset($default_data['billing_other_addr']) ? $default_data['billing_other_addr'] : '') }}" type="text">
                                    </div>
									*/ ?>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                </div>

                    <div class="block-content">
                        <div class="box">
                            <strong class="box-title">
                                <span>{{ t('Megjegyzés') }}</span>
                            </strong>
                            <div class="box-content">
                                <textarea class="form-control description" onblur="loadOverViewBlock()" name="description">{{ ((isset($default_data['description']) and $default_data['description']) ? $default_data['description'] : '') }}</textarea>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

        </div>





        </div>
        <aside class="modal-custom opc-sidebar opc-summary-wrapper custom-slide" id="cart-right-sidebar" data-role="modal" data-type="custom" tabindex="0">

        @include('webshop.cart.blocks.personal_right')
        </aside>

        {{ csrf_field() }}


        </div>
        <input type="hidden" name="country" value="{{$default_data['country']}}">
        <input type="hidden" name="coupon" value="{{(isset($default_data['coupon']) ? $default_data['coupon'] : '')}}">
        <input type="hidden" name="billing_country" value="{{$default_data['billing_country']}}">
        </form>
    @else
        <div class="cart-empty">
            <p>{{ t('Az Ön kosara üres!') }}</p>
            <p>{{ t('A vásárlás folytatásához rakjon a kosarába terméket') }}</p>
        </div>
    @endif
</div></div>
</main>

@stop
@section('footer_js')
    <script>loadOverViewBlock();</script>
<?php
}//else
?>
@append

