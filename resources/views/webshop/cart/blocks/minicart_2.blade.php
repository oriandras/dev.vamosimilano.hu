<?php $content = Cart::getContent(); ?>
@if(Cart::countItems())
<p class="lead px-2 text-center"><i class="far fa-2x fa-shopping-bag" aria-hidden="true"></i><br>{{ t('Kosár') }}</p>
<ul class="list-unstyled px-2">
@foreach($content as $item)
<?php
	if ($item->id == 'floor'){
		continue;
	}
	$product = Product::lang()->active()->find($item['attributes']->product_id);
	if (!isset($product)) {
		continue;
	}
	if ($product->category_id == 119) {
		continue;
	}
?>
	<li class="border-bottom border-light white-text mt-1">
		<p class="mb-1">{{ $product->getName() }}
		<br><small>{{$item->quantity." ".t('db')}} <span class="float-right">{{money($item->price)}}</span></small></p>
	</li>
@endforeach
	<li class="border-bottom border-light white-text mt-1">
		<p class="mb-1">{{ t('Összesen') }} ({{ Cart::countItems() }} termék): <strong class="float-right">{{money(Cart::getCartTotal())}}</strong></p>
	</li>
</ul>
<p class="px-2"><button class="btn btn-primary btn-block btn-sm" onClick="javascript:location.href='{{ url('/kosar') }}'">{{ t('Tovább a kosárhoz') }}</button></p>
@else
<p class="mt-3 text-center"><i class="icon-sadcart"></i></p>
<p class="text-center">{{ t('Még nincs termék a kosárban.') }}</p>
@endif