    <div data-role="focusable-start" tabindex="0"></div>
    <div class="modal-inner-wrap" data-role="focusable-scope">
        <header class="modal-header">

            <button class="action-close" data-role="closeBtn" type="button">
                <span>{{ t('Bezár') }}</span>
            </button>
        </header>
        <div class="modal-content" data-role="content"><div id="opc-sidebar" >

<div class="opc-block-summary" data-bind="blockLoader: isLoading">
    <span class="title">{{ t('Kalkuláció') }}</span>




<div id="cart-totals" class="cart-totals border-top0">

<div class="table-wrapper" >
    <table class="data table totals">
        <caption class="table-caption">{{ t('Kalkuláció') }}</caption>
        <tbody>

<?

$shippingcost = Cart::getShipping();

//dubug
/*
if (Session::has('cart_data')) {
	$cart_data = Session::get('cart_data');
	$params = [
		'city' => (isset($cart_data['city']) ? $cart_data['city'] : ''),
		'address' => (isset($cart_data['address']) ? $cart_data['address'] : ''),
		'zip' => (isset($cart_data['zip']) ? $cart_data['zip'] : ''),
		'delivery_mode' => (isset($cart_data['delivery_mode']) ? $cart_data['delivery_mode'] : ''),
	];
	$price_array = Shipping::getShippingCostMiles($params);
}
*/
?>

<tr class="grand totals">
    <th class="mark" scope="row">
        <strong >{{ t('Szállítási költség') }}</strong>
    </th>
    <td class="amount" >
		@if ($shippingcost>0)
			<strong><span class="price">{{ money($shippingcost) }}</span></strong>
		@else
			<strong><span class="price">{{ $shippingcost }}</span></strong>
		@endif
		<? //debug
		/*
		<strong><span class="price"><? var_export($params) ?></span></strong>
		<strong><span class="price"><? var_export($price_array) ?></span></strong>
		*/
		?>
    </td>
</tr>
@if(isset($default_data['floor']) and Cart::get('floor'))
<tr class="totals-tax">
    <th  class="mark" colspan="1" scope="row">{{ t('Emelet költség') }}</th>
    <td  class="amount">
        <span class="price">+{{ money(Cart::get('floor')->getPriceSum()) }}</span>
    </td>
</tr>

@endif

        </tbody>
    </table>
</div>

<ul class="checkout methods items checkout-methods-items">
        <li class="item">
        <a href="javascript:loadOverViewBlock();" class="action primary checkout text-center" style="display: block;">
        <span>{{ t('Újra számolás') }}</span>
    </a>
</li>
</ul>

</div>


</div>
<!-- /ko -->
    <!--/ko-->


</div></div>




    </div>
    <div data-role="focusable-end" tabindex="0"></div>






