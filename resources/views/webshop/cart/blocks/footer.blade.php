                        <div class="col-sm-10 col-xs-12 cart-price fix-height-footer">
                            {{ t('Részösszeg') }}:
                        </div>
                        <div class="col-sm-2 col-xs-12 cart-price fix-height-footer-left">
                            <span>
                                <span class="price">{{ money($item->getPriceSum()) }}</span>
                            </span>
                        </div>
                        <div class="col-sm-10 col-xs-12 cart-price fix-height-footer">
                            {{ t('Szállítási költség') }}:
                        </div>
                        <div class="col-sm-2 col-xs-12 cart-price fix-height-footer-left">
                            <span>
                                <span class="price">{{ money(0) }}</span>
                            </span>
                        </div>
                        <div class="col-sm-10 col-xs-12 cart-price fix-height-footer">
                            {{ t('Fizetendő') }}:
                        </div>
                        <div class="col-sm-2 col-xs-12 cart-price fix-height-footer-left">
                            <span class="newprice">
                                <span class="price">{{money(Cart::getCartTotal())}}</span>
                            </span>
                        </div>

