@extends('layouts.default')
@section('content')
<?php
$headversion="newhead";
if(isset($headversion) && $headversion==="newhead"){
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
		<?php
		if( ismobile() ) {
		?>
			<h1 class="h1-responsive text-uppercase title-center my-4">{{ t('Személyes adatok') }</h1>
		<?php
		}
		else {
		?>
			<ul class="stepper stepper-horizontal" id="cart-stepper">
				<li class="">
					<a href="javascript:void(0)">
						<span class="circle"><i class="fal fa-check"></i></span>
						<span class="label text-uppercase">Kosár tartalma</span>
					</a>
				</li>
				<li class="">
					<a href="javascript:void(0)">
						<span class="circle"><i class="fal fa-check"></i></span>
						<span class="label text-uppercase">Személyes adatok</span>
					</a>
				</li>
				<li class="active">
					<a href="javascript:void(0)">
						<span class="circle danger-color-dark" style="background-color: #c00!important;"><i class="fas fa-exclamation mt-1"></i></span>
						<span class="label text-uppercase">Fizetés és befejezés</span>
					</a>
				</li>
			</ul>
		<?php
		}
		?>
		</div>
		<div class="col-12 text-center">
			<h1 class="h1-responsive text-uppercase title-center mb-4">{{ t('Hiba történt a rendelés közben!') }}</h1>
			@if (empty($order))
			<p class="lead">Nincs feldolgozható adat!</p>
			@else
			<p class="lead">{{ t('Kedves')." ".$order->firstname }}!</p>
			<p>{{ t('Hiba történt a rendelés feldolgozása közben!') }}</p>
			<p>{{ t('Rendelés azonosítója') }}: {{ $order->order_number }}</p>
				@if (sizeof($payment))
				<div class="alert alert-danger" role="alert">
				<p><b>{{ t('Tranzakciója sikertelen volt!') }}</b></p>

				<p>{{ t('Tranzakció azonosítója') }}: {{ $payment['trans_id'] }}</p>

				<p>{{ t('Rendelés státusza').": <b>" .$order->getStatus() }}</b></p>

				<p><b>{{ t('Tranzakció részleti') }}</b>: {{ UniCredit::getResultString($payment['result']) }}</p>

				<p><b>{{ t('Összeg') }}</b>: {{ money($payment['price']) }}</p>
				</div>
				@endif
				@if ($order->status == 'NEW')
					@if ($order->payment == 'FULL_CARD')
					<button class="btn btn-primary btn-lg mb-5" onclick="window.location.href='{{action('UnicreditController@fullStart', $order->order_number)}}'" title="Elküldés" type="submit">
						<span>{{ t('Fizetés újrapróbálása') }}</span>
					</button>
					@endif
				@endif
			@endif
		</div>
	</div>
	<div class="container">
		<div class="row">
		</div>
	</div>
</div>
<?php
}
else {
?>
<div class="clearfix"></div>
<div class="breadcrumbs">
    <ul class="items">
        <li class="item home">
            <a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">
                    {{ t('Főoldal') }} </a>
                        </li>
                    <li class="item cms_page">
                            <strong>{{ t('Hiba történt') }}</strong>
                        </li>
            </ul>
</div>

<main class="page-main" id="maincontent">

<div class="entry-content min-height">


    <div class="row">
        <div class="row-wrapper container">
            <div class="row">
                <div class="col-md-12">
                    @if (empty($order))
                        <p>{{ t('Hiba történt a rendelés közben!') }}</p>
                    @else
                    <h3>{{ t('Kedves')." ".$order->firstname }}!</h3>
        			<p>{{ t('Hiba történt a rendelés közben!') }}</p>
                    <p>{{ t('Rendelés azonosítója') }}: {{ $order->order_number }}</p>
                    @if (sizeof($payment))
                        <div class="alert alert-danger" role="alert">
                        <p><b>{{ t('Tranzakciója sikertelen volt!') }}</b></p>

                        <p>{{ t('Tranzakció azonosítója') }}: {{ $payment['trans_id'] }}</p>

                        <p>{{ t('Rendelés státusza').": <b>" .$order->getStatus() }}</b></p>

                        <p><b>{{ t('Tranzakció részleti') }}</b>: {{ UniCredit::getResultString($payment['result']) }}</p>

                        <p><b>{{ t('Összeg') }}</b>: {{ money($payment['price']) }}</p>
                        </div>
                    @endif
                    @if ($order->status == 'NEW')
                        @if ($order->payment == 'FULL_CARD')
                        <button class="action submit primary margin-top2" onclick="window.location.href='{{action('UnicreditController@fullStart', $order->order_number)}}'" title="Elküldés" type="submit">
                    <span>{{ t('Fizetés újrapróbálása') }}</span>
                </button>

                        @endif

                    @endif
                    @endif


                </div>
            </div>
        </div>
    </div>



</div>
</div></div>
</main>
<?php
}
?>
@stop
@section('footer_js')
@append