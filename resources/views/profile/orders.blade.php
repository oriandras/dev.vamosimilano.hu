<?php
$headversion = "newhead";
?>
@extends('layouts.default')
@section('content')
<?php
if( isset($headversion) && $headversion === "newhead" ) {
?>
<div class="container">
	<div class="row my-5">
		<div class="col-lg-3 col-md-4">
			@include('profile.sidebar', ['aktiv'=>'orders'])
		</div>
		<div class="col-lg-9 col-md-8">
			@if (!sizeof($orders))
				<p class="text-center">{{ t('Nincs feladott rendelése') }}</p>
			@else
				<?php
				$counter = 0; 
				foreach ($orders as $i=>$order) {
					if($counter==0) {
						/*
						Ha nem az első elem, akkor össze vannak csukva a kártyák
						*/
						$open = "show";
						$aria = "true";
						$collapse = "";
					}
					else {
						/*
						Ha nincs kifizetve a kártyás rendelés nyitva marad mindenképpen a rendelési kártya
						*/
						if ( $order->status == 'NEW' && $order->payment == 'FULL_CARD' ) {
							$open = "show";
							$aria = "true";
							$collapse = "";
						}
						else {
							$open = "";
							$aria = "false";
							$collapse = "collapsed";
						}
					}
				?>
			<div class="card mb-4">
				<div class="accordion md-accordion" id="orderlist" role="tablist" aria-multiselectable="false">
					<div class="card">
						<div class="card-header elegant-color rounded-0" role="tab" id="headingOne1">
							<a data-toggle="collapse" data-parent="#orderlist" href="#order-{{$order->order_number}}" aria-expanded="{{$aria}}" aria-controls="collapseOne1" class="{{$collapse}}">
								<p class="font-weight-bold text-white mb-0"><span class="badge badge-primary font-weight-normal pt-1 text-lowercase">{{ $order->getStatus() }}</span> {{$order->order_number}} rendelés: <span class="font-weight-normal">{{$order->created_at}}</span> <i class="fas fa-angle-down rotate-icon"></i></p>
							</a>
						</div>
						<div id="order-{{$order->order_number}}" class="collapse {{$open}}" role="tabpanel" aria-labelledby="headingOne1" data-parent="#accordionEx">
							<div class="card-body p-0">
								<div class="row elegant-color text-white mx-0">
									<div class="col-12">
										<?php
										if( $order->status == 'NEW' ){
											if( $order->payment == 'FULL_CARD' ){
										?>
										<div class="alert alert-primary" role="alert">
											<strong><i class="fal fa-cog fa-spin mr-2"></i><a onclick="window.location.href='{{action('UnicreditController@fullStart', $order->order_number)}}'">{{ t('Sikertelen fizetés') }}: {{ t('Ide kattintva újra megpróbálhat fizetni') }}</a></strong>
										</div>
										<?php
											}//if
										}//if
										?>
										<ul>
											<li>{{ t('Rendelés státusza') }}: {{ $order->getStatus() }}</li>
											<li>{{ t('Szállítási mód') }}: {{ t('shipping_mode_'.$order->delivery_mode) }}</li>
											<li>{{ t('Fizetési mód') }}: {{ t('billing_mode_'.$order->payment) }}</li>
										</ul>
									</div>
								</div>
								<div class="row mx-0 mt-3">
									<?php
									if (ismobile()) {
									?>
									<div class="col-sm-6">
										<table class="table table-sm table-striped table-bordered">
											<thead>
												<tr>
													<th colspan="2">{{ t('Szállítási adatok') }}</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<th>{{ t('Név') }}</th>
													<td>{{$order->lastname }} {{ $order->firstname}}</td>
												</tr>
												<tr>
													<th>{{ t('Telefonszám') }}</th>
													<td>{{ $order->phone }}</td>
												</tr>
												<tr>
													<th>{{ t('Cím') }}</th>
													<td>{{ $order->zip." ".$order->city }}, {{$order->address." ".$order->other_addr.($order->floor ? " ".t('Emelet').": ".$order->floor : '') }}</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="col-sm-6">
										<table class="table table-sm table-striped table-bordered" width="100%">
											<thead>
												<tr>
													<th colspan="2">{{ t('Számlázási adatok') }}</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<th>{{ t('Név') }}</th>
													<td>{{$order->billing_name}}</td>
												</tr>
												<tr>
													<th>{{ t('Telefonszám') }}</th>
													<td>{{ $order->phone }}</td>
												</tr>
												<tr>
													<th>{{ t('Cím') }}</th>
													<td>{{$order->billing_zip}} {{$order->billing_city}}, {{$order->billing_address}} {{$order->billing_other_addr}}</td>
												</tr>
												@if($order->billing_tax_number)
												<tr>
													<th>{{ t('Adószám') }}</th>
													<td>{{$order->billing_tax_number}}</td>
												</tr>
												@endif
											</tbody>
										</table>
									</div>
									<?php
									}//if mobil
									else {
									?>
									<div class="col-md-12">
										<table class="table table-striped table-bordered">
											<thead>
												<tr>
													<th width="20%">Adatok</th>
													<th width="40%">Szállítás</th>
													<th width="40%">Számlázás</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<th>{{ t('Név') }}</th>
													<td>{{ $order->lastname }} {{ $order->firstname }}</td>
													<td>{{ $order->billing_name }}</td>
												</tr>
												<tr>
													<th>{{ t('Cím') }}</th>
													<td>{{ $order->zip }} {{ $order->city }}, {{$order->address}} {{$order->other_addr}} {{ ($order->floor ? " ".t('Emelet').": ".$order->floor : '') }}</td>
													<td>{{$order->billing_zip}} {{$order->billing_city}}, {{$order->billing_address}} {{$order->billing_other_addr}}</td>
												</tr>
												<tr>
													<th>{{ t('Telefonszám') }}</th>
													<td>{{ $order->phone }}</td>
													<td></td>
												</tr>
												@if($order->billing_tax_number)
												<tr>
													<th>{{ t('Adószám') }}</th>
													<td> - </td>
													<td>{{$order->billing_tax_number}}</td>
												</tr>
												@endif
											</tbody>
										</table>
									</div>
									<?php
									}
									?>
								</div>
								<div class="row mx-0 mt-3">
									<?php
									if (ismobile()) {
									?>
									<div class="col-md-12">
										<div class="row">
											<div class="col-12">
												<p class="lead text-center">{{ t("Tételek") }}:</p>
											</div>
											<?php
											$citemcount = 1;
											foreach ($order->items() as $item) {
												$price_array = CartHelper::calculateItemPrice(unserialize($item->options),true);
												$optionstext = CartHelper::writeDataByOptions(unserialize($item->options));
											?>
											<div class="col-12">
												<p class="font-weight-bold mb-0">
													{{$citemcount}}.: {{ $item->item_name }} {{($item->qty>1 ? t('(').$item->qty.t(' db)') : '')}} 
													@if( $optionstext != "" || $price_array['textile_price'] )
													<a data-toggle="collapse" href="#detail{{$citemcount}}" aria-expanded="false" aria-controls="detail{{$citemcount}}" class="font-weight-normal">[részletek]</a>
													@endif
												</p>
												<div class="collapse" id="detail{{$citemcount}}">
													@if ($optionstext != "")
													<p class="mb-0">{{ CartHelper::writeDataByOptions(unserialize($item->options)) }}</p>
													@endif
													@if ($price_array['textile_price'])
													<p class="mb-0"><strong>{{ t('Bútor alapár') }}:</strong><span class="float-right">{{ money($price_array['furniture_price']) }}</span></p>
													<p class="mb-0"><strong>{{ t('Szövet felár') }}:</strong><span class="float-right">{{ money($price_array['textile_price']) }}</span></p>
													@endif
												</div>
												<p>{{ t('Vételár') }}:<span class="float-right font-weight-bold">{{ money($item->item_payable_price*$item->qty) }}</span></p>
											</div>
											<?php
												$citemcount++;
											}
											?>
											<div class="col-12">
												<p class="mb-0">{{ t('Részösszeg') }}: <span class="float-right font-weight-bold">{{ money($order->total_price) }}</span></p>
												<p class="mb-0">{{ t('Szállítási költség') }}: <span class="float-right font-weight-bold">{{ money($order->delivery_price) }}</span></p>
												@if($order->cost_price > 0)
												<p class="mb-0">{{ t('Emelet költség') }}: <span class="float-right font-weight-bold">{{ money($order->cost_price) }}</span></p>
												@endif
												@if($order->discount_price > 0)
												<p class="mb-0">{{ t('Kedvezmény') }}: <span class="float-right font-weight-bold">{{ ($order->discount_price ? '-'.money($order->discount_price) :'') }}</span></p>
												@endif
												@if($order->advance_price > 0)
												<p class="mb-0">{{ t('Előleg') }}: <span class="float-right font-weight-bold">{{ money($order->advance_price) }}</span></p>
												@endif
												<p>{{ t('Fizetendő') }}: <span class="float-right font-weight-bold">{{ money($order->payable_price) }}</span></p>
											</div>
										</div>
									</div>
									<?php
									}//if mobil
									else {
									?>
									<div class="col-md-12">
										<table class="table table-striped table-bordered">
											<thead>
												<tr>
													<th class="text-center">{{ t("Tétel") }}</th>
													<th class="text-center">{{ t("db") }}</th>
													<th class="text-center">{{ t("Darab ár") }}</th>
													<th class="text-center">{{ t("Ár") }}</th>
												</tr>
											</thead>
											<tbody>
											@foreach ($order->items() as $item)
											<?php
												$price_array = CartHelper::calculateItemPrice(unserialize($item->options),true);
											?>
											<tr>
												<th>{{ $item->item_name }}
													@if ($item->options)
														<br>{{ CartHelper::writeDataByOptions(unserialize($item->options)) }}
													@endif
													@if ($price_array['textile_price'])
														{{ t('Bútor alapár').": ".money($price_array['furniture_price']) }} + {{ t('Szövet felár').": ".money($price_array['textile_price']) }}
													@endif
												</th>
												<td class="text-center">{{ $item->qty." ".t('db') }}</td>
												<td class="text-right">{{ money($item->item_payable_price) }}</td>
												<td class="text-right">{{ money($item->item_payable_price*$item->qty) }}</td>
											</tr>
											@endforeach
												<tr>
													<td>{{ t('Részösszeg') }}</td>
													<td colspan="3" class="text-right">{{ money($order->total_price) }}</td>
												</tr>
												<tr>
													<td>{{ t('Szállítási költség') }}</td>
													<td colspan="3" class="text-right">{{ money($order->delivery_price) }}</td>
												</tr>
												@if($order->cost_price > 0)
												<tr>
													<td>{{ t('Emelet költség') }}</td>
													<td colspan="3" class="text-right">{{ money($order->cost_price) }}</td>
												</tr>
												@endif
												@if($order->discount_price > 0)
												<tr>
													<td>{{ t('Kedvezmény') }}</td>
													<td colspan="3" class="text-right">{{ ($order->discount_price ? '-'.money($order->discount_price) :'') }}</td>
												</tr>
												@endif
												@if($order->advance_price > 0)
												<tr>
													<td>{{ t('Előleg') }}</td>
													<td colspan="3" class="text-right">{{ money($order->advance_price) }}</td>
												</tr>
												@endif
												<tr>
													<td>{{ t('Fizetendő') }}</td>
													<td colspan="3" class="text-right">{{ money($order->payable_price) }}</td>
												</tr>
											</tbody>
										</table>
									</div>
									<?php
									}// else nem mobil
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
					$counter++;
				}//foreach
			?>
			@endif
		</div>
	</div>
</div>
<?php
}
else {
?>
<div class="clearfix"></div>
<div class="breadcrumbs">
    <ul class="items">
        <li class="item home">
            <a title="{{ ('Irány a főoldal') }}" href="{{ url('/') }}">
                    {{ t('Főoldal') }} </a>
                        </li>
                    <li class="item cms_page">
                            <strong>{{ t('Rendelések listája') }}</strong>
                        </li>
            </ul>
</div>

<main class="page-main" id="maincontent">
        <div class="columns">
    <div class="column main">

    <form class="form form-edit-account"  onsubmit="return false" method="post" id="form-validate" enctype="multipart/form-data">

    <fieldset class="fieldset info" style="width: 100%; padding: 0px;margin:0px;">
        <legend class="legend"><span>{{ t('Rendelések listája') }}</span></legend><br>

        @if (!sizeof($orders))
            <p>{{ t('Nincs feladott rendelése') }}</p>

        @else

        <p>{{ t('Kattintson a rendelés azonosítójára a részletekért') }}</p>
        @foreach ($orders as $i=>$order)
         <div class="panel-group">
          <div class="panel panel-default">

            <div class="panel-heading">
              <h4 class="panel-title" data-toggle="collapse" href="#collapse_{{$i}}">
                {{$order->order_number}} ({{ $order->getStatus() }}) <small class="pull-right">{{$order->created_at}}</small>
              </h4>
            </div>
            <div id="collapse_{{$i}}" class="panel-collapse collapse">
              <div class="panel-body">
                <div class="block block-dashboard-info">
                <div class="block-content">
                    <div class="box" style="width: 100%;">
                        <strong class="box-title">
                            <span>{{ t('Státusz') }}</span>
                        </strong>
                        <div class="box-content">
                            <p>{{ t('Rendelés státusza') }}: {{ $order->getStatus() }}</p>
                            <p>{{ t('Szállítási mód') }}: {{ t('shipping_mode_'.$order->delivery_mode) }}</p>
                            <p>{{ t('Fizetési mód') }}: {{ t('billing_mode_'.$order->payment) }}</p>
                            @if ($order->status == 'NEW')
                                    @if ($order->payment == 'FULL_CARD')
                                    <button class="action submit primary margin-top2" onclick="window.location.href='{{action('UnicreditController@fullStart', $order->order_number)}}'" title="Elküldés" type="submit">
                                <span>{{ t('Fizetés újrapróbálása') }}</span>
                            </button>

                            @endif
                            @endif
                        </div>
                    </div>
                </div>
                <br>
                <div class="block-content">

                    <div class="box">
                        <strong class="box-title">
                            <span>{{ t('Szállítási adatok') }}</span>
                        </strong>
                        <div class="box-content">
                            <p>{{ t('Név, telefonszám') }}: <b>{{ $order->lastname.' '.$order->firstname.", ".$order->phone }}</b></p>

    <p>{{ $order->zip.", ".$order->city."<br>".$order->address."<br>".$order->other_addr.($order->floor ? "<br>".t('Emelet').": ".$order->floor : '') }}</p>
    @if($order->message)
        <p><strong>{{ t('Megjegyzés') }}:</strong> {{ $order->message }}<br /></p>
    @endif
                        </div>
                    </div>
                    <div class="box">
                        <strong class="box-title">
                            <span>{{ t('Számlázási adatok') }}</span>
                        </strong>
                        <div class="box-content">
                            <p>{{ $order->billing_name.", ".$order->billing_zip.", ".$order->billing_city."<br> ".$order->billing_address."<br> ".$order->billing_other_addr }}</p>
                            @if($order->billing_tax_number)
                            {{ t('Adószám') }}: {{$order->billing_tax_number}}
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="box" style="width: 100%; margin-top:10px;">
                        <strong class="box-title">
                            <span>{{ t('Termékek') }}</span>
                        </strong>
                        <div class="box-content">
                        <table style="width: 100%" cellpadding="0" cellspacing="0">

                                @foreach ($order->items() as $item)
                                <?php
                                    $price_array = CartHelper::calculateItemPrice(unserialize($item->options),true);
                                ?>
                                <tr>
                                    <td class="padding5" style="border-bottom: 1px solid #b74173;"><b>{{ $item->item_name }}</b>
                                    @if ($item->options)
                                        <br>{{ CartHelper::writeDataByOptions(unserialize($item->options)) }}
                                    @endif

                                    @if ($price_array['textile_price'])
                                        {{ t('Bútor alapár').": ".money($price_array['furniture_price']) }} + {{ t('Szövet felár').": ".money($price_array['textile_price']) }}
                                    @endif
                                    </td>
                                    <td class="padding5" style=" border-bottom: 1px solid #b74173;">{{ $item->qty." ".t('db') }}</td>
                                    <td align="right" class="padding5" style="border-bottom: 1px solid #b74173;">{{ money($item->item_payable_price) }}</td>
                                    <td align="right" class="padding5" style="border-bottom: 1px solid #b74173;">{{ money($item->item_payable_price*$item->qty) }}</td>
                                </tr>

                                @endforeach

                                <tr>
                                    <td colspan="3" align="right" class="padding5">{{ t('Részösszeg') }}</td>
                                    <td  class="padding5" align="right">{{ money($order->total_price) }}</td>

                                </tr>
                                <tr>
                                    <td colspan="3" align="right" class="padding5">{{ t('Szállítási költség') }}</td>
                                    <td  class="padding5" align="right">{{ money($order->delivery_price) }}</td>

                                </tr>
                                <tr>
                                    <td colspan="3" align="right" class="padding5">{{ t('Emelet költség') }}</td>
                                    <td  class="padding5" align="right">{{ money($order->cost_price) }}</td>

                                </tr>
                                <tr>
                                    <td colspan="3" align="right" class="padding5">{{ t('Kedvezmény') }}</td>
                                    <td  class="padding5" align="right">{{ ($order->discount_price ? '-'.money($order->discount_price) :'') }}</td>

                                </tr>
                                <tr>
                                    <td colspan="3" align="right" class="padding5">{{ t('Előleg') }}</td>
                                    <td  class="padding5" align="right">{{ money($order->advance_price) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="right" class="padding5">{{ t('Fizetendő') }}</td>
                                    <td  class="padding5" align="right">{{ money($order->payable_price) }}</td>
                                </tr>
                                </table>

                        </div>
                    </div>

                </div>
                </div>

              </div>

            </div>
          </div>
        </div>
        @endforeach
        @endif





    </fieldset>
    <div class="clearfix"></div>
    </form>
    <div class="clearfix"></div>




</div>

    @include('profile.sidebar')

    </div>
</main>
<?php
}
?>
@stop