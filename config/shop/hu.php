<?php

return array(
    'shop_currency' => 'Ft',
	'currency_code' => 'HUF',
     'billing_mode' => [
            'FULL_CARD',
            'ADV_CARD',
            'FULL_TRANSFER',
            'ADV_TRANSFER',
			'COD'
    ],
    'shipping_mode' => [
		//'PERSONAL_BP',
        //'PERSONAL_DEB',
        //'PERSONAL_MISK',
		//'PERSONAL_SZ',
		//'PERSONAL_GYOR',
		'COURIER',
		'MPL',
        //'OTHER',
    ],
    'advance_enable_limit' => 10000000, //Előleg bekapcsolásának limitje - Nem használjuk.
    'advance_x' => 0.30, //Az előleg osztója
    
    'maps_api_key' => 'AIzaSyD-2e09J6RRvWiyp_p0PR_zpiQuzM0mWqk',
    'default_city' => 'Budapest',
    //'central_address' => 'Budapest, XXI. kerület, Déli utca 8, 1211',
	'central_address' => '47.446461, 19.073293',
    'default_city_zip_start' => 1, //Budapest irányítószám
    'default_city_zip_price' => 14990, //Ha 1-es az irányítószám, akkor Budapesti
    'default_city_zone' => 67000, //Ennyi méterig a budapest határa...
    'default_city_zone_price' => 19990,
    //'other_city_km' => 110, //A zónán kívül ennyi az ár KM-enként.
	'other_city_km' => 125, //A zónán kívül ennyi az ár KM-enként.
    'other_city_price' => 0, //16900, //A többi telephelyen ennyi az átvétel díja.
	'floor_cost' => 1000,
    
    'textile_extra_once_price' => 5000, //bútoron 2 ilyen szövet van, mert a harmadik 1-es kategóriás szövetnél van egy fix, egyszeri 5000 ft-os felár.

    /*
    'unicredit_ecomm_server_url'     => 'https://secureshop-test.firstdata.lv:8443/ecomm/MerchantHandler',
    'unicredit_ecomm_client_url'     => 'https://secureshop-test.firstdata.lv/ecomm/ClientHandler',
	'unicredit_cert_url'             => '/var/www/clients/client1/web1/web/storage/app/public/test/IF000271keystore.pem', //full path to keystore file
	'unicredit_cert_pass'            => '84ZKBJb*e', //keystore password teszt
	*/
	
	/*
	'unicredit_ecomm_server_url'     => 'https://secureshop-test.firstdata.lv:8443/ecomm/MerchantHandler',
    'unicredit_ecomm_client_url'     => 'https://secureshop-test.firstdata.lv/ecomm/ClientHandler',
	'unicredit_cert_url'             => '/var/www/clients/client1/web1/web/storage/app/public/test/IF000536keystore.pem', //full path to keystore file
	'unicredit_cert_pass'            => '84ZKBJb*e', //keystore password teszt
	*/
	
	
	
	'unicredit_ecomm_server_url'     => 'https://secureshop.firstdata.lv:8443/ecomm/MerchantHandler',
    'unicredit_ecomm_client_url'     => 'https://secureshop.firstdata.lv/ecomm/ClientHandler',
    'unicredit_cert_url'             => '/var/www/clients/client1/web1/web/storage/app/public/IF000536keystore.pem', //full path to keystore file
    'unicredit_cert_pass'            => 'n4k3Rh26rt', //keystore password éles
	
	
    'unicredit_currency'             => '348', //978=EUR 840=USD 941=RSD 703=SKK 440=LTL 233=EEK 643=RUB 891=YUM
    //'unicredit_ip'                   => '74.124.217.20',
	'unicredit_ip'                   => '89.107.253.82',

	'manufacturecategory' => [112,101,4,9,12,24,104,106,107,127,110,130,122,129],
	'outletcategory' => [131,139,140,132,135,137,133,141,155,142,134],
	
	'maincategory' => [101,4,9,12,24],
	'additionalcategory' => [104,106,107,110,122,153],
);
