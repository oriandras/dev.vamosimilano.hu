<?php

return array(
    'system_email' => 'noreply@vamosimilano.hu',
    
    'static_root_path' => '/var/www/clients/client1/web1/web/public/static',
    'static_root_url' => 'https://static.vamosimilano.hu',
    'static_new_root_url' => 'https://staticnew.quizbank.eu',
    
     'image_path' => '/var/www/clients/client1/web1/web/public/static/product',
    'image_url' => 'https://static.vamosimilano.hu/product',
    'cms_image_path' => '/var/www/clients/client1/web1/web/public/static/cms',
    'cms_image_url' => 'https://static.vamosimilano.hu/cms',
    'category_image_path' => '/var/www/clients/client1/web1/web/public/static/category',
    'category_image_url' => 'https://static.vamosimilano.hu/category',

    'banner_image_path' => '/var/www/clients/client1/web1/web/public/static/categorybanner',
    'banner_image_url' => 'https://static.vamosimilano.hu/categorybanner',

    'textiles_image_path' => '/var/www/clients/client1/web1/web/public/static/textiles',
    'textiles_image_url' => 'https://static.vamosimilano.hu/textiles',

    'product_image_path' => '/var/www/clients/client1/web1/web/public/static/product',
    'product_image_url' => 'https://static.vamosimilano.hu/product',

    'product_photo_path' => '/var/www/clients/client1/web1/web/public/static/productphoto',
    'product_photo_url' => 'https://static.vamosimilano.hu/productphoto',


    'carousel_image_path' => '/var/www/clients/client1/web1/web/public/static/carousel',
    'carousel_image_url' => 'https://static.vamosimilano.hu/carousel',
    
    'product_slider_image_path' => '/var/www/clients/client1/web1/web/public/static/product',
    'product_slider_image_url' => 'https://static.vamosimilano.hu/product',
	
	'fb_app_id' => '279645012445636',

    'image_sizes' => [
        '150' => 100,
        '279' => 140,
        '212' => 212,
        '350' => 150,
        '370' => 254,
		'640' => 480,
        '1024' => 768,
        '1280' => 500,
    ],
    'image_cms_sizes' => [
        '150' => 100,
        '1280' => 500,
        '1024' => 768,
        '1025' => 683,
    ],
    'banner_sizes' => [
        '900' => 375,
    ],
    'product_sizes' => [
        '600' => 375,
    ],
    'product_slider_sizes' => [
        '600' => 375,
    ],

    '404image' => 'static/img/404_default.png',
    '404imagePath' => 'static/img/404/',
    '404imageUrl' => '/static/img/404/',


    'enable_cms_content' => ['page','blog'],
    
    'contact_mails' => [
        '1' => 'ertekesites@vamosimilano.hu',
        '2' => 'ertekesitessz@vamosimilano.hu',
        '3' => 'ertekesitesvp@vamosimilano.hu',
        '4' => 'ertekesitesdb@vamosimilano.hu',
		'5' => 'ertekesitesm@vamosimilano.hu',
    ],
	'tagmanager' => 'GTM-WDMKZMV',

);
